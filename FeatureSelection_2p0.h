#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>

///////////////////////////////////
 #define COMMENT_OUT_ALL_PRINTS

#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)

#define FEA_DISACTIVATED (-2)
#define NUMBER_OF_ACTIVE_FEAS_IS_INSUFFICIENT (-3)

#define FEA_FOR_TRIAL 1
#define FEA_NOT_FOR_TRIAL 0

#define INITIAL_TEST_TRAN_DATASETS_INCLUDED 

//#define TESTING_TRAIN_VECS_WITHOUT_UPDATING

//used in 'PassAgg_Shifted_Optimized'
	#define USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1

	//#define INITIAL_ORTHONORMALIOZATION
//#define NORMALIOZATION_IN_OrthonormalizationOfVectorsByGrammSchimdt
#define USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

#define TESTING_TRAIN_VECS_WITHOUT_UPDATING_BY_READING_A_MODEL
#define TESTING_TEST_VECS_WITHOUT_UPDATING_BY_READING_A_MODEL


#define fWeight_Train 0.5 //0.7
#define fWeight_Test 0.5 //0.3

#endif //#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY
//#define EXIT_AFTER_TRAINING

#define CALCULATING_Z_SCORES_OF_ALL_FEAS

//#define fLarge_ForReading 10.0
#define fLarge_ForReading 2.0

#define fZ_ScoreLimit 1.5

///////////////////////////////////////////////

#define nLarge 1000000
#define fLarge 1.0E+12 //9

#define NO_VecInit
//#define PRINT_DEBUGGING_INFO
#define eps 1.0E-9

#define fLimitForOrthogonalization 1.0E-1

#define fFeaMin (-5000.0)
#define fFeaMax 5000.0

//#define nLengthOneLineMax 1000 //400 //200 //120000 //50000
#define nLengthOneLineMax 4000 //400 //200 //120000 //50000

#define nInputLineLengthMax (nLengthOneLineMax) //200 //100
#define nSubstringLenMax 20
///////////////////////////////////////////////////

//#define nNumVecTrainTot 4178 // for svmguide1_train_2000_2178.txt
//#define nNumVecTrainTot 3089 // for svmguide1_train.txt
//#define nNumVecTrainTot 5267 //3089
//#define nNumVecTrainTot 540 //
//#define nNumVecTrainTot 500 //250 + 250
//#define nNumVecTrainTot 920 //(460+460) // for svmguide1_train_2000_2178.txt
#define nNumVecTrainTot 922 //(461+461) // for svmguide1_train_2000_2178.txt

//#define nNumVecTestTot 4000
//#define nNumVecTestTot 84 //60 + 22
//#define nNumVecTestTot 209 //94 + 115
#define nNumVecTestTot 178 //63 + 115

////////////////////////////////////////////////////////////////////
//change 'nLengthOneLineMax' as well
//#define nDim 20 //10 //4 //8000
//#define nDim_DifEvo 200 
#define nDim_DifEvo 100 //50
#define nDim_DifEvo_WithConst (nDim_DifEvo + 1) //8000

#define nNumOfActiveFeasTot_Min 6 //2 // < nDim_FeasInit

//for reading init data
#define nProdTrain_DifEvoTot (nDim_DifEvo*nNumVecTrainTot)
#define nProdTest_DifEvoTot (nDim_DifEvo*nNumVecTestTot)

#define nProd_WithConstTrainTot (nDim_DifEvo_WithConst*nNumVecTrainTot)
#define nProd_WithConstTestTot (nDim_DifEvo_WithConst*nNumVecTestTot)

////////////////////////////////////////////
//#define nDim_FeasInit 5 //10 // <= nDim_DifEvo
//#define nDim_FeasInit 10 // <= nDim_DifEvo
//#define nDim_FeasInit 50  // <= nDim_DifEvo

#define nDim_FeasInit 15 // <= nDim_DifEvo
//#define nDim_FeasInit 30 // <= nDim_DifEvo

////////
#define fFeaRangeMin 0.0
#define fFeaRangeMax 1.0

#define fFeaDeviat 0.1  
//#define fFeaDeviat 0.02  

#define fFeaRangeMin_WithDeviat (fFeaRangeMin - fFeaDeviat)
#define fFeaRangeMax_WithDeviat (fFeaRangeMax + fFeaDeviat)

//#define fCoefForTrial 0.85
#define fCoefForTrial 0.3
#define fProbOfFeaForTrialMax 0.2
//////////////
#define nNumOfGenerTot 1200 //200 //10 //50 //500 //20
//#define nNumOfGenerTot 700 //200 //10 //50 //500 //20
//#define nNumOfGenerTot 200 
//#define nNumOfGenerTot 100 
//#define nNumOfGenerTot 50
//#define nNumOfGenerTot 20

//#define nNumOfVecsInOnePopulTot 10 
//#define nNumOfVecsInOnePopulTot 20 //100
#define nNumOfVecsInOnePopulTot 40 

//#define nDimOfAllFeas_inOnePopul (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGener)
#define nDimOfAllFeas_inOnePopul (nDim_FeasInit*nNumOfVecsInOnePopulTot)

#define nDimOfAllFeas_inAllGeners (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)

#define fWidthOfIntervalForOneFea ((fFeaRangeMax - fFeaRangeMin)/nDim_DifEvo)

////////////////////////////
#define nPositOfFeaBeyondRange_Min ((int)(fFeaRangeMin_WithDeviat/fWidthOfIntervalForOneFea))

#define nPositOfFeaBeyondRange_Max ((int)(fFeaRangeMax_WithDeviat/fWidthOfIntervalForOneFea))

#define nLowestNumberMax 8 // < nDim_DifEvo

#define nNumOfFeaIfThereAreNoFeas 2 // < nDim_DifEvo

/////////////////////////////
#define nDimOfAllVecs_inAllGeners (nNumOfVecsInOnePopulTot*nNumOfGenerTot)

#define nDim_D (nDim_FeasInit) //15
#define nDim_D_WithConst (nDim_FeasInit + 1) //16

///////////////////////////////////////////////////////////////////////////

#define nNumOfFitnessOfOneFeaVecTot_ForPrinting (-1)
//dimension of the nonlinear/transformed space

#define nDim_H 8
//#define nDim_H 16
//#define nDim_H 32

//#define nNumOfHyperplanes_Min 2 //4 //3
//#define nNumOfHyperplanes_Max 64 //128 //256 //512
//#define nNumOfHyperplanes_Max 32 //128 //256 //512
//#define nNumOfHyperplanes_Max 8 //16 
//#define nNumOfHyperplanes_Max 4 
//#define nNumOfHyperplanes_Max 2 

//#define nNumOfHyperplanes_Factor 2

#define nNumOfHyperplanes 2

#define nK (nNumOfHyperplanes)
////////////////////

//#define nNumOfItersOfTrainingTot 8 //10
//define nNumOfItersOfTrainingTot 4 //-- OK
#define nNumOfItersOfTrainingTot 2 //3 //10#define nNumOfItersOfTrainingTot 1 //10 //5 
//#define nNumOfItersOfTrainingTot 1

//#define nNumOfItersOfTraining_RefinedTot 6 //4 //-- OK
#define nNumOfItersOfTraining_RefinedTot 4 //-- OK

//////////////////////////////////////////////////////////////////
#define fW_Init_Min (-5.0) //(-2.0) 
#define fW_Init_Max (5.0) //2.0
/////////////////////////////////
//#define nDim_U_Init (nDim_D_WithConst*nDim_H*nK)

/////////////////////////////////////////////////////////
#define fU_Init_Min (-4.0) //--the last
#define fU_Init_Max 4.0 

#define fBiasForClassifByLossFunction 0.0

#define fFeaConst_UnRefined_Init 2.0 // fFeaConst_ForRefined_Glob_Min <= fFeaConst_UnRefined_Init <= fFeaConst_ForRefined_Glob_Max
///////////////////////////////
#define nSrandInit_Unrefin_Min 1

#define nSrandInit__Unrefin_Max 100 //50 //200 //400

#define nSrandInit_Step 1
////////////////////////////////////////

//for 'doPasAggMaxOut_TrainTest_WithSelecFeas_Refined'

//#define fPercentageOfCorrect_ToApply_Refinement 195.0

//#define fPercentageOfCorrect_ToApply_Refinement 193.0
//#define fPercentageOfCorrect_ToApply_Refinement 190.0
//#define fPercentageOfCorrect_ToApply_Refinement 170.0
#define fPercentageOfCorrect_ToApply_Refinement 185.0

//#define fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_AnUnrifinedModel 192.0 
//#define fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_AnUnrifinedModel 175.0 

#define fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_AnUnrifinedModel 185.0 

//#define fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_ARefinedModel 196.0 
//#define fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_ARefinedModel 187.0 
//#define fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_ARefinedModel 180.0 
#define fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_ARefinedModel 190.0 

//#define MODEL_WRITING_ONLY_FOR_REFINEMENT
//#define nDim_H_Min 64 //4
#define nDim_H_Refined_Max 32 //128 //256 // should be adjusted depending on the number of features

//#define nDim_H_Refined_Min 8 // should be adjusted depending on the number of features
#define nDim_H_Refined_Min 32 // should be adjusted depending on the number of features

#define nDim_H_Refined_Factor 2

#define nNumOfHyperplanes_Min 2  //3
#define nNumOfHyperplanes_Max 2 //4 

#define nNumOfHyperplanes_Factor 2

//nK_Refined_Min >= nK_Refined_Factor
#define nK_Refined_Min (nNumOfHyperplanes_Min)
#define nK_Refined_Max (nNumOfHyperplanes_Max)
#define nK_Refined_Factor (nNumOfHyperplanes_Factor)

#define fW_Init_HalfRange_Min 0.5
#define fW_Init_HalfRange_Max 5.0

#define fW_Init_HalfRange_Step 0.5
/////////////////////////////////

//#define nDim_U (nDim_D_WithConst*nDim_H*nK)

/////////////////////////////////////////////////////////

#define fU_Init_HalfRange_Min 0.5
#define fU_Init_HalfRange_Max 5.0

#define fU_Init_HalfRange_Step 0.5
//////////////////////////////////////////////
// change 'nNumVecTrainTot' as well

/////////////////////////////////////
#define fFeaConst_ForRefined_Glob_Min (0.5)
#define fFeaConst_ForRefined_Glob_Max (5.0)

#define fFeaConst_ForRefined_Step 0.5 //0.2

//////////////////////////////////
#define nSrandInit_Refined_Min 1

//#define nSrandInit_Refined_Max 100 //20 //
#define nSrandInit_Refined_Max 200 //100 //20 //
//#define nSrandInit_Refined_Max 400

#define nSrand_Refined_Step 1
///////////////////////////////////////

#define fAlpha 0.9 //const float fAlphaf, // < 1.0
#define fEpsilon 0.05 //?? -- linearly depends on variance

#define fCr 0.125
#define fC 0.125

//////////////////////////////////
//#define nSrandInit 2
///////////////////////////////////////////////////////////////////

#define fScalarProdOfNormalizedHyperplanesMax 0.1

///////////////////////////
//#define fR_Const 0.61803399
#define fC_Const ( (-1.0 + sqrt(5.0) )/2.0 ) //(1.0-R)
#define fR_Const (1.0 - fC_Const)

#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

#define SHFT2(a,b,c) (a)=(b);(b)=(c);
#define SHFT3(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

#define fEfficOfArr1stRequired 0.9

const float fPrecisionOf_Golden_Search = 0.01; // 0.005
const float eps_thesame = 1.0E-5; //-2; // precision of the position
/////////////////////////////////////////
//reading the model
#define nDim_Selec_Read 15 //14
#define nDim_SelecFeas_WithConst_Read (nDim_Selec_Read + 1)

#define nProdSelec_WithConstTrainTot (nDim_SelecFeas_WithConst_Read*nNumVecTrainTot)
#define nProdSelec_WithConstTestTot (nDim_SelecFeas_WithConst_Read*nNumVecTestTot)

#define nDim_H_Read 8 //32

#define nK_Read 2

#define nDim_U_Read 256 //960

typedef struct
{
	int nNumOfPosit_Y_Totf;
	int	nNumOfNegat_Y_Totf;

	int	nNumOfCorrect_Y_Totf;
	int	nNumOfVecs_Totf;

	int	nNumOfPositCorrect_Y_Totf;
	int	nNumOfNegatCorrect_Y_Totf;

	float fPercentageOfCorrectTotf;
	float fPercentageOfCorrect_Positf;
	float fPercentageOfCorrect_Negatf;

} PAS_AGG_MAX_OUT_RESUTS;

