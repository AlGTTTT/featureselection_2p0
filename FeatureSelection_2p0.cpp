
#include "FeatureSelection_2p0.h"

FILE *fin_Train;
FILE *fin_Test;

FILE *fin_Model;
FILE *fin_Min_Max_Mean_StDev;

FILE *fout;
FILE *fout_Models_Refined;
FILE *fout_Models_UnRefined;

int
	nY_Train_Actual_Glob = -2, // no training yet

	iVec_Train_Glob = -2,// no training yet

	nSrandInit_Glob = 4,

	nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob,
	nNumOfItersOfTrainingTot_Glob = nNumOfItersOfTrainingTot,

	nNumOfFitnessOfOneFeaVecTot_Glob = 0,

	iVec_Updating_Glob = -2, 
	nNumOfGener_Glob = 0,
	//nK_Glob, // = nK,
	nDim_H_Glob, // = nDim_H,

	//nDim_U_Glob,
	//nSrandInit_Glob_ForMax,

	nNumOf_TrialFeas_OffLimits_Glob = 0, 

	nSeedArr_Glob[nDim_DifEvo],
	nSeedInit_Glob = 1,

	//for 'doPasAggMaxOut_TrainTest_WithSelecFeas_Refined'
	nK_Glob, // = nK_Glob * nK_Refined_Factor;
	nDim_U_Glob, // = nDim_D_WithConst * nDim_H_Glob*nK_Glob;

	iFea1_Glob,
	iFea2_Glob;

float
	fEpsilon_Glob = fEpsilon,

	fPercentageOfCorrect_NoRefin_Glob = -fLarge,
	fPercentageOfCorrect_Refined_Max_Glob = -fLarge,

	fBiasForClassifByLossFunction_Glob = fBiasForClassifByLossFunction,

//initialized inside 'doPasAggMaxOut_TrainTest_WithSelecFeas' or 
// 'doPasAggMaxOut_TrainTest_WithSelecFeas_Refined'
	fU_Init_Min_Glob, // = fU_Init_Min,
	fU_Init_Max_Glob, // = fU_Init_Max,

	fW_Init_Min_Glob, // = fW_Init_Min,
	fW_Init_Max_Glob, // = fW_Init_Max,
//////////////////////////////////////////////////
//for 'doPasAggMaxOut_TrainTest_WithSelecFeas_Refined'

	fFeaConst_Glob = fFeaConst_UnRefined_Init, //initially

	fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob;

int main()
{
int Normalizing_Every_Fea_To_ARange(
				const float fLargef,
				const float fepsf,

				const int nDimf,

				const int nVec1stf,
				const int nVec2ndf, 

				float fFeaMinArrf[],
				float fFeaMaxArrf[],
				
				float fFeaArr1stf[], //to be normalized
				float fFeaArr2ndf[]); //to be normalized

int NumOfNonZeros_In_IntArr(
				  const int nDimf,
				  const int nArrf[], // [nDimf]

				  int &nNumOfNonZeros_In_IntArrf);

int Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas(
	const int nDimf,
	const int nDimSelecf,

	const int nNumVecf,

	const int nPosOfSelec_FeasArrf[], //[nDimSelecf]

	const float fVecArr[], //[nDimf*nNumVecf]
	//////////////
	float fVecSelecArr[]); //[nDimSelecf*nNumVecf]

/*
int Preparing_Data_For_PasAggMaxOut_TrainTest(

			const int nNumVecTrainTotf,
			const int nNumVecTestTotf,
			/////////////////////////
			const int nDimf, //for reading
			//const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space

		///////////////////////////////////////////////////
	//float fFea_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], 
		float fFea_Train_Arrf[], //[nProd_WithConstTrainTot], 

		int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	//float fFea_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
		float fFea_Test_Arrf[], //[nProd_WithConstTestTot],
		int nY_Test_Actual_Arrf[]);  //[nNumVecTestTot],
*/

//the train and test data has been already shuffled and normalized 
int Reading_Data_For_TrainTest_NoShuffleNormal(

	const int nNumVecTrainTotf,
	const int nNumVecTestTotf,
	/////////////////////////
	const int nDimf, //for reading	//const int nDimf, // = dimension of the original space

///////////////////////////////////////////////////
	//float fFea_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], 
	float fFea_Train_Arrf[], //[nProdTrain_DifEvoTot], 
	int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	//float fFea_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
	float fFea_Test_Arrf[], //[nProdTest_DifEvoTot],
	int nY_Test_Actual_Arrf[]);  //[nNumVecTestTot],
//////////////

int TestingAllVecs_ByOneVec_WithReadingAModel_2p0(
	const int nDim_DifEvof, // = dimension of the original space

	const int nDim_DifEvo_WithConstf, // 
	const int nVecTestf,

	const float fFeaTest_Arrf[], //[nDim_DifEvof*nVecTestf]

	const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],
///////////////////////////////////////////////////

	PAS_AGG_MAX_OUT_RESUTS *sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results);

int doFeatureSelection_2p0(

	const float fProbOfFeaForTrialMaxf,
	const float fCoefForTrialf,

	const int nDim_DifEvof,
	const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

	const int nNumOfGenerTotf,
	//const int nNumOfGenerCurf,

	//const int nNumOfOneVecFromPopulf,
	const int nNumOfVecsInOnePopulTotf,
	const int nDimOfAllFeas_inOnePopulf,

	const int nDimOfAllVecs_inAllGenersf,
	const int nDimOfAllFeas_inAllGenersf, // == (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)

	//////////////////////////////////
	const float fFeaRangeMinf,
	const float fFeaRangeMaxf,
	const float fWidthOfIntervalForOneFeaf,
	//////////////

	const float fFeaConst_UnRefined_Initf,
	/////////////////////////////////////
	//train
	const int nNumOfItersOfTrainingTotf,
	const int nNumOfItersOfTraining_RefinedTotf,

	//after shuffling
	const float fFea_Train_DifEvo_Arrf[], //[nProdTrain_DifEvoTot], already normalized
	const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	const int nNumVecTrainTotf, //== nVecTrainf,
	///////////////////////////////////////
	//test
	const float fFea_Test_DifEvo_Arrf[], //[nProdTest_DifEvoTot],
	const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

	const int nNumVecTestTotf, // == nVecTestf,
	/////////////////////////////////////

	const int nDim_D_SelecFeasf, //for reading

	const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	//const int nDim_U_Initf, //(nDim_D_WithConst*nDim_H*nK)

	///////////////////////
	const float fAlphaf, // < 1.0
	const float fEpsilonf,
	const float fCrf,
	const float fCf,
	////////////////////////////
	float fPopul_Of_FeaVecsArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

	float &fFitnessAver_OfLastGenerf,
	float &fBestFitnessf);

///////////////
int
	nResult;

//for optimization
int
	iFea,
	nY_Train_Actual_Arr[nNumVecTrainTot],
	nY_Test_Actual_Arr[nNumVecTestTot],

	nNumOfSrandInitf = ((nSrandInit__Unrefin_Max - nSrandInit_Unrefin_Min) / nSrandInit_Step) + 1;

float
	fFitnessAver_OfLastGener,
	fBestFitness,

	fPopul_Of_FeaVecsArr[nDimOfAllFeas_inOnePopul], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

	fFea_Train_DifEvo_Arr[nProdTrain_DifEvoTot],
	fFea_Test_DifEvo_Arr[nProdTest_DifEvoTot];

//////////////////////////////////////////////////////////////////////////////////////////

//!change 'nNumVecTrainTot' as well
//fin_Train = fopen("100feas_270Normal_270Malignant_train.txt", "r"); //nNumVecTrainTot = 3089
//fin_Train = fopen("20feas_337Normal_336Malignant_train.txt", "r"); //nNumVecTrainTot = 4178

//by copying
//fin_Train = fopen("100feas_250Normal_250Malignant_train.txt", "r"); //nNumVecTrainTot = 4178
//fin_Train = fopen("200feas_460Normal_460Malignant_train.txt", "r"); //nNumVecTrainTot = 4178
fin_Train = fopen("100feas_461Nor_461Mal_Proce_train.txt", "r"); //nNumVecTrainTot = 4178


//!change 'nNumVecTrainTot' as well
//fin_Train = fopen("svmguide1_train_3times_Negatives.txt", "r"); //
//fin_Train = fopen("svmguide1_train_Pos49_Neg101.txt", "r"); //actually, 49 and 151

//fin_Test = fopen("200feas_94Normal_115Malignant_test.txt", "r");
//fin_Test = fopen("100feas_62Normal_22Malignant_test.txt", "r");
//fin_Test = fopen("100feas_67Normal_31Malignant_test.txt", "r");
//fin_Test = fopen("50feas_60Normal_60Malignant_test.txt", "r");

fin_Test = fopen("100feas_63Nor_115Mal_Proce_test.txt", "r");

if (fin_Train == NULL || fin_Test == NULL)
{
printf("\n\n An error: fin_Train == NULL || ...");
getchar();	exit(1);
} //if (fin_Train == NULL || ...)
////////////////////////

fin_Model = fopen("rModel_15feas_Nor_Mal_UnRefined_ForAnna.txt", "r");
if (fin_Model == NULL) // 
{
	printf("\n\nAn error: fin_Model == NULL");
	getchar();	exit(1);
} //if (fin_Model == NULL )
/////////////////////////

fin_Min_Max_Mean_StDev = fopen("r100Feas_250Nor_461Mal_train_Min_Max_Mean_StDev.txt", "r");
if (fin_Min_Max_Mean_StDev == NULL) // 
{
	printf("\n\nAn error: fin_Min_Max_Mean_StDev == NULL");
	getchar();	exit(1);
} //if (fin_Min_Max_Mean_StDev == NULL )

//////////////////////////////////////////////////////////////////////////////////////////

//#ifndef COMMENT_OUT_ALL_PRINTS

fout = fopen("wMain_15feas.txt","w");

if (fout == NULL) // 
{
printf("\n\nAn error: fout == NULL");
getchar();	exit(1);
} //if (fout == NULL )

fout_Models_Refined = fopen("wModels_30feas_Nor_Mal_Refined.txt", "w");

if (fout_Models_Refined == NULL) // 
{
	printf("\n\nAn error: wModels_15feas_Nor_Mal_Refined == NULL");
	getchar();	exit(1);
} //if (fout_Models_Refined == NULL )
////////////////////////////////////////

fout_Models_UnRefined= fopen("wModels_30feas_Nor_Mal_UnRefined.txt", "w");

if (fout_Models_UnRefined == NULL) // 
{
	printf("\n\nAn error: fout_Models_15feas_UnRefined== NULL");
	getchar();	exit(1);

} //if (fout_Models_UnRefined== NULL )

if (nLowestNumberMax >= nDim_DifEvo)
{
	printf("\n\nAn error: nLowestNumberMax = %d >= nDim_DifEvo = %d", nLowestNumberMax,nDim_DifEvo);
	getchar();	exit(1);

} //if (nLowestNumberMax >= nDim_DifEvo)

fprintf(fout, "\n\n The constants: nDim_DifEvo_WithConst = %d, nDim_FeasInit = %d, nNumOfHyperplanes = nK = %d", nDim_DifEvo_WithConst, nDim_FeasInit, nK);
fprintf(fout, "\n\n  nNumOfVecsInOnePopulTot = %d, nNumOfGenerTot = %d", nNumOfVecsInOnePopulTot, nNumOfGenerTot);

//fprintf(fout, "\n nDim_U_Init = ((nDim_D_WithConst*nDim_H*nK) = %d, fAlpha = %E, fEpsilon = %E, fCr = %E, fC = %E",
	//nDim_U_Init, fAlpha, fEpsilon, fCr, fC);
fprintf(fout, "\n fAlpha = %E, fEpsilon = %E, fCr = %E, fC = %E", fAlpha, fEpsilon, fCr, fC);

fprintf(fout, "\n fAlpha = %E, fEpsilon = %E, fCr = %E, fC = %E",fAlpha, fEpsilon, fCr, fC);

fprintf(fout, "\n fFeaConst_UnRefined_Init = %E", fFeaConst_UnRefined_Init);

fprintf(fout, "\n fW_Init_Min = %E, fW_Init_Max = %E", fW_Init_Min, fW_Init_Max);

fprintf(fout, "\n\n  nNumVecTrainTot = %d, nNumVecTestTot = %d, nNumOfItersOfTrainingTot = %d", nNumVecTrainTot, nNumVecTestTot, nNumOfItersOfTrainingTot);

	#ifdef USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1
	fprintf(fout, "\n\n All feas are normalized to mean 0 and stdev 1");
	#endif //#ifdef USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
/////////////////////////////////////////////
//
	for (iFea = 0; iFea < nDim_DifEvo; iFea++)
		nSeedArr_Glob[iFea] = iFea;
	//////////////////////////////////////////////////////////////////
	srand(nSrandInit_Glob); //initially, for initialization of the whole population; all generations
//////////////////////////////////////////////////////////////////////////

//the train and test data has been already shuffled and normalized to (0,1)
	//nResult = Preparing_Data_For_PasAggMaxOut_TrainTest(
	nResult = Reading_Data_For_TrainTest_NoShuffleNormal(
		nNumVecTrainTot, //const int nNumVecTrainTotf,
		nNumVecTestTot, //const int nNumVecTestTotf,
		/////////////////////////
		nDim_DifEvo, //const int nDimf, //for reading	 // = dimension of the original space

	///////////////////////////////////////////////////
		fFea_Train_DifEvo_Arr, //float fFea_WithConstTrain_Arrf[], //[nProdTrain_DifEvoTot], 
		nY_Train_Actual_Arr, //int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		fFea_Test_DifEvo_Arr, //float fFea_WithConstTest_Arrf[], //[nProdTest_DifEvoTot],
		nY_Test_Actual_Arr); // int nY_Test_Actual_Arrf[]);  //[nNumVecTestTot],

	if (nResult == UNSUCCESSFUL_RETURN)
	{
//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\nAn error in 'main' by 'Reading_Data_For_TrainTest_NoShuffleNormal'");
		fprintf(fout, "\n\nAn error in 'main' by 'Reading_Data_For_TrainTest_NoShuffleNormal'");

		getchar();	exit(1);
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResult == UNSUCCESSFUL_RETURN)

	fclose(fin_Train);
	fclose(fin_Test);
/////////////////////////////////////////////

#ifdef TESTING_TRAIN_VECS_WITHOUT_UPDATING_BY_READING_A_MODEL
	printf("\n\n Before 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0'"); //getchar();

	PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TrainResultsRightAfterInitf;

	nResult = TestingAllVecs_ByOneVec_WithReadingAModel_2p0(
		nDim_DifEvo, //const int nDim_DifEvof, // = dimension of the original space

		nDim_DifEvo_WithConst, //const int nDim_DifEvo_WithConstf, //

		nNumVecTrainTot, //const int nVecTestf,

		fFea_Train_DifEvo_Arr, //const float fFeaTrain_Arrf[], ///[nDim_Df*nVecTestf]

		nY_Train_Actual_Arr, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

	///////////////////////////////////////////////////

		&sPasAggMaxOut_TrainResultsRightAfterInitf); // PAS_AGG_MAX_OUT_RESUTS *sTestingAllVecs_ByOneVec_WithReadingAModelResults);

	if (nResult == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'main' by 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' (train)");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest' by 'TestingAllVecs_ByOneVec_WithReadingAModel'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);
	
		return UNSUCCESSFUL_RETURN;
	} //if (nResult == UNSUCCESSFUL_RETURN)

	if (sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf > 0)
	{
		fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob = (float)(sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf) / (float)(sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf);

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'doPasAggMaxOut_TrainTest':  fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob = %E", fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob);
		fprintf(fout, "\n\n   fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob = %E", fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} // if (sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf > 0)
	else
	{
		printf("\n\n An error in 'main':  sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf);
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest': sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);
		return UNSUCCESSFUL_RETURN;
	}//else

//#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n///////////////////////////////////////////////////////////");
	printf("\n\n After initial 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' in 'main': sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf = %E, ", 
		sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf);
	printf("\n ...AfterInitf.fPercentageOfCorrect_Positf = %E, ...AfterInitf.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Negatf);

	printf("\n\n ...AfterInitf->nNumOfPosit_Y_Totf = %d, ...AfterInitf->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf);

	printf("\n\n ...AfterInitf->nNumOfNegat_Y_Totf = %d, ...AfterInitf->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf);


	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n  After initial 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' in 'main': sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf = %E, ", 
		sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf);

	fprintf(fout, "\n ...AfterInitf.fPercentageOfCorrect_Positf = %E, ...AfterInitf.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n ...AfterInitf->nNumOfPosit_Y_Totf = %d, ...AfterInitf->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n ...AfterInitf->nNumOfNegat_Y_Totf = %d, ...AfterInitf->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf);

	printf("\n\n Please press any key to continue:"); fflush(fout);  getchar();
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#endif //#ifdef TESTING_TRAIN_VECS_WITHOUT_UPDATING_BY_READING_A_MODEL

#ifdef TESTING_TEST_VECS_WITHOUT_UPDATING_BY_READING_A_MODEL
	PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TestResultsRightAfterInitf;

	nResult = TestingAllVecs_ByOneVec_WithReadingAModel_2p0(
		nDim_DifEvo, //const int nDim_DifEvof, // = dimension of the original space

		nDim_DifEvo_WithConst, //const int nDim_DifEvo_WithConstf, //

		nNumVecTestTot, //const int nVecTestf,

		fFea_Test_DifEvo_Arr, //const float fFeaTrain_Arrf[], ///[nDim_Df*nVecTestf]

		nY_Test_Actual_Arr, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

	///////////////////////////////////////////////////

		&sPasAggMaxOut_TestResultsRightAfterInitf); // PAS_AGG_MAX_OUT_RESUTS *sTestingAllVecs_ByOneVec_WithReadingAModelResults);
	///////////////////////////////////////////////////

	if (nResult == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'doPasAggMaxOut_TrainTest' by 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' (test)");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest' by 'TestingAllVecs_ByOneVec_WithReadingAModel'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nResult == UNSUCCESSFUL_RETURN)

	if (sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegat_Y_Totf > 0)
	{
		fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob = (float)(sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfPosit_Y_Totf) / (float)(sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegat_Y_Totf);

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'doPasAggMaxOut_TrainTest':  fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob = %E", fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob);
		fprintf(fout, "\n\n   fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob = %E", fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} // if (sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegat_Y_Totf > 0)
	else
	{
		printf("\n\n An error in 'doPasAggMaxOut_TrainTest':  sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegat_Y_Totf = %d", sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegat_Y_Totf);
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest': sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegat_Y_Totf = %d", sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegat_Y_Totf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);
		return UNSUCCESSFUL_RETURN;
	}//else

//#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n///////////////////////////////////////////////////////////");
	printf("\n\n After initial 'TestingAllVecs_ByOneVec_WithReadingAModel' in 'main': sPasAggMaxOut_TestResultsRightAfterInitf.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TestResultsRightAfterInitf.fPercentageOfCorrectTotf);
	printf("\n ...AfterInitf.fPercentageOfCorrect_Positf = %E, ...AfterInitf.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TestResultsRightAfterInitf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResultsRightAfterInitf.fPercentageOfCorrect_Negatf);

	printf("\n\n ...AfterInitf->nNumOfPosit_Y_Totf = %d, ...AfterInitf->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf);

	printf("\n\n ...AfterInitf->nNumOfNegat_Y_Totf = %d, ...AfterInitf->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf);


	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n  After initial 'TestingAllVecs_ByOneVec_WithReadingAModel' in 'main': sPasAggMaxOut_TestResultsRightAfterInitf.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TestResultsRightAfterInitf.fPercentageOfCorrectTotf);

	fprintf(fout, "\n ...AfterInitf.fPercentageOfCorrect_Positf = %E, ...AfterInitf.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TestResultsRightAfterInitf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResultsRightAfterInitf.fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n ...AfterInitf->nNumOfPosit_Y_Totf = %d, ...AfterInitf->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n ...AfterInitf->nNumOfNegat_Y_Totf = %d, ...AfterInitf->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TestResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf);

	printf("\n\n Please press any key to exit:"); fflush(fout);  getchar(); exit(1);
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#endif //#ifdef TESTING_TEST_VECS_WITHOUT_UPDATING_BY_READING_A_MODEL

//////////////////////

	nResult =  doFeatureSelection_2p0(

		fProbOfFeaForTrialMax, //const float fProbOfFeaForTrialMaxf,
		fCoefForTrial, //const float fCoefForTrialf,

		nDim_DifEvo, //const int nDim_DifEvof,
		nDim_FeasInit, //const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

		nNumOfGenerTot, //const int nNumOfGenerTotf,
		//const int nNumOfGenerCurf,

		//const int nNumOfOneVecFromPopulf,
		nNumOfVecsInOnePopulTot, //const int nNumOfVecsInOnePopulTotf,
		nDimOfAllFeas_inOnePopul, //const int nDimOfAllFeas_inOnePopulf,

		nDimOfAllVecs_inAllGeners, //const int nDimOfAllVecs_inAllGenersf,
		nDimOfAllFeas_inAllGeners, //const int nDimOfAllFeas_inAllGenersf, // == (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)

		//////////////////////////////////
		fFeaRangeMin, //const float fFeaRangeMinf,
		fFeaRangeMax, //const float fFeaRangeMaxf,
		fWidthOfIntervalForOneFea, //const float fWidthOfIntervalForOneFeaf,
		//////////////

		fFeaConst_UnRefined_Init, //const float fFeaConst_UnRefined_Initf, == 2.0
		/////////////////////////////////////
		//train
		nNumOfItersOfTrainingTot, //const int nNumOfItersOfTrainingTotf,
		nNumOfItersOfTraining_RefinedTot, //const int nNumOfItersOfTraining_RefinedTotf,

		//after shuffling
		fFea_Train_DifEvo_Arr, //const float fFea_Train_DifEvo_Arrf[], //[nProdTrain_DifEvoTot], already normalized
		nY_Train_Actual_Arr, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		nNumVecTrainTot, //const int nNumVecTrainTotf, //== nVecTrainf,
		///////////////////////////////////////
		//test
		fFea_Test_DifEvo_Arr, //const float fFea_Test_DifEvo_Arrf[], //[nProdTest_DifEvoTot],
		nY_Test_Actual_Arr, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		nNumVecTestTot, //const int nNumVecTestTotf, // == nVecTestf,
		/////////////////////////////////////

		nDim_D, //const int nDim_D_SelecFeasf, //== nDim_FeasInit == 15

		nDim_D_WithConst, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		nDim_H, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nK, //const int nKf, //nNumOfHyperplanes

		//nDim_U_Init, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		fAlpha, //const float fAlphaf, // < 1.0
		fEpsilon, //const float fEpsilonf,
		fCr, //const float fCrf,
		fC, //const float fCf,
		////////////////////////////
		fPopul_Of_FeaVecsArr, //float fPopul_Of_FeaVecsArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

		fFitnessAver_OfLastGener, //float &fFitnessAver_OfLastGenerf,
		fBestFitness); // float &fBestFitnessf)

		if (nResult == UNSUCCESSFUL_RETURN)
		{
			//#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\nAn error in 'main' by 'doFeatureSelection_2p0'");
			fprintf(fout, "\n\nAn error in 'main' by 'doFeatureSelection_2p0'");

			getchar();	exit(1);
			//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nResult == UNSUCCESSFUL_RETURN)

	printf( "\n\n Final: fFitnessAver_OfLastGener = %E, fBestFitness = %E", fFitnessAver_OfLastGener, fBestFitness);
	printf( "\n\n fPercentageOfCorrect_Refined_Max_Glob = %E", fPercentageOfCorrect_Refined_Max_Glob);

	fprintf(fout, "\n\n Final: fFitnessAver_OfLastGener = %E, fBestFitness = %E", fFitnessAver_OfLastGener, fBestFitness);
	fprintf(fout, "\n\n fPercentageOfCorrect_Refined_Max_Glob = %E", fPercentageOfCorrect_Refined_Max_Glob);

///////////////////////////

//#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\n The end:\n");
fprintf(fout,"\n\n The end:\n"); 

printf("\n\nPlease press any key:"); fflush(fout); getchar();
fclose(fout);
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

return 1;
} //int main()
//////////////////////////////////////////////////////

int	VecInit(
		const int nDimf,
		const int nNumVecf,
		const int nNumIterMaxForVecInitf,

		const int nIndic_1st_2ndf,
		float fVecArr[])
{
int
	//nRand_1f,
	//nRand_2f,

	iIterf,
	nNumVecFilled = 0,
	//iVecf,
	iFeaf;

/*
float
	fRand_1f,
	fRand_2f;
*/

int* nRandArrf = new int[nDimf];
float *fRandArrf = new float[nDimf];

if (nRandArrf == NULL || fRandArrf == NULL)
{
printf("\n\nAn error in 'VecInit': nRandArrf == NULL || fRandArrf == NULL");
getchar(); exit(1);
} //if (nRandArrf == NULL || fRandArrf == NULL)


if (nIndic_1st_2ndf != 1 && nIndic_1st_2ndf != -1)
{
printf("\n\nAn error in 'VecInit': nIndic_1st_2ndf = %d",nIndic_1st_2ndf);
getchar(); exit(1);
} // if (nIndic_1st_2ndf != 1 && nIndic_1st_2ndf != -1)

for (iIterf = 0; iIterf < nNumIterMaxForVecInitf; iIterf++)
{

	//for (iFeaf = 0; iFeaf < 2; iFeaf++)
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
	nRandArrf[iFeaf] = rand();
	fRandArrf[iFeaf] = (float)(nRandArrf[iFeaf])/(float)(RAND_MAX);
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)


/*
nRand_1f = srand();
nRand_2f = srand();

fRand_1f = (float)(nRand_1f)/(float)(RAND_MAX);
fRand_2f = (float)(nRand_2f)/(float)(RAND_MAX);
*/
	if (nIndic_1st_2ndf == 1 && fRandArrf[1] >= fRandArrf[0])
	{
	nNumVecFilled += 1;

		//for (iFeaf = 0; iFeaf < 2; iFeaf++)
		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{
		fVecArr[iFeaf + (nNumVecFilled - 1)*nDimf] = fRandArrf[iFeaf];
		} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		if (nNumVecFilled >= nNumVecf)
			break;
	} // if (nIndic_1st_2ndf == 1 && fRandArrf[1] >= fRandArrf[0])

	if (nIndic_1st_2ndf == -1 && fRandArrf[1] < fRandArrf[0])
	{
	nNumVecFilled += 1;

		//for (iFeaf = 0; iFeaf < 2; iFeaf++)
		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{
		fVecArr[iFeaf + (nNumVecFilled - 1)*nDimf] = fRandArrf[iFeaf];
		} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		if (nNumVecFilled >= nNumVecf)
			break;
	} // if (nIndic_1st_2ndf == 1 && fRandArrf[1] < fRandArrf[0)

} // for (iIterf = 0; iIterf < nNumIterMaxForVecInitf; iIterf++)


if (nNumVecFilled != nNumVecf)
{
printf("\n\nAn error in 'VecInit': nNumVecFilled = %d != nNumVecf = %d",nNumVecFilled,nNumVecf);
printf("\n\nPlease increase 'nNumIterMaxForVecInitf'\n");
getchar(); exit(1);
} //if (nNumVecFilled != nNumVecf)

delete [] fRandArrf;
delete [] nRandArrf;
return 1;
} // int VecInit(
//////////////////////////////////////////////////////

float fSclarProd(
				const int nDimf,
				 const float fArr1f[],
				 const float fArr2f[])
{
int
	i1;

float
	fScalarProdf = 0.0;

for (i1 = 0; i1 < nDimf; i1++)
{

fScalarProdf += fArr1f[i1]*fArr2f[i1];
} // for (i1 = 0; i1 < i1++)

return fScalarProdf;
} //float fSclarProd(
///////////////////////////////////////////////////////

int SettingInitArr(int nIndexDimf, int nIndexForActiveArrf[])
{

int
	i1;

for (i1 = 0; i1 < nIndexDimf; i1++)
	nIndexForActiveArrf[i1] = i1;

return 1;
} // int SettingInitArr(int nIndexDimf, int nIndexForActiveArrf[])
////////////////////////////////////////////////////////

int Shuffle_and_Select_2Dim_FloatArr (
				const int nDimf,
				const int nNumOfVecs_Initf,
				const int nNumOfVecs_Selecf, // < nNumOfVecs_Initf
				const float fFeaAll_InitArrf[],
				
				float fFeaSelecAll_FeasArr[])
{
int
	nRanSelecf, // <= nNumOfVecs_Selecf
	iFeaf,
	iVecf;

for (iVecf = 0; iVecf < nNumOfVecs_Selecf; iVecf++)
{
nRanSelecf = (int)( nNumOfVecs_Initf*(float)( rand() )/(float)(RAND_MAX) );

	if (nRanSelecf == nNumOfVecs_Initf)
	{
	nRanSelecf = nNumOfVecs_Initf - 1;
	} //if (nRanSelecf == nNumOfVecs_Initf)
	else if (nRanSelecf > nNumOfVecs_Initf)
	{
	printf("\n\nAn error in 'Shuffle_and_Select_2Dim_FloatArr': nRanSelecf = %d > nNumOfVecs_Initf = %d",
							nRanSelecf,nNumOfVecs_Initf);
	fprintf(fout,"\n\nAn error in 'Shuffle_and_Select_2Dim_FloatArr': nRanSelecf = %d > nNumOfVecs_Initf = %d",
							nRanSelecf,nNumOfVecs_Initf);

	getchar();	exit(1);
	} // else if (nRanSelecf > nNumOfVecs_Initf)

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
	fFeaSelecAll_FeasArr[iFeaf + (iVecf*nDimf)] = fFeaAll_InitArrf[iFeaf + (nRanSelecf*nDimf)];
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

} // for (iVecf = 0; iVecf < nNumOfVecs_Selecf; iVecf++)

return 1;
} //int Shuffle_and_Select_2Dim_FloatArr (...
////////////////////////////////////////////////////////////////////

int Float_Belongs_To_Arr_Or_Not(
											const float epsf,
											const float fLargef,

											const int nDimf,
											const float fTestf,
											
											int &nNumBelongsf, //'1' --> belongs and '-1' otherwise

											float fArrf[])
{
int
	i1;

float
	fDiff1f,
	fDiff2f;

//fDiff1f = fLargef - fTestf;
fDiff1f = -fLargef - fTestf;

if (fDiff1f > -epsf && fDiff1f < epsf)
{
printf("\n\nAn error in 'Float_Belongs_To_Arr_Or_Not': fTestf = %E",fTestf);
fprintf(fout,"\n\nAn error in 'Float_Belongs_To_Arr_Or_Not': fTestf = %E",fTestf);

getchar();	exit(1);
} // if (fDiff1f > -epsf && fDiff1f < epsf)

nNumBelongsf = 0; //impossible

for (i1 = 0; i1 < nDimf; i1++)
{
fDiff1f = fArrf[i1] - fTestf;

	if (fDiff1f > -epsf && fDiff1f < epsf)
	{
	nNumBelongsf = 1; //belongs
	break;
	} // if (fDiff1f > -epsf && fDiff1f < epsf)
	
//fDiff2f = fArrf[i1] - fLargef;
fDiff2f = fArrf[i1] + fLargef;
	if (fDiff2f > -epsf && fDiff2f < epsf)
	{
	nNumBelongsf = -1; //does not belong

	fArrf[i1] = fTestf;
	break;
	} // if (fDiff2f > -epsf && fDiff2f < epsf)

} // for (i1 = 0; i1 < nDimf; i1++)

if (nNumBelongsf == 0)
{
printf("\n\nAn error in 'Float_Belongs_To_Arr_Or_Not': nNumBelongsf == 0, fTestf = %E",fTestf);
fprintf(fout,"\n\nAn error in 'Float_Belongs_To_Arr_Or_Not': nNumBelongsf == 0, fTestf = %E",fTestf);

getchar();	exit(1);
} // if (nNumBelongsf == 0)

return 1;
} // int Float_Belongs_To_Arr_Or_Not(...
///////////////////////////////////////////////////////

int Int_Belongs_To_Arr_Or_Not(
								const int nDimf,
								const int nTestf,
								const int nArrf[],

								int &nNumBelongsf, //'1' --> belongs and '-1' otherwise
								
								int nPosArrf[]) //[nDimf]
{
int
	i1,

	nDiff1f;

nNumBelongsf = 0; //impossible
for (i1 = 0; i1 < nDimf; i1++)
	nPosArrf[i1] = -1;

for (i1 = 0; i1 < nDimf; i1++)
{
nDiff1f = nArrf[i1] - nTestf;

	if (nDiff1f == 0)
	{
	nNumBelongsf += 1; //belongs
	nPosArrf[nNumBelongsf - 1] = i1;
	//return 1;
	} // if (nDiff1f == 0)

} // for (i1 = 0; i1 < nDimf; i1++)

return 1;
} // int Int_Belongs_To_Arr_Or_Not(...

int compa_int (const void * na, const void * nb)
{
return ( *(int*)na - *(int*)nb );
} //int compa_int (const void * na, const void * nb)
////////////////////////////////

int fMeanOfFloatArr( const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,
					const float fArrf[], //	ObjDataSymbArrAllf.nNumLongFailureAskIntervArrf, //nNumLongEntriesCurf, //const int nCurf,
						
					float &fMeanf)
{
int 
	nNumRealMembf = 0,
	i1;

fMeanf = 0.0;
for (i1 = 0; i1 < nDimf; i1++)
{
	if (fArrf[i1] <= 0.0)
		continue;

nNumRealMembf += 1;
fMeanf += (float)(fArrf[i1]);
} //for (i1 = 0; i1 < nDimf; i1++)

if (nNumRealMembf == 0)
{
fMeanf = -1.0;
return UNSUCCESSFUL_RETURN;
} //if (nNumRealMembf == 0)
else
{
fMeanf = fMeanf/nNumRealMembf;
} //else

return SUCCESSFUL_RETURN;
} //int fMeanOfFloatArr( const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,

int Normalizing_Every_Fea_To_ARange(
				const float fLargef,
				const float fepsf,

				const int nDimf,

				const int nVec1stf,
				const int nVec2ndf, 

				float fFeaMinArrf[],
				float fFeaMaxArrf[],
				
				float fFeaArr1stf[], //to be normalized
				float fFeaArr2ndf[]) //to be normalized
{

int
	//nResf,
	iFeaf,
	iVecf;

float
	fFeaCurf,
	fDiffFeaMaxMinCurf;

for(iFeaf = 0; iFeaf < nDimf; iFeaf++)
{
fFeaMinArrf[iFeaf] = fLargef;
fFeaMaxArrf[iFeaf] = -fLargef;

	for (iVecf = 0; iVecf < nVec1stf; iVecf++)
	{
	fFeaCurf = fFeaArr1stf[iFeaf + iVecf*nDimf];

		if (fFeaCurf < fFeaMinArrf[iFeaf])
			fFeaMinArrf[iFeaf] = fFeaCurf;

		if (fFeaCurf > fFeaMaxArrf[iFeaf])
			fFeaMaxArrf[iFeaf] = fFeaCurf;

	} // for (iVecf = 0; iVecf < nVec1stf; iVecf++)

	for (iVecf = 0; iVecf < nVec2ndf; iVecf++)
	{
	fFeaCurf = fFeaArr2ndf[iFeaf + iVecf*nDimf];

		if (fFeaCurf < fFeaMinArrf[iFeaf])
			fFeaMinArrf[iFeaf] = fFeaCurf;

		if (fFeaCurf > fFeaMaxArrf[iFeaf])
			fFeaMaxArrf[iFeaf] = fFeaCurf;

	} // for (iVecf = 0; iVecf < nVec2ndf; iVecf++)

} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

//Normalization

for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
{
fDiffFeaMaxMinCurf = fFeaMaxArrf[iFeaf] - fFeaMinArrf[iFeaf];

	if (fDiffFeaMaxMinCurf > fepsf)
	{
		for (iVecf = 0; iVecf < nVec1stf; iVecf++)
		{
		fFeaCurf = fFeaArr1stf[iFeaf + iVecf*nDimf];

		fFeaArr1stf[iFeaf + iVecf*nDimf] = (fFeaCurf - fFeaMinArrf[iFeaf])/fDiffFeaMaxMinCurf;

			if (fFeaArr1stf[iFeaf + iVecf*nDimf] < -fepsf || fFeaArr1stf[iFeaf + iVecf*nDimf] > 1.0 + fepsf)
			{
			printf("\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fFeaArr1stf[iFeaf + iVecf*nDimf] = %E < -fepsf ||... , iFeaf = %d, iVecf = %d",
				fFeaArr1stf[iFeaf + iVecf*nDimf], iFeaf, iVecf);
			
			fprintf(fout,"\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fFeaArr1stf[iFeaf + iVecf*nDimf] = %E < -fepsf ||... , iFeaf = %d, iVecf = %d",
				fFeaArr1stf[iFeaf + iVecf*nDimf], iFeaf, iVecf);

			getchar(); exit(1);
			} //if (fFeaArr1stf[iFeaf + iVecf*nDimf] < -fepsf || fFeaArr1stf[iFeaf + iVecf*nDimf] > 1.0 + fepsf)

		} // for (iVecf = 0; iVecf < nVec1stf; iVecf++)

		for (iVecf = 0; iVecf < nVec2ndf; iVecf++)
		{
		fFeaCurf = fFeaArr2ndf[iFeaf + iVecf*nDimf];

		fFeaArr2ndf[iFeaf + iVecf*nDimf] = (fFeaCurf - fFeaMinArrf[iFeaf])/fDiffFeaMaxMinCurf;

			if (fFeaArr2ndf[iFeaf + iVecf*nDimf] < -fepsf || fFeaArr2ndf[iFeaf + iVecf*nDimf] > 1.0 + fepsf)
			{
			printf("\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fFeaArr2ndf[iFeaf + iVecf*nDimf] = %E < -fepsf ||... , iFeaf = %d, iVecf = %d",
				fFeaArr2ndf[iFeaf + iVecf*nDimf], iFeaf, iVecf);
			
			fprintf(fout,"\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fFeaArr2ndf[iFeaf + iVecf*nDimf] = %E < -fepsf ||... , iFeaf = %d, iVecf = %d",
				fFeaArr2ndf[iFeaf + iVecf*nDimf], iFeaf, iVecf);

			getchar(); exit(1);
			} //if (fFeaArr2ndf[iFeaf + iVecf*nDimf] < -fepsf || fFeaArr2ndf[iFeaf + iVecf*nDimf] > 1.0 + fepsf)
		
		} // for (iVecf = 0; iVecf < nVec2ndf; iVecf++)

	} // if (fDiffFeaMaxMinCurf > fepsf)
	else if (fDiffFeaMaxMinCurf > -fepsf && fDiffFeaMaxMinCurf < fepsf)
	{
	fFeaArr1stf[iFeaf + iVecf*nDimf] = 0.0;
	fFeaArr2ndf[iFeaf + iVecf*nDimf] = 0.0;

	} //else if (fDiffFeaMaxMinCurf > -fepsf && fDiffFeaMaxMinCurf < fepsf)
	else
	{
	printf("\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fDiffFeaMaxMinCurf = %E, iFeaf = %d",
		fDiffFeaMaxMinCurf,iFeaf);

	fprintf(fout,"\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fDiffFeaMaxMinCurf = %E, iFeaf = %d",
		fDiffFeaMaxMinCurf,iFeaf);

	getchar(); exit(1);
	} //else

} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

return 1;
} //int Normalizing_Every_Fea_To_ARange (...
//////////////////////////////////////////////////////////

int Normalizing_Every_Fea_To_Mean_0_And_StDev_1(
	const float fLargef,
	const float fepsf,

	const int nDimf,

	const int nVecTrainf,
	const int nVecTestf,
/////////////////////////////////////////////
	float fFeaMin_TrainArrf[], //[nDimf]
	float fFeaMax_TrainArrf[], //[nDimf]

	float fFea_TrainArrf[], //to be normalized
	float fFea_TestArrf[]) //to be normalized using the train Mean and StDev
{

	int
		iFeaf,
		iVecf;

	float
		fMeanFor_OneFeaf,
		fVarianceCurf,
		fStDevf,

		fFeaCurf,
		fDiff_Fea_And_MeanCurf, 
		fDiffFeaMaxMinCurf;
//////////////////////////////////////////
	if (nVecTrainf <= 0 || nVecTestf <= 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Normalizing_Every_Fea_To_Mean_0_And_StDev_1': nVecTrainf = %d, nVecTestf = %d", nVecTrainf, nVecTestf);
		fprintf(fout, "\n\nAn error in 'Normalizing_Every_Fea_To_Mean_0_And_StDev_1': nVecTrainf = %d, nVecTestf = %d", nVecTrainf, nVecTestf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nVecTrainf <= 0)
/////////////////////////////////////////////////

	float *fMean_All_FeasArrf = new float[nDimf];
	if (fMean_All_FeasArrf == NULL)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\nAn error in 'Normalizing_Every_Fea_To_Mean_0_And_StDev_1': fMean_All_FeasArrf == NULL");
		fprintf(fout, "\n\nAn error in 'Normalizing_Every_Fea_To_Mean_0_And_StDev_1': fMean_All_FeasArrf == NULL");
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fMean_All_FeasArrf == NULL)

	float *fStDev_All_FeasArrf = new float[nDimf];
	if (fStDev_All_FeasArrf == NULL)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\nAn error in 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': fStDev_All_FeasArrf == NULL");
		fprintf(fout, "\n\nAn error in 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': fStDev_All_FeasArrf == NULL");

		delete[] fMean_All_FeasArrf;
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fStDev_All_FeasArrf == NULL)

//////////////////////////////////////////////////
//Mean
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeaMin_TrainArrf[iFeaf] = fLargef;
		fFeaMax_TrainArrf[iFeaf] = -fLargef;

		fMeanFor_OneFeaf = 0.0;
		for (iVecf = 0; iVecf < nVecTrainf; iVecf++)
		{
			fFeaCurf = fFea_TrainArrf[iFeaf + iVecf * nDimf];

			if (fFeaCurf < fFeaMin_TrainArrf[iFeaf])
				fFeaMin_TrainArrf[iFeaf] = fFeaCurf;

			if (fFeaCurf > fFeaMax_TrainArrf[iFeaf])
				fFeaMax_TrainArrf[iFeaf] = fFeaCurf;

			fMeanFor_OneFeaf += fFeaCurf;
		} // for (iVecf = 0; iVecf < nVecTrainf; iVecf++)

		fMeanFor_OneFeaf = fMeanFor_OneFeaf / nVecTrainf;
		fMean_All_FeasArrf[iFeaf] = fMeanFor_OneFeaf;
	} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)
///////////////////////////////////////////////////////////
//StDev 

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fDiff_Fea_And_MeanCurf = 0.0;
		fVarianceCurf = 0.0;

		for (iVecf = 0; iVecf < nVecTrainf; iVecf++)
		{
			fFeaCurf = fFea_TrainArrf[iFeaf + iVecf * nDimf];
			fDiff_Fea_And_MeanCurf = fFeaCurf - fMean_All_FeasArrf[iFeaf];

			fVarianceCurf += fDiff_Fea_And_MeanCurf*fDiff_Fea_And_MeanCurf;
		} // for (iVecf = 0; iVecf < nVecTrainf; iVecf++)

		fVarianceCurf = fVarianceCurf / nVecTrainf;

		fStDev_All_FeasArrf[iFeaf] = sqrt(fVarianceCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
		printf( "\n\n 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': fMean_All_FeasArrf[%d] = %E, fStDev_All_FeasArrf[%d] = %E",
			iFeaf, fMean_All_FeasArrf[iFeaf], iFeaf, fStDev_All_FeasArrf[iFeaf]);

		printf( "\n fFeaMin_TrainArrf[%d] = %E, fFeaMax_TrainArrf[%d] = %E",
			iFeaf, fFeaMin_TrainArrf[iFeaf], iFeaf, fFeaMax_TrainArrf[iFeaf]);

		fprintf(fout, "\n\n 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': fMean_All_FeasArrf[%d] = %E, fStDev_All_FeasArrf[%d] = %E",
			iFeaf, fMean_All_FeasArrf[iFeaf], iFeaf, fStDev_All_FeasArrf[iFeaf]);

		fprintf(fout, "\n fFeaMin_TrainArrf[%d] = %E, fFeaMax_TrainArrf[%d] = %E",
			iFeaf, fFeaMin_TrainArrf[iFeaf], iFeaf, fFeaMax_TrainArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (fStDev_All_FeasArrf[iFeaf] <= eps)
		{
			printf( "\n\n A warning in 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': too small fStDev_All_FeasArrf[%d] = %E",
				iFeaf, fStDev_All_FeasArrf[iFeaf]);
			fprintf(fout, "\n\n A warning in 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': too small fStDev_All_FeasArrf[%d] = %E",
				iFeaf, fStDev_All_FeasArrf[iFeaf]);
		} //if (fStDev_All_FeasArrf[iFeaf] <= eps)
	} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

//////////////////////////////////////////////////
	//Normalization for train vecs
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		for (iVecf = 0; iVecf < nVecTrainf; iVecf++)
		{
			fFeaCurf = fFea_TrainArrf[iFeaf + iVecf * nDimf];

			if (fStDev_All_FeasArrf[iFeaf] > eps)
			{
				fFea_TrainArrf[iFeaf + iVecf * nDimf] = (fFeaCurf - fMean_All_FeasArrf[iFeaf]) / fStDev_All_FeasArrf[iFeaf];
			} //if (fStDev_All_FeasArrf[iFeaf] > eps)
			else if (fStDev_All_FeasArrf[iFeaf] <= eps)
			{
				fFea_TrainArrf[iFeaf + iVecf * nDimf] = 0.0;
			} //else if (fStDev_All_FeasArrf[iFeaf] <= eps)

		} // for (iVecf = 0; iVecf < nVecTrainf; iVecf++)

	} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

//////////////////////////////////////////////////
	//Normalization of test vecs
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		for (iVecf = 0; iVecf < nVecTestf; iVecf++)
		{
			fFeaCurf = fFea_TestArrf[iFeaf + iVecf * nDimf];

			if (fStDev_All_FeasArrf[iFeaf] > eps)
			{
				fFea_TestArrf[iFeaf + iVecf * nDimf] = (fFeaCurf - fMean_All_FeasArrf[iFeaf]) / fStDev_All_FeasArrf[iFeaf];
			} //if (fStDev_All_FeasArrf[iFeaf] > eps)
			else if (fStDev_All_FeasArrf[iFeaf] <= eps)
			{
				fFea_TestArrf[iFeaf + iVecf * nDimf] = 0.0;
			} //else if (fStDev_All_FeasArrf[iFeaf] <= eps)

		} // for (iVecf = 0; iVecf < nVecTestf; iVecf++)

	} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

////////////////////////////////////////////////
	delete[] fMean_All_FeasArrf;
	delete[] fStDev_All_FeasArrf;

	return SUCCESSFUL_RETURN;
} //int Normalizing_Every_Fea_To_Mean_0_And_StDev_1 (...
//////////////////////////////////////////////////////////

int Extracting_Data_From_OneLine_1(								 
								 const char *cInputOneLinef,
									const int nDim_D_SelecFeasf,

								 int &nYf,
	
								 float fFeaOneLineArrf[]) //[nDim_D_WithConst]
{	
	int
		iFeaf,
		iPositf,
		nPosInTheCurrentSegmentf = 0,
		nSegmentNumberCurf = 0,

		nNumOfFeaCurf = -1, //initially
		nPosInTheLinef = 0,
		nLengthOfInputCharLinef = strlen(cInputOneLinef);

	char
		cCharSubstringf[nSubstringLenMax];

	if (nLengthOfInputCharLinef >= nLengthOneLineMax - 5)
	{
		printf("\n\n An error in 'Extracting_Data_From_OneLine_1': nLengthOfInputCharLinef >= nLengthOneLineMax - 5");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\nAn error in 'Extracting_Data_From_OneLine_1': nLengthOfInputCharLinef >= nLengthOneLineMax - 5");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();	exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nLengthOfInputCharLinef >= nLengthOneLineMax - 5)

	memset(cCharSubstringf, 0, sizeof(cCharSubstringf));

	/////////////////////////////////////////////////////////
	//for verification
	nYf = -nLarge;
	for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)
	{
		fFeaOneLineArrf[iFeaf] = -fLarge;
	} //for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)

	//printf("\n\n'Extracting_Data_From_OneLine_1': %s\n", cInputOneLinef);
	//printf("\n\n nLengthOfInputCharLinef = %d\n", nLengthOfInputCharLinef);

///////////////////////////////////////////
	nPosInTheCurrentSegmentf = 0;

	//do // while(nPosInTheLinef < nLengthOfInputCharLinef);
	for (nPosInTheLinef = 0; nPosInTheLinef < nLengthOfInputCharLinef; nPosInTheLinef++)
	{
		//printf("\n The next nPosInTheLinef = %d, nPosInTheCurrentSegmentf = %d, nSegmentNumberCurf = %d", nPosInTheLinef, nPosInTheCurrentSegmentf, nSegmentNumberCurf);

		if (nPosInTheCurrentSegmentf == 0)
		{
			if (cInputOneLinef[nPosInTheLinef] == ' ' || cInputOneLinef[nPosInTheLinef] == ':' || cInputOneLinef[nPosInTheLinef] == ',' || cInputOneLinef[nPosInTheLinef] == '\t'
				|| cInputOneLinef[nPosInTheLinef] == '\0' || cInputOneLinef[nPosInTheLinef] == '\r' || cInputOneLinef[nPosInTheLinef] == '\n')
			{
				//printf("\n\n Not a substring symbol at nPosInTheLinef = %d, nPosInTheCurrentSegmentf = %d, nSegmentNumberCurf = %d", nPosInTheLinef, nPosInTheCurrentSegmentf, nSegmentNumberCurf);

				//printf("\n\nPlease press any key to continue:"); getchar();
				continue;
			} //if (cInputOneLinef[nPosInTheLinef] == ' ' || ...
			else
			{
				nPosInTheCurrentSegmentf = 1;
				cCharSubstringf[0] = cInputOneLinef[nPosInTheLinef];

				//printf("\n\n The beginning of next 'cCharSubstringf': %s", cCharSubstringf);

				nSegmentNumberCurf += 1;
				//printf("\n nPosInTheLinef = %d, nPosInTheCurrentSegmentf = %d, the new nSegmentNumberCurf = %d", nPosInTheLinef, nPosInTheCurrentSegmentf, nSegmentNumberCurf);

				//printf("\n\nPlease press any key to continue:"); getchar();
				continue;
			} //

		} //if (nPosInTheCurrentSegmentf == 0)
		else if (nPosInTheCurrentSegmentf != 0)
		{
			if (cInputOneLinef[nPosInTheLinef] == ' ' || cInputOneLinef[nPosInTheLinef] == ':' || cInputOneLinef[nPosInTheLinef] == ',' || cInputOneLinef[nPosInTheLinef] == '\t'
				|| cInputOneLinef[nPosInTheLinef] == '\0' || cInputOneLinef[nPosInTheLinef] == '\r' || cInputOneLinef[nPosInTheLinef] == '\n')
			{
				/////////////////////////////////////////////
				//the end of the substring -- converting the substring to a number
								//printf("\n\n The end of 'cCharSubstringf': %s", cCharSubstringf);
								//printf("\n nPosInTheLinef = %d, nPosInTheCurrentSegmentf = %d, nSegmentNumberCurf = %d", nPosInTheLinef, nPosInTheCurrentSegmentf, nSegmentNumberCurf);

								//if (nSegmentNumberCurf == 0)
				if (nSegmentNumberCurf == 1)
				{
					nYf = atoi(cCharSubstringf);

					//printf("\n\n A new nYf = %d, nDim_D_SelecFeas_WithConstf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
						//nYf, nDim_D_SelecFeas_WithConstf, nPosInTheLinef, nSegmentNumberCurf);

					if (nYf != 0 && nYf != 1)
					{
						printf("\n\n An error in 'Extracting_Data_From_OneLine_1': nYf = %d, nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nYf, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine_1': nYf = %d, nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nYf, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
						getchar();	exit(1);

						return UNSUCCESSFUL_RETURN;
					} //if (nYf != 0 && nYf != 1) 

					memset(cCharSubstringf, 0, sizeof(cCharSubstringf));
					nPosInTheCurrentSegmentf = 0;
					//printf("\n\nPlease press any key to continue:"); getchar();
					continue;
				} //if (nSegmentNumberCurf == 1)
				//else if (nSegmentNumberCurf > 1 && (nSegmentNumberCurf / 2) * 2 != nSegmentNumberCurf)
				else if (nSegmentNumberCurf > 1 && (nSegmentNumberCurf / 2) * 2 == nSegmentNumberCurf)
				{
					nNumOfFeaCurf = atoi(cCharSubstringf);

					//printf("\n\n A new nNumOfFeaCurf = %d, nDim_D_SelecFeas_WithConstf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
						//nNumOfFeaCurf, nDim_D_SelecFeas_WithConstf, nPosInTheLinef, nSegmentNumberCurf);

					//if (nNumOfFeaCurf < 1 || nNumOfFeaCurf > nDim_D_SelecFeasf) //for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
					if (nNumOfFeaCurf < 0 || nNumOfFeaCurf > nDim_D_SelecFeasf - 1) //for 'Best_Feas_Normal_Malignant_Train.txt'
					{
						printf("\n\n An error in 'Extracting_Data_From_OneLine_1': nNumOfFeaCurf = %d, nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nNumOfFeaCurf, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine_1': nNumOfFeaCurf = %d, nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nNumOfFeaCurf, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						getchar();	exit(1);
						return UNSUCCESSFUL_RETURN;
					}//if (nNumOfFeaCurf != 1)

					memset(cCharSubstringf, 0, sizeof(cCharSubstringf));

					nPosInTheCurrentSegmentf = 0;
					//printf("\n\nPlease press any key to continue:"); getchar();
					continue;
				} //else if (nSegmentNumberCurf > 1 && (nSegmentNumberCurf/2)*2 == nSegmentNumberCurf)
				else if (nSegmentNumberCurf > 1 && (nSegmentNumberCurf / 2) * 2 != nSegmentNumberCurf)
				{

					//if (nNumOfFeaCurf - 1 < 0 || nNumOfFeaCurf - 1 > nDim_D_SelecFeasf - 1)  //for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
					if (nNumOfFeaCurf < 0 || nNumOfFeaCurf > nDim_D_SelecFeasf - 1) //for 'Best_Feas_Normal_Malignant_Train.txt'
					{
						//for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
						//printf("\n\n An error in 'Extracting_Data_From_OneLine_1': nNumOfFeaCurf - 1 = %d, nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							//nNumOfFeaCurf - 1, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);

						//for 'Best_Feas_Normal_Malignant_Train.txt'
						printf("\n\n An error in 'Extracting_Data_From_OneLine_1': nNumOfFeaCurf = %d, nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nNumOfFeaCurf, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
						//for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
						//fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine_1': nNumOfFeaCurf - 1 = %d, nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							//nNumOfFeaCurf - 1, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);

						//for 'Best_Feas_Normal_Malignant_Train.txt'
						fprintf(fout, "\n\n An error in 'Extracting_Data_From_OneLine_1': nNumOfFeaCurf = %d, nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nNumOfFeaCurf, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
						getchar();	exit(1);

						return UNSUCCESSFUL_RETURN;
					} //if (nNumOfFeaCurf - 1 < 0 || nNumOfFeaCurf - 1 > nDim_D_SelecFeasf - 1)

					//fFeaOneLineArrf[nNumOfFeaCurf - 1] = atof(cCharSubstringf); //for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
					fFeaOneLineArrf[nNumOfFeaCurf] = atof(cCharSubstringf); //for 'Best_Feas_Normal_Malignant_Train.txt'

					//if (fFeaOneLineArrf[nNumOfFeaCurf - 1] < fFeaMin || fFeaOneLineArrf[nNumOfFeaCurf - 1] > fFeaMax) //for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
					if (fFeaOneLineArrf[nNumOfFeaCurf] < fFeaMin || fFeaOneLineArrf[nNumOfFeaCurf] > fFeaMax) //for 'Best_Feas_Normal_Malignant_Train.txt'
					{
						//for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
						//printf("\n\n An error in 'Extracting_Data_From_OneLine_1': fFeaOneLineArrf[%d] = %E, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							//nNumOfFeaCurf - 1, fFeaOneLineArrf[nNumOfFeaCurf - 1], nPosInTheLinef, nSegmentNumberCurf);

						printf("\n\n An error in 'Extracting_Data_From_OneLine_1': fFeaOneLineArrf[%d] = %E, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nNumOfFeaCurf, fFeaOneLineArrf[nNumOfFeaCurf], nPosInTheLinef, nSegmentNumberCurf);

						printf("\n fFeaMin = %E, fFeaMax = %E", fFeaMin, fFeaMax);
#ifndef COMMENT_OUT_ALL_PRINTS
						//for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
						//fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine_1': fFeaOneLineArrf[%d] = %E, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							//nNumOfFeaCurf - 1, fFeaOneLineArrf[nNumOfFeaCurf - 1], nPosInTheLinef, nSegmentNumberCurf);

						//for 'Best_Feas_Normal_Malignant_Train.txt'
						fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine_1': fFeaOneLineArrf[%d] = %E, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nNumOfFeaCurf, fFeaOneLineArrf[nNumOfFeaCurf], nPosInTheLinef, nSegmentNumberCurf);

						fprintf(fout, "\n fFeaMin = %E, fFeaMax = %E", fFeaMin, fFeaMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
						getchar();	exit(1);

						return UNSUCCESSFUL_RETURN;
					}//if (fFeaOneLineArrf[nNumOfFeaCurf - 1] < fFeaMin || fFeaOneLineArrf[nNumOfFeaCurf - 1] > fFeaMax)

				//	printf("\n\n Fea 'cCharSubstringf': %s", cCharSubstringf);
					//printf("\n A new fFeaOneLineArrf[%d] = %E, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
					//	nNumOfFeaCurf, fFeaOneLineArrf[nNumOfFeaCurf], nPosInTheLinef, nSegmentNumberCurf);

					memset(cCharSubstringf, 0, sizeof(cCharSubstringf));
					nPosInTheCurrentSegmentf = 0;
					//printf("\n\nPlease press any key to continue:"); getchar(); 
					continue;
				} //else if (nSegmentNumberCurf > 1 && (nSegmentNumberCurf / 2) * 2 != nSegmentNumberCurf)

				memset(cCharSubstringf, 0, sizeof(cCharSubstringf));

				//printf("\n\n The end of the substring 'cCharSubstringf' = %s", cCharSubstringf);
				//printf("\n nPosInTheLinef = %d, nSegmentNumberCurf = %d, nNumOfFeaCurf = %d", nPosInTheLinef, nSegmentNumberCurf, nNumOfFeaCurf);

				//printf("\n\nPlease press any key to continue:"); getchar();
///////////////////////////////////////////////////
				nPosInTheCurrentSegmentf = 0;
				continue;
			} //if (cInputOneLinef[nPosInTheLinef] == ' ' || ...
			else // no end of the substring
			{
				nPosInTheCurrentSegmentf += 1;
				cCharSubstringf[nPosInTheCurrentSegmentf - 1] = cInputOneLinef[nPosInTheLinef];

				//printf("\n\n Increasing 'cCharSubstringf': %s", cCharSubstringf);
				//printf("\n nPosInTheLinef = %d, a new nPosInTheCurrentSegmentf = %d, the same nSegmentNumberCurf = %d", nPosInTheLinef, nPosInTheCurrentSegmentf, nSegmentNumberCurf);

				//printf("\n\nPlease press any key to continue:"); getchar();
				continue;

			} // else // no end of the substring

		} //else if (nPosInTheCurrentSegmentf != 0)

	//printf("\n\n The end of the loop: nPosInTheLinef = %d, nSegmentNumberCurf = %d, nNumOfFeaCurf = %d", nPosInTheLinef, nSegmentNumberCurf, nNumOfFeaCurf);
	} //for (nPosInTheLinef = 0; nPosInTheLinef < nLengthOfInputCharLinef; nPosInTheLinef++)

	//if (nNumOfFeaCurf != nDim_D_SelecFeasf) //for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
	if (nNumOfFeaCurf + 1 != nDim_D_SelecFeasf) //for 'Best_Feas_Normal_Malignant_Train.txt'
	{
		//for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
		//printf("\n\n An error in 'Extracting_Data_From_OneLine_1': nNumOfFeaCurf = %d != nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
			//nNumOfFeaCurf, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);

		printf("\n\n An error in 'Extracting_Data_From_OneLine_1': nNumOfFeaCurf + 1 = %d != nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
			nNumOfFeaCurf + 1, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
		//for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
		//fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine_1': nNumOfFeaCurf = %d != nDim_D_SelecFeasf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
			//nNumOfFeaCurf, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);

		//for 'Best_Feas_Normal_Malignant_Train.txt'
		fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine_1': nNumOfFeaCurf + 1 = %d != nDim_D_SelecFeasf= %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
			nNumOfFeaCurf + 1, nDim_D_SelecFeasf, nPosInTheLinef, nSegmentNumberCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfFeaCurf + 1 != nDim_D_SelecFeasf)

////////////////////////////////
/*
	printf("\n\n nYf = %d", nYf);
	for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)
	{
		printf("\n fFeaOneLineArrf[%d] = %E", iFeaf, fFeaOneLineArrf[iFeaf]);
	} //for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)

	printf("\n\nPlease press any key:"); getchar();
*/
	return SUCCESSFUL_RETURN;
}// int Extracting_Data_From_OneLine_1(...)
////////////////////////////////////////////////////////////////////////////////////

int Reading_All_LinesOfData_OfOneFile(
	const int nDim_D_SelecFeasf,
	const int nNumOfVecsTotf,

	const int nReadTrainOrTestf, // 1-- train, (-1) -- test
	///////////////////////////////////////////////////////
	int nY_Arrf[], //[nNumOfVecsTotf]

	float fFeasAll_Arrf[]) //[nProdTot]
{
	int Extracting_Data_From_OneLine_1(
		const char *cInputOneLinef,
		const int nDim_D_SelecFeasf,

		int &nYf,

		float fFeaOneLineArrf[]); //[nDim_D_WithConst]

	int
		iVecf = -1,
		nIndexf,
		iFeaf,
		nTempf,
		nInputLineLengthf,

		nResf,
		nY_Curf;
	
	float
		fFeaOneLineArrf[nDim_DifEvo_WithConst]; //float &fPriceOpenf,;

	char cInputLinef[nInputLineLengthMax];

	rewind(fin_Train);
	rewind(fin_Test);
	for (; ;)
	{
		memset(cInputLinef, 0, sizeof(cInputLinef));

		iVecf += 1;

		if (nReadTrainOrTestf == 1)
		{
			if (fgets(cInputLinef, nInputLineLengthMax, fin_Train) == NULL)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n No data to read: fgets(cInputLinef, nInputLineLengthMaxf, fin_Train) == NULL, iVecf = %d", iVecf);
				fprintf(fout,"\n\n No data to read: fgets(cInputLinef, nInputLineLengthMaxf, fin_Train) == NULL, iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return SUCCESSFUL_RETURN;
			} //if (fgets(cInputLinef, nInputLineLengthMax, fin_Train) == NULL)

		} // if (nReadTrainOrTestf == 1)
		else if (nReadTrainOrTestf == -1)
		{
			if (fgets(cInputLinef, nInputLineLengthMax, fin_Test) == NULL)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n No data to read: fgets(cInputLinef, nInputLineLengthMaxf, fin_Test) == NULL, iVecf = %d", iVecf);
				fprintf(fout, "\n\n No data to read: fgets(cInputLinef, nInputLineLengthMaxf, fin_Test) == NULL, iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return SUCCESSFUL_RETURN;
			} //if (fgets(cInputLinef, nInputLineLengthMax, fin_Test) == NULL)

		} // if (nReadTrainOrTestf == -1)
/////////////////////////////////////
		nResf = Extracting_Data_From_OneLine_1(
			cInputLinef, //const char *cInputOneLinef,
			nDim_D_SelecFeasf, //const int nDim_D_SelecFeasf,

			nY_Curf, //int &nYf,

			fFeaOneLineArrf); // float fFeaOneLineArrf[]); //[nDim_D_WithConst]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n An error in 'Reading_All_LinesOfData_OfOneFile' for iVecf = %d", iVecf);
			fprintf(fout, "\n\n An error in 'Reading_All_LinesOfData_OfOneFile' for iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}// if (nResf == UNSUCCESSFUL_RETURN)

		nY_Arrf[iVecf] = nY_Curf;

		nTempf = iVecf * nDim_D_SelecFeasf;
		for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)
		{
			nIndexf = nTempf + iFeaf;

			fFeasAll_Arrf[nIndexf] = fFeaOneLineArrf[iFeaf];
		}//for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)

	} //for( ; ;)

	return SUCCESSFUL_RETURN;
} //int Reading_All_LinesOfData_OfOneFile(...
/////////////////////////////////////////////////////////////////////////////////

int Reading_All_TrainTest_Data(
	const int nDim_D_SelecFeasf,

	const int nNumVecTrainTotf,
	const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
	int nY_Train_Arrf[], //[nNumVecTrainTot]
	int nY_Test_Arrf[], //[nNumVecTestTot]

	float fFeaTrain_Arrf[], //[nProdTrainTot]
	float fFeaTest_Arrf[]) //[nProdTestTot]
{
	int Reading_All_LinesOfData_OfOneFile(
		const int nDim_D_SelecFeasf,
		const int nNumOfVecsTotf,

		const int nReadTrainOrTestf, // 1-- train, (-1) -- test
		///////////////////////////////////////////////////////
		int nY_Arrf[], //[nNumOfVecsTotf]

		float fFeasAll_Arrf[]); //[nProdTrainTot]

	int
		nResf,
		nReadTrainOrTestf; // 1-- train, (-1) -- test

////////////////////////////////////////////////////
//Train
	nReadTrainOrTestf = 1;
	nResf = Reading_All_LinesOfData_OfOneFile(
				nDim_D_SelecFeasf, //const int nDim_D_SelecFeasf,
				nNumVecTrainTotf, //const int nNumOfVecsTotf,

				nReadTrainOrTestf, //const int nReadTrainOrTestf, // 1-- train, (-1) -- test
				///////////////////////////////////////////////////////
				nY_Train_Arrf, //int nY_Arrf[], //[nNumOfVecsTotf]

				fFeaTrain_Arrf); // float fFeasAll_Arrf[]); //[nProdTrainTot]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in 'Reading_All_TrainTest_Data' for train data");
		fprintf(fout, "\n\n An error in 'Reading_All_TrainTest_Data' for train data");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}// if (nResf == UNSUCCESSFUL_RETURN)

////////////////////
//Test
	nReadTrainOrTestf = -1;
	nResf = Reading_All_LinesOfData_OfOneFile(
			nDim_D_SelecFeasf, //const int nDim_D_SelecFeasf,
			nNumVecTestTotf, //const int nNumOfVecsTotf,

			nReadTrainOrTestf, //const int nReadTestOrTestf, // 1-- train, (-1) -- test
			///////////////////////////////////////////////////////
			nY_Test_Arrf, //int nY_Arrf[], //[nNumOfVecsTotf]

			fFeaTest_Arrf); // float fFeasAll_Arrf[]); //[nProdTestTot]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in 'Reading_All_TrainTest_Data' for test data");
		fprintf(fout, "\n\n An error in 'Reading_All_TrainTest_Data' for test data");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}// if (nResf == UNSUCCESSFUL_RETURN)

	return SUCCESSFUL_RETURN;
}//int Reading_All_TrainTest_Data(...

//////////////////////////////////////////////////////////////////////////////////
int Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas(
					const int nDimf,
					const int nDimSelecf, // < nDimf

					const int nNumVecf,

					const int nPosOfSelec_FeasArrf[], //[nDimSelecf]

					const float fVecArr[], //[nDimf*nNumVecf]
					float fVecSelecArr[]) //[nDimSelecf*nNumVecf]
{
int
	iFeaf,
	iVecf,
	nPosSelecCurf;
	
for (iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)
{
nPosSelecCurf = nPosOfSelec_FeasArrf[iFeaf];
	if (nPosSelecCurf < 0 || nPosSelecCurf > nDimf - 1)
	{
	printf("\n\nAn error in 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas': nPosSelecCurf = %d < 0 ||..., iFeaf = %d", 
					nPosSelecCurf,iFeaf);
	
	fprintf(fout,"\n\nAn error in 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas': nPosSelecCurf = %d < 0 ||..., iFeaf = %d", 
					nPosSelecCurf,iFeaf);

	getchar();	exit(1);
	} //if (nPosSelecCurf < 0 || nPosSelecCurf > nDimf - 1)

	for (iVecf = 0; iVecf < nNumVecf; iVecf++)
	{
	fVecSelecArr[iFeaf + iVecf*nDimSelecf] = fVecArr[nPosSelecCurf + iVecf*nDimf];
	} // for (iVecf = 0; iVecf < nNumVecf; iVecf++)

} //for (iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)

return 1;
} //int Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas(...
///////////////////////////////

int Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas_And_WithConst(
	const int nDimf,
	const int nDimSelecf, // < nDimf

	const float fFeaConst_UnRefined_Initf,
	const int nNumVecf,

	const int nPosOfSelec_FeasArrf[], //[nDimSelecf]

	const float fVecArr[], //[nDimf*nNumVecf]
	float fVecSelec_WithConstArr[]) //[(nDimSelecf + 1)*nNumVecf]
{
	int
		iFeaf,
		iVecf,
		nIndexf,

		nDimSelecWithConstf = nDimSelecf + 1,
		nProdTempf,
		nPosSelecCurf;

	for (iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)
	{
		nPosSelecCurf = nPosOfSelec_FeasArrf[iFeaf];
		if (nPosSelecCurf < 0 || nPosSelecCurf > nDimf - 1)
		{
			printf("\n\nAn error in 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas_And_WithConst': nPosSelecCurf = %d < 0 ||..., iFeaf = %d",
				nPosSelecCurf, iFeaf);

			fprintf(fout, "\n\nAn error in 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas_And_WithConst': nPosSelecCurf = %d < 0 ||..., iFeaf = %d",
				nPosSelecCurf, iFeaf);

			fflush(fout);  getchar();	exit(1);
		} //if (nPosSelecCurf < 0 || nPosSelecCurf > nDimf - 1)

		for (iVecf = 0; iVecf < nNumVecf; iVecf++)
		{
			nProdTempf = iVecf * nDimSelecWithConstf;
			nIndexf = iFeaf + nProdTempf;

			fVecSelec_WithConstArr[nIndexf] = fVecArr[nPosSelecCurf + iVecf * nDimf];

			if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66
			{
				fprintf(fout, "\n\n 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas_And_WithConst': nPosSelecCurf = %d, iVecf = %d, iFeaf = %d",
					nPosSelecCurf, iVecf, iFeaf);

				fprintf(fout, "\n fVecSelec_WithConstArr[%d] = %E", nIndexf,fVecSelec_WithConstArr[nIndexf] );
			} //if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66

		} // for (iVecf = 0; iVecf < nNumVecf; iVecf++)

	} //for (iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)
	//printf("\n\n Debugging 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas_And_WithConst: 1"); fflush(fout);  getchar();

///////////////////////
//the last fea in a row (at iFeaf = nDimSelecf) is fFeaConst_UnRefined_Initf
	for (iVecf = 0; iVecf < nNumVecf; iVecf++)
	{
		nProdTempf = iVecf * nDimSelecWithConstf;

		nIndexf = nDimSelecf + nProdTempf;

		fVecSelec_WithConstArr[nIndexf] = fFeaConst_UnRefined_Initf;

		if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66
		{
			fprintf(fout, "\n\n 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas_And_WithConst': a const, fVecSelec_WithConstArr[%d] = %E, iVecf = %d",
				nIndexf,fVecSelec_WithConstArr[nIndexf], iVecf);
		} //if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66

	} // for (iVecf = 0; iVecf < nNumVecf; iVecf++)

	return SUCCESSFUL_RETURN;
} //int Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas_And_WithConst(...
////////////////////////////////////////////////////////////////

int ReplacingAConst_In_2DimFloatArr(
	//const int nDimf,
	const int nDimSelecf, // < nDimf and < nDim_SelecFeas_WithConstf

	const float fFeaConstNewf,

	const int nNumVecf,

	float fVecSelec_WithConstArr[]) //[(nDimSelecf + 1)*nNumVecf]
{
	int
		iVecf,
		nIndexf,

		nDimSelecWithConstf = nDimSelecf + 1,
		nProdTempf;

	float
		fConstReadPrevf,
		fConstReadCurf;
///////////////////////
//the last fea in a row (at iFeaf = nDimSelecf) is fFeaConst_UnRefined_Initf
	for (iVecf = 0; iVecf < nNumVecf; iVecf++)
	{
		nProdTempf = iVecf * nDimSelecWithConstf;

		nIndexf = nDimSelecf + nProdTempf;

		fConstReadCurf = fVecSelec_WithConstArr[nIndexf];

		if (fConstReadCurf < fFeaConst_ForRefined_Glob_Min || fConstReadCurf > fFeaConst_ForRefined_Glob_Max)
		{
			printf("\n\n An error in 'ReplacingAConst_In_2DimFloatArr': fConstReadCurf = %E < fFeaConst_ForRefined_Glob_Min = %E ||..., iVecf = %d",
				fConstReadCurf, fFeaConst_ForRefined_Glob_Min,iVecf);

			fprintf(fout, "\n\nAn error in 'ReplacingAConst_In_2DimFloatArr': fConstReadCurf = %E < fFeaConst_ForRefined_Glob_Min = %E ||..., iVecf = %d",
				fConstReadCurf, fFeaConst_ForRefined_Glob_Min, iVecf);

			fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (fConstReadCurf < fFeaConst_ForRefined_Glob_Min || fConstReadCurf > fFeaConst_ForRefined_Glob_Max)

		if (iVecf > 0 && fConstReadCurf != fConstReadPrevf)
		{
			printf("\n\n An error in 'ReplacingAConst_In_2DimFloatArr': fConstReadCurf = %E != fConstReadPrevf = %E, iVecf = %d",
				fConstReadCurf, fConstReadPrevf, iVecf);

			fprintf(fout, "\n\nAn error in 'ReplacingAConst_In_2DimFloatArr': fConstReadCurf = %E != fConstReadPrevf = %E, iVecf = %d",
				fConstReadCurf, fConstReadPrevf, iVecf);

			fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (iVecf > 0 && fConstReadCurf != fConstReadPrevf)

		//fVecSelec_WithConstArr[nIndexf] = fFeaConst_UnRefined_Initf;
		fVecSelec_WithConstArr[nIndexf] = fFeaConstNewf;

		if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66
		{
			fprintf(fout, "\n\n 'ReplacingAConst_In_2DimFloatArr': a const, fVecSelec_WithConstArr[%d] = %E, iVecf = %d",
				nIndexf, fVecSelec_WithConstArr[nIndexf], iVecf);
		} //if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66

		fConstReadPrevf = fConstReadCurf;
	} // for (iVecf = 0; iVecf < nNumVecf; iVecf++)

	return SUCCESSFUL_RETURN;
} //	int ReplacingAConst_In_2DimFloatArr(...
////////////////////////////////////////////////////////

int NumOfNonZeros_In_IntArr(
				  const int nDimf,
				  const int nArrf[], // [nDimf]

				int &nNumOfNonZeros_In_IntArrf) 
{
int
	i1;

nNumOfNonZeros_In_IntArrf = 0;

for(i1 = 0; i1 < nDimf;  i1++)
{
	if (nArrf[i1] > 0)
		nNumOfNonZeros_In_IntArrf +=1;
	else if (nArrf[i1] < 0)
	{
	printf("\n\nAn error in 'NumOfNonZeros_In_IntArr': nArrf[%d] = %d < 0", 
						i1,nArrf[i1]);
	fprintf(fout,"\n\nAn error in 'NumOfNonZeros_In_IntArr': nArrf[%d] = %d < 0", 
						i1,nArrf[i1]);

	getchar();	exit(1);
	} //else if (nArrf[i1] < 0)

} //for(i1 = 0; i1 < nDimf;  i1++)

return 1;
} //int NumOfNonZeros_In_IntArr(
/////////////////////////////////////////////////////////

void Copying_Float_Arr1_To_Arr2(
				  const int nDimf,
				  const float fArr1f[], // [nDimf]
				  float fArr2f[]) // [nDimf]
{
int 
	i1;

	for (i1 = 0; i1 < nDimf; i1++)
	{
		fArr2f[i1] = fArr1f[i1];
	} //for (i1 = 0; i1 < nDimf; i1++)

} // void Copying_Float_Arr1_To_Arr2(...
/////////////////////////////////////////////////////////

void Copying_Int_Arr1_To_Arr2(
	const int nDimf,
	const int nArr1f[], // [nDimf]
	int nArr2f[]) // [nDimf]
{
	int
		i1;

	for (i1 = 0; i1 < nDimf; i1++)
		nArr2f[i1] = nArr1f[i1];

} // void Copying_Int_Arr1_To_Arr2(...
///////////////////////////////////////////////////

int Int_Belongs_To_IntArr_Or_Not_With_Update(
											const int nDimMaxf,

											const int nDimInitf,
											const int nTestf,
											
											int &nBelongsf, //'1' --> belongs and '-1' otherwise
											int &nDimCurf,

											int nArrf[])
{
int
	i1;

int
	nDifff;

nBelongsf = 0; //impossible
nDimCurf = -1;

for (i1 = 0; i1 < nDimInitf; i1++)
{
nDifff = nArrf[i1] - nTestf;

	if (nDifff == 0)
	{
	nBelongsf = 1; //belongs
	nDimCurf = nDimInitf;

	break;
	} // if (nDifff > -fepsf && nDifff < fepsf)
	
} // for (i1 = 0; i1 < nDimInitf; i1++)

if (nBelongsf == 0)
{
//Updating
nDimCurf = nDimInitf + 1;
	if (nDimCurf > nDimMaxf)
	{
	printf("\n\nAn error in 'Int_Belongs_To_IntArr_Or_Not_With_Update': nDimCurf = %d > nDimMaxf = %d", 
		nDimCurf,nDimMaxf);

	fprintf(fout,"\n\nAn error in 'Int_Belongs_To_IntArr_Or_Not_With_Update': nDimCurf = %d > nDimMaxf = %d", 
		nDimCurf,nDimMaxf);

	getchar();	exit(1);
	} //if (nDimCurf > nDimMaxf)

nArrf[nDimInitf] = nTestf;
} // if (nBelongsf == 0)

return 1;
} // int Int_Belongs_To_IntArr_Or_Not_With_Update(...

/////////////////////////////////////////////////////////////////////////////////////////////////////////
void Loss(
	const int nDim_Hf,
	const int nYtf, // 1 or -1 (not 0)

	const float fZ_Arrf[], //[nDim_Hf]

	const float fW_Arrf[], //[nDim_Hf]

	int &nY_Estimatedf, //0 or 1 (not -1 or 1)
	float &fLossf)
{
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);
	////////////////////////////////////////
	int
		nPrintedf = 0,
		iFea_Hf;

	float
		fProdf,
		fScalar_Prodf,
		fScalar_ProdfWithBiasf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Loss': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, nYtf = %d", iVec_Train_Glob, nY_Train_Actual_Glob, nYtf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	Scalar_Product(
		nDim_Hf, //const int nDimf,

		fZ_Arrf, //const float fFeas_Arr_1f[],

		fW_Arrf, //const float fFeas_Arr_2f[],
		fScalar_Prodf); // float &fScalar_Prodf);

	fScalar_ProdfWithBiasf = fScalar_Prodf + fBiasForClassifByLossFunction_Glob;

	//if (fScalar_Prodf < 0.0)
	if (fScalar_ProdfWithBiasf < 0.0)
	{
		nY_Estimatedf = 0; // not -1;
	}// if (fScalar_ProdfWithBiasf < 0.0)
	else
	{
		nY_Estimatedf = 1;
	}//else

	fProdf = (float)(nYtf)*fScalar_Prodf;

	if (fProdf >= 1.0)
	{
		fLossf = 0.0;
	}//if ( (float)(nYtf)*fScalar_Prodf >= 1.0)
	else
	{
		fLossf = 1.0 - fProdf;
	}//else
	
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n 'Loss': fLossf = %E, fScalar_Prodf = %E, nY_Estimatedf = %d", fLossf, fScalar_Prodf, nY_Estimatedf);

	if (nYtf == -1 && fLossf > 0.0)
	{
		fprintf(fout, "\n A loss for a neg vector");

		if (nPrintedf == 0)
		{
			for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
			{
				fprintf(fout, "\n fZ_Arrf[%d] = %E, fW_Arrf[%d] = %E", iFea_Hf, fZ_Arrf[iFea_Hf], iFea_Hf, fW_Arrf[iFea_Hf]);

			} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

			nPrintedf = 1;
		} //if (nPrintedf == 0)

	} //if (nYtf == -1 && fLossf > 0.0)

	if (nY_Train_Actual_Glob != nY_Estimatedf && nYtf == -1)
	{
		fprintf(fout, "\n A different classification in 'Loss' for a neg vector");

		if (nPrintedf == 0)
		{
			for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
			{
				fprintf(fout, "\n fZ_Arrf[%d] = %E, fW_Arrf[%d] = %E", iFea_Hf, fZ_Arrf[iFea_Hf], iFea_Hf, fW_Arrf[iFea_Hf]);

			} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

			//nPrintedf = 1;
		} //if (nPrintedf == 0)

	} //if (nY_Train_Actual_Glob != nY_Estimatedf  && nYtf == -1) 

#endif //#ifndef COMMENT_OUT_ALL_PRINTS


}// void Loss(...

//////////////////////////////////////////////////////////////////////////////////////////////

int Updating_W_Arr(
	const int nDim_Hf,

	const float fCf,
	const float fAlphaf, // < 1.0

	const float fLossf,

	const int nYtf, // 1 or -1

	const float fZ_Arrf[], //[nDim_Hf]

	const float fW_Init_Arrf[], //[nDim_Hf]

	float fW_Fin_Arrf[]) //[nDim_Hf]

{
	int NormEuclidean_Of_A_FloatVector(
		const int nDimf,
		const float fFeas_Arrf[],

		float &fNormEuclid_Of_A_Vectorf);

	int
		nResf,
		iFea_Hf;

	float
		fNormEuclid_Of_A_Vectorf,
		fTempf,

		fChange_For_fWf,
		fTauf;

	if (fLossf < 0.0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_W_Arr': fLossf = %E < 0.0", fLossf);
		fprintf(fout, "\n\n  An error in 'Updating_W_Arr': fLossf = %E < 0.0", fLossf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (fLossf < 0.0)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Updating_W_Arr': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, fLossf = %E", iVec_Train_Glob, nY_Train_Actual_Glob, fLossf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nResf = NormEuclidean_Of_A_FloatVector(
		nDim_Hf, //const int nDimf,
		fZ_Arrf, //const float fFeas_InitArrf[],

		fNormEuclid_Of_A_Vectorf); // float &fNormOfAVectorf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Updating_W_Arr' by 'NormEuclidean_Of_A_FloatVector' ");
			fprintf(fout, "\n\n  An error in 'Updating_W_Arr' by 'NormEuclidean_Of_A_FloatVector' ");
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	if (fNormEuclid_Of_A_Vectorf < eps)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_W_Arr': fNormEuclid_Of_A_Vectorf = %E",fNormEuclid_Of_A_Vectorf);
		fprintf(fout, "\n\n  An error in 'Updating_W_Arr': fNormEuclid_Of_A_Vectorf = %E", fNormEuclid_Of_A_Vectorf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (fNormEuclid_Of_A_Vectorf < eps)
///////////////////////////
	fTempf = (1.0 - fAlphaf)*fLossf/(fNormEuclid_Of_A_Vectorf);

	if (fTempf < fCf)
	{
		fTauf = fTempf;
	}//if (fTempf < fCf)
	else
	{
		fTauf = fCf;
	}//else

///////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n 'Updating_W_Arr': fNormEuclid_Of_A_Vectorf = %E, fTauf = %E, nYtf = %d, fTempf = %E, fCf = %E, fAlphaf = %E",
		fNormEuclid_Of_A_Vectorf, fTauf, nYtf,fTempf, fCf, fAlphaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		fChange_For_fWf = fTauf * nYtf * fZ_Arrf[iFea_Hf];

		fW_Fin_Arrf[iFea_Hf] = fW_Init_Arrf[iFea_Hf] + fChange_For_fWf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n 'Updating_W_Arr': iFea_Hf = %d, fChange_For_fWf = %E, fW_Init_Arrf[iFea_Hf] = %E, fW_Fin_Arrf[iFea_Hf] = %E, fZ_Arrf[iFea_Hf] = %E",
			iFea_Hf, fChange_For_fWf, fW_Init_Arrf[iFea_Hf], fW_Fin_Arrf[iFea_Hf], fZ_Arrf[iFea_Hf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
}// int Updating_W_Arr(...

//////////////////////////////////////////////////////////////////////////////////////////////
int Updating_Z(
	const int nDim_Hf,

	const float fLossf,

	const int nYtf, // 1 or -1

	const float fW_Fin_Arrf[], //[nDim_Hf]

	const float fZ_Init_Arrf[], //[nDim_Hf]

	float fZ_Fin_Arrf[]) //[nDim_Hf]

{
	int NormEuclidean_Of_A_FloatVector(
		const int nDimf,
		const float fFeas_Arrf[],

		float &fNormEuclid_Of_A_Vectorf);

	int
		nResf,
		iFea_Hf;

	float
		fChangef,
		fNormEuclid_Of_A_Vectorf,
		fTauf;

	nResf = NormEuclidean_Of_A_FloatVector(
		nDim_Hf, //const int nDimf,
		fZ_Init_Arrf, //const float fFeas_InitArrf[],

		fNormEuclid_Of_A_Vectorf); // float &fNormOfAVectorf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Updating_Z': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, nYtf = %d", iVec_Train_Glob, nY_Train_Actual_Glob, nYtf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Updating_Z' by 'StDev_Of_A_FloatVector' ");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'Updating_Z' by 'StDev_Of_A_FloatVector' ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	if (fNormEuclid_Of_A_Vectorf < eps)
	{
		printf("\n\n An error in 'Updating_Z': fNormEuclid_Of_A_Vectorf = %E", fNormEuclid_Of_A_Vectorf);
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'Updating_Z': fNormEuclid_Of_A_Vectorf = %E", fNormEuclid_Of_A_Vectorf);
		
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();	exit(1);
		return UNSUCCESSFUL_RETURN;
	}//if (fNormEuclid_Of_A_Vectorf < eps)
/////////////////////////////////////////////////////
	fTauf = fLossf/ fNormEuclid_Of_A_Vectorf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Updating_Z': fTauf = %E, fLossf = %E, fNormEuclid_Of_A_Vectorf = %E", fTauf, fLossf, fNormEuclid_Of_A_Vectorf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		fChangef = fTauf * nYtf * fW_Fin_Arrf[iFea_Hf];
		fZ_Fin_Arrf[iFea_Hf] = fZ_Init_Arrf[iFea_Hf] + fChangef;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n 'Updating_Z': iFea_Hf = %d, fChangef = %E, fZ_Init_Arrf[iFea_Hf] = %E, fZ_Fin_Arrf[iFea_Hf] = %E",
			iFea_Hf, fChangef, fZ_Init_Arrf[iFea_Hf], fZ_Fin_Arrf[iFea_Hf]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (fZ_Fin_Arrf[iFea_Hf] > fLarge || fZ_Fin_Arrf[iFea_Hf] < -fLarge)
		{
			printf("\n\n An error in 'Updating_Z': fZ_Fin_Arrf[iFea_Hf] > fLarge || ...");
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Updating_Z': fZ_Fin_Arrf[iFea_Hf] > fLarge || ...");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (fZ_Fin_Arrf[iFea_Hf] > fLarge || fZ_Fin_Arrf[iFea_Hf] < -fLarge)

	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
}// int Updating_Z(...

//////////////////////////////////////////////////////////////////////////////////////////////
int Selecting_Hyperplane_For_OneFea_OfNonlinearSpace(
	const int nStrategyForSelectingHyperplanef, //1 or 2

	const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space

	const int nDim_Hf, // dimension of nonlinear space

	const int nKf, // nNumOfHyperplanes per one fea in nonlinear space
	
	const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

	const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

	const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
	const float fZ_Fin_Arrf[], //[nDim_Hf]

	const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

	int &nHyperplaneSelectedf)
{
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);

	int
		iFeaf,

		nIndexf,

		nIndexMaxf = nDim_D_SelecFeas_WithConstf * nDim_Hf * nKf - 1,
		iHyperplanef;

	float
		fZ_Targetf = fZ_Fin_Arrf[nFea_Hf],
		fScalar_Prodf,

		fDiffAbsolValueMinf = fLarge,

		fDiffAbsolValueCurf;

		//fU_ForAFea_H_Arrf[nDim_D_WithConst];

	float* fU_ForAFea_H_Arrf = new float[nDim_D_SelecFeas_WithConstf];
	if (fU_ForAFea_H_Arrf == NULL)
	{
		printf("\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': fU_ForAFea_H_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': fU_ForAFea_H_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fU_ForAFea_H_Arrf == NULL)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nStrategyForSelectingHyperplanef == 1) //hyperplane with max scalar prod
	{
		nHyperplaneSelectedf = nHyperplaneWithMaxScaProdArrf[nFea_Hf];

		if (nHyperplaneSelectedf < 0 || nHyperplaneSelectedf >= nKf)
		{
			printf("\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace' 1: nHyperplaneSelectedf = %d, nKf = %d", nHyperplaneSelectedf, nKf);

#ifndef COMMENT_OUT_ALL_PRINTS

			fprintf(fout, "\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace' 1: nHyperplaneSelectedf = %d, nKf = %d", nHyperplaneSelectedf, nKf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			delete[] fU_ForAFea_H_Arrf;
			return UNSUCCESSFUL_RETURN;
		}//if (nHyperplaneSelectedf < 0 || nHyperplaneSelectedf >= nKf)

		return SUCCESSFUL_RETURN;
	}//if (nStrategyForSelectingHyperplanef == 1)
	else if (nStrategyForSelectingHyperplanef == 2) //hyperplane with scalar prod closest to fZ_Fin_Arrf[nFea_Hf]
	{
		for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
		{
			for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
			{
				nIndexf = (nDim_D_SelecFeas_WithConstf*nDim_Hf*iHyperplanef) + (nDim_D_SelecFeas_WithConstf*nFea_Hf) + iFeaf;

				if (nIndexf > nIndexMaxf)
				{
					printf("\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': nIndexf = %d < nIndexMaxf = %d", nIndexf, nIndexMaxf);
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': nIndexf = %d < nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					getchar();	exit(1);

					delete[] fU_ForAFea_H_Arrf;
					return UNSUCCESSFUL_RETURN;
				}//if (nIndexf > nIndexMaxf)

				fU_ForAFea_H_Arrf[iFeaf] = fU_Arrf[nIndexf];
			}//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)

			Scalar_Product(
				nDim_D_SelecFeas_WithConstf, //const int nDimf,

				fX_Arrf, //const float fFeas_Arr_1f[],

				fU_ForAFea_H_Arrf, //const float fFeas_Arr_2f[],
				fScalar_Prodf); // float &fScalar_Prodf);

			if (fScalar_Prodf > fZ_Targetf)
			{
				fDiffAbsolValueCurf = fScalar_Prodf - fZ_Targetf;

			} // if (fScalar_Prodf > fZ_Targetf)
			else
			{
				fDiffAbsolValueCurf = fZ_Targetf - fScalar_Prodf;
			}//else

			if (fDiffAbsolValueCurf < fDiffAbsolValueMinf)
			{
				fDiffAbsolValueMinf = fDiffAbsolValueCurf;

				nHyperplaneSelectedf = iHyperplanef;
			} // if (fDiffAbsolValueCurf < fDiffAbsolValueMinf)

		} //for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)

		if (nHyperplaneSelectedf < 0 || nHyperplaneSelectedf >= nKf)
		{
			printf("\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace' 2: nHyperplaneSelectedf = %d, nKf = %d", nHyperplaneSelectedf, nKf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace' 2: nHyperplaneSelectedf = %d, nKf = %d", nHyperplaneSelectedf, nKf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);
			delete[] fU_ForAFea_H_Arrf;

			return UNSUCCESSFUL_RETURN;
		}//if (nHyperplaneSelectedf < 0 || nHyperplaneSelectedf >= nKf)

		delete[] fU_ForAFea_H_Arrf;
		return SUCCESSFUL_RETURN;
	}//else if (nStrategyForSelectingHyperplanef == 2) //hyperplane with scalar prod closest to fZ_Fin_Arrf[nFea_Hf]
	else 
	{
		printf("\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': nStrategyForSelectingHyperplanef = %d is not equal to 1 or 2", nStrategyForSelectingHyperplanef);

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout, "\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': nStrategyForSelectingHyperplanef = %d is not equal to 1 or 2", nStrategyForSelectingHyperplanef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		delete[] fU_ForAFea_H_Arrf;

		return UNSUCCESSFUL_RETURN;
	}//else

}// int Selecting_Hyperplane_For_OneFea_OfNonlinearSpace(...
////////////////////////////////////////////////////////////////////////////////////////////////

int Selecting_Hyperplanes_For_AllFeas_OfNonlinearSpace(
	const int nStrategyForSelectingHyperplanef, //1 or 2

	const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space

	const int nDim_Hf, // dimension of nonlinear space

	const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

	const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

	const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

	const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
	const float fZ_Fin_Arrf[], //[nDim_Hf]

	const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

	int nHyperplaneSelected_For_Z_Arrf[]) //[nDim_Hf]
{
	int Selecting_Hyperplane_For_OneFea_OfNonlinearSpace(
		const int nStrategyForSelectingHyperplanef, //1 or 2

		const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space

		const int nDim_Hf, // dimension of nonlinear space

		const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

		const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

		const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

		const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
		const float fZ_Fin_Arrf[], //[nDim_Hf]

		const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

		int &nHyperplaneSelectedf);

	int
		nResf,
		iFea_Hf,

		nHyperplaneSelectedf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Selecting_Hyperplanes_For_AllFeas_OfNonlinearSpace': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nResf = Selecting_Hyperplane_For_OneFea_OfNonlinearSpace(
					nStrategyForSelectingHyperplanef, //const int nStrategyForSelectingHyperplanef, //1 or 2

					nDim_D_SelecFeas_WithConstf, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space

					nDim_Hf, //const int nDim_Hf, // dimension of nonlinear space

					nKf, //const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

					nFea_Hf, //const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

					nHyperplaneWithMaxScaProdArrf, //const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

					fX_Arrf, //const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
					fZ_Fin_Arrf, //const float fZ_Fin_Arrf[], //[nDim_Hf]

					fU_Arrf, //const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

					nHyperplaneSelectedf); // int &nHyperplaneSelectedf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n An error in 'Selecting_Hyperplanes_For_AllFeas_OfNonlinearSpace' for iFea_Hf = %d", iFea_Hf);
			fprintf(fout, "\n\n An error in 'Selecting_Hyperplanes_For_AllFeas_OfNonlinearSpace' for iFea_Hf = %d", iFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		nHyperplaneSelected_For_Z_Arrf[iFea_Hf] = nHyperplaneSelectedf;
	} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
} //Selecting_Hyperplanes_For_AllFeas_OfNonlinearSpace(...
////////////////////////////////////////////////////////////////////////////////////////////////

void LossEpsilon(
	const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space

	const int nDim_Hf, // dimension of nonlinear space
	
	const int nHyperplaneSelectedf, //< nKf

	const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
	const float fZ_Fin_Arrf[], //[nDim_Hf]

	const float fU_ForAFea_H_Arrf[], // [nDim_D_SelecFeas_WithConstf]

	const float fEpsilonf,

	float &fScalar_Prod_OfUij_and_Xf,
	float &fLossEpsf)
{
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);

	float	
		fZf = fZ_Fin_Arrf[nHyperplaneSelectedf],
		fDiff,
		fScalar_Prodf;
//////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'LossEpsilon': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	Scalar_Product(
		nDim_D_SelecFeas_WithConstf, //const int nDimf,

		fX_Arrf, //const float fFeas_Arr_1f[],

		fU_ForAFea_H_Arrf, //const float fFeas_Arr_2f[],

		fScalar_Prod_OfUij_and_Xf); // float &fScalar_Prodf);

	if (fScalar_Prod_OfUij_and_Xf >= fZf)
	{
		fDiff = fScalar_Prod_OfUij_and_Xf - fZf;
	}//if ( (float)(nYtf)*fScalar_Prodf >= 1.0)
	else
	{
		fDiff = fZf - fScalar_Prod_OfUij_and_Xf;
	}//else
/////////////////////////////////////////////
	if (fDiff <= fEpsilonf)
	{
		fLossEpsf = 0.0;
	}//
	else
	{
		fLossEpsf = fDiff - fEpsilonf;
	}//else

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n 'LossEpsilon': fScalar_Prod_OfUij_and_Xf = %E, fDiff = %E, fLossEpsf = %E, fEpsilonf = %E", fScalar_Prod_OfUij_and_Xf, fDiff, fLossEpsf, fEpsilonf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

}// void LossEpsilon(...
////////////////////////////////////////////////////////////////////////////////////////////////

// i == nFea_Hf, j == nHyperplanef
int Updating_Uij( 

	const int nDim_SelecFeas_WithConstf, // <= dimension of the original space

	const int nDim_Hf, // dimension of nonlinear space

	const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

	const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf
	const int nHyperplanef, //the number of a specific hyperplane of the specific nonlinear fea; < nKf

	//const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

	const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
	const float fZ_Fin_Arrf[], //[nDim_Hf]

	const int nHyperplaneSelected_For_Z_Arrf[], //[nDim_Hf]

	const float fEpsilonf,
	const float fCrf,

	///////////////////////////////////////////////////////
	float fU_Arrf[]) // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes
{
/*
	int StDev_Of_A_FloatVector(
		const int nDimf,
		const float fFeas_InitArrf[],

		float &StDev_Of_A_FloatVectorf);
*/

	int NormEuclidean_Of_A_FloatVector(
		const int nDimf,
		const float fFeas_Arrf[],

		float &fNormEuclid_Of_A_Vectorf);

	void LossEpsilon(
		const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space

		const int nDim_Hf, // dimension of nonlinear space

		const int nHyperplaneSelectedf, //< nKf

		const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
		const float fZ_Fin_Arrf[], //[nDim_Hf]

		const float fU_ForAFea_H_Arrf[], // [nDim_D_WithConst]

		const float fEpsilonf,

		float &fScalar_Prod_OfUij_and_Xf,
		float &fLossEpsf);

	int
		nResf,
		nIndexf,
		nIndexMaxf = (nDim_SelecFeas_WithConstf * nDim_Hf * nKf) - 1,

		nSignf,
		nHyperplaneSelected_For_Zf, // = nHyperplaneSelected_For_Z_Arrf[nFea_Hf], // < nKf

		nTempf,

		iFeaf;

	float
		fRatio_OfLossAndSquaredNormf,

		fZf, // = fZ_Fin_Arrf[nHyperplaneSelected_For_Zf],

		fScalar_Prod_OfUij_and_Xf,
		fLossEpsf,

		fChangef,
		fU_Prevf,

		fNormEuclid_Of_An_X_Vectorf,
		//fU_ForAFea_H_Arrf[nDim_D_WithConst],

		fTauf;

	float* fU_ForAFea_H_Arrf = new float[nDim_SelecFeas_WithConstf];
	if (fU_ForAFea_H_Arrf == NULL)
	{
		printf("\n\n An error in 'Updating_Uij': fU_ForAFea_H_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Updating_Uij': fU_ForAFea_H_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fU_ForAFea_H_Arrf == NULL)

	if (nHyperplanef < 0 || nHyperplanef >= nKf)
	{
		printf("\n\n An error in 'Updating_Uij': nHyperplanef = %d >= nKf = %d", nHyperplanef, nKf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Updating_Uij': nHyperplanef = %d >= nKf = %d", nHyperplanef, nKf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		delete[] fU_ForAFea_H_Arrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nHyperplanef < 0 || nHyperplanef >= nKf)

////////////////////////////////////////////////////////////////////////////////////////////
	if (nFea_Hf < 0 || nFea_Hf >= nDim_Hf)
	{
		printf("\n\n An error in 'Updating_Uij': nFea_Hf = %d >= nDim_Hf = %d", nFea_Hf, nDim_Hf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Updating_Uij': nFea_Hf = %d >= nDim_Hf = %d", nFea_Hf, nDim_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		delete[] fU_ForAFea_H_Arrf;
		return UNSUCCESSFUL_RETURN;
	} // if (nFea_Hf < 0 || nFea_Hf >= nDim_Hf)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Updating_Uij': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, nFea_Hf = %d, nHyperplanef = %d", 
		iVec_Train_Glob, nY_Train_Actual_Glob, nFea_Hf, nHyperplanef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	///////////////////////////////////////////////
	nResf = NormEuclidean_Of_A_FloatVector(
		nDim_SelecFeas_WithConstf, //const int nDimf,
		fX_Arrf, //const float fFeas_InitArrf[],

		fNormEuclid_Of_An_X_Vectorf); // float &fNormOfAVectorf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Updating_Uij' by 'StDev_Of_A_FloatVector' ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'Updating_Uij' by 'StDev_Of_A_FloatVector' ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		delete[] fU_ForAFea_H_Arrf;
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	if (fNormEuclid_Of_An_X_Vectorf < eps)
	{
		printf("\n\n An error in 'Updating_Uij': fNormEuclid_Of_An_X_Vectorf = %E", fNormEuclid_Of_An_X_Vectorf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'Updating_Uij': fNormEuclid_Of_An_X_Vectorf = %E", fNormEuclid_Of_An_X_Vectorf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		delete[] fU_ForAFea_H_Arrf;
		return UNSUCCESSFUL_RETURN;
	}//if (fNormEuclid_Of_An_X_Vectorf < eps)

	///////////////////////////////////////////////////////////////
	nHyperplaneSelected_For_Zf = nHyperplaneSelected_For_Z_Arrf[nFea_Hf]; // < nKf

	if (nHyperplaneSelected_For_Zf < 0 || nHyperplaneSelected_For_Zf >= nKf)
	{
		printf("\n\n An error in 'Updating_Uij': nHyperplaneSelected_For_Zf = %d >= nKf = %d", nHyperplaneSelected_For_Zf, nKf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Updating_Uij': nHyperplaneSelected_For_Zf = %d >= nKf = %d", nHyperplaneSelected_For_Zf, nKf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		delete[] fU_ForAFea_H_Arrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nHyperplaneSelected_For_Zf < 0 || nHyperplaneSelected_For_Zf >= nKf)

	if (nHyperplaneSelected_For_Zf != nHyperplanef)
	{
		printf("\n\n An error in 'Updating_Uij': nHyperplaneSelected_For_Zf = %d != nHyperplanef = %d", nHyperplaneSelected_For_Zf, nHyperplanef);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Updating_Uij': nHyperplaneSelected_For_Zf = %d != nHyperplanef = %d", nHyperplaneSelected_For_Zf, nHyperplanef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		delete[] fU_ForAFea_H_Arrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nHyperplaneSelected_For_Zf != nHyperplanef)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n 'Updating_Uij': fNormEuclid_Of_An_X_Vectorf = %E, nHyperplaneSelected_For_Zf = %d, nFea_Hf = %d", fNormEuclid_Of_An_X_Vectorf, nHyperplaneSelected_For_Zf, nFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////
	fZf = fZ_Fin_Arrf[nHyperplaneSelected_For_Zf];

	//////////////////////////////////////////////////////////////////////////////////////
	nTempf = (nFea_Hf*nDim_SelecFeas_WithConstf) + (nHyperplanef*nDim_SelecFeas_WithConstf*nDim_Hf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n'Updating_Uij': nFea_Hf = %d, nHyperplanef = %d, fZf = %E, nTempf = %d",nFea_Hf, nHyperplanef, fZf, nTempf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
	{
		//nIndexf = (nDim_D_SelecFeas_WithConstf*nDim_Hf*nHyperplaneSelected_For_Zf) + (nDim_D_SelecFeas_WithConstf*nFea_Hf) + iFeaf;
//	nIndexf = iFeaf + nProd_nFea_Hf_nDim_D_SelecFeas_WithConstf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf;

		nIndexf = nTempf + iFeaf;

		if (nIndexf > nIndexMaxf)
		{
			printf("\n\n An error in 'Updating_Uij': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n An error in 'Updating_Uij': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			delete[] fU_ForAFea_H_Arrf;
			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

		fU_ForAFea_H_Arrf[iFeaf] = fU_Arrf[nIndexf];

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n'Updating_Uij': fU_ForAFea_H_Arrf[%d] = %E = fU_Arrf[nIndexf], nIndexf = %d", iFeaf, fU_ForAFea_H_Arrf[iFeaf], nIndexf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

/////////////////////////////
	LossEpsilon(
		nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space

		nDim_Hf, //const int nDim_Hf, // dimension of nonlinear space

		nHyperplaneSelected_For_Zf, //const int nHyperplaneSelected_For_Zf, //< nKf

		fX_Arrf, //const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
		fZ_Fin_Arrf, //const float fZ_Fin_Arrf[], //[nDim_Hf]

		fU_ForAFea_H_Arrf, //const float fU_ForAFea_H_Arrf[], // [nDim_D_WithConst]

		fEpsilonf, //const float fEpsilonf,

		fScalar_Prod_OfUij_and_Xf, //float &fScalar_Prod_OfUij_and_Xf,
		fLossEpsf); // float &fLossEpsf);

	///////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n'Updating_Uij' (after LossEpsilon): fLossEpsf = %E, fScalar_Prod_OfUij_and_Xf = %E, fNormEuclid_Of_An_X_Vectorf = %E", 
		fLossEpsf, fScalar_Prod_OfUij_and_Xf, fNormEuclid_Of_An_X_Vectorf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	fRatio_OfLossAndSquaredNormf = fLossEpsf / (fNormEuclid_Of_An_X_Vectorf);

	if (fRatio_OfLossAndSquaredNormf < fCrf)
	{
		fTauf = fRatio_OfLossAndSquaredNormf;
	}//if (fRatio_OfLossAndSquaredNormf < fCf)
	else
	{
		fTauf = fCrf;
	}//else
//////////////////////////////////////////
	if (fZf >= fScalar_Prod_OfUij_and_Xf)
	{
		nSignf = 1;
	} // if (fZf >= fScalar_Prod_OfUij_and_Xf)
	else
	{
		nSignf = -1;
	}//else

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n'Updating_Uij': fRatio_OfLossAndSquaredNormf = %E, fCrf = %E, fTauf = %E, nSignf = %d", fRatio_OfLossAndSquaredNormf, fCrf, fTauf, nSignf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

////////////////////////////////////////////////////
	nTempf = (nFea_Hf*nDim_SelecFeas_WithConstf) + (nHyperplanef*nDim_SelecFeas_WithConstf*nDim_Hf);
	for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
	{
		//nIndexf = (nDim_SelecFeas_WithConstf*nDim_Hf*nHyperplaneSelected_For_Zf) + (nDim_SelecFeas_WithConstf*nFea_Hf) + iFeaf;
		nIndexf = nTempf + iFeaf;

		fChangef = nSignf * fTauf*fX_Arrf[iFeaf];

		fU_Prevf = fU_Arrf[nIndexf];

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n Before updating: fU_Arrf[nIndexf] = fU_Prevf = %E, nSignf = %d, fTauf = %E, iFeaf = %d, nIndexf = %d",
			fU_Arrf[nIndexf], nSignf, fTauf, iFeaf, nIndexf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		fU_Arrf[nIndexf] = fU_Prevf + fChangef;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n After updating: a new fU_Arrf[nIndexf] = %E, fU_Prevf = %E, fX_Arrf[%d] = %E, fChangef = %E", 
				fU_Arrf[nIndexf], fU_Prevf, iFeaf,fX_Arrf[iFeaf],fChangef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)

	delete[] fU_ForAFea_H_Arrf;
	return SUCCESSFUL_RETURN;
}// int Updating_Uij(...
//////////////////////////////////////////////////////////////////////////////////////////////

int Updating_U_ForSelectedHyperplanes_Arr(

	const int nDim_SelecFeas_WithConstf, // <= dimension of the original space

	const int nDim_Hf, // dimension of nonlinear space

	const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

	const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
	const float fZ_Fin_Arrf[], //[nDim_Hf]

	const int nHyperplaneSelected_For_Z_Arrf[], //[nDim_Hf]

	const float fEpsilonf,
	const float fCrf,

	///////////////////////////////////////////////////////
	float fU_Arrf[]) // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes
{
	// i == nFea_Hf, j == nHyperplanef
	int Updating_Uij(

		const int nDim_SelecFeas_WithConstf, // <= dimension of the original space

		const int nDim_Hf, // dimension of nonlinear space

		const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

		const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf
		const int nHyperplanef, //the number of a specific hyperplane of the specific nonlinear fea; < nKf

		//const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

		const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
		const float fZ_Fin_Arrf[], //[nDim_Hf]

		const int nHyperplaneSelected_For_Z_Arrf[], //[nDim_Hf]

		const float fEpsilonf,
		const float fCrf,

		///////////////////////////////////////////////////////
		float fU_Arrf[]); // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

	int
		nResf,
		iFea_Hf, //nonlinear
		//iFea_Hyperplanef;
		nFea_Hyperplanef;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Updating_U_ForSelectedHyperplanes_Arr': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		//for (iFea_Hyperplanef = 0; iFea_Hyperplanef < nKf; iFea_Hyperplanef++)
		nFea_Hyperplanef = nHyperplaneSelected_For_Z_Arrf[iFea_Hf];

		if (nFea_Hyperplanef < 0 || nFea_Hyperplanef > nKf - 1)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Updating_U_ForSelectedHyperplanes_Arr': nFea_Hyperplanef = %d, nKf - 1 = %d", nFea_Hyperplanef, nKf - 1);
			fprintf(fout, "\n\n An error in 'Updating_Uij': nFea_Hyperplanef = %d, nKf - 1 = %d", nFea_Hyperplanef, nKf - 1);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nFea_Hyperplanef < 0 || nFea_Hyperplanef > nKf - 1)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 'Updating_U_ForSelectedHyperplanes_Arr': iFea_Hf = %d, nFea_Hyperplanef = %d", iFea_Hf, nFea_Hyperplanef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nResf = Updating_Uij(

				nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space

				nDim_Hf, //const int nDim_Hf, // dimension of nonlinear space

				nKf, //const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

				iFea_Hf, //const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf
				nFea_Hyperplanef, //const int nHyperplanef, //the number of a specific hyperplane of the specific nonlinear fea; < nKf

				//const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

				fX_Arrf, //const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
				fZ_Fin_Arrf, //const float fZ_Fin_Arrf[], //[nDim_Hf]

				nHyperplaneSelected_For_Z_Arrf, //const int nHyperplaneSelected_For_Z_Arrf[], //[nDim_Hf]

				fEpsilonf, //const float fEpsilonf,
				fCrf, //const float fCrf,

				///////////////////////////////////////////////////////
				fU_Arrf); // float fU_Arrf[]); // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Updating_U_ForSelectedHyperplanes_Arr': iFea_Hf = %d, nFea_Hyperplanef = %d", iFea_Hf, nFea_Hyperplanef);
				fprintf(fout, "\n\n An error in 'Updating_Uij': iFea_Hf = %d, nFea_Hyperplanef = %d", iFea_Hf, nFea_Hyperplanef);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nResf == UNSUCCESSFUL_RETURN)

	} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
} // int Updating_U_ForSelectedHyperplanes_Arr(...

//////////////////////////////////////////////////////////////////////////////////////////////
void Scalar_Product(
	const int nDimf,

	const float fFeas_Arr_1f[],

	const float fFeas_Arr_2f[],
	float &fScalar_Prodf)
{
	int
		iFeaf;

	fScalar_Prodf = 0.0;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fScalar_Prodf += fFeas_Arr_1f[iFeaf] * fFeas_Arr_2f[iFeaf];

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

}// void Scalar_Product(...
/////////////////////////////////////////////////////////////////////////////////////////////

int OneFea_OfNonlinearSpace(
	const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

	const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]

	const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes
	
	int &nHyperplaneWithMaxScaProdf,
	float &fZf)
{
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);

	int
		iFeaf,

		nIndexf,

		nIndexMaxf = nDim_D_SelecFeas_WithConstf * nDim_Hf * nKf - 1,

		nProd_nFea_Hf_nDim_D_SelecFeas_WithConstf = nFea_Hf*nDim_D_SelecFeas_WithConstf,

		nProd_nDim_SelecFeas_WithConstf_nDim_Hf = nDim_D_SelecFeas_WithConstf * nDim_Hf,

		nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf,

		iHyperplanef;

	float
		fSumTempf = 0.0,
		fProdTempf,

		//fX_WithConstArrf[nDim_D_WithConst],
		fScalar_Prodf;
		//fU_ForAFea_H_Arrf[nDim_D_WithConst];

	float* fU_ForAFea_H_Arrf = new float[nDim_D_SelecFeas_WithConstf];
	if (fU_ForAFea_H_Arrf == NULL)
	{
		printf("\n\n An error in 'OneFea_OfNonlinearSpace': fU_ForAFea_H_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'OneFea_OfNonlinearSpace': fU_ForAFea_H_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fU_ForAFea_H_Arrf == NULL)

	nHyperplaneWithMaxScaProdf = -1; //initially only
	fZf = -fLarge;

	if (nFea_Hf < 0 || nFea_Hf >= nDim_Hf)
	{
		printf("\n\n An error in 'OneFea_OfNonlinearSpace': nFea_Hf = %d >= nDim_Hf = %d", nFea_Hf, nDim_Hf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'OneFea_OfNonlinearSpace': nFea_Hf = %d >= nDim_Hf = %d", nFea_Hf, nDim_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		delete[] fU_ForAFea_H_Arrf;
		return UNSUCCESSFUL_RETURN;
	} // if (nFea_Hf < 0 || nFea_Hf >= nDim_Hf)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'OneFea_OfNonlinearSpace': nFea_Hf = %d", nFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'OneFea_OfNonlinearSpace': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	//nFea_Hf is fixed
	for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
	{
		nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;

		for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
		{
	
//			nIndexf = iFeaf + (nFea_Hf*nDim_D_SelecFeas_WithConstf) + (nDim_D_SelecFeas_WithConstf*nDim_Hf*iHyperplanef);
			nIndexf = iFeaf + nProd_nFea_Hf_nDim_D_SelecFeas_WithConstf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf;

			if (nIndexf > nIndexMaxf)
			{
				printf("\n\n An error in 'OneFea_OfNonlinearSpace': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

	#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n An error in 'OneFea_OfNonlinearSpace': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
	#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar();	exit(1);

				delete[] fU_ForAFea_H_Arrf;

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexf > nIndexMaxf)

		fU_ForAFea_H_Arrf[iFeaf] = fU_Arrf[nIndexf];

		fProdTempf = fU_ForAFea_H_Arrf[iFeaf] * fX_Arrf[iFeaf];
		fSumTempf += fProdTempf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 'OneFea_OfNonlinearSpace': iFeaf = %d, nIndexf = %d, iHyperplanef = %d, nFea_Hf = %d", 
			 iFeaf, nIndexf,iHyperplanef, nFea_Hf);

		fprintf(fout, "\n fU_ForAFea_H_Arrf[%d] = %E, fX_Arrf[%d] = %E, fProdTempf = %E, fSumTempf = %E",iFeaf, fU_ForAFea_H_Arrf[iFeaf], iFeaf, fX_Arrf[iFeaf], fProdTempf, fSumTempf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		}//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)

		Scalar_Product(
			nDim_D_SelecFeas_WithConstf, //const int nDimf,

			fX_Arrf, //const float fFeas_Arr_1f[],

			fU_ForAFea_H_Arrf, //const float fFeas_Arr_2f[],
			fScalar_Prodf); // float &fScalar_Prodf);

		if (fScalar_Prodf > fZf)
		{
			nHyperplaneWithMaxScaProdf = iHyperplanef;
			fZf = fScalar_Prodf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'OneFea_OfNonlinearSpace': a new fZf = %E, iHyperplanef = %d, nFea_Hf = %d",
				fZf, nIndexf, iHyperplanef, nFea_Hf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} // if (fScalar_Prodf > fZf)

	} //for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)

	delete[] fU_ForAFea_H_Arrf;

	return SUCCESSFUL_RETURN;
}//int OneFea_OfNonlinearSpace(
///////////////////////////////////////////////////////////////////////////////////////////////

int All_Feas_OfNonlinearSpace(
	const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space == 5

	const int nKf, //nNumOfHyperplanes == 3

	const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]// == 4

	const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 3 // the number of hyperplanes

	int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]

	float fZ_Arrf[]) //[nDim_H]

{
	int OneFea_OfNonlinearSpace(
		const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space == 4
		const int nDim_Hf, //dimension of the nonlinear/transformed space == 5

		const int nKf, //nNumOfHyperplanes == 3
		const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

		const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]

		const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 3 // the number of hyperplanes

		int &nHyperplaneWithMaxScaProdf,

		float &fZf);

	int
		nResf,
		nHyperplaneWithMaxScaProdf,
		iFea_Hf;

	float
		fZf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n 'All_Fefs_OfNonlinearSpace':");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'All_Fefs_OfNonlinearSpace': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nResf = OneFea_OfNonlinearSpace(
			nDim_D_SelecFeas_WithConstf, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
			nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

			nKf, //const int nKf, //nNumOfHyperplanes
			iFea_Hf, //const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

			fX_Arrf, //const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]

			fU_Arrf, //const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 3 // the number of hyperplanes

			nHyperplaneWithMaxScaProdf, //int &nHyperplaneWithMaxScaProdf,
			fZf); // float &fZf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n An error in 'All_Fefs_OfNonlinearSpace' for iFea_Hf = %d", iFea_Hf);
			fprintf(fout, "\n\n An error in 'All_Fefs_OfNonlinearSpace' for iFea_Hf = %d", iFea_Hf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nHyperplaneWithMaxScaProdf == -1)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'All_Fefs_OfNonlinearSpace' for iFea_Hf = %d, nHyperplaneWithMaxScaProdf == -1", iFea_Hf);
			fprintf(fout, "\n\n An error in 'All_Fefs_OfNonlinearSpace' for iFea_Hf = %d, nHyperplaneWithMaxScaProdf == -1", iFea_Hf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nHyperplaneWithMaxScaProdf == -1)

		nHyperplaneWithMaxScaProdArrf[iFea_Hf] = nHyperplaneWithMaxScaProdf;

		fZ_Arrf[iFea_Hf] = fZf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n 'All_Fefs_OfNonlinearSpace': fZ_Arrf[%d] = %E, nHyperplaneWithMaxScaProdArrf[%d] = %d", 
			iFea_Hf, fZ_Arrf[iFea_Hf], iFea_Hf, nHyperplaneWithMaxScaProdArrf[iFea_Hf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
}//int All_Feas_OfNonlinearSpace(...

///////////////////////////////////////////////////////////////////////////////////////////
int Selection_Of_RandFloat_2DimArr_From_2DimFloatArr(
	const int nDimf,
	const int nVecInitf,

	const int nVecSelecf, // < nVecInitf
	const float fFeaAll_InitArrf[],

	float fFeaSelecArr[])
{
	int
		nRanSelecf, // <= nVecSelecf
		nProdCurf,
		iFeaf,
		iVecf;

	for (iVecf = 0; iVecf < nVecSelecf; iVecf++)
	{
		nProdCurf = iVecf * nDimf;
		nRanSelecf = (int)(nVecInitf*(float)(rand()) / (float)(RAND_MAX));

		if (nRanSelecf == nVecInitf)
		{
			nRanSelecf = nVecInitf - 1;
		} //if (nRanSelecf == nVecInitf)
		else if (nRanSelecf > nVecInitf)
		{
			printf("\n\nAn error in 'Selection_Of_RandFloat_2DimArr_From_2DimFloatArr': nRanSelecf = %d > nVecInitf = %d",
				nRanSelecf, nVecInitf);
			fprintf(fout, "\n\nAn error in 'Selection_Of_RandFloat_2DimArr_From_2DimFloatArr': nRanSelecf = %d > nVecInitf = %d",
				nRanSelecf, nVecInitf);

			getchar();	exit(1);
		} // else if (nRanSelecf > nVecInitf)

		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{
			fFeaSelecArr[iFeaf + nProdCurf] = fFeaAll_InitArrf[iFeaf + (nRanSelecf*nDimf)];
		} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	} // for (iVecf = 0; iVecf < nVecSelecf; iVecf++)

	return SUCCESSFUL_RETURN;
} //int Selection_Of_RandFloat_2DimArr_From_2DimFloatArr(...
///////////////////////////////////////////

int Shuffle_and_Select_2Dim_FloatArr_Of_2_Int(
	const int nDimf,
	const int nNumOfRandSelecCouplesf,
	const int nNumOfRandInitializingLoopsMaxf,

	int nPositOf1stIntArrf[],  //nNumOfRandSelecCouplesf
	int nPositOf2ndIntArrf[])
{
	int
		nDimLargerf = 3 * nDimf,

		nCounterOfValidCouplesf = 0,

		nRanSelecCurf,
		nRanSelec1stf, // <= nVecSelecf
		nRanSelec2ndf, // <= nVecSelecf

		nNumOfRandInitializingLoopsf = 0,
		i1;

	printf("\n\nInside'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int' 1"); fflush(stdout);

	int *nRanSelec1stArrf = new int[nDimLargerf];
	int *nRanSelec2ndArrf = new int[nDimLargerf];

	printf("\n\nInside'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int' 2"); fflush(stdout);

MarkInitializing: nNumOfRandInitializingLoopsf += 1;

	for (i1 = 0; i1 < nDimLargerf; i1++)
	{
		nRanSelecCurf = (int)(nDimf*(float)(rand()) / (float)(RAND_MAX));

		if (nRanSelecCurf == nDimf)
		{
			nRanSelecCurf = nDimf - 1;
		} //if (nRanSelecCurf == nDimf)
		else if (nRanSelecCurf > nDimf || nRanSelecCurf < 0)
		{
			printf("\n\nAn error in 'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int': nRanSelecCurf = %d > nDimf = %d, ...",
				nRanSelecCurf, nDimf);
			fprintf(fout, "\n\nAn error in 'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int': nRanSelecCurf = %d > nDimf = %d, ...",
				nRanSelecCurf, nDimf);

			getchar();	exit(1);
		} // else if (nRanSelecCurf > nDimf || ...)

		nRanSelec1stArrf[i1] = nRanSelecCurf;
	} //for (i1 = 0; i1 < nDimLargerf; i1++)

	for (i1 = 0; i1 < nDimLargerf; i1++)
	{
		nRanSelecCurf = (int)(nDimf*(float)(rand()) / (float)(RAND_MAX));

		if (nRanSelecCurf == nDimf)
		{
			nRanSelecCurf = nDimf - 1;
		} //if (nRanSelecCurf == nDimf)
		else if (nRanSelecCurf > nDimf || nRanSelecCurf < 0)
		{
			printf("\n\nAn error in 'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int': nRanSelecCurf = %d > nDimf = %d, ...",
				nRanSelecCurf, nDimf);
			fprintf(fout, "\n\nAn error in 'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int': nRanSelecCurf = %d > nDimf = %d, ...",
				nRanSelecCurf, nDimf);

			getchar();	exit(1);
		} // else if (nRanSelecCurf > nDimf || ...)

		nRanSelec2ndArrf[i1] = nRanSelecCurf;
	} //for (i1 = 0; i1 < nDimLargerf; i1++)

	printf("\n\nInside'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int' 3"); fflush(stdout);

	//It is assumed that  'nRanSelec1stArrf[i1] < nRanSelec2ndArrf[i1']
	for (i1 = 0; i1 < nDimLargerf; i1++)
	{
		nRanSelec1stf = nRanSelec1stArrf[i1];
		nRanSelec2ndf = nRanSelec2ndArrf[i1];

		if (nRanSelec1stf != nRanSelec2ndf)
		{
			nCounterOfValidCouplesf += 1;
			if (nRanSelec1stf > nRanSelec2ndf) //must be vice versa
			{
				nPositOf1stIntArrf[nCounterOfValidCouplesf - 1] = nRanSelec2ndf;
				nPositOf2ndIntArrf[nCounterOfValidCouplesf - 1] = nRanSelec1stf;
			} //if (nRanSelec1stf > nRanSelec2ndf) //must be vice versa
			else //the order is preserved
			{
				nPositOf1stIntArrf[nCounterOfValidCouplesf - 1] = nRanSelec1stf;
				nPositOf2ndIntArrf[nCounterOfValidCouplesf - 1] = nRanSelec2ndf;
			} //the order is preserved

			if (nCounterOfValidCouplesf == nNumOfRandSelecCouplesf)
				break;
		} //if (nRanSelec1stf != nRanSelec2ndf)

	} // for (i1 = 0; i1 < nDimLargerf; i1++)

	printf("\n\nInside'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int' 4, nCounterOfValidCouplesf = %d", nCounterOfValidCouplesf); fflush(stdout);

	if (nCounterOfValidCouplesf < nNumOfRandSelecCouplesf)
	{
		if (nNumOfRandInitializingLoopsf < nNumOfRandInitializingLoopsMaxf)
			goto MarkInitializing;
		else
		{
			printf("\n\nAn error in 'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int': nNumOfRandInitializingLoopsf = %d >= nNumOfRandInitializingLoopsMaxf = %d",
				nNumOfRandInitializingLoopsf, nNumOfRandInitializingLoopsMaxf);
			fprintf(fout, "\n\nAn error in 'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int': nNumOfRandInitializingLoopsf = %d >= nNumOfRandInitializingLoopsMaxf = %d",
				nNumOfRandInitializingLoopsf, nNumOfRandInitializingLoopsMaxf);

			getchar();	exit(1);
		} //else

	}// if (nCounterOfValidCouplesf < nNumOfRandSelecCouplesf)
	else if (nCounterOfValidCouplesf > nNumOfRandSelecCouplesf)
	{
		printf("\n\nAn error in 'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int': nCounterOfValidCouplesf = %d > nNumOfRandSelecCouplesf = %d, nNumOfRandInitializingLoopsf = %d",
			nCounterOfValidCouplesf, nNumOfRandSelecCouplesf, nNumOfRandInitializingLoopsf);
		fprintf(fout, "\n\nAn error in 'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int': nCounterOfValidCouplesf = %d > nNumOfRandSelecCouplesf = %d, nNumOfRandInitializingLoopsf = %d",
			nCounterOfValidCouplesf, nNumOfRandSelecCouplesf, nNumOfRandInitializingLoopsf);

		getchar();	exit(1);
	} //else if (nCounterOfValidCouplesf > nNumOfRandSelecCouplesf)

	printf("\n\nInside'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int' 5, nCounterOfValidCouplesf = %d", nCounterOfValidCouplesf); fflush(stdout);

	delete[] nRanSelec1stArrf;
	delete[] nRanSelec2ndArrf;

	printf("\n\nInside'Shuffle_and_Select_2Dim_FloatArr_Of_2_Int' 6, nCounterOfValidCouplesf = %d", nCounterOfValidCouplesf); fflush(stdout);

	return 1;
} //int Shuffle_and_Select_2Dim_FloatArr_Of_2_Int(...

///////////////////////////////////////////////////////////////////////////////
void Initialization_Of_OneRandFloatVec_WithinARange(
	const int nDimForRandf,

	const float fMinf,
	const float fMaxf,

	float fRandArr[]) //[nDimForRandf]
{
	int
		iPositionf;

	float
		fRangef = fMaxf - fMinf, 

		fValueRandCurf;

	for (iPositionf = 0; iPositionf < nDimForRandf; iPositionf++)
	{
		fValueRandCurf = (float)(rand()) / (float)(RAND_MAX); // from 0.0 to 1.0

		fRandArr[iPositionf] = (fValueRandCurf* fRangef) + fMinf;
	} // for (iPositionf = 0; iPositionf < nDimForRandf; iPositionf++)

} // void Initialization_Of_OneRandFloatVec_WithinARange(...
//////////////////////////////////////////////////////////

void Initialization_Of_OneRand_IntVec_WithinARange(
	const int nDimForRandf,

	const int nMinf,
	const int nMaxf,

	int nRandArr[]) //[nDimForRandf]
{
	int
		iPositionf;

	float
		fRangef = (float)(nMaxf - nMinf),

		fValueRandCurf;

	for (iPositionf = 0; iPositionf < nDimForRandf; iPositionf++)
	{
		fValueRandCurf = (float)(rand()) / (float)(RAND_MAX); // from 0.0 to 1.0

		nRandArr[iPositionf] = (int)(fValueRandCurf* fRangef) + nMinf;
	} // for (iPositionf = 0; iPositionf < nDimForRandf; iPositionf++)

} // void Initialization_Of_OneRand_IntVec_WithinARange(...
///////////////////////////////////////////////////////////////////////////

void Initializing_W_Arr(
		const int nDim_Hf,
		const float fW_Init_Min_Globf,
		const float fW_Init_Max_Globf,

		float fW_Arrf[])
{
	void Initialization_Of_OneRandFloatVec_WithinARange(
		const int nDimForRandf,

		const float fMinf,
		const float fMaxf,

		float fRandArr[]); //[nDimForRandf]

	Initialization_Of_OneRandFloatVec_WithinARange(
		nDim_Hf, //const int nDimForRandf,

		fW_Init_Min_Globf, //const float fMinf,
		fW_Init_Max_Globf, //const float fMaxf,

		fW_Arrf); // float fRandArr[]); //[nDimForRandf]

}//void Initializing_W_Arr(...

/////////////////////////////////////////////////////////////////////////////////
void Initializing_U_Arr(
	const int nDim_Uf,
	const float fU_Init_Min_Globf,
	const float fU_Init_Max_Globf,

	float fU_Arrf[])
{
	void Initialization_Of_OneRandFloatVec_WithinARange(
		const int nDimForRandf,

		const float fMinf,
		const float fMaxf,

		float fRandArr[]); //[nDimForRandf]

	Initialization_Of_OneRandFloatVec_WithinARange(
		nDim_Uf, //const int nDimForRandf,

		fU_Init_Min_Globf, //const float fMinf,
		fU_Init_Max_Globf, //const float fMaxf,

		fU_Arrf); // float fRandArr[]); //[nDimForRandf]

}//void InitializingU_Arr(...

///////////////////////////////////////////////////////////////////////////////////////
int Normalizing_OneDim_FloatVector_To_ARange(
	const int nDimf,
	const float fFin_Minf,
	const float fFin_Maxf,

	const float fFeas_InitArrf[],

	float fFeas_NormArrf[])
{
	int
		iFeaf;
	float
		fDiffMax_Min_Finf = fFin_Maxf - fFin_Minf,

		fDiffMax_Min_Initf,

		fRatioOfDiff,

		fMinf = fLarge,
		fMaxf = -fLarge;

	if (fDiffMax_Min_Finf < eps)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Normalizing_OneDim_FloatVector_To_ARange': fFin_Maxf = %E - fFin_Minf = %E < eps = %E", fFin_Maxf, fFin_Minf, eps);
		fprintf(fout, "\n\n An error in 'Vec_Normalization': fFin_Maxf = %E - fFin_Minf = %E < eps = %E", fFin_Maxf, fFin_Minf, eps);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (fDiffMax_Min_Initf < eps)

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		if (fFeas_InitArrf[iFeaf] < fMinf)
			fMinf = fFeas_InitArrf[iFeaf];

		if (fFeas_InitArrf[iFeaf] > fMaxf)
			fMaxf = fFeas_InitArrf[iFeaf];

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	fDiffMax_Min_Initf = fMaxf - fMinf;

	if (fDiffMax_Min_Initf < eps)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Normalizing_OneDim_FloatVector_To_ARange': fMaxf = %E - fMinf = %E < eps = %E", fMaxf, fMinf, eps);
		fprintf(fout, "\n\n An error in 'Vec_Normalization': fMaxf = %E - fMinf = %E < eps = %E", fMaxf, fMinf, eps);
		#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (fDiffMax_Min_Initf < eps)

//to (0, fDiffMax_Min_Initf)
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeas_NormArrf[iFeaf] = fFeas_InitArrf[iFeaf] - fMinf;

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

//to (0, fDiffMax_Min_Finf)
	fRatioOfDiff = fDiffMax_Min_Finf / fDiffMax_Min_Initf;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeas_NormArrf[iFeaf] = fFeas_InitArrf[iFeaf]* fRatioOfDiff;

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)


//to (fFin_Minf, fFin_Maxf)

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeas_NormArrf[iFeaf] = fFeas_InitArrf[iFeaf] - fFin_Minf;

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;

} //int Normalizing_OneDim_FloatVector_To_ARange (...

/////////////////////////////////////////////////////////////////////////////////
int StDev_Of_A_FloatVector(
	const int nDimf,
	const float fFeas_InitArrf[],

	float &StDev_Of_A_FloatVectorf)
{
	int
		iFeaf;

	if (nDimf < 2 || nDimf > nLarge)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'StDev_Of_A_FloatVector': nDimf = %d", nDimf);
		fprintf(fout, "\n\n  An error in 'StDev_Of_A_FloatVector':  nDimf = %d", nDimf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nDimf < 2 || nDimf > nLarge)

////////////////////////////////////////////
	StDev_Of_A_FloatVectorf = 0.0;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		StDev_Of_A_FloatVectorf += fFeas_InitArrf[iFeaf] * fFeas_InitArrf[iFeaf];
	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	StDev_Of_A_FloatVectorf = sqrt(StDev_Of_A_FloatVectorf/ nDimf);

	if (StDev_Of_A_FloatVectorf < eps)
	{
		printf("\n\n A very small 'StDev_Of_A_FloatVectorf'  in 'StDev_Of_A_FloatVector': StDev_Of_A_FloatVectorf = %E", StDev_Of_A_FloatVectorf);
//#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  A very small 'StDev_Of_A_FloatVectorf'  in 'StDev_Of_A_FloatVector': StDev_Of_A_FloatVectorf = %E", StDev_Of_A_FloatVectorf);
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return (-2);
	} //if (StDev_Of_A_FloatVectorf < eps || StDev_Of_A_FloatVectorf > fLarge)
	else if (StDev_Of_A_FloatVectorf > fLarge)
	{
		printf("\n\n An error in 'StDev_Of_A_FloatVector': StDev_Of_A_FloatVectorf = %E", StDev_Of_A_FloatVectorf);
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'StDev_Of_A_FloatVector': StDev_Of_A_FloatVectorf = %E", StDev_Of_A_FloatVectorf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();	exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (StDev_Of_A_FloatVectorf < eps || StDev_Of_A_FloatVectorf > fLarge)

	else
		return SUCCESSFUL_RETURN;

} //int StDev_Of_A_FloatVector (...
////////////////////////////////////////////////////////////

int NormEuclidean_Of_A_FloatVector(
	const int nDimf,
	const float fFeas_Arrf[],

	float &fNormEuclid_Of_A_Vectorf)
{
	int
		iFeaf;

	if (nDimf < 2 || nDimf > nLarge)
	{
		printf("\n\n An error in 'NormEuclidean_Of_A_FloatVector': nDimf = %d", nDimf);
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'NormEuclidean_Of_A_FloatVector':  nDimf = %d", nDimf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nDimf < 2 || nDimf > nLarge)

////////////////////////////////////////////
	fNormEuclid_Of_A_Vectorf = 0.0;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fNormEuclid_Of_A_Vectorf += fFeas_Arrf[iFeaf] * fFeas_Arrf[iFeaf];
	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	if (fNormEuclid_Of_A_Vectorf > fLarge || fNormEuclid_Of_A_Vectorf < 0.0)
	{
		printf("\n\n An error in 'NormEuclidean_Of_A_FloatVector': fNormEuclid_Of_A_Vectorf = %E", fNormEuclid_Of_A_Vectorf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'NormEuclidean_Of_A_FloatVector':  fNormEuclid_Of_A_Vectorf = %E", fNormEuclid_Of_A_Vectorf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fNormEuclid_Of_A_Vectorf > fLarge || ...)

	return SUCCESSFUL_RETURN;
} //int NormEuclidean_Of_A_FloatVector (...
////////////////////////////////////////////////////////////////////////////////

int Normalizing_FloatVector_ByStDev(
	const int nDimf,
	const float fFeas_InitArrf[],
	
	float fFeasNormalized_Arrf[])
{
	int StDev_Of_A_FloatVector(
		const int nDimf,
		const float fFeas_InitArrf[],

		float &StDev_Of_A_FloatVectorf);

	int
		nResf,
		iFeaf;

	float 
		StDev_Of_A_FloatVectorf;

	nResf = StDev_Of_A_FloatVector(
		nDimf, //const int nDimf,
		fFeas_InitArrf, //const float fFeas_InitArrf[],

		StDev_Of_A_FloatVectorf); // float &StDev_Of_A_FloatVectorf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Normalizing_FloatVector_ByStDev': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, StDev_Of_A_FloatVectorf = %E", iVec_Train_Glob, nY_Train_Actual_Glob, StDev_Of_A_FloatVectorf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Normalizing_FloatVector_ByStDev' by 'StDev_Of_A_FloatVector' ");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'Normalizing_FloatVector_ByStDev' by 'StDev_Of_A_FloatVector' ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();	exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	if (nResf == -2)//all feas are zeros or constants
	{
		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{
			fFeasNormalized_Arrf[iFeaf] = 0.0; // fFeas_InitArrf[iFeaf] / StDev_Of_A_FloatVectorf;
		}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		return SUCCESSFUL_RETURN;
	} //if (nResf == -2)

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeasNormalized_Arrf[iFeaf] = fFeas_InitArrf[iFeaf]/ StDev_Of_A_FloatVectorf;

		if (fFeasNormalized_Arrf[iFeaf] < -fLarge || fFeasNormalized_Arrf[iFeaf] > fLarge)
		{
			printf("\n\n An error in 'Normalizing_FloatVector_ByStDev': fFeasNormalized_Arrf[iFeaf] =  %E < -fLarge || ..., iFeaf = %d", 
				fFeasNormalized_Arrf[iFeaf], iFeaf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Normalizing_FloatVector_ByStDev': fFeasNormalized_Arrf[iFeaf] =  %E < -fLarge || ..., iFeaf = %d", fFeasNormalized_Arrf[iFeaf], iFeaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			return UNSUCCESSFUL_RETURN;
		}//if (fFeasNormalized_Arrf[iFeaf] < -fLarge || fFeasNormalized_Arrf[iFeaf] > fLarge)

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;
} //int Normalizing_FloatVector_ByStDev (...

/////////////////////////////////////////////////////////////////////////////////
int Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
	const int nDimf,
		const int nNumOfVecsTotf,
	const int nNumOfOneVecf,

		const float fFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

		float fFeas_OneVec_Arrf[]) //[nDimf]
{
	int
		nIndexf,

		nIndexMaxf = (nDimf* nNumOfVecsTotf) - 1,
		nTempf,
		iFeaf;

	nTempf = nNumOfOneVecf * nDimf;

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nVecf = %d", nDimf, nNumOfVecsTotf, nVecf);
	fprintf(fout, "\n\n  'Extracting_A_FloatVec_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nNumOfOneVecf = %d, nTempf = %d", nDimf, nNumOfVecsTotf, nNumOfOneVecf, nTempf);
	fprintf(fout, "\n\n iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nIndexf = iFeaf + nTempf;
		if (nIndexf > nIndexMaxf)
		{
			printf("\n\n An error in 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

#ifndef COMMENT_OUT_ALL_PRINTS
		//if (nVecf < 4)
		{
		//	fprintf(fout, "\n 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs': nNumOfOneVecf = %d, iFeaf = %d, nIndexf = %d, fFeas_All_Arrf[nIndexf] = %E", nNumOfOneVecf, iFeaf,nIndexf, fFeas_All_Arrf[nIndexf]);
		}//if (nVecf < 4)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		fFeas_OneVec_Arrf[iFeaf] = fFeas_All_Arrf[nIndexf];
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;
}// int Extracting_A_FloatVec_From_2DimArrOf_AllVecs{...
///////////////////////////////////////////////////////

int Extracting_An_IntVec_From_2DimArrOf_AllVecs(
	const int nDimf,
	const int nNumOfVecsTotf,
	const int nNumOfOneVecf,

	const int nPosit_All_Arrf[], //[nDimf*nNumOfVecsTotf]

	int nPosit_OneVec_Arrf[]) //[nDimf]
{
	int
		nIndexf,

		nIndexMaxf = (nDimf* nNumOfVecsTotf) - 1,
		nTempf,
		iFeaf;

	nTempf = nNumOfOneVecf * nDimf;

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n 'Extracting_An_IntVec_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nVecf = %d", nDimf, nNumOfVecsTotf, nVecf);
	fprintf(fout, "\n\n  'Extracting_An_IntVec_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nNumOfOneVecf = %d, nTempf = %d", nDimf, nNumOfVecsTotf, nNumOfOneVecf, nTempf);
	fprintf(fout, "\n\n iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nIndexf = iFeaf + nTempf;
		if (nIndexf > nIndexMaxf)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Extracting_An_IntVec_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
			fprintf(fout, "\n\n  An error in 'Extracting_An_IntVec_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

#ifndef COMMENT_OUT_ALL_PRINTS
		//if (nVecf < 4)
		{
			//fprintf(fout, "\n 'Extracting_An_IntVec_From_2DimArrOf_AllVecs': nNumOfOneVecf = %d, iFeaf = %d, nIndexf = %d, fFeas_All_Arrf[nIndexf] = %E", nNumOfOneVecf, iFeaf, nIndexf, fFeas_All_Arrf[nIndexf]);
		}//if (nVecf < 4)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		//

		if (nPosit_All_Arrf[nIndexf] >= 0 && nPosit_All_Arrf[nIndexf] < nDim_DifEvo)
		{
			nPosit_OneVec_Arrf[iFeaf] = nPosit_All_Arrf[nIndexf];
		} //if (nPosit_All_Arrf[nIndexf] >= 0 && nPosit_All_Arrf[nIndexf] < nDim_DifEvo)
		else
		{
			nPosit_OneVec_Arrf[iFeaf] = FEA_DISACTIVATED;
		} //else

	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;
}// int Extracting_An_IntVec_From_2DimArrOf_AllVecs{...


//////////////////////////////////////////////////////////
int Writing_OneDimVec_To_2DimVec(
	const int nDimf,
	const int nNumOfVecsTotf,

	const int nVecf,

	const float fFeas_OneVec_Arrf[], //[nDim_D_WithConst]

	float fFeas_All_Arrf[]) //[nDimf*nVecTotf]
{

	int
		nIndexf,

		nIndexMaxf = (nDimf* nNumOfVecsTotf) - 1,
		nTempf,
		iFeaf;

	nTempf = nVecf * nDimf;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nIndexf = iFeaf + nTempf;
		if (nIndexf > nIndexMaxf)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Writing_OneDimVec_To_2DimVec': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
			fprintf(fout, "\n\n  An error in 'Writing_OneDimVec_To_2DimVec': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

		fFeas_All_Arrf[nIndexf] = fFeas_OneVec_Arrf[iFeaf];
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;
}// int Writing_OneDimVec_To_2DimVec{...

///////////////////////////////////////////////////
int PasAggMaxOut_Train(
	const int nNumOfItersOfTrainingTotf,

	//after shuffling
	const float fFeasSelec_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], to be normalized
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	const int nVecTrainf,
/////////////////////////

	const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

///////////////////////
	const float fAlphaf, // < 1.0
	const float fEpsilonf,
	const float fCrf,
	const float fCf,
///////////////////////////////////////////////////
		float fW_Arrf[],
		float fU_Arrf[], //[nDim_U_Glob],

	PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults)
{
	void Initializing_U_Arr(
		const int nDim_Uf,
		const float fU_Init_Min_Globf,
		const float fU_Init_Max_Globf,

		float fU_Arrf[]);

	void Initializing_W_Arr(
		const int nDim_Hf,
		const float fW_Init_Min_Globf,
		const float fW_Init_Max_Globf,

		float fW_Arrf[]);

	int Normalizing_FloatVector_ByStDev(
		const int nDimf,
		const float fFeas_InitArrf[],

		float fFeasNormalized_Arrf[]);

	int Normalizing_OneDim_FloatVector_To_ARange(
		const int nDimf,
		const float fFin_Minf,
		const float fFin_Maxf,

		const float fFeas_InitArrf[],

		float fFeas_NormArrf[]);

	int All_Feas_OfNonlinearSpace(
					const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
					const int nDim_Hf, //dimension of the nonlinear/transformed space

					const int nKf, //nNumOfHyperplanes

					const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]

					const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

		//////////////////////////////////////////
					int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]

					float fZ_Arrf[]); //[nDim_H]

//////////////////////////////////////////////////

	int OrthonormalizationOf_fU_Arrf(
		const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		float fU_Arrf[]); //nDim_Uf

	//////////////////////////////////////////////
		int Updating_W_Arr(
			const int nDim_Hf,

			const float fCf,
			const float fAlphaf, // < 1.0

			const float fLossf,

			const int nYtf, // 1 or -1

			const float fZ_Arrf[], //[nDim_Hf]

			const float fW_Init_Arrf[], //[nDim_Hf]

			float fW_Fin_Arrf[]); ////[nDim_Hf]

	int Updating_Z(
		const int nDim_Hf,

		const float fLossf,

		const int nYtf, // 1 or -1

		const float fW_Fin_Arrf[], //[nDim_Hf]

		const float fZ_Init_Arrf[], //[nDim_Hf]

		float fZ_Fin_Arrf[]);	//[nDim_Hf]

	int StDev_Of_A_FloatVector(
		const int nDimf,
		const float fFeas_InitArrf[],

		float &StDev_Of_A_FloatVectorf);

	int Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
		const int nDimf,
			const int nNumOfVecsTotf,

		const int nVecf,

		const float fFeas_All_Arrf[], //[nProdTrainTot]

		float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

	void Loss(
		const int nDim_Hf,
		const int nYtf, // 1 or -1 (not 0)

		const float fZ_Arrf[], //[nDim_Hf]

		const float fW_Arrf[], //[nDim_Hf]

		int &nY_Estimatedf,

		float &fLossf); 

	void Copying_Float_Arr1_To_Arr2(
		const int nDimf,
		const float fArr1f[], // [nDimf]
		float fArr2f[]); // [nDimf]

	int Print_fU_Arr(
		const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		const float fU_Arrf[]); //nDim_Uf

	void Print_A_FloatOneDim_Arr(
		const int nDimf, //

		const float fArrf[]); //nDimf

	int PasAggMaxOut_Test(
		const float fFea_WithConstTest_Arrf[], //[nProd_WithConstTestTot], 
		const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		//const int nDimf, == nDim_D_SelecFeas_WithConstf

		const int nVecTestf,

		/////////////////////////
		const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		const float fW_Train_Arrf[], //[nDim_Hf]
		const float fU_Train_Arrf[], //[nDim_U_Glob],
		///////////////////////////////////////////////////

		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);
/////////////////////////////////////////////////
	int
		nResf,
			//nHyperplaneWithMaxScaProdArrf[nDim_H], //[]

		//nHyperplaneSelected_For_Z_Arrf[nDim_H],

		nY_For_LossCurf,
		nY_Estimatedf,
			nY_Estimated_Arrf[nNumVecTrainTot],

		nNumOfPosit_Y_Totf = 0,
		nNumOfNegat_Y_Totf = 0,

		nNumOfCorrect_Y_Totf = 0,

		nNumOfPositCorrect_Y_Totf = 0,
		nNumOfNegatCorrect_Y_Totf = 0,

		nY_Actualf,

		iFeaf,

		iFea_Nonlinearf,
		iItersOfTrainingf,
		iVecf;

	float
		fScalar_Prodf,
		fLossf,

		fPercentageOfCorrectTotf = 0.0,
		fPercentageOfCorrect_Positf = 0.0,
		fPercentageOfCorrect_Negatf = 0.0;

			//fU_Arrf[nDim_U_Glob],
			//fW_Arrf[nDim_H],
			//fW_Fin_Arrf[nDim_H],
			
	//	fZ_Arrf[nDim_H],
		//fZ_NormalizedArrf[nDim_H],
		//fZ_Fin_Arrf[nDim_H],

		//fFea_OneVec_Train_Arrf[nDim_D_WithConst],

		//fFea_OneVec_TrainNormalized_Arrf[nDim_D_WithConst];

	float* fFea_OneVec_Train_Arrf = new float[nDim_SelecFeas_WithConstf];
	if (fFea_OneVec_Train_Arrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Train': fFea_OneVec_Train_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Train': fFea_OneVec_Train_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fFea_OneVec_Train_Arrf == NULL)
/////////////////////////////////

	float* fFea_OneVec_TrainNormalized_Arrf = new float[nDim_SelecFeas_WithConstf];
	if (fFea_OneVec_TrainNormalized_Arrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Train': fFea_OneVec_TrainNormalized_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Train': fFea_OneVec_TrainNormalized_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fFea_OneVec_Train_Arrf;

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fFea_OneVec_TrainNormalized_Arrf == NULL)

////////////////////////////////	
	int* nHyperplaneWithMaxScaProdArrf = new int[nDim_Hf];
	if (nHyperplaneWithMaxScaProdArrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Train': fZ_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Train': fZ_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] fFea_OneVec_Train_Arrf;
		delete[] fFea_OneVec_TrainNormalized_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nHyperplaneWithMaxScaProdArrf == NULL)

	float* fZ_Arrf = new float[nDim_Hf];
	if (fZ_Arrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Train': fZ_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Train': fZ_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] fFea_OneVec_Train_Arrf;
		delete[] fFea_OneVec_TrainNormalized_Arrf;
		delete[] nHyperplaneWithMaxScaProdArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fZ_Arrf == NULL)

	float* fZ_NormalizedArrf = new float[nDim_Hf];
	if (fZ_NormalizedArrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Train': fZ_NormalizedArrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Train': fZ_NormalizedArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);
		delete[] fFea_OneVec_Train_Arrf;
		delete[] fFea_OneVec_TrainNormalized_Arrf;

		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fZ_NormalizedArrf == NULL)

	float* fZ_Fin_Arrf = new float[nDim_Hf];
	if (fZ_Fin_Arrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Train': fZ_Fin_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Train': fZ_Fin_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);
		delete[] fFea_OneVec_Train_Arrf;
		delete[] fFea_OneVec_TrainNormalized_Arrf;

		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;
		delete[] fZ_NormalizedArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fZ_Fin_Arrf == NULL)

	float* fW_Fin_Arrf = new float[nDim_Hf];
	if (fW_Fin_Arrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Train': fW_Fin_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Train': fW_Fin_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);
		delete[] fFea_OneVec_Train_Arrf;
		delete[] fFea_OneVec_TrainNormalized_Arrf;

		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;
		delete[] fZ_NormalizedArrf;
		delete[] fZ_Fin_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fW_Fin_Arrf == NULL)

///////////////////////////
/*
	int
		nRandOutputf;

	printf("\n\n nSrandInit_Glob = %d, RAND_MAX = %d", nSrandInit_Glob, RAND_MAX);

	srand(nSrandInit_Glob);

	for (int iRandf = 0; iRandf < 5; iRandf++)
	{
		nRandOutputf = rand();
		printf("\n iRandf = %d, nRandOutputf = %d", iRandf, nRandOutputf);
	} //
	getchar();
*/
	Initializing_W_Arr(
		nDim_Hf, //const int nDim_Hf,
		fW_Init_Min_Glob, //const float fW_Init_Min_Globf,
		fW_Init_Max_Glob, //const float fW_Init_Max_Globf,

		fW_Arrf); // float fW_Arrf[]);

/////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'PasAggMaxOut_Train': init fW_Arrf[]");
	fprintf(fout, "\n\n  'PasAggMaxOut_Train': init fW_Arrf[]");

	Print_A_FloatOneDim_Arr(
		nDim_Hf, //const int nDimf, //

		fW_Arrf); // const float fArrf[]) //nDimf
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

///////////////////////
	Initializing_U_Arr(
		nDim_Uf, //const int nDim_Uf,
		fU_Init_Min_Glob, //const float fU_Init_Min_Globf, // -1.0
		fU_Init_Max_Glob, //const float fU_Init_Max_Globf, //+1.0

		fU_Arrf); // float fU_Arrf[]);

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'PasAggMaxOut_Train': fU_Arrf[] after initialization");
	fprintf(fout, "\n\n 'PasAggMaxOut_Train': fU_Arrf[] after initialization");
	
	nResf = Print_fU_Arr(
		nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes

//nDim_D_WithConst?
		nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		fU_Arrf); // float fU_Arrf[]); //nDim_Uf
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
////////////////////////////////////////////////////

#ifdef INITIAL_ORTHONORMALIOZATION
	nResf = OrthonormalizationOf_fU_Arrf(
		nDim_SelecFeas_WithConstf, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		fU_Arrf); // float fU_Arrf[]); //nDim_Uf

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'PasAggMaxOut_Train' by 'OrthonormalizationOf_fU_Arrf'");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'OrthonormalizationOf_fU_Arrf'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);
		delete[] fFea_OneVec_Train_Arrf;
		delete[] fFea_OneVec_TrainNormalized_Arrf;

		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;
		delete[] fZ_NormalizedArrf;
		delete[] fZ_Fin_Arrf;
		delete[] fW_Fin_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n After Verification of normality: please press any key to exit"); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#endif //#ifdef INITIAL_ORTHONORMALIOZATION

	//////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'PasAggMaxOut_Test' for training data and init fW_Arrf[] and fU_Arrf[], nVecTrainf = %d", nVecTrainf);
	fprintf(fout, "\n\n PasAggMaxOut_Test' for training data and init fW_Arrf[] and fU_Arrf[], nVecTrainf = %d", nVecTrainf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/////////////////////////////////////
	PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TrainResultsRightAfterInitf;
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n///////////////////////////////////////////////////////////");
	printf("\n\n After initial 'PasAggMaxOut_Test' in 'PasAggMaxOut_Train': sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf);
	printf("\n ...AfterInitf.fPercentageOfCorrect_Positf = %E, ...AfterInitf.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Negatf);

	printf("\n\n ...AfterInitf->nNumOfPosit_Y_Totf = %d, ...AfterInitf->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf);

	printf("\n\n ...AfterInitf->nNumOfNegat_Y_Totf = %d, ...AfterInitf->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf);


	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n  After initial 'PasAggMaxOut_Test' in 'PasAggMaxOut_Train': sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf);

	fprintf(fout, "\n ...AfterInitf.fPercentageOfCorrect_Positf = %E, ...AfterInitf.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n ...AfterInitf->nNumOfPosit_Y_Totf = %d, ...AfterInitf->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n ...AfterInitf->nNumOfNegat_Y_Totf = %d, ...AfterInitf->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf);

	//printf("\n\n Please press any key to continue:"); fflush(fout);  getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//	printf("\n\n Please press any key to continue:"); fflush(fout);  getchar();
///////////////////////////////////////////////////////////////


//	srand(4);
	for (iItersOfTrainingf = 0; iItersOfTrainingf < nNumOfItersOfTrainingTotf; iItersOfTrainingf++) //  nNumOfItersOfTrainingTot == 1 initially
	{
		nNumOfPosit_Y_Totf = 0;
			nNumOfNegat_Y_Totf = 0;

			nNumOfCorrect_Y_Totf = 0;

			nNumOfPositCorrect_Y_Totf = 0;
			nNumOfNegatCorrect_Y_Totf = 0;

		fPercentageOfCorrectTotf = 0.0;
		fPercentageOfCorrect_Positf = 0.0;
		fPercentageOfCorrect_Negatf = 0.0;

		for (iVecf = 0; iVecf < nVecTrainf; iVecf++)
		{
			iVec_Train_Glob = iVecf;

			////////////////////////////////////////////////////////////////
			nY_Actualf = nY_Train_Actual_Arrf[iVecf];
			nY_Train_Actual_Glob = nY_Actualf;

			if (nY_Actualf == 1)
			{
				nNumOfPosit_Y_Totf += 1;
			} // if (nY_Actualf == 1)
			else if (nY_Actualf == 0) //-1)
			{
				nNumOfNegat_Y_Totf += 1;
			} //else if (nY_Actualf == 0) //-1)
			else
			{
				printf("\n\n An error in 'PasAggMaxOut_Train': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar();	exit(1);
				delete[] fFea_OneVec_Train_Arrf;
				delete[] fFea_OneVec_TrainNormalized_Arrf;

				delete[] nHyperplaneWithMaxScaProdArrf;
				delete[] fZ_Arrf;
				delete[] fZ_NormalizedArrf;
				delete[] fZ_Fin_Arrf;
				delete[] fW_Fin_Arrf;

				return UNSUCCESSFUL_RETURN;
			}//else

			if (nY_Actualf == 0)
				nY_For_LossCurf = -1;
			else
				nY_For_LossCurf = 1;

///////////////////////////////
			nResf = Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
						nDim_SelecFeas_WithConstf, //const int nDimf,

						nNumVecTrainTot, //const int nNumOfVecsTotf,

						iVecf, //const int nVecf,

						fFeasSelec_WithConstTrain_Arrf, //const float fFeas_All_Arrf[], //[nProdTrainTot]

						fFea_OneVec_Train_Arrf); // float fFeas_OneVec_Arrf[]); //[nDim_SelecFeas_WithConstf]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'PasAggMaxOut_Train' by 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs'at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar();	exit(1);
				delete[] fFea_OneVec_Train_Arrf;
				delete[] fFea_OneVec_TrainNormalized_Arrf;

				delete[] nHyperplaneWithMaxScaProdArrf;
				delete[] fZ_Arrf;
				delete[] fZ_NormalizedArrf;
				delete[] fZ_Fin_Arrf;
				delete[] fW_Fin_Arrf;

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

			if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66
			{
				printf("\n\n  'PasAggMaxOut_Train': printing the extracted 'fFea_OneVec_Train_Arrf[]' at iVecf = %d", iVecf);
				fprintf(fout, "\n\n  'PasAggMaxOut_Train': printing the extracted 'fFea_OneVec_Train_Arrf[]' at iVecf = %d", iVecf);
				fprintf(fout, "\n iVecf = %d, iItersOfTrainingf = %d, nDim_SelecFeas_WithConstf = %d\n", iVecf, iItersOfTrainingf, nDim_SelecFeas_WithConstf);

				Print_A_FloatOneDim_Arr(
					nDim_SelecFeas_WithConstf, //const int nDimf, //

					fFea_OneVec_Train_Arrf); // const float fArrf[]) //nDimf

				fflush(fout);
			} //if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'PasAggMaxOut_Train:' 'fFea_OneVec_Train_Arrf[]' after 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs' "); 
			fprintf(fout, "\n iVecf = %d, iItersOfTrainingf = %d, nDim_D_SelecFeas_WithConstf = %d\n", iVecf, iItersOfTrainingf, nDim_D_SelecFeas_WithConstf);
			for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
			{
				fprintf(fout, "%d:%E, ", iFeaf, fFea_OneVec_Train_Arrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
			fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//int Normalizing_OneDim_FloatVector_To_ARange() ??
			nResf = Normalizing_FloatVector_ByStDev(
					nDim_SelecFeas_WithConstf, //const int nDimf,
						fFea_OneVec_Train_Arrf, //const float fFeas_InitArrf[],

						fFea_OneVec_TrainNormalized_Arrf); // float fFeasNormalized_Arrf[]);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'PasAggMaxOut_Train' by 'Normalizing_FloatVector_ByStDev' 1 at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Normalizing_FloatVector_ByStDev' 1 at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar();	exit(1);

				delete[] fFea_OneVec_Train_Arrf;
				delete[] fFea_OneVec_TrainNormalized_Arrf;

				delete[] nHyperplaneWithMaxScaProdArrf;
				delete[] fZ_Arrf;
				delete[] fZ_NormalizedArrf;
				delete[] fZ_Fin_Arrf;
				delete[] fW_Fin_Arrf;
				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'fFea_OneVec_TrainNormalized_Arrf[]', fFeaConst_Glob = %E\n", fFeaConst_Glob);
			for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
			{
				fprintf(fout, "%d:%E, ", iFeaf, fFea_OneVec_TrainNormalized_Arrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
			fflush(fout);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////

			nResf = All_Feas_OfNonlinearSpace(
						nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // = dimension of the original space
						nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

						nKf, //const int nKf, //nNumOfHyperplanes

						fFea_OneVec_TrainNormalized_Arrf, //const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]

						fU_Arrf, //const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

						nHyperplaneWithMaxScaProdArrf, //int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]ec

						fZ_Arrf); // float fZ_Arrf[]); //[nDim_H]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'PasAggMaxOut_Train' by 'All_Feas_OfNonlinearSpace' at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'All_Feas_OfNonlinearSpace' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar();	exit(1);

				delete[] fFea_OneVec_Train_Arrf;
				delete[] fFea_OneVec_TrainNormalized_Arrf;

				delete[] nHyperplaneWithMaxScaProdArrf;
				delete[] fZ_Arrf;
				delete[] fZ_NormalizedArrf;
				delete[] fZ_Fin_Arrf;
				delete[] fW_Fin_Arrf;

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'fZ_Arrf[]' and 'nHyperplaneWithMaxScaProdArrf[]': nDim_Hf = %d, iVecf = %d, iItersOfTrainingf = %d\n", nDim_Hf, iVecf, iItersOfTrainingf);

			for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)
			{
				fprintf(fout, "\n 'nHyperplaneWithMaxScaProdArrf[%d] = %d, fZ_Arrf[%d] = %E",iFeaf,nHyperplaneWithMaxScaProdArrf[iFeaf], iFeaf, fZ_Arrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//////////////////////////////////////////////////////////////
			nResf = Normalizing_FloatVector_ByStDev(
						nDim_Hf, //const int nDimf,
						fZ_Arrf, //const float fFeas_InitArrf[],

						fZ_NormalizedArrf); // float fFeasNormalized_Arrf[]);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'PasAggMaxOut_Train' by 'Normalizing_FloatVector_ByStDev' 2 at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Normalizing_FloatVector_ByStDev' 2 at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar();	exit(1);

				delete[] fFea_OneVec_Train_Arrf;
				delete[] fFea_OneVec_TrainNormalized_Arrf;

				delete[] nHyperplaneWithMaxScaProdArrf;
				delete[] fZ_Arrf;
				delete[] fZ_NormalizedArrf;
				delete[] fZ_Fin_Arrf;
				delete[] fW_Fin_Arrf;

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'fZ_NormalizedArrf[]':\n");
			for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)
			{
				fprintf(fout, "%d:%E, ", iFeaf, fZ_NormalizedArrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/////////////////////////////
			Loss(
				nDim_Hf, //const int nDim_Hf,
				nY_For_LossCurf, //const int nYtf, // 1 or -1

				fZ_NormalizedArrf, //const float fZ_Arrf[], //[nDim_Hf]

				fW_Arrf, //const float fW_Arrf[], //[nDim_Hf]

				nY_Estimatedf, //int &nY_Estimatedf, //0 or 1

				fLossf); // float &fLossf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n After 'Loss() for 'fZ_NormalizedArrf[]' and 'fW_Arrf[]': nY_For_LossCurf = %d, nY_Estimatedf = %d, fLossf = %E, iVecf = %d", nY_For_LossCurf, nY_Estimatedf, fLossf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifdef INCLUDE_BIAS_POS_TO_NEG
			if (fLossf > 0.0)
			{
				if (nY_Actualf == 1)
				{
					if (fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob < 1.0) //fewer pos vecs than neg -- increasing the loss each pos vec
					{
						fLossf = fLossf * (2.0 - fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob);

#ifndef COMMENT_OUT_ALL_PRINTS
						//printf("\n\n Adjusting a loss for a pos vec: nY_Actualf = %d, fLossf = %E, iVecf = %d", nY_Actualf, fLossf, iVecf);
						fprintf(fout, "\n\n  Adjusting a loss for a pos vec: nY_Actualf = %d, fLossf = %E, iVecf = %d", nY_Actualf, fLossf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					} // if (fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob < 1.0)

				} // if (nY_Actualf == 1)
				else if (nY_Actualf == 0)
				{
					if (fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob > 1.0)
					{
						fLossf = fLossf * fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob;

#ifndef COMMENT_OUT_ALL_PRINTS
						//printf("\n\n Adjusting a loss for a neg vec: nY_Actualf = %d, fLossf = %E, iVecf = %d", nY_Actualf, fLossf, iVecf);
						fprintf(fout, "\n\n  Adjusting a loss for a neg vec: nY_Actualf = %d, fLossf = %E, iVecf = %d", nY_Actualf, fLossf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					}//else if (fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob > 1.0)
				} //else if (nY_Actualf == 0)
			} // if (fLossf > 0.0)
#endif // #ifdef INCLUDE_BIAS_POS_TO_NEG

			nY_Estimated_Arrf[iVecf] = nY_Estimatedf;

			if (nY_Estimatedf == nY_Actualf)
			{
				nNumOfCorrect_Y_Totf += 1;

				if (nY_Actualf == 1)
				{
					nNumOfPositCorrect_Y_Totf += 1;
				} // if (nY_Actualf == 1)
				else if (nY_Actualf == 0) //-1)
				{
					nNumOfNegatCorrect_Y_Totf += 1;
				} //else if (nY_Actualf == 0) //-1)

			} //if (nY_Estimatedf == nY_Actualf)

			fPercentageOfCorrectTotf = (float)(100.0)*((float)(nNumOfCorrect_Y_Totf) / (float)(iVecf + 1));

			if (nNumOfPosit_Y_Totf > 0)
			{
				fPercentageOfCorrect_Positf = (float)(100.0)*((float)(nNumOfPositCorrect_Y_Totf) / (float)(nNumOfPosit_Y_Totf));
			} //if (nNumOfPosit_Y_Totf > 0)

			if (nNumOfNegat_Y_Totf > 0)
			{
				fPercentageOfCorrect_Negatf = (float)(100.0)*((float)(nNumOfNegatCorrect_Y_Totf) / (float)(nNumOfNegat_Y_Totf));
			} //if (nNumOfNegat_Y_Totf > 0)

#ifndef COMMENT_OUT_ALL_PRINTS

			if ((iVecf / 50) * 50 == iVecf)
			{
				printf("\n\n 'PasAggMaxOut_Train': iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E", iVecf, nY_Estimatedf, nY_Actualf, fLossf);

				printf("\n iItersOfTrainingf = %d, nNumOfItersOfTrainingTotf = %d", iItersOfTrainingf, nNumOfItersOfTrainingTotf);

				printf("\n\n nNumOfCorrect_Y_Totf = %d, iVecf + 1 = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, iVecf + 1, fPercentageOfCorrectTotf);

				printf("\n\n nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
					nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

				printf("\n nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
					nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

			} //f ( (iVecf / 50) * 50 == iVecf)

			fprintf(fout, "\n\n  'PasAggMaxOut_Train': iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E", iVecf, nY_Estimatedf, nY_Actualf, fLossf);

			fprintf(fout, "\n iItersOfTrainingf = %d, nNumOfItersOfTrainingTotf = %d",iItersOfTrainingf,nNumOfItersOfTrainingTotf);

			fprintf(fout, "\n\n nNumOfCorrect_Y_Totf = %d, iVecf + 1 = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, iVecf + 1, fPercentageOfCorrectTotf);

			fprintf(fout, "\n\n nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
				nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

			fprintf(fout, "\n nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
				nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (fLossf > 0.0) //nY_Estimatedf != nY_Actualf)
			{
				nResf = Updating_W_Arr(
							nDim_Hf, //const int nDim_Hf,

							fCf, //const float fCf, //0.125
							fAlphaf, //const float fAlphaf, // 0.9 < 1.0

							fLossf, //const float fLossf,

								nY_For_LossCurf, //not nY_Actualf, //const int nYtf, // 1 or -1

							fZ_NormalizedArrf, //const float fZ_Arrf[], //[nDim_Hf]

							fW_Arrf, //const float fW_Init_Arrf[], //[nDim_Hf]

							fW_Fin_Arrf); // float fW_Fin_Arrf[]); ////[nDim_Hf]

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'PasAggMaxOut_Train' by 'Updating_W_Arr' at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Updating_W_Arr' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					getchar();	exit(1);
					delete[] fFea_OneVec_Train_Arrf;
					delete[] fFea_OneVec_TrainNormalized_Arrf;

					delete[] nHyperplaneWithMaxScaProdArrf;
					delete[] fZ_Arrf;
					delete[] fZ_NormalizedArrf;
					delete[] fZ_Fin_Arrf;
					delete[] fW_Fin_Arrf;

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n 'fW_Fin_Arrf[]' after 'Updating_W_Arr': \n");
				for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)
				{
					fprintf(fout, "%d:%E, ", iFeaf, fW_Fin_Arrf[iFeaf]);
				} //for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
//////////////////////////////////////////////
				Copying_Float_Arr1_To_Arr2(
					nDim_Hf, //const int nDimf,
					fW_Fin_Arrf, //const float fArr1f[], // [nDimf]
					fW_Arrf); // float fArr2f[]) // [nDimf]
	///////////////////////////////////////////////////

				nResf = Updating_Z(
							nDim_Hf, //const int nDim_Hf,

							fLossf, //const float fLossf,

								nY_For_LossCurf, //not nY_Actualf, //const int nYtf, // 1 or -1

							fW_Fin_Arrf, //const float fW_Fin_Arrf[], //[nDim_Hf]

							fZ_NormalizedArrf, //const float fZ_Init_Arrf[], //[nDim_Hf]

							fZ_Fin_Arrf); // float fZ_Fin_Arrf[]);	//[nDim_Hf]

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'PasAggMaxOut_Train' by 'Updating_Z' at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Updating_Z' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					getchar();	exit(1);

					delete[] fFea_OneVec_Train_Arrf;
					delete[] fFea_OneVec_TrainNormalized_Arrf;

					delete[] nHyperplaneWithMaxScaProdArrf;
					delete[] fZ_Arrf;
					delete[] fZ_NormalizedArrf;
					delete[] fZ_Fin_Arrf;
					delete[] fW_Fin_Arrf;

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

//				Print_A_FloatOneDim_Arr(
	//				nDim_Hf, //const int nDimf, //

//					const float fArrf[]) //nDimf

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n 'fZ_Fin_Arrf[]' after 'Updating_Z': \n");
				for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)
				{
					fprintf(fout, "\n fZ_NormalizedArrf[%d] = %E,  fZ_Fin_Arrf[%d] = %E", iFeaf, fZ_NormalizedArrf[iFeaf], iFeaf, fZ_Fin_Arrf[iFeaf]);
				} //for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//not needed -- fZ_Arrf[] is calculated anew with each iVecf?
				Copying_Float_Arr1_To_Arr2(
					nDim_Hf, //const int nDimf,
					fZ_Fin_Arrf, //const float fArr1f[], // [nDimf]
					fZ_Arrf); // float fArr2f[]) // [nDimf]

	/////////////////////////////////////////////////////////////////////////////
				nResf = Updating_U_ForSelectedHyperplanes_Arr(

					nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // = dimension of the original space

					nDim_Hf, //const int nDim_Hf, // dimension of nonlinear space

					nKf, //const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

					//const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf
					//const int nHyperplanef, //the number of a specific hyperplane of the specific nonlinear fea; < nKf
					//nHyperplaneWithMaxScaProdArrf, //const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

					fFea_OneVec_TrainNormalized_Arrf, //const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]
					fZ_Fin_Arrf, //const float fZ_Fin_Arrf[], //[nDim_Hf]

					nHyperplaneWithMaxScaProdArrf, //const int nHyperplaneSelected_For_Z_Arrf[], //[nDim_Hf]

					fEpsilonf, //const float fEpsilonf,
					fCrf, //const float fCrf,

					///////////////////////////////////////////////////////
					fU_Arrf); // float fU_Arrf[]);// [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes


				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'PasAggMaxOut_Train' by 'Updating_U_ForSelectedHyperplanes_Arr' at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Updating_U_ForSelectedHyperplanes_Arr' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					getchar();	exit(1);

					delete[] fFea_OneVec_Train_Arrf;
					delete[] fFea_OneVec_TrainNormalized_Arrf;

					delete[] nHyperplaneWithMaxScaProdArrf;
					delete[] fZ_Arrf;
					delete[] fZ_NormalizedArrf;
					delete[] fZ_Fin_Arrf;
					delete[] fW_Fin_Arrf;

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

			} //if (fLossf > 0.0)

#ifndef COMMENT_OUT_ALL_PRINTS
			fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//for (iVecf = 0; iVecf < nVecTrainf; iVecf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'PasAggMaxOut_Train': after iItersOfTrainingf = %d, fin fW_Arrf[]", iItersOfTrainingf);
		fprintf(fout, "\n\n  'PasAggMaxOut_Train': after iItersOfTrainingf = %d, fin fW_Arrf[]", iItersOfTrainingf);

		Print_A_FloatOneDim_Arr(
			nDim_Hf, //const int nDimf, //

			fW_Arrf); // const float fArrf[]) //nDimf

		printf("\n\n 'PasAggMaxOut_Train': after iItersOfTrainingf = %d, nNumOfCorrect_Y_Totf = %d,nVecTrainf = %d, fPercentageOfCorrectTotf = %E",
			iItersOfTrainingf,nNumOfCorrect_Y_Totf, nVecTrainf, fPercentageOfCorrectTotf);

		printf("\n\n Train: nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
			nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

		printf("\n  Train: nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
			nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

		fprintf(fout, "\n\n 'PasAggMaxOut_Train': after iItersOfTrainingf = %d, nNumOfCorrect_Y_Totf = %d,nVecTrainf = %d, fPercentageOfCorrectTotf = %E",
			iItersOfTrainingf, nNumOfCorrect_Y_Totf, nVecTrainf, fPercentageOfCorrectTotf);

		fprintf(fout, "\n\n  Train: nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
			nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

		fprintf(fout, "\n  Train: nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
			nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

		fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} // for (iItersOfTrainingf= 0; iItersOfTrainingf < nNumOfItersOfTrainingTotf; iItersOfTrainingf++)
	//////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'PasAggMaxOut_Train': the final fW_Arrf[]");
	fprintf(fout, "\n\n  'PasAggMaxOut_Train': the final fW_Arrf[]");

	Print_A_FloatOneDim_Arr(
		nDim_Hf, //const int nDimf, //

		fW_Arrf); // const float fArrf[]) //nDimf
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	/////////////////////////////////////////////
	

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'PasAggMaxOut_Train': final nNumOfCorrect_Y_Totf = %d, nVecTrainf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTrainf, fPercentageOfCorrectTotf);
	
	fprintf(fout, "\n\n  Train: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
		nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

	fprintf(fout, "\n  Train: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
		nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

	fflush(fout);

	printf("\n\n 'PasAggMaxOut_Train': final train nNumOfCorrect_Y_Totf = %d,nVecTrainf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTrainf, fPercentageOfCorrectTotf);

	printf("\n\n Train: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
		nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

	printf("\n  Train: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
		nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'PasAggMaxOut_Train': fin fW_Arrf[]");
	fprintf(fout, "\n\n  'PasAggMaxOut_Train': fin fW_Arrf[]");

	Print_A_FloatOneDim_Arr(
		nDim_Hf, //const int nDimf, //

		fW_Arrf); // const float fArrf[]) //nDimf
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end of 'PasAggMaxOut_Train': fU_Arrf[]");
	fprintf(fout, "\n\nThe end of 'PasAggMaxOut_Train': fU_Arrf[]");

	nResf = Print_fU_Arr(
		nDim_D_SelecFeas_WithConstf, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		fU_Arrf); // float fU_Arrf[]); //nDim_Uf

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
/////////////////////////////////////////////
#ifdef TESTING_TRAIN_VECS_WITHOUT_UPDATING
//testing without updating

	nNumOfPosit_Y_Totf = 0;
	nNumOfNegat_Y_Totf = 0;

	nNumOfCorrect_Y_Totf = 0;

	nNumOfPositCorrect_Y_Totf = 0;
	nNumOfNegatCorrect_Y_Totf = 0;

	fPercentageOfCorrectTotf = 0.0;
	fPercentageOfCorrect_Positf = 0.0;
	fPercentageOfCorrect_Negatf = 0.0;

	for (iVecf = 0; iVecf < nVecTrainf; iVecf++)
	{
		iVec_Train_Glob = iVecf;

		////////////////////////////////////////////////////////////////
		nY_Actualf = nY_Train_Actual_Arrf[iVecf];
		nY_Train_Actual_Glob = nY_Actualf;

		if (nY_Actualf == 1)
		{
			nNumOfPosit_Y_Totf += 1;
		} // if (nY_Actualf == 1)
		else if (nY_Actualf == 0) //-1)
		{
			nNumOfNegat_Y_Totf += 1;
		} //else if (nY_Actualf == 0) //-1)
		else
		{
			printf("\n\n An error in 'PasAggMaxOut_Train': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);
			delete[] fFea_OneVec_Train_Arrf;
			delete[] fFea_OneVec_TrainNormalized_Arrf;

			delete[] nHyperplaneWithMaxScaProdArrf;
			delete[] fZ_Arrf;
			delete[] fZ_NormalizedArrf;
			delete[] fZ_Fin_Arrf;
			delete[] fW_Fin_Arrf;

			return UNSUCCESSFUL_RETURN;
		}//else

		if (nY_Actualf == 0)
			nY_For_LossCurf = -1;
		else
			nY_For_LossCurf = 1;

		///////////////////////////////
		nResf = Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
			nDim_SelecFeas_WithConstf, //const int nDimf,

			nNumVecTrainTot, //const int nNumOfVecsTotf,

			iVecf, //const int nVecf,

			fFeasSelec_WithConstTrain_Arrf, //const float fFeas_All_Arrf[], //[nProdTrainTot]

			fFea_OneVec_Train_Arrf); // float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'PasAggMaxOut_Train' by 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs'at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);
			delete[] fFea_OneVec_Train_Arrf;
			delete[] fFea_OneVec_TrainNormalized_Arrf;

			delete[] nHyperplaneWithMaxScaProdArrf;
			delete[] fZ_Arrf;
			delete[] fZ_NormalizedArrf;
			delete[] fZ_Fin_Arrf;
			delete[] fW_Fin_Arrf;

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

//#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 'fFea_OneVec_Train_Arrf[]' after 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs' ");
		fprintf(fout, "\n iVecf = %d, nY_Actualf = %d, iItersOfTrainingf = %d, nDim_SelecFeas_WithConstf = %d\n", iVecf, nY_Actualf,iItersOfTrainingf, nDim_SelecFeas_WithConstf);
		for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
		{
			fprintf(fout, "%d:%E, ", iFeaf, fFea_OneVec_Train_Arrf[iFeaf]);
		} //for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//int Normalizing_OneDim_FloatVector_To_ARange() ??
		nResf = Normalizing_FloatVector_ByStDev(
			nDim_SelecFeas_WithConstf, //const int nDimf,
			fFea_OneVec_Train_Arrf, //const float fFeas_InitArrf[],

			fFea_OneVec_TrainNormalized_Arrf); // float fFeasNormalized_Arrf[]);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'PasAggMaxOut_Train' by 'Normalizing_FloatVector_ByStDev' 1 at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Normalizing_FloatVector_ByStDev' 1 at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			delete[] fFea_OneVec_Train_Arrf;
			delete[] fFea_OneVec_TrainNormalized_Arrf;

			delete[] nHyperplaneWithMaxScaProdArrf;
			delete[] fZ_Arrf;
			delete[] fZ_NormalizedArrf;
			delete[] fZ_Fin_Arrf;
			delete[] fW_Fin_Arrf;
			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 'fFea_OneVec_TrainNormalized_Arrf[]', fFeaConst_Glob = %E\n", fFeaConst_Glob);
		for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
		{
			fprintf(fout, "%d:%E, ", iFeaf, fFea_OneVec_TrainNormalized_Arrf[iFeaf]);
		} //for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////

		nResf = All_Feas_OfNonlinearSpace(
			nDim_SelecFeas_WithConstf, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
			nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

			nKf, //const int nKf, //nNumOfHyperplanes

			fFea_OneVec_TrainNormalized_Arrf, //const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]

			fU_Arrf, //const float fU_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

			nHyperplaneWithMaxScaProdArrf, //int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]ec

			fZ_Arrf); // float fZ_Arrf[]); //[nDim_H]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'PasAggMaxOut_Train' by 'All_Feas_OfNonlinearSpace' at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'All_Feas_OfNonlinearSpace' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			delete[] fFea_OneVec_Train_Arrf;
			delete[] fFea_OneVec_TrainNormalized_Arrf;

			delete[] nHyperplaneWithMaxScaProdArrf;
			delete[] fZ_Arrf;
			delete[] fZ_NormalizedArrf;
			delete[] fZ_Fin_Arrf;
			delete[] fW_Fin_Arrf;

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 'fZ_Arrf[]' and 'nHyperplaneWithMaxScaProdArrf[]': nDim_Hf = %d, iVecf = %d, iItersOfTrainingf = %d\n", nDim_Hf, iVecf, iItersOfTrainingf);

		for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)
		{
			fprintf(fout, "\n 'nHyperplaneWithMaxScaProdArrf[%d] = %d, fZ_Arrf[%d] = %E", iFeaf, nHyperplaneWithMaxScaProdArrf[iFeaf], iFeaf, fZ_Arrf[iFeaf]);
		} //for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//////////////////////////////////////////////////////////////
		nResf = Normalizing_FloatVector_ByStDev(
			nDim_Hf, //const int nDimf,
			fZ_Arrf, //const float fFeas_InitArrf[],

			fZ_NormalizedArrf); // float fFeasNormalized_Arrf[]);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'PasAggMaxOut_Train' by 'Normalizing_FloatVector_ByStDev' 2 at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Normalizing_FloatVector_ByStDev' 2 at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			delete[] fFea_OneVec_Train_Arrf;
			delete[] fFea_OneVec_TrainNormalized_Arrf;

			delete[] nHyperplaneWithMaxScaProdArrf;
			delete[] fZ_Arrf;
			delete[] fZ_NormalizedArrf;
			delete[] fZ_Fin_Arrf;
			delete[] fW_Fin_Arrf;

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 'fZ_NormalizedArrf[]':\n");
		for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)
		{
			fprintf(fout, "%d:%E, ", iFeaf, fZ_NormalizedArrf[iFeaf]);
		} //for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/////////////////////////////
		Loss(
			nDim_Hf, //const int nDim_Hf,
			nY_For_LossCurf, //const int nYtf, // 1 or -1

			fZ_NormalizedArrf, //const float fZ_Arrf[], //[nDim_Hf]

			fW_Arrf, //const float fW_Arrf[], //[nDim_Hf]

			nY_Estimatedf, //int &nY_Estimatedf, //0 or 1

			fLossf); // float &fLossf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n After 'Loss() for 'fZ_NormalizedArrf[]' and 'fW_Arrf[]': nY_For_LossCurf = %d, nY_Estimatedf = %d, fLossf = %E, iVecf = %d", nY_For_LossCurf, nY_Estimatedf, fLossf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nY_Estimated_Arrf[iVecf] = nY_Estimatedf;

		if (nY_Estimatedf == nY_Actualf)
		{
			nNumOfCorrect_Y_Totf += 1;

			if (nY_Actualf == 1)
			{
				nNumOfPositCorrect_Y_Totf += 1;
			} // if (nY_Actualf == 1)
			else if (nY_Actualf == 0) //-1)
			{
				nNumOfNegatCorrect_Y_Totf += 1;
			} //else if (nY_Actualf == 0) //-1)

		} //if (nY_Estimatedf == nY_Actualf)


#ifndef COMMENT_OUT_ALL_PRINTS
		fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//for (iVecf = 0; iVecf < nVecTrainf; iVecf++)

	fPercentageOfCorrectTotf = (float)(100.0)*((float)(nNumOfCorrect_Y_Totf) / (float)(nVecTrainf));

	if (nNumOfPosit_Y_Totf > 0)
	{
		fPercentageOfCorrect_Positf = (float)(100.0)*((float)(nNumOfPositCorrect_Y_Totf) / (float)(nNumOfPosit_Y_Totf));
	} //if (nNumOfPosit_Y_Totf > 0)

	if (nNumOfNegat_Y_Totf > 0)
	{
		fPercentageOfCorrect_Negatf = (float)(100.0)*((float)(nNumOfNegatCorrect_Y_Totf) / (float)(nNumOfNegat_Y_Totf));
	} //if (nNumOfNegat_Y_Totf > 0)

	printf("\n\n///////////////////////////////////////////////////////////");
	printf("\n\n After testing without updating in 'PasAggMaxOut_Train': fPercentageOfCorrectTotf = %E, ", fPercentageOfCorrectTotf);

	printf("\n fPercentageOfCorrect_Positf = %E, fPercentageOfCorrect_Negatf = %E",
		fPercentageOfCorrect_Positf, fPercentageOfCorrect_Negatf);

	printf("\n\n nNumOfPosit_Y_Totf = %d, nNumOfPositCorrect_Y_Totf = %d", nNumOfPosit_Y_Totf,
		nNumOfPositCorrect_Y_Totf);

	printf("\n\n nNumOfNegat_Y_Totf = %d, nNumOfNegatCorrect_Y_Totf = %d", nNumOfNegat_Y_Totf,
		nNumOfNegatCorrect_Y_Totf);

	fprintf(fout,"\n\n After testing without updating in 'PasAggMaxOut_Train': fPercentageOfCorrectTotf = %E, ", fPercentageOfCorrectTotf);

	fprintf(fout,"\n fPercentageOfCorrect_Positf = %E, fPercentageOfCorrect_Negatf = %E",
		fPercentageOfCorrect_Positf, fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n nNumOfPosit_Y_Totf = %d, nNumOfPositCorrect_Y_Totf = %d", nNumOfPosit_Y_Totf,
		nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n\n nNumOfNegat_Y_Totf = %d, nNumOfNegatCorrect_Y_Totf = %d", nNumOfNegat_Y_Totf,
		nNumOfNegatCorrect_Y_Totf);

	printf("\n\n Please press any key to exit"); fflush(fout);  getchar();  exit(1);

#endif //#ifdef TESTING_TRAIN_VECS_WITHOUT_UPDATING

/////////////////////////////////////////
//testing the train data
	nResf = PasAggMaxOut_Test(
		fFeasSelec_WithConstTrain_Arrf, //const float fFeaTest_Arrf[], //[nProdTestTot], to be normalized
		nY_Train_Actual_Arrf, //[nNumVecTrainTot], const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		//const int nDimf, == nDim_D_SelecFeas_WithConstf

		nVecTrainf, //const int nVecTestf,

		/////////////////////////
		nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		fW_Arrf, //const float fW_Train_Arrf[], //[nDim_Hf]
		fU_Arrf, //const float fU_Train_Arrf[], //[nDim_U_Glob],
		///////////////////////////////////////////////////

		&sPasAggMaxOut_TrainResultsRightAfterInitf); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'PasAggMaxOut_Train' by 'PasAggMaxOut_Test' 2");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'PasAggMaxOut_Test' 2");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar();	exit(1);
		delete[] fFea_OneVec_Train_Arrf;
		delete[] fFea_OneVec_TrainNormalized_Arrf;

		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;
		delete[] fZ_NormalizedArrf;
		delete[] fZ_Fin_Arrf;
		delete[] fW_Fin_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

/*
	printf("\n\n///////////////////////////////////////////////////////////");
	printf("\n\n After final 'PasAggMaxOut_Test' in 'PasAggMaxOut_Train': sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf);
	printf("\n ...AfterInitf.fPercentageOfCorrect_Positf = %E, ...AfterInitf.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Negatf);

	printf("\n\n ...AfterInitf->nNumOfPosit_Y_Totf = %d, ...AfterInitf->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf);

	printf("\n\n ...AfterInitf->nNumOfNegat_Y_Totf = %d, ...AfterInitf->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf);
*/
	//printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);

	sPasAggMaxOut_TrainResults->nNumOfVecs_Totf = nVecTrainf;
	sPasAggMaxOut_TrainResults->nNumOfCorrect_Y_Totf = sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfCorrect_Y_Totf;

	sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf = sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf;
	sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf = sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf;

	sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf = sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf;
	sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf = sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf;

	sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf = sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf;
	sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf = sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Positf;
	sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf = sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Negatf;

#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n  After final 'PasAggMaxOut_Test' in 'PasAggMaxOut_Train': sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf);

	fprintf(fout, "\n ...AfterInitf.fPercentageOfCorrect_Positf = %E, ...AfterInitf.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n ...AfterInitf->nNumOfPosit_Y_Totf = %d, ...AfterInitf->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n ...AfterInitf->nNumOfNegat_Y_Totf = %d, ...AfterInitf->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] fFea_OneVec_Train_Arrf;
	delete[] fFea_OneVec_TrainNormalized_Arrf;

	delete[] nHyperplaneWithMaxScaProdArrf;
	delete[] fZ_Arrf;
	delete[] fZ_NormalizedArrf;
	delete[] fZ_Fin_Arrf;
	delete[] fW_Fin_Arrf;

	return SUCCESSFUL_RETURN;
} // int PasAggMaxOut_Train(...
//////////////////////////////////////////////////////////////////////////////////////////

int PasAggMaxOut_Test(
			//const float fFea_WithConstTest_Arrf[], //[nProdTestTot], to be normalized
			const float fFeasSelec_WithConstTest_Arrf[], //[],

			const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

			//const int nDimf, == nDim_D_SelecFeas_WithConstf

			const int nVecTestf,

			/////////////////////////
			const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
			const int nDim_Hf, //dimension of the nonlinear/transformed space

			const int nKf, //nNumOfHyperplanes
			const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

			///////////////////////
			const float fW_Train_Arrf[], //[nDim_Hf]
			const float fU_Train_Arrf[], //[nDim_U_Glob],
			///////////////////////////////////////////////////

			PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults)
{
/*
	int Normalizing_OneDim_FloatVector_To_ARange(
		const int nDimf,
		const float fFin_Minf,
		const float fFin_Maxf,

		const float fFeas_InitArrf[],

		float fFeas_NormArrf[]);
*/
	int All_Feas_OfNonlinearSpace(
		const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes

		const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]

		const float fU_Train_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

//////////////////////////////////////////
		int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]

		float fZ_Arrf[]); //[nDim_H]

//////////////////////////////////////////////
	
	int Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
		const int nDimf,
		const int nNumOfVecsTotf,

		const int nVecf,

		const float fFeas_All_Arrf[], //[nProdTestTot]

		float fFeas_OneVec_Arrf[]); //[nDim_SelecFeas_WithConstf]

	int Normalizing_FloatVector_ByStDev(
		const int nDimf,
		const float fFeas_InitArrf[],

		float fFeasNormalized_Arrf[]);

/////////////////////////////////////////////////
	int
		nResf,
	//	nHyperplaneWithMaxScaProdArrf[nDim_H], //[]

		iFea_Nonlinearf,
		nY_Estimatedf,

		//nY_Estimated_Arrf[nNumVecTestTot],

		nNumOfPosit_Y_Totf = 0,
		nNumOfNegat_Y_Totf = 0,

		nNumOfCorrect_Y_Totf = 0,

		nNumOfPositCorrect_Y_Totf = 0,
		nNumOfNegatCorrect_Y_Totf = 0,

		nY_Actualf,
		nY_For_LossCurf,
		iFeaf,
		iVecf;

	float
		fScalar_Prodf,
		fLossf,

		fLossMinf = fLarge,

		fLossMaxf = -fLarge,
		fPercentageOfCorrectTotf = 0.0,
		fPercentageOfCorrect_Positf = 0.0,
		fPercentageOfCorrect_Negatf = 0.0;

		//fW_Fin_Arrf[nDim_H],

		//fZ_Arrf[nDim_H],
		//fZ_NormalizedArrf[nDim_H],
		//fZ_Fin_Arrf[nDim_H],

		//fFea_OneVec_Arrf[nDim_D_WithConst],
		//fFea_OneVecNormalizedByStDev_Arrf[nDim_D_WithConst];
//////////////////////////////

	float* fFea_OneVec_Arrf = new float[nDim_SelecFeas_WithConstf];
	if (fFea_OneVec_Arrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Test': fFea_OneVec_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Test': fFea_OneVec_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fFea_OneVec_Arrf == NULL)
/////////////////////////////////

	float* fFea_OneVecNormalizedByStDev_Arrf = new float[nDim_SelecFeas_WithConstf];
	if (fFea_OneVecNormalizedByStDev_Arrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Test': fFea_OneVecNormalizedByStDev_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Test': fFea_OneVecNormalizedByStDev_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fFea_OneVec_Arrf;

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fFea_OneVecNormalizedByStDev_Arrf == NULL)
	///////////////////////////////

	  	int* nHyperplaneWithMaxScaProdArrf = new int[nDim_Hf];
	if (nHyperplaneWithMaxScaProdArrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Test': nHyperplaneWithMaxScaProdArrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Test': nHyperplaneWithMaxScaProdArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] fFea_OneVec_Arrf;
		delete[] fFea_OneVecNormalizedByStDev_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nHyperplaneWithMaxScaProdArrf == NULL)

	float* fZ_Arrf = new float[nDim_Hf];
	if (fZ_Arrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Test': fZ_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Test': fZ_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] fFea_OneVec_Arrf;
		delete[] fFea_OneVecNormalizedByStDev_Arrf;

		delete[] nHyperplaneWithMaxScaProdArrf;
		return UNSUCCESSFUL_RETURN;
	} //if (fZ_Arrf == NULL)

	float* fZ_NormalizedArrf = new float[nDim_Hf];
	if (fZ_NormalizedArrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Test': fZ_NormalizedArrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Test': fZ_NormalizedArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] fFea_OneVec_Arrf;
		delete[] fFea_OneVecNormalizedByStDev_Arrf;

		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fZ_NormalizedArrf == NULL)

	float* fZ_Fin_Arrf = new float[nDim_Hf];
	if (fZ_Fin_Arrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Test': fZ_Fin_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Test': fZ_Fin_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] fFea_OneVec_Arrf;
		delete[] fFea_OneVecNormalizedByStDev_Arrf;

		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;
		delete[] fZ_NormalizedArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fZ_Fin_Arrf == NULL)
/////////////////////////////////////////////////

	int* nY_Estimated_Arrf = new int[nVecTestf];
	if (nY_Estimated_Arrf == NULL)
	{
		printf("\n\n An error in 'PasAggMaxOut_Test': nY_Estimated_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Test': nY_Estimated_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] fFea_OneVec_Arrf;
		delete[] fFea_OneVecNormalizedByStDev_Arrf;

		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;
		delete[] fZ_NormalizedArrf;
		delete[] fZ_Fin_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nY_Estimated_Arrf == NULL)
///////////////////////
		for (iVecf = 0; iVecf < nVecTestf; iVecf++)
		{
			nResf = Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
					nDim_SelecFeas_WithConstf, //const int nDimf,

						nVecTestf, //const int nNumOfVecsTotf,

						iVecf, //const int nVecf,

						fFeasSelec_WithConstTest_Arrf, //const float fFeas_All_Arrf[], //[nProd_WithConstTestTot]

						fFea_OneVec_Arrf); // float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'PasAggMaxOut_Test' by 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs'at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test' by 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar(); exit(1);

				delete[] fFea_OneVec_Arrf;
				delete[] fFea_OneVecNormalizedByStDev_Arrf;
				delete[] nY_Estimated_Arrf;

				delete[] nHyperplaneWithMaxScaProdArrf;
				delete[] fZ_Arrf;
				delete[] fZ_NormalizedArrf;
				delete[] fZ_Fin_Arrf;

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

//int Normalizing_OneDim_FloatVector_To_ARange() ??
			nResf = Normalizing_FloatVector_ByStDev(
					nDim_SelecFeas_WithConstf, //const int nDimf,
						fFea_OneVec_Arrf, //const float fFeas_InitArrf[],

						fFea_OneVecNormalizedByStDev_Arrf); // float fFeasNormalized_Arrf[]);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'PasAggMaxOut_Test' by 'Normalizing_FloatVector_ByStDev' 1 at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test' by 'Normalizing_FloatVector_ByStDev' 1 at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar(); exit(1);

				delete[] fFea_OneVec_Arrf;
				delete[] fFea_OneVecNormalizedByStDev_Arrf;
				delete[] nY_Estimated_Arrf;
				delete[] nHyperplaneWithMaxScaProdArrf;
				delete[] fZ_Arrf;
				delete[] fZ_NormalizedArrf;
				delete[] fZ_Fin_Arrf;
				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'PasAggMaxOut_Test':'fFea_OneVec_Arrf[]', fFeaConst_Glob = %E\n", fFeaConst_Glob);
			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				fprintf(fout, "%d:%E, ", iFeaf, fFea_OneVec_Arrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)


			fprintf(fout, "\n\n 'PasAggMaxOut_Test':'fFea_OneVec_TrainNormalized_Arrf[]', fFeaConst_Glob = %E\n", fFeaConst_Glob);
			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				fprintf(fout, "%d:%E, ", iFeaf, fFea_OneVecNormalizedByStDev_Arrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////

			nResf = All_Feas_OfNonlinearSpace(
						nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
						nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

						nKf, //const int nKf, //nNumOfHyperplanes

						fFea_OneVecNormalizedByStDev_Arrf, //const float fX_Arrf[], //[nDim_D_SelecFeas_WithConstf]

						fU_Train_Arrf, //const float fU_Train_Arrf[], // [nDim_U_Glob] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

						nHyperplaneWithMaxScaProdArrf, //int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]

						fZ_Arrf); // float fZ_Arrf[]); //[nDim_H]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'PasAggMaxOut_Test' by 'All_Feas_OfNonlinearSpace' at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test' by 'All_Feas_OfNonlinearSpace' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar(); exit(1);

				delete[] fFea_OneVec_Arrf;
				delete[] fFea_OneVecNormalizedByStDev_Arrf;

				delete[] nY_Estimated_Arrf;
				delete[] nHyperplaneWithMaxScaProdArrf;
				delete[] fZ_Arrf;
				delete[] fZ_NormalizedArrf;
				delete[] fZ_Fin_Arrf;

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)
			
#ifndef COMMENT_OUT_ALL_PRINTS
			for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_Hf; iFea_Nonlinearf++)
			{
				fprintf(fout, "\n 'PasAggMaxOut_Test' (after 'All_Feas_OfNonlinearSpace'): iVecf = %d, nHyperplaneWithMaxScaProdArrf[%d] = %d, fZ_Arrf[%d] = %E", 
					iVecf,iFea_Nonlinearf,nHyperplaneWithMaxScaProdArrf[iFea_Nonlinearf], iFea_Nonlinearf,fZ_Arrf[iFea_Nonlinearf]);
			} //for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_Hf; iFea_Nonlinearf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//////////////////////////////////////////////////////////////
			nResf = Normalizing_FloatVector_ByStDev(
				nDim_Hf, //const int nDimf,
				fZ_Arrf, //const float fFeas_InitArrf[],

				fZ_NormalizedArrf); // float fFeasNormalized_Arrf[]);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'PasAggMaxOut_Test' by 'Normalizing_FloatVector_ByStDev' 2 at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test' by 'Normalizing_FloatVector_ByStDev' 2 at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar(); exit(1);

				delete[] fFea_OneVec_Arrf;
				delete[] fFea_OneVecNormalizedByStDev_Arrf;

				delete[] nY_Estimated_Arrf;
				delete[] nHyperplaneWithMaxScaProdArrf;
				delete[] fZ_Arrf;
				delete[] fZ_NormalizedArrf;
				delete[] fZ_Fin_Arrf;
				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

			nY_Actualf = nY_Test_Actual_Arrf[iVecf];

			if (nY_Actualf == 1)
			{
				nNumOfPosit_Y_Totf += 1;
			} // if (nY_Actualf == 1)
			else if (nY_Actualf == 0) //-1)
			{
				nNumOfNegat_Y_Totf += 1;
			} //else if (nY_Actualf == 0) //-1)
			else
			{
				printf("\n\n An error in 'PasAggMaxOut_Test': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar(); exit(1);

				delete[] fFea_OneVec_Arrf;
				delete[] fFea_OneVecNormalizedByStDev_Arrf;

				delete[] nY_Estimated_Arrf;
				delete[] nHyperplaneWithMaxScaProdArrf;
				delete[] fZ_Arrf;
				delete[] fZ_NormalizedArrf;
				delete[] fZ_Fin_Arrf;
				return UNSUCCESSFUL_RETURN;
			}//else

#ifndef COMMENT_OUT_ALL_PRINTS
			for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_Hf; iFea_Nonlinearf++)
			{
				fprintf(fout, "\n 'PasAggMaxOut_Test' (after normalizing): iVecf = %d, nY_Actualf = %d, fZ_Arrf[%d] = %E, fZ_NormalizedArrf[%d] = %E",
					iVecf, nY_Actualf,iFea_Nonlinearf, fZ_Arrf[iFea_Nonlinearf], iFea_Nonlinearf, fZ_NormalizedArrf[iFea_Nonlinearf]);
			} //for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_Hf; iFea_Nonlinearf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/////////////////////////////
			if (nY_Actualf == 0)
				nY_For_LossCurf = -1;
			else
				nY_For_LossCurf = 1;

			Loss(
				nDim_Hf, //const int nDim_Hf,
				nY_For_LossCurf, //const int nYtf, // 1 or -1 (not 0)

				fZ_NormalizedArrf, //const float fZ_Arrf[], //[nDim_Hf]

				fW_Train_Arrf, //const float fW_Train_Arrf[], //[nDim_Hf]

				nY_Estimatedf, //int &nY_Estimatedf, //0 or 1

				fLossf); // float &fLossf);

			if (fLossf < fLossMinf)
				fLossMinf = fLossf;

			if (fLossf > fLossMaxf)
				fLossMaxf = fLossf;

			nY_Estimated_Arrf[iVecf] = nY_Estimatedf;

			if (nY_Estimatedf == nY_Actualf)
			{
				nNumOfCorrect_Y_Totf += 1;

				if (nY_Actualf == 1)
				{
					nNumOfPositCorrect_Y_Totf += 1;
				} // if (nY_Actualf == 1)
				else if (nY_Actualf == 0) //-1)
				{
					nNumOfNegatCorrect_Y_Totf += 1;
				} //else if (nY_Actualf == 0) //-1)

			} //if (nY_Estimatedf == nY_Actualf)

			if (nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob == 1 && nNumOfFitnessOfOneFeaVecTot_Glob == nNumOfFitnessOfOneFeaVecTot_ForPrinting)
			{
				fprintf(fout, "\n  'PasAggMaxOut_Test' (the test set): iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E",
					iVecf, nY_Estimatedf, nY_Actualf, fLossf);

			} //if (nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob == 1 && nNumOfFitnessOfOneFeaVecTot_Glob == nNumOfFitnessOfOneFeaVecTot_ForPrinting)

#ifndef COMMENT_OUT_ALL_PRINTS

			if ((iVecf / 50) * 50 == iVecf)
			{
				printf("\n\n 'PasAggMaxOut_Test': iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E", iVecf, nY_Estimatedf, nY_Actualf, fLossf);

				printf("\n\n nNumOfCorrect_Y_Totf = %d, iVecf + 1 = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, iVecf + 1, fPercentageOfCorrectTotf);

				printf("\n\n nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
					nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

				printf("\n nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
					nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

			} //f ( (iVecf / 50) * 50 == iVecf)

			fprintf(fout, "\n\n  'PasAggMaxOut_Test': iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E", iVecf, nY_Estimatedf, nY_Actualf, fLossf);

			fprintf(fout, "\n\n nNumOfCorrect_Y_Totf = %d, iVecf + 1 = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, iVecf + 1, fPercentageOfCorrectTotf);

			fprintf(fout, "\n\n nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
				nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

			fprintf(fout, "\n nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
				nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//for (iVecf = 0; iVecf < nVecTestf; iVecf++)
//////////////////////////

		if (nNumOfPosit_Y_Totf > 0)
		{
			fPercentageOfCorrect_Positf = (float)(100.0)*(float)(nNumOfPositCorrect_Y_Totf) / (float)(nNumOfPosit_Y_Totf);
		} //if (nNumOfPosit_Y_Totf > 0)

		if (nNumOfNegat_Y_Totf > 0)
		{
			fPercentageOfCorrect_Negatf = (float)(100.0)*(float)(nNumOfNegatCorrect_Y_Totf) / (float)(nNumOfNegat_Y_Totf);
		} //if (nNumOfNegat_Y_Totf > 0)

		if (nNumOfPosit_Y_Totf <= 0 || nNumOfNegat_Y_Totf <= 0)
		{
			printf("\n\n An error in 'PasAggMaxOut_Test': nNumOfPosit_Y_Totf = %d <= 0 || nNumOfNegat_Y_Totf <= %d", nNumOfPosit_Y_Totf,nNumOfNegat_Y_Totf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test': nNumOfPosit_Y_Totf <= 0 || nNumOfNegat_Y_Totf <= %d", nNumOfPosit_Y_Totf, nNumOfNegat_Y_Totf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar(); exit(1);

			delete[] fFea_OneVec_Arrf;
			delete[] fFea_OneVecNormalizedByStDev_Arrf;

			delete[] nY_Estimated_Arrf;
			delete[] nHyperplaneWithMaxScaProdArrf;
			delete[] fZ_Arrf;
			delete[] fZ_NormalizedArrf;
			delete[] fZ_Fin_Arrf;

			return UNSUCCESSFUL_RETURN;
		}//if (nNumOfPosit_Y_Totf <= 0 || nNumOfNegat_Y_Totf <= 0)
		//fPercentageOfCorrectTotf = (float)(100.0)*(float)(nNumOfCorrect_Y_Totf) / (float)(iVecf + 1);

		fPercentageOfCorrectTotf = (fPercentageOfCorrect_Positf + fPercentageOfCorrect_Negatf) / 2.0;
	/////////////////////////////////////////////
	sPasAggMaxOut_TestResults->nNumOfVecs_Totf = nVecTestf;
	sPasAggMaxOut_TestResults->nNumOfCorrect_Y_Totf = nNumOfCorrect_Y_Totf;

	sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf = nNumOfPosit_Y_Totf;
	sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf = nNumOfPositCorrect_Y_Totf;

	sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf = nNumOfNegat_Y_Totf;
	sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf = nNumOfNegatCorrect_Y_Totf;

	sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf = fPercentageOfCorrectTotf;
	sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf = fPercentageOfCorrect_Positf;
	sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf = fPercentageOfCorrect_Negatf;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'PasAggMaxOut_Test': final nNumOfCorrect_Y_Totf = %d, nVecTestf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTestf, fPercentageOfCorrectTotf);
	
	printf("\n\n Test: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
		nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

	printf("\n Test: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
		nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n 'PasAggMaxOut_Test': final nNumOfCorrect_Y_Totf = %d, nVecTestf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTestf, fPercentageOfCorrectTotf);

	fprintf(fout, "\n\n Test: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
		nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

	fprintf(fout, "\n Test: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
		nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

	fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	if (nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob == 1 && nNumOfFitnessOfOneFeaVecTot_Glob == nNumOfFitnessOfOneFeaVecTot_ForPrinting)
	{
		fprintf(fout, "\n\n 'PasAggMaxOut_Test': final nNumOfCorrect_Y_Totf = %d, nVecTestf = %d, fPercentageOfCorrectTotf = %E", 
			nNumOfCorrect_Y_Totf, nVecTestf, fPercentageOfCorrectTotf);

		fprintf(fout, "\n\n Test: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
			nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

		fprintf(fout, "\n Test: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
			nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);
		fflush(fout);
	} //if (nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob == 1 && nNumOfFitnessOfOneFeaVecTot_Glob == nNumOfFitnessOfOneFeaVecTot_ForPrinting)

	//printf("\n\n The end of 'PasAggMaxOut_Test': fLossMinf = %E, fLossMaxf = %E", fLossMinf, fLossMaxf);
	delete[] fFea_OneVec_Arrf;
	delete[] fFea_OneVecNormalizedByStDev_Arrf;

	delete[] nY_Estimated_Arrf;

	delete[] nHyperplaneWithMaxScaProdArrf;
	delete[] fZ_Arrf;
	delete[] fZ_NormalizedArrf;
	delete[] fZ_Fin_Arrf;

	return SUCCESSFUL_RETURN;
} // int PasAggMaxOut_Test(...

//////////////////////////////////////////////////////////////////////////////
void Swap_2Ints(
	int nX_1f, 
	int nX_2f) 
{
	int nTempf = nX_1f;

	nX_1f = nX_2f;
	nX_2f = nTempf;
}//void Swap_2Ints(...

//////////////////////////////////////////
//by Fisher-Yates (Wiki)
void Shuffling_AnIntArr(
	const int nDimf,
	int nArrf[]) //[nDimf]; initially [0,1,2,...,nDimf - 1]
{
	void Swap_2Ints(
		int nX_1f,
		int nX_2f);

	//srand(4);

	int
		nTempf,

		iFea_1f,
		nFea_2f;

	for (iFea_1f = nDimf - 1; iFea_1f > 0; iFea_1f--)
	{
		nFea_2f = rand() % (iFea_1f + 1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n iFea_1f = %d, nFea_2f = %d, init nArrf[iFea_1f] = %d, nArrf[nFea_2f] = %d", iFea_1f, nFea_2f, nArrf[iFea_1f], nArrf[nFea_2f]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nTempf = nArrf[nFea_2f];

		nArrf[nFea_2f] = nArrf[iFea_1f];
		nArrf[iFea_1f] = nTempf;
		
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n Fin nArrf[%d] = %d, nArrf[%d] = %d", iFea_1f, nArrf[iFea_1f], nFea_2f,nArrf[nFea_2f]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} // for (iFea_1f = nDimf - 1; iFea_1f > 0; iFea_1f--)

}//void Shuffling_AnIntArr(...
/////////////////////////////////////////

int Shuffle_OneDim_Int_And_2Dim_Float_Arrs(
	const int nDimf,
	const int nNumOfVecsTotf,

	int nY_Arrf[], // [nNumOfVecsTotf]
	float fFeas_All_Arrf[]) //[//[nDimf*nNumOfVecsTotf]]
{
	void Shuffling_AnIntArr(
		const int nDimf,
		int nArrf[]); //[nDimf]; initially [0,1,2,...,nDimf - 1]

	int Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
		const int nDimf,
			const int nNumOfVecsTotf,
		const int nVecf,

		const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

		float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

	int Writing_OneDimVec_To_2DimVec(
		const int nDimf,
		const int nNumOfVecsTotf,

		const int nVecf,

		const float fFeas_OneVec_Arrf[], //[nDim_D_WithConst]

		float fFeas_All_Arrf[]); //[nDimf*nNumOfVecsTotf]

	void Copying_Int_Arr1_To_Arr2(
		const int nDimf,
		const int nArr1f[], // [nDimf]
		int nArr2f[]); // [nDimf]

	void Copying_Float_Arr1_To_Arr2(
		const int nDimf,
		const float fArr1f[], // [nDimf]
		float fArr2f[]); // [nDimf];

	int
		nResf,
		iVecf,
		nVecCurf,

		nPosition_OfVecTempf,

		nIndexf,

		nSizeOf_2DimVecf = nDimf * nNumOfVecsTotf,

		nTempf,
		//nY_Tempf,
		nPosition_OfVecMaxf = nNumOfVecsTotf - 1,
		iFeaf;

int* nPositionsOfVecsArrf = new int[nNumOfVecsTotf];

if (nPositionsOfVecsArrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nPositionsOfVecsArrf == NULL");
	fprintf(fout, "\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nPositionsOfVecsArrf == NULL");
	//getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return UNSUCCESSFUL_RETURN;
} //if (nPositionsOfVecsArrf == NULL)

int* nY_TempArrf = new int[nNumOfVecsTotf];
if (nY_TempArrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nY_TempArrf == NULL");
	fprintf(fout, "\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nY_TempArrf == NULL");
	//getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] nPositionsOfVecsArrf;

	return UNSUCCESSFUL_RETURN;
} //if (nPositionsOfVecsArrf == NULL)

for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
{
	nPositionsOfVecsArrf[iVecf] = iVecf;
} // for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)


///////////////////////////////////////////////
Shuffling_AnIntArr(
	nNumOfVecsTotf, //const int nDimf,
	nPositionsOfVecsArrf); // int nArrf[]); //[nDimf]; initially [0,1,2,...,nDimf - 1]

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n Before 'Shuffling_AnIntArr':");
	for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
	{
		nTempf = iVecf * nDimf;
		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;
			fprintf(fout, "\n iVecf = %d, iFeaf = %d, fFeas_All_Arrf[%d] = %E", iVecf, iFeaf, nIndexf,fFeas_All_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	} //for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 1: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E", fFeas_All_Arrf[4], fFeas_All_Arrf[5]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
{
	nPosition_OfVecTempf = nPositionsOfVecsArrf[iVecf];

	if (nPosition_OfVecTempf < 0 || nPosition_OfVecTempf > nPosition_OfVecMaxf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nPosition_OfVecTempf = %d, nPosition_OfVecMaxf = %d", nPosition_OfVecTempf, nPosition_OfVecMaxf);
		fprintf(fout, "\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nPosition_OfVecTempf = %d, nPosition_OfVecMaxf = %d", nPosition_OfVecTempf, nPosition_OfVecMaxf);
		//getchar(); exit(1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nPositionsOfVecsArrf;
		delete[] nY_TempArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nPosition_OfVecTempf < 0 || nPosition_OfVecTempf > nPosition_OfVecMaxf)

	nY_TempArrf[iVecf] = nY_Arrf[nPosition_OfVecTempf];

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n iVecf = %d, nPosition_OfVecTempf = %d, nPosition_OfVecMaxf = %d, nY_TempArrf[iVecf] = %d", 
		iVecf, nPosition_OfVecTempf, nPosition_OfVecMaxf, nY_TempArrf[iVecf]);

	//getchar(); exit(1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

} //for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 2: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E", fFeas_All_Arrf[4], fFeas_All_Arrf[5]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//fin 'nY_Arrf[]' by copying from 'nY_TempArrf[]'
Copying_Int_Arr1_To_Arr2(
	nNumOfVecsTotf, //const int nDimf,

	nY_TempArrf, //const int nArr1f[], // [nDimf]
	nY_Arrf); // int nArr2f[]); // [nDimf]

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n After 'Copying_Int_Arr1_To_Arr2': nDimf = %d, nNumOfVecsTotf = %d", nDimf, nNumOfVecsTotf);
for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
{
	fprintf(fout, "\n nY_Arrf[%d] = %d", iVecf, nY_Arrf[iVecf]);
} //for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 2_1: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E", fFeas_All_Arrf[4], fFeas_All_Arrf[5]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////////////////////
float* fFeasOf_OneVec_1Arrf = new float[nDimf];

if (fFeasOf_OneVec_1Arrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': fFeasOf_OneVec_1Arrf == NULL");
	fprintf(fout, "\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': fFeasOf_OneVec_1Arrf == NULL");
	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] nPositionsOfVecsArrf;
	delete[] nY_TempArrf;

	return UNSUCCESSFUL_RETURN;
} //if (fFeasOf_OneVec_1Arrf == NULL)

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 2_2: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E", fFeas_All_Arrf[4], fFeas_All_Arrf[5]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

float* fFeas_All_TempArrf = new float[nSizeOf_2DimVecf];
if (fFeas_All_TempArrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': fFeas_All_TempArrf == NULL");
	fprintf(fout, "\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs':  fFeas_All_TempArrf == NULL");
	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] nPositionsOfVecsArrf;
	delete[] nY_TempArrf;
	delete[] fFeasOf_OneVec_1Arrf;

	return UNSUCCESSFUL_RETURN;
} //if ( fFeas_All_TempArrf == NULL");)

//////////////////////////////////////////////////////////////////////
for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
{
	nPosition_OfVecTempf = nPositionsOfVecsArrf[iVecf];

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n Inside the loop for shuffling: iVecf = %d, nPosition_OfVecTempf = %d", iVecf, nPosition_OfVecTempf);
	if (nPosition_OfVecTempf < 6)
	{
		fprintf(fout, "\n\n 3: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E", fFeas_All_Arrf[4], fFeas_All_Arrf[5]);
	} // if (nPosition_OfVecTempf < 6)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 3_1: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E, iVecf = %d, nPosition_OfVecTempf = %d", fFeas_All_Arrf[4], fFeas_All_Arrf[5], iVecf, nPosition_OfVecTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

///////////////////////////////////////
	nResf = Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
		nDimf, //const int nDimf,
			nNumOfVecsTotf, //const int nNumOfVecsTotf,
		
		nPosition_OfVecTempf, //const int nVecf,

		fFeas_All_Arrf, //const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

		fFeasOf_OneVec_1Arrf); // float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs' by 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs' 1 at iVecf = %d", iVecf);
		fprintf(fout, "\n\n  An error in  'Shuffle_OneDim_Int_And_2Dim_Float_Arrs' by 'Extracting_A_FloatVec_From_2DimArrOf_AllVecs' 1 at iVecf = %d", iVecf);
		getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nPositionsOfVecsArrf;
		delete[] nY_TempArrf;

		delete[] fFeasOf_OneVec_1Arrf;
		delete[] fFeas_All_TempArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)


#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 3_2:  fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E, iVecf = %d, nPosition_OfVecTempf = %d", fFeas_All_Arrf[4], fFeas_All_Arrf[5], iVecf, nPosition_OfVecTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n Shuffling: iVecf = %d, nPosition_OfVecTempf = %d", iVecf, nPosition_OfVecTempf);

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fprintf(fout, "\n iVecf = %d, fFeasOf_OneVec_1Arrf[%d] = %E", iVecf, iFeaf, fFeasOf_OneVec_1Arrf[iFeaf]);
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS


#ifndef COMMENT_OUT_ALL_PRINTS
	//fprintf(fout, "\n\n 3_3: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E, iVecf = %d, nPosition_OfVecTempf = %d", fFeas_All_Arrf[4], fFeas_All_Arrf[5], iVecf, nPosition_OfVecTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
//////////////////////////////////

	nResf = Writing_OneDimVec_To_2DimVec(
		nDimf, //const int nDimf,
		nNumOfVecsTotf, //const int nNumOfVecsTotf,

		iVecf, //const int nVecf,

		fFeasOf_OneVec_1Arrf, //const float fFeas_OneVec_Arrf[], //[nDim_D_WithConst]

		fFeas_All_TempArrf); // float fFeas_All_Arrf[]); //[nDimf*nNumOfVecsTotf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs' by 'Writing_OneDimVec_To_2DimVec' at iVecf = %d", iVecf);
		fprintf(fout, "\n\n  An error in  'Shuffle_OneDim_Int_And_2Dim_Float_Arrs' by 'Writing_OneDimVec_To_2DimVec' at iVecf = %d", iVecf);
		getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nPositionsOfVecsArrf;
		delete[] nY_TempArrf;

		delete[] fFeasOf_OneVec_1Arrf;
		delete[] fFeas_All_TempArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)
///////////////////////////
	
#ifndef COMMENT_OUT_ALL_PRINTS
	//fprintf(fout, "\n\n 3_4: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E, iVecf = %d, nPosition_OfVecTempf = %d", fFeas_All_Arrf[4], fFeas_All_Arrf[5], iVecf, nPosition_OfVecTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

}//for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

Copying_Float_Arr1_To_Arr2(
	nSizeOf_2DimVecf, //const int nDimf,

	fFeas_All_TempArrf, //const float fArr1f[], // [nDimf]
	fFeas_All_Arrf); // float fArr2f[]); // [nDimf];

#ifndef COMMENT_OUT_ALL_PRINTS
 //fprintf(fout, "\n\n 3_5: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E, iVecf = %d, nPosition_OfVecTempf = %d", fFeas_All_Arrf[4], fFeas_All_Arrf[5], iVecf, nPosition_OfVecTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/*
for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
{
	nPositionsOfVecsArrf[iFeaf] = iFeaf;
} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

*/
//[nDimf*nVecTotf]
delete [] nPositionsOfVecsArrf;
delete[] nY_TempArrf;

delete[] fFeasOf_OneVec_1Arrf;
delete[] fFeas_All_TempArrf;

return SUCCESSFUL_RETURN;
}//int Shuffle_OneDim_Int_And_2Dim_Float_Arrs(...
///////////////////////////////////////////////////////////////////////////////////

int Preparing_Data_For_PasAggMaxOut_TrainTest(

	const int nNumVecTrainTotf,
	const int nNumVecTestTotf,
	/////////////////////////
	const int nDimf, //for reading	//const int nDimf, // = dimension of the original space

///////////////////////////////////////////////////
	//float fFea_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], 
	float fFea_Train_Arrf[], //[nProdTrain_DifEvoTot], 
	int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	//float fFea_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
	float fFea_Test_Arrf[], //[nProdTest_DifEvoTot],
	int nY_Test_Actual_Arrf[])  //[nNumVecTestTot],
{
	///////////////////////////////////////////////////
	int Reading_All_TrainTest_Data(
		const int nDim_D_SelecFeasf,

		const int nNumVecTrainTotf,
		const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		int nY_Train_Arrf[], //[nNumVecTrainTot]
		int nY_Test_Arrf[], //[nNumVecTestTot]

		float fFeaTrain_Arrf[], //[nProdTrainTot]
		float fFeaTest_Arrf[]); //[nProdTestTot]

	
	int Shuffle_OneDim_Int_And_2Dim_Float_Arrs(
		const int nDimf,
		const int nNumOfVecsTotf,

		int nY_Arrf[], // [nNumOfVecsTotf]
		float fFeas_All_Arrf[]); //[//[nDimf*nNumOfVecsTotf]]


	int Normalizing_Every_Fea_To_Mean_0_And_StDev_1(
		const float fLargef,
		const float fepsf,

		const int nDimf,

		const int nVecTrainf,
		const int nVecTestf,
		/////////////////////////////////////////////
		float fFeaMin_TrainArrf[],
		float fFeaMax_TrainArrf[],

		float fFea_TrainArrf[], //to be normalized
		float fFea_TestArrf[]); //to be normalized using the train Mean and StDev

#ifdef CALCULATING_Z_SCORES_OF_ALL_FEAS
	void Z_Scores_ForAllFeas(
		const int nDimf,

		const int nNumVecTrainTotf,
		const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		const float fFeaTrain_Arrf[], //[nProdTrainTot]
		const float fFeaTest_Arrf[],

		const float fZ_ScoreLimitf,

		float &fZ_ScoreMaxf,

		int &nNumOfFeasWithZ_ScoreAboveLimitf,

		float fZ_ScoresArrf[]); //[nDimf]

#endif //#ifdef CALCULATING_Z_SCORES_OF_ALL_FEAS

	int
		iVecf,
		iFeaf,

		//nNumOfActualFeasf = nDim_D_SelecFeas_WithConstf - 1,

		nIndexf,
		nIndexWithConstf,

		nTempf,
		nTempWithConstf,

		nResf;

	float
		fFeaMin_TrainArrf[nDim_DifEvo],
		fFeaMax_TrainArrf[nDim_DifEvo];

		//fFeaTrain_Arrf[nProdTrain_DifEvoTot], //[]
		//fFeaTest_Arrf[nProdTest_DifEvoTot];

		////////////////////////////////////////////////

	nResf = Reading_All_TrainTest_Data(
		nDimf, //const int nDim_D_SelecFeasf, // dimension of original feature space

		nNumVecTrainTotf, //const int nNumVecTrainTotf,
		nNumVecTestTotf, //const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		nY_Train_Actual_Arrf, //int nY_Train_Arrf[], //[nNumVecTrainTot]
		nY_Test_Actual_Arrf, //int nY_Test_Arrf[], //[nNumVecTestTot]

		fFea_Train_Arrf, //float fFeaTrain_Arrf[], //[nProdTrainTot]
		fFea_Test_Arrf); // float fFeaTest_Arrf[]); //[nProdTestTot]

	//printf("\n\n After 'Reading_All_TrainTest_Data'");

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Preparing_Data_For_PasAggMaxOut_TrainTest' by 'Reading_All_TrainTest_Data'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'Preparing_Data_For_PasAggMaxOut_TrainTest' by 'Reading_All_TrainTest_Data'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	if (nDimf != nDim_DifEvo)
	{
		printf("\n\n An error in 'Preparing_Data_For_PasAggMaxOut_TrainTest': nDimf = %d != nDim_DifEvo = %d", nDimf,nDim_DifEvo);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in ''Preparing_Data_For_PasAggMaxOut_TrainTest': nDimf = %d != nDim_DifEvo = %d", nDimf, nDim_DifEvo);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;

	} //if (nDimf != nDim_DifEvo)

#ifdef CALCULATING_Z_SCORES_OF_ALL_FEAS

	float
		//fZ_ScoresArrf[nDim_D],
		fZ_ScoresArrf[nDim_DifEvo],
		fZ_ScoreMaxf;

	int
		nNumOfFeasWithZ_ScoreAboveLimit;

	Z_Scores_ForAllFeas(
		nDim_DifEvo, //const int nDimf,

		nNumVecTrainTot, //const int nNumVecTrainTotf,
		nNumVecTestTot, //const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		fFea_Train_Arrf, //const float fFeaTrain_Arrf[], //[nProdTrainTot]
		fFea_Test_Arrf, //const float fFeaTest_Arrf[],

		fZ_ScoreLimit, //const float fZ_ScoreLimitf,

		fZ_ScoreMaxf, //float &fZ_ScoreMaxf,

		nNumOfFeasWithZ_ScoreAboveLimit, //int &nNumOfFeasWithZ_ScoreAboveLimitf,

		fZ_ScoresArrf); // float fZ_ScoresArrf[]); //[nDimf]

	printf("\n\nPlease press any key to continue: "); getchar();

#endif //#ifdef CALCULATING_Z_SCORES_OF_ALL_FEAS


//shuffling train vectors only -- not test ones
	nResf = Shuffle_OneDim_Int_And_2Dim_Float_Arrs(
		nDimf, //const int nDimf,
		nNumVecTrainTotf, //const int nNumOfVecsTotf,

		nY_Train_Actual_Arrf, //int nY_Arrf[], // [nNumOfVecsTotf]
		fFea_Train_Arrf); // float fFeas_All_Arrf[]); //[//[nDimf*nNumOfVecsTotf]]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Preparing_Data_For_PasAggMaxOut_TrainTest' by 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout,"\n\n An error in 'Preparing_Data_For_PasAggMaxOut_TrainTest' by 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs'");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n The train vectors: fFeaConst_Glob = %E", fFeaConst_Glob);

	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		//nTempf = iVecf* nDim_D_SelecFeas_WithConstf;
		nTempf = iVecf * nDim_D_SelecFeasf;

		fprintf(fout, "\n (train) fFeaConst_Glob = %E, iVecf = %d, ", fFeaConst_Glob, iVecf);
		for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFea_Train_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)

		fprintf(fout, ", nY_Train_Actual_Arrf[%d] = %d, ", iVecf, nY_Train_Actual_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

////////////////////////////
	fprintf(fout, "\n\n The test vectors: fFeaConst_Glob = %E", fFeaConst_Glob);
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		//		nTempf = iVecf * nDim_D_SelecFeas_WithConstf;
		nTempf = iVecf * nDim_D_SelecFeasf;

		fprintf(fout, "\n (test) fFeaConst_Glob = %E, iVecf = %d, ", fFeaConst_Glob, iVecf);
		//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
		for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFea_Test_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)

		fprintf(fout, ", nY_Test_Actual_Arrf[%d] = %d", iVecf, nY_Test_Actual_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)

	//printf("\n\n After 'Reading_All_TrainTest_Data': please press any key"); fflush(fout);  getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifdef USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1

	nResf = Normalizing_Every_Fea_To_Mean_0_And_StDev_1(
		fLarge, //const float fLargef,
		eps, //const float fepsf,

		nDimf, //const int nDimf,

		nNumVecTrainTotf, //const int nVecTrainf,
		nNumVecTestTotf, //const int nVecTestf,
		/////////////////////////////////////////////
		fFeaMin_TrainArrf, //float fFeaMin_TrainArrf[],
		fFeaMax_TrainArrf, //float fFeaMax_TrainArrf[],

		fFea_Train_Arrf, //float fFea_TrainArrf[], //to be normalized
		fFea_Test_Arrf); // float fFea_TestArrf[]) //to be normalized using the train Mean and StDev


#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout, "\n\n The train vectors after normalization to mean 0 and stdev 1, fFeaConst_Glob = %E", fFeaConst_Glob);

	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		nTempf = iVecf * nDim_D_SelecFeasf;
		fprintf(fout, "\n fFeaConst_Glob = %E, normalized train iVecf = %d, ", fFeaConst_Glob, iVecf);
		for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFeaTrain_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)

		fprintf(fout, " nY_Train_Arrf[%d] = %d, ", iVecf, nY_Train_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

////////////////////////////
	fprintf(fout, "\n\n The test vectors after normalization to mean 0 and stdev 1, fFeaConst_Glob = %E", fFeaConst_Glob);
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		//nTempf = iVecf * nDim_D_SelecFeas_WithConstf;
		nTempf = iVecf * nDim_D_SelecFeasf;
		fprintf(fout, "\n fFeaConst_Glob = %E, normalized test iVecf = %d, ", fFeaConst_Glob, iVecf);
		//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
		for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFeaTest_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)

		fprintf(fout, " nY_Test_Actual_Arrf[%d] = %d", iVecf, nY_Test_Actual_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)

//printf("\n\n After 'Reading_All_TrainTest_Data': please press any key"); fflush(fout);  getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#endif //#ifdef USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1
///////////////////////////////////////////////////////

/*
//including the const into the fea vector
	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		nTempf = iVecf * nDim_D_SelecFeasf;
		nTempWithConstf = iVecf * nDim_D_SelecFeas_WithConstf;

		for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
		{
			nIndexWithConstf = iFeaf + nTempWithConstf;
			if (iFeaf < nNumOfActualFeasf)
			{
				nIndexf = iFeaf + nTempf;

				fFea_WithConstTrain_Arrf[nIndexWithConstf] = fFeaTrain_Arrf[nIndexf];
			} // if (iFeaf < nNumOfActualFeasf)
			else
			{
				fFea_WithConstTrain_Arrf[nIndexWithConstf] = fFeaConst_Glob;
				//fprintf(fout, "\n Init const (train): fFea_WithConstTrain_Arrf[%d] = %E, iFeaf = %d, iVecf = %d", nIndexWithConstf, fFea_WithConstTrain_Arrf[nIndexWithConstf], iFeaf, iVecf);

			} //else	

		} // for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)

		//fprintf(fout, " nY_Train_Arrf[%d] = %d, ", iVecf, nY_Train_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

////////////////////////////
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		nTempf = iVecf * nDim_D_SelecFeasf;
		nTempWithConstf = iVecf * nDim_D_SelecFeas_WithConstf;

		for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
		{
			nIndexWithConstf = iFeaf + nTempWithConstf;

			if (iFeaf < nNumOfActualFeasf)
			{
				nIndexf = iFeaf + nTempf;

				fFea_WithConstTest_Arrf[nIndexWithConstf] = fFeaTest_Arrf[nIndexf];
			} // if (iFeaf < nNumOfActualFeasf)
			else
			{
				fFea_WithConstTest_Arrf[nIndexWithConstf] = fFeaConst_Glob;
				//	fprintf(fout, "\n Init const (test): fFea_WithConstTest_Arrf[%d] = %E, iFeaf = %d, iVecf = %d", nIndexWithConstf, fFea_WithConstTest_Arrf[nIndexWithConstf], iFeaf, iVecf);

			} //else	

		} // for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)

	}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
*/

	return SUCCESSFUL_RETURN;
}//int Preparing_Data_For_PasAggMaxOut_TrainTest(...
///////////////////////////////////////////////////////////////////////////////////

int Reading_Data_For_TrainTest_NoShuffleNormal(

	const int nNumVecTrainTotf,
	const int nNumVecTestTotf,
	/////////////////////////
	const int nDimf, //for reading	//const int nDimf, // = dimension of the original space == nDim_DifEvo

///////////////////////////////////////////////////
	//float fFea_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], 
	float fFea_Train_Arrf[], //[nProdTrain_DifEvoTot], 
	int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	//float fFea_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
	float fFea_Test_Arrf[], //[nProdTest_DifEvoTot],
	int nY_Test_Actual_Arrf[])  //[nNumVecTestTot],
{
	///////////////////////////////////////////////////
	int Reading_All_TrainTest_Data(
		const int nDim_D_SelecFeasf,

		const int nNumVecTrainTotf,
		const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		int nY_Train_Arrf[], //[nNumVecTrainTot]
		int nY_Test_Arrf[], //[nNumVecTestTot]

		float fFeaTrain_Arrf[], //[nProdTrainTot]
		float fFeaTest_Arrf[]); //[nProdTestTot]

#ifdef CALCULATING_Z_SCORES_OF_ALL_FEAS
	void Z_Scores_ForAllFeas(
		const int nDimf,

		const int nNumVecTrainTotf,
		const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		const float fFeaTrain_Arrf[], //[nProdTrainTot]
		const float fFeaTest_Arrf[],

		const float fZ_ScoreLimitf,

		float &fZ_ScoreMaxf,

		int &nNumOfFeasWithZ_ScoreAboveLimitf,

		float fZ_ScoresArrf[]); //[nDimf]

#endif //#ifdef CALCULATING_Z_SCORES_OF_ALL_FEAS

	int
		iVecf,
		iFeaf,

		//nNumOfActualFeasf = nDim_D_SelecFeas_WithConstf - 1,

		nIndexf,
		nIndexWithConstf,

		nTempf,
		nTempWithConstf,

		nResf;

	float
		fFeaMin_TrainArrf[nDim_DifEvo],
		fFeaMax_TrainArrf[nDim_DifEvo];

	//fFeaTrain_Arrf[nProdTrain_DifEvoTot], //[]
	//fFeaTest_Arrf[nProdTest_DifEvoTot];

	////////////////////////////////////////////////

	nResf = Reading_All_TrainTest_Data(
		nDimf, //const int nDim_D_SelecFeasf, // dimension of original feature space

		nNumVecTrainTotf, //const int nNumVecTrainTotf,
		nNumVecTestTotf, //const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		nY_Train_Actual_Arrf, //int nY_Train_Arrf[], //[nNumVecTrainTot]
		nY_Test_Actual_Arrf, //int nY_Test_Arrf[], //[nNumVecTestTot]

		fFea_Train_Arrf, //float fFeaTrain_Arrf[], //[nProdTrainTot]
		fFea_Test_Arrf); // float fFeaTest_Arrf[]); //[nProdTestTot]

	//printf("\n\n After 'Reading_All_TrainTest_Data'");

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Reading_Data_For_TrainTest_NoShuffleNormal' by 'Reading_All_TrainTest_Data'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'Reading_Data_For_TrainTest_NoShuffleNormal' by 'Reading_All_TrainTest_Data'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	if (nDimf != nDim_DifEvo)
	{
		printf("\n\n An error in 'Reading_Data_For_TrainTest_NoShuffleNormal': nDimf = %d != nDim_DifEvo = %d", nDimf, nDim_DifEvo);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in ''Reading_Data_For_TrainTest_NoShuffleNormal': nDimf = %d != nDim_DifEvo = %d", nDimf, nDim_DifEvo);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;

	} //if (nDimf != nDim_DifEvo)

#ifdef CALCULATING_Z_SCORES_OF_ALL_FEAS

	float
		//fZ_ScoresArrf[nDim_D],
		fZ_ScoresArrf[nDim_DifEvo],
		fZ_ScoreMaxf;

	int
		nNumOfFeasWithZ_ScoreAboveLimit;

	Z_Scores_ForAllFeas(
		nDim_DifEvo, //const int nDimf,

		nNumVecTrainTot, //const int nNumVecTrainTotf,
		nNumVecTestTot, //const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		fFea_Train_Arrf, //const float fFeaTrain_Arrf[], //[nProdTrainTot]
		fFea_Test_Arrf, //const float fFeaTest_Arrf[],

		fZ_ScoreLimit, //const float fZ_ScoreLimitf,

		fZ_ScoreMaxf, //float &fZ_ScoreMaxf,

		nNumOfFeasWithZ_ScoreAboveLimit, //int &nNumOfFeasWithZ_ScoreAboveLimitf,

		fZ_ScoresArrf); // float fZ_ScoresArrf[]); //[nDimf]

	printf("\n\nPlease press any key to continue: "); getchar();

#endif //#ifdef CALCULATING_Z_SCORES_OF_ALL_FEAS

//#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n The train vectors in 'Reading_Data_For_TrainTest_NoShuffleNormal': nNumVecTrainTotf = %d", nNumVecTrainTotf);

	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		//nTempf = iVecf* nDim_D_SelecFeas_WithConstf;
		nTempf = iVecf * nDim_DifEvo;

		fprintf(fout, "\n (train) iVecf = %d, ", iVecf);
		for (iFeaf = 0; iFeaf < nDim_DifEvo; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, "%d:%E ", iFeaf,fFea_Train_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_DifEvo; iFeaf++)

		fprintf(fout, "nY_Train_Actual_Arrf[%d] = %d", iVecf, nY_Train_Actual_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

////////////////////////////
	fprintf(fout, "\n\n The test vectors: in 'Reading_Data_For_TrainTest_NoShuffleNormal': nNumVecTestTotf = %d", nNumVecTestTotf);
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		//		nTempf = iVecf * nDim_D_SelecFeas_WithConstf;
		nTempf = iVecf * nDim_DifEvo;

		fprintf(fout, "\n (test) iVecf = %d, ", iVecf);
		for (iFeaf = 0; iFeaf < nDim_DifEvo; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, "%d:%E ", iFeaf, fFea_Test_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_DifEvo; iFeaf++)

		fprintf(fout, "nY_Test_Actual_Arrf[%d] = %d", iVecf, nY_Test_Actual_Arrf[iVecf]); fflush(fout);
	}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	//fflush(fout);
	//printf("\n\n After 'Reading_All_TrainTest_Data': please press any key"); fflush(fout);  getchar();

//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

return SUCCESSFUL_RETURN;
}//int Reading_Data_For_TrainTest_NoShuffleNormal(...


void Converting_OneFeaVec_DifEvo_To_Positions_Of_Feas(
	const int nDim_DifEvof,
	const float fOneFeaVec_DifEvoArrf[], //[nDim_DifEvo]

	const float fFeaRangeMinf,
	const float fFeaRangeMaxf,
	/////////////////////////////////

	int &nNumOfActiveFeasTotf,
	int nPositOfFeasArrf[]) //[nDim_DifEvo]

{
	int
		nFeaCurf,
		iFeaf;

	float
		fDistBetween_Neighb_Feasf = (fFeaRangeMaxf - fFeaRangeMinf) / (float)(nDim_DifEvo);
///////////////////////////////////////////////

	nNumOfActiveFeasTotf = 0;

	for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
	{
		nFeaCurf = floor(fOneFeaVec_DifEvoArrf[iFeaf] / fDistBetween_Neighb_Feasf);

		if (nFeaCurf < 0 || nFeaCurf >= nDim_DifEvof)
		{
			nPositOfFeasArrf[iFeaf] = FEA_DISACTIVATED;
		} //if (nFeaCurf < 0 || nFeaCurf >= nDim_DifEvof)
		else
		{
			nNumOfActiveFeasTotf += 1;
			nPositOfFeasArrf[iFeaf] = nFeaCurf;
		} // else
			
	} //for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)

}//void Converting_OneFeaVec_DifEvo_To_Positions_Of_Feas(...
///////////////////////////////////////////////////////////////////////////////////

int FitnessOfOneFeaVec(
			const int nDim_DifEvof,
			const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

			const int nNumOfOneVecFromPopulf,
			const int nNumOfVecsInOnePopulTotf,

			//const float fOneFeaVec_DifEvoArrf[], //[nDim_DifEvo]
			const int nPositsOf_Popul_FeasArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

			const float fFeaConst_UnRefined_Initf, // == 2.0

			/////////////////////////////////////
			//train
			const int nNumOfItersOfTrainingTotf,
			const int nNumOfItersOfTraining_RefinedTotf,

			//after shuffling
			const float fFea_Train_DifEvo_Arrf[], //[nProdTrain_DifEvoTot], already normalized
			const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

			const int nNumVecTrainTotf, //== nVecTrainf,
			///////////////////////////////////////
			//test
			const float fFea_Test_DifEvo_Arrf[], //[nProdTest_DifEvoTot],
			const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

			const int nNumVecTestTotf, // == nVecTestf,

			/////////////////////////////////////

			const int nDim_D_SelecFeasf, //for reading

			const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
			const int nDim_Hf, //dimension of the nonlinear/transformed space

			const int nKf, //nNumOfHyperplanes
			//const int nDim_Uf, //(nDim_SelecFeas_WithConstf*nDim_H*nK)

			///////////////////////
			const float fAlphaf, // < 1.0
			const float fEpsilonf,
			const float fCrf,
			const float fCf,
	////////////////////////////

			int &nNumOfActiveFeasForOneFeaVecTotf,
			float &fFitnOfOneFeaVecf)
{
/*
	void Converting_OneFeaVec_DifEvo_To_Positions_Of_Feas(
		const int nDim_DifEvof,
		const float fOneFeaVec_DifEvoArrf[], //[nDim_DifEvo]

		const float fFeaRangeMinf,
		const float fFeaRangeMaxf,
		/////////////////////////////////

		int &nNumOfActiveFeasTotf,
		int nPositOfFeasArrf[]); //[nDim_DifEvo]
*/
	int Extracting_An_IntVec_From_2DimArrOf_AllVecs(
		const int nDimf,
		const int nNumOfVecsTotf,
		const int nNumOfOneVecf,

		const int nPosit_All_Arrf[], //[nDimf*nNumOfVecsTotf]

		int nPosit_OneVec_Arrf[]); //[nDimf]

	int Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas_And_WithConst(
		const int nDimf,
		const int nDimSelecf, // < nDimf

		const float fFeaConst_UnRefined_Initf,
		const int nNumVecf,

		const int nPosOfSelec_FeasArrf[], //[nDimSelecf]

		const float fVecArr[], //[nDimf*nNumVecf]
		float fVecSelec_WithConstArr[]); //[(nDimSelecf + 1)*nNumVecf]

	int doPasAggMaxOut_TrainTest_WithSelecFeas(
	//train
		const int nNumOfItersOfTrainingTotf,

		//after shuffling
		const float fFeasSelec_WithConstTrain_Arrf[], //[nProdSelec_WithConstTrainTot], already normalized
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		const int nNumVecTrainTotf, //== nVecTrainf,
	///////////////////////////////////////
	//test
		const float fFeasSelec_WithConstTest_Arrf[], //[nProdSelec_WithConstTestTot],
		const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		const int nNumVecTestTotf, // == nVecTestf,

		/////////////////////////////////////

		const int nDim_SelecFeasf, // == nNumOfSelecFeasTotCurf

		const int nDim_SelecFeas_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_SelecFeas_WithConstf*nDim_H*nK)

		///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
		///////////////////////////////////////////////////
//for model writing
		const int nNumOfSelecFeasTotCurf,
		const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]

		float &fWeightedTrainTest_EfficiencyMaxf,
		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults,
		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

	int doPasAggMaxOut_TrainTest_WithSelecFeas_Refined(
		//train
		const int nNumOfItersOfTraining_RefinedTotf,

		//after shuffling
//a const can be changed
			float fFeasSelec_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], already normalized
			const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

			const int nNumVecTrainTotf, //== nVecTrainf,
			///////////////////////////////////////
					//test
			//a const can be changed
			float fFeasSelec_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
			const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],


		const int nNumVecTestTotf, // == nVecTestf,

		/////////////////////////////////////

		const int nDim_SelecFeasf, //== nNumOfSelecFeasTotCurf

		const int nDim_SelecFeas_WithConstf, // <= dimension of the original space

		/*
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)
		*/
		///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
		///////////////////////////////////////////////////
			//for model writing
		const int nNumOfSelecFeasTotCurf,
		const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]

		float &fWeightedTrainTest_Efficiency_Refinedf,
		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults_Refined,
		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults_Refined);

	/////////////////////////////////////////////7
	int
		iFeaf,
		iVecf,
		nResf,

		nPosOfFeaf,
		nNumOfSelecFeasTotCurf,

		nProdTempf,
		nIndex_2Dimf,

		nDim_SelecFeasf, // == nNumOfSelecFeasTotCurf <= nDim_D_SelecFeasf
		nDim_SelecFeas_WithConstf, // = nNumOfSelecFeasTotCurf + 1, <= nDim_D_SelecFeas_WithConstf
		nProd_WithConstTrainTot_Curf,
		nProd_WithConstTestTot_Curf,

		nDim_Uf,

		nPosit_OneVec_Arrf[nDim_FeasInit];

	float
		fWeightedTrainTest_Efficiency_Refinedf;

	///////////////////////////////////
	nNumOfFitnessOfOneFeaVecTot_Glob += 1;

	fFitnOfOneFeaVecf = -fLarge; //
	nNumOfActiveFeasForOneFeaVecTotf = -1; //no active feas initially
///////////////////////
/*
	Converting_OneFeaVec_DifEvo_To_Positions_Of_Feas(
		nDim_DifEvof, //const int nDim_DifEvof,
		fOneFeaVec_DifEvoArrf, //const float fOneFeaVec_DifEvoArrf[], //[nDim_DifEvo]

		fFeaRangeMinf, //const float fFeaRangeMinf,
		fFeaRangeMaxf, //const float fFeaRangeMaxf,
		/////////////////////////////////

		nNumOfActiveFeasTotf, //int &nNumOfActiveFeasTotf,
		nPositOfFeasArrf); // int nPositOfFeasArrf[]); //[nDim_DifEvo]
*/
	
	nResf = Extracting_An_IntVec_From_2DimArrOf_AllVecs(
		nDim_FeasInitf, //const int nDimf,

		nNumOfVecsInOnePopulTotf, //const int nNumOfVecsTotf,

		nNumOfOneVecFromPopulf,//const int nNumOfOneVecf,

		nPositsOf_Popul_FeasArrf, //const int nPosit_All_Arrf[], //[nDimf*nNumOfVecsTotf]

		nPosit_OneVec_Arrf); // int nPosit_OneVec_Arrf[]) //[nDimf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'FitnessOfOneFeaVec' by 'Extracting_An_IntVec_From_2DimArrOf_AllVecs'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'FitnessOfOneFeaVec' by 'Extracting_An_IntVec_From_2DimArrOf_AllVecs'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)
	///////////////////////////////////////////////

	fprintf(fout, "\n\n 'FitnessOfOneFeaVec': the positions of selected feas, nNumOfOneVecFromPopulf = %d, nNumOfFitnessOfOneFeaVecTot_Glob = %d", 
		nNumOfOneVecFromPopulf, nNumOfFitnessOfOneFeaVecTot_Glob);
	for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)
	{
		fprintf(fout, "\n nPosit_OneVec_Arrf[%d] = %d", iFeaf, nPosit_OneVec_Arrf[iFeaf]);

	} //for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)
	//printf("\n\n Debugging'FitnessOfOneFeaVec: 1"); fflush(fout);  getchar();

	fprintf(fout, "\n\n nNumOfFitnessOfOneFeaVecTot_Glob = %d", nNumOfFitnessOfOneFeaVecTot_Glob);

	nNumOfSelecFeasTotCurf = 0;
	for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)
	{
		if (nPosit_OneVec_Arrf[iFeaf] == FEA_DISACTIVATED)
			continue;

		nNumOfSelecFeasTotCurf += 1;
	} //for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)

	if (nNumOfSelecFeasTotCurf < nDim_FeasInitf)
	{
		fprintf(fout, "\n\n The number of feas is less than initial one, nNumOfSelecFeasTotCurf = %d < nDim_FeasInitf = %d, iVec_Updating_Glob = %d, nNumOfGener_Glob = %d",
			nNumOfSelecFeasTotCurf, nDim_FeasInitf, iVec_Updating_Glob, nNumOfGener_Glob);

	} //if (nNumOfSelecFeasTotCurf < nDim_FeasInitf)
///////////////////////////////////////////////

	if (nNumOfSelecFeasTotCurf < nNumOfActiveFeasTot_Min)
	//if (nNumOfActiveFeasTotf < nDim_FeasInitf)
	{
		return NUMBER_OF_ACTIVE_FEAS_IS_INSUFFICIENT;
	} //if (nNumOfSelecFeasTotCurf < nNumOfActiveFeasTot_Min)

///////////////////////////////
	int  *nPosOfSelec_FeasArrf = new int[nNumOfSelecFeasTotCurf];
	if (nPosOfSelec_FeasArrf == NULL)
	{
		printf("\n\n An error in 'FitnessOfOneFeaVec': nPosOfSelec_FeasArrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'FitnessOfOneFeaVec': nPosOfSelec_FeasArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nPosOfSelec_FeasArrf == NULL)
/////////////////////

	fprintf(fout, "\n\ nNumOfFitnessOfOneFeaVecTot_Glob = %d", nNumOfFitnessOfOneFeaVecTot_Glob);
	nPosOfFeaf = 0;
	for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)
	{
		if (nPosit_OneVec_Arrf[iFeaf] == FEA_DISACTIVATED)
			continue;

		if (nPosit_OneVec_Arrf[iFeaf] < 0 || nPosit_OneVec_Arrf[iFeaf] >= nDim_DifEvof)
		{
			printf("\n\n An error in 'FitnessOfOneFeaVec': nPosit_OneVec_Arrf[%d] = %d, nDim_FeasInitf = %d", iFeaf,nPosit_OneVec_Arrf[iFeaf], nDim_FeasInitf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'FitnessOfOneFeaVec': nPosit_OneVec_Arrf[%d] = %d, nDim_FeasInitf = %d", iFeaf, nPosit_OneVec_Arrf[iFeaf], nDim_FeasInitf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			delete[] nPosOfSelec_FeasArrf;
			getchar();  exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (nPosit_OneVec_Arrf[iFeaf] < 0 || nPosit_OneVec_Arrf[iFeaf] >= nDim_DifEvof)
		
		nPosOfSelec_FeasArrf[nPosOfFeaf] = nPosit_OneVec_Arrf[iFeaf];

		//fprintf(fout, "\n nPosOfSelec_FeasArrf[%d] = %d", nPosOfFeaf,nPosOfSelec_FeasArrf[nPosOfFeaf]);

		nPosOfFeaf += 1;
	} //for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)

	//printf("\n\n Debugging'FitnessOfOneFeaVec: 2"); fflush(fout);  getchar();

	if (nPosOfFeaf != nNumOfSelecFeasTotCurf)
	{
		printf("\n\n An error in 'FitnessOfOneFeaVec': nPosOfFeaf = %d != nNumOfSelecFeasTotCurf = %d, nDim_DifEvof = %d", nPosOfFeaf, nNumOfSelecFeasTotCurf,  nDim_DifEvof);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'FitnessOfOneFeaVec': nPosOfFeaf = %d != nNumOfSelecFeasTotCurf = %d, nDim_DifEvof = %d", nPosOfFeaf, nNumOfSelecFeasTotCurf, nDim_DifEvof);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] nPosOfSelec_FeasArrf;

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nPosOfFeaf != nNumOfSelecFeasTotCurf)

////////////////////
	nProd_WithConstTrainTot_Curf = (nNumOfSelecFeasTotCurf + 1)*nNumVecTrainTotf;

	float *fFeasSelec_WithConstTrain_Arrf = new float[nProd_WithConstTrainTot_Curf];
	if (fFeasSelec_WithConstTrain_Arrf == NULL)
	{
		printf("\n\n An error in 'FitnessOfOneFeaVec': fFeasSelec_WithConstTrain_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'FitnessOfOneFeaVec': fFeasSelec_WithConstTrain_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] nPosOfSelec_FeasArrf;

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fFeasSelec_WithConstTrain_Arrf == NULL)
////////////////////
	nProd_WithConstTestTot_Curf = (nNumOfSelecFeasTotCurf + 1)*nNumVecTestTotf;

	float *fFeasSelec_WithConstTest_Arrf = new float[nProd_WithConstTestTot_Curf];
	if (fFeasSelec_WithConstTest_Arrf == NULL)
	{
		printf("\n\n An error in 'FitnessOfOneFeaVec': fFeasSelec_WithConstTest_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'FitnessOfOneFeaVec': fFeasSelec_WithConstTest_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] nPosOfSelec_FeasArrf;
		delete[] fFeasSelec_WithConstTrain_Arrf;

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fFeasSelec_WithConstTest_Arrf == NULL)

	//printf("\n\n Debugging'FitnessOfOneFeaVec: 3");

////////////////////////////////////
/*
	fprintf(fout, "\n\n Input training vecs in 'FitnessOfOneFeaVec':   = %E, nNumOfSelecFeasTotCurf = %d, nNumOfOneVecFromPopulf = %d", 
		fFeaConst_UnRefined_Initf, nNumOfSelecFeasTotCurf, nNumOfOneVecFromPopulf);
	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		nProdTempf = iVecf * nDim_DifEvof;

		fprintf(fout, "\n Input train: iVecf = %d, ", iVecf);
		for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
		{
			nIndex_2Dimf = iFeaf + nProdTempf;
			fprintf(fout, "%d:%E, ", iFeaf, fFea_Train_DifEvo_Arrf[nIndex_2Dimf]);

		} //for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
	} //for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

	fprintf(fout, "\n\n Input testing vecs in 'FitnessOfOneFeaVec': fFeaConst_UnRefined_Initf = %E, nNumOfSelecFeasTotCurf = %d, nNumOfOneVecFromPopulf = %d",
		fFeaConst_UnRefined_Initf, nNumOfSelecFeasTotCurf, nNumOfOneVecFromPopulf);
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		nProdTempf = iVecf * nDim_DifEvof;

		fprintf(fout, "\n Input test: iVecf = %d, ", iVecf);
		for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
		{
			nIndex_2Dimf = iFeaf + nProdTempf;
			fprintf(fout, "%d:%E, ", iFeaf, fFea_Test_DifEvo_Arrf[nIndex_2Dimf]);

		} //for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
	} //for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
*/

//train	
	nResf = Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas_And_WithConst(

		nDim_DifEvof, //const int nDimf,
		nNumOfSelecFeasTotCurf, //const int nDimSelecf, // < nDimf

		fFeaConst_UnRefined_Initf, //const float fFeaConst_UnRefined_Initf, // 2.0

		nNumVecTrainTotf, //const int nNumVecf,

		nPosOfSelec_FeasArrf, //const int nPosOfSelec_FeasArrf[], //[nDimSelecf]

		fFea_Train_DifEvo_Arrf, //const float fVecArr[], //[nDimf*nNumVecf]
		fFeasSelec_WithConstTrain_Arrf); // float fVecSelecArr[]); //[nDimSelecf*nNumVecf]
			
	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'FitnessOfOneFeaVec' by 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas' (train)");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'FitnessOfOneFeaVec' by 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas' (train)");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] nPosOfSelec_FeasArrf;
		delete[] fFeasSelec_WithConstTrain_Arrf;
		delete[] fFeasSelec_WithConstTest_Arrf;

		getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)
////////////////////////////////////////
	//printf("\n\n Debugging'FitnessOfOneFeaVec: 3"); fflush(fout);  getchar();

	nDim_SelecFeasf = nNumOfSelecFeasTotCurf;
	nDim_SelecFeas_WithConstf = nNumOfSelecFeasTotCurf + 1;

	nDim_Uf = nDim_SelecFeas_WithConstf * nDim_Hf*nKf;

	//if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66
	{
		fprintf(fout, "\n\n 'FitnessOfOneFeaVec', converted training vecs with selec feas, fFeaConst_UnRefined_Initf = %E, nNumOfSelecFeasTotCurf = %d, nNumOfOneVecFromPopulf = %d",
			fFeaConst_UnRefined_Initf, nNumOfSelecFeasTotCurf, nNumOfOneVecFromPopulf);
		for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
		{
			nProdTempf = iVecf * nDim_SelecFeas_WithConstf;

			fprintf(fout, "\n Train: iVecf = %d, ", iVecf);
			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				nIndex_2Dimf = iFeaf + nProdTempf;
				fprintf(fout, "%d:%E, ", iFeaf, fFeasSelec_WithConstTrain_Arrf[nIndex_2Dimf]);

			} //for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
		} //for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	} //if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66

/////////////////////////////

//test
	nResf = Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas_And_WithConst(

		nDim_DifEvof, //const int nDimf,
		nNumOfSelecFeasTotCurf, //const int nDimSelecf, // < nDimf

		fFeaConst_UnRefined_Initf, //const float fFeaConst_UnRefined_Initf,

		nNumVecTestTotf, //const int nNumVecf,

		nPosOfSelec_FeasArrf, //const int nPosOfSelec_FeasArrf[], //[nDimSelecf]

		fFea_Test_DifEvo_Arrf, //const float fVecArr[], //[nDimf*nNumVecf]

		fFeasSelec_WithConstTest_Arrf); // float fVecSelecArr[]); //[nDimSelecf*nNumVecf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'FitnessOfOneFeaVec' by 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas' (test)");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'FitnessOfOneFeaVec' by 'Converting_2DimFloatArr_To_2DimFloatArr_WithSelecFeas' (test)");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] nPosOfSelec_FeasArrf;
		delete[] fFeasSelec_WithConstTrain_Arrf;
		delete[] fFeasSelec_WithConstTest_Arrf;

		getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	//if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66
	{
//nDim_SelecFeas_WithConstf = nNumOfSelecFeasTotCurf + 1;

		fprintf(fout, "\n\n 'FitnessOfOneFeaVec': converted testing vecs with selec feas, fFeaConst_UnRefined_Initf = %E, nNumOfSelecFeasTotCurf = %d, nNumOfOneVecFromPopulf = %d",
			fFeaConst_UnRefined_Initf, nNumOfSelecFeasTotCurf, nNumOfOneVecFromPopulf);
		for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
		{
			nProdTempf = iVecf * nDim_SelecFeas_WithConstf;

			fprintf(fout, "\n Test: iVecf = %d, ", iVecf);
			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				nIndex_2Dimf = iFeaf + nProdTempf;
				fprintf(fout, "%d:%E, ", iFeaf, fFeasSelec_WithConstTest_Arrf[nIndex_2Dimf]);

			} //for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

		} //for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	} // if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66

		//printf("\n\n  'FitnessOfOneFeaVec: before 'doPasAggMaxOut_TrainTest_WithSelecFeas': please press any key to exit"); fflush(fout); getchar(); exit(1);

	  //////////////////////////////
	PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TrainResults;
	PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TestResults;

	//printf("\n\n Before 'doPasAggMaxOut_TrainTest_WithSelecFeas' in 'FitnessOfOneFeaVec': please press any key:"); getchar();
	nResf = doPasAggMaxOut_TrainTest_WithSelecFeas(
		//train
		nNumOfItersOfTrainingTotf, //const int nNumOfItersOfTrainingTotf,

		//after shuffling
		fFeasSelec_WithConstTrain_Arrf, //const float fFeasSelec_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], already normalized to (0,1)
		nY_Train_Actual_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		nNumVecTrainTotf, //const int nNumVecTrainTotf, //== nVecTrainf,
	///////////////////////////////////////
	//test
		fFeasSelec_WithConstTest_Arrf, //const float fFeasSelec_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
		nY_Test_Actual_Arrf, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		nNumVecTestTotf, //const int nNumVecTestTotf, // == nVecTestf,

		/////////////////////////////////////

		nDim_SelecFeasf, // == nNumOfSelecFeasTotCurf, //const int nDim_SelecFeasf, //

		//
		nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_Uf, //const int nDim_Uf, //(nDim_SelecFeas_WithConstf*nDim_H*nK)

		///////////////////////
		fAlphaf, //const float fAlphaf, // < 1.0
		fEpsilonf, //const float fEpsilonf,
		fCrf, //const float fCrf,
		fCf, //const float fCf,
		///////////////////////////////////////////////////
		//for model writing
		nNumOfSelecFeasTotCurf, //const int nNumOfSelecFeasTotCurf,
		nPosOfSelec_FeasArrf, //const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]

		fFitnOfOneFeaVecf, //float &fWeightedTrainTest_EfficiencyMaxf,

		&sPasAggMaxOut_TrainResults, //PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults,
		&sPasAggMaxOut_TestResults); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'FitnessOfOneFeaVec' by 'doPasAggMaxOut_TrainTest_WithSelecFeas'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'FitnessOfOneFeaVec' by 'doPasAggMaxOut_TrainTest_WithSelecFeas'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] nPosOfSelec_FeasArrf;
		delete[] fFeasSelec_WithConstTrain_Arrf;
		delete[] fFeasSelec_WithConstTest_Arrf;

		getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

//////////////////////////////////
	nNumOfActiveFeasForOneFeaVecTotf = nNumOfSelecFeasTotCurf;

	printf("\n\n 'FitnessOfOneFeaVec: after 'doPasAggMaxOut_TrainTest_WithSelecFeas', nNumOfOneVecFromPopulf = %d, fFitnOfOneFeaVecf = %E",
		nNumOfOneVecFromPopulf, fFitnOfOneFeaVecf);
	
	fprintf(fout, "\n\n 'FitnessOfOneFeaVec: after 'doPasAggMaxOut_TrainTest_WithSelecFeas', nNumOfOneVecFromPopulf = %d, fFitnOfOneFeaVecf = %E",
		nNumOfOneVecFromPopulf, fFitnOfOneFeaVecf);
//sPasAggMaxOut_TestResults. and sPasAggMaxOut_TrainResults. corresponding to 'fFitnOfOneFeaVecf' are given inside 'doPasAggMaxOut_TrainTest_WithSelecFeas'


#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY
///////////////////////////////////////////////////////////////////////
	//fFitnOfOneFeaVecf = ((sPasAggMaxOut_TrainResults.fPercentageOfCorrect_Positf + sPasAggMaxOut_TrainResults.fPercentageOfCorrect_Negatf)*fWeight_Train) +

		//((sPasAggMaxOut_TestResults.fPercentageOfCorrect_Positf + sPasAggMaxOut_TestResults.fPercentageOfCorrect_Negatf)*fWeight_Test);

#endif //#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY
	
#ifndef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

	fFitnOfOneFeaVecf = sPasAggMaxOut_TrainResults.fPercentageOfCorrect_Positf + sPasAggMaxOut_TrainResults.fPercentageOfCorrect_Negatf +
		sPasAggMaxOut_TestResults.fPercentageOfCorrect_Positf + sPasAggMaxOut_TestResults.fPercentageOfCorrect_Negatf;
#endif //#ifndef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

	printf("\n\n The end of 'FitnessOfOneFeaVec' (before the possible refinement): fFitnOfOneFeaVecf = %E, nNumOfFitnessOfOneFeaVecTot_Glob = %d, nNumOfGener_Glob = %d", 
		fFitnOfOneFeaVecf, nNumOfFitnessOfOneFeaVecTot_Glob, nNumOfGener_Glob);	

	printf( "\n\n The constants: nNumOfVecsInOnePopulTot = %d, nNumOfGenerTot = %d",nNumOfVecsInOnePopulTot, nNumOfGenerTot);

	printf( "\n\n So far: fPercentageOfCorrect_NoRefin_Glob = %E, fPercentageOfCorrect_Refined_Max_Glob = %E, nNumOfGener_Glob = %d", 
		fPercentageOfCorrect_NoRefin_Glob,fPercentageOfCorrect_Refined_Max_Glob, nNumOfGener_Glob);

	fprintf(fout, "\n\n The end of 'FitnessOfOneFeaVec' (before the possible refinement): fFitnOfOneFeaVecf = %E, nNumOfFitnessOfOneFeaVecTot_Glob = %d, nNumOfGener_Glob = %d",
		fFitnOfOneFeaVecf, nNumOfFitnessOfOneFeaVecTot_Glob, nNumOfGener_Glob);
	
	fprintf(fout, "\n\n  The constants: nNumOfVecsInOnePopulTot = %d, nNumOfGenerTot = %d", nNumOfVecsInOnePopulTot, nNumOfGenerTot);

	fprintf(fout, "\n\n So far: fPercentageOfCorrect_NoRefin_Glob = %E, fPercentageOfCorrect_Refined_Max_Glob = %E, nNumOfGener_Glob = %d", 
		fPercentageOfCorrect_NoRefin_Glob, fPercentageOfCorrect_Refined_Max_Glob, nNumOfGener_Glob);

	fprintf(fout, "\n");
	for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)
	{
		printf("\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf, nPosOfSelec_FeasArrf[iFeaf]);
		//fprintf(fout, "\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf, nPosOfSelec_FeasArrf[iFeaf]);
	} //for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)
////////////////////////////////////////////////////////////////////

	PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TrainResults_Refinedf;
	PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TestResults_Refinedf;

	if (fFitnOfOneFeaVecf > fPercentageOfCorrect_ToApply_Refinement)
	{
		printf( "\n\n 'FitnessOfOneFeaVec': going to the refinement: fFitnOfOneFeaVecf = %E > fPercentageOfCorrect_ToApply_Refinement = %e, nNumOfFitnessOfOneFeaVecTot_Glob = %d, nNumOfGener_Glob = %d",
			fFitnOfOneFeaVecf, fPercentageOfCorrect_ToApply_Refinement, nNumOfFitnessOfOneFeaVecTot_Glob, nNumOfGener_Glob);

		fprintf(fout, "\n\n 'FitnessOfOneFeaVec': going to the refinement: fFitnOfOneFeaVecf = %E > fPercentageOfCorrect_ToApply_Refinement = %e, nNumOfFitnessOfOneFeaVecTot_Glob = %d, nNumOfGener_Glob = %d",
			fFitnOfOneFeaVecf, fPercentageOfCorrect_ToApply_Refinement, nNumOfFitnessOfOneFeaVecTot_Glob, nNumOfGener_Glob);

		//getchar();
		nResf = doPasAggMaxOut_TrainTest_WithSelecFeas_Refined(
			//train
			nNumOfItersOfTraining_RefinedTotf, //const int nNumOfItersOfTrainingTotf,

			//after shuffling
//a const can be changed
			fFeasSelec_WithConstTrain_Arrf, //float fFeasSelec_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], already normalized  to (0,1)
			nY_Train_Actual_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

			nNumVecTrainTotf, //const int nNumVecTrainTotf, //== nVecTrainf,
		///////////////////////////////////////
		//test
//a const can be changed
			fFeasSelec_WithConstTest_Arrf, //float fFeasSelec_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
			nY_Test_Actual_Arrf, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

			nNumVecTestTotf, //const int nNumVecTestTotf, // == nVecTestf,
			/////////////////////////////////////

			nDim_SelecFeasf, //const int nDim_SelecFeasf, //== nNumOfSelecFeasTotCurf

			//nDim_SelecFeas_WithConstf = nNumOfSelecFeasTotCurf + 1;
			nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf,  <= dimension of the original space

			/*
			const int nDim_Hf, //dimension of the nonlinear/transformed space

			const int nKf, //nNumOfHyperplanes
			const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)
			*/
			///////////////////////
			fAlphaf, //const float fAlphaf, // < 1.0
			fEpsilonf, //const float fEpsilonf,
			fCrf, //const float fCrf,
			fCf, //const float fCf,
			///////////////////////////////////////////////////
			//for model writing
			nNumOfSelecFeasTotCurf, //const int nNumOfSelecFeasTotCurf,
			nPosOfSelec_FeasArrf, //const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]

			fWeightedTrainTest_Efficiency_Refinedf, //float &fWeightedTrainTest_Efficiency_Refinedf,
			&sPasAggMaxOut_TrainResults_Refinedf, //PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults_Refined,
			&sPasAggMaxOut_TestResults_Refinedf); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults_Refined)

		if (fFitnOfOneFeaVecf >= fWeightedTrainTest_Efficiency_Refinedf)
		{
			printf("\n\n An unexpected refined fitness in'FitnessOfOneFeaVec'");

			printf("\n fFitnOfOneFeaVecf = %E >= fWeightedTrainTest_Efficiency_Refinedf = %E,  nNumOfFitnessOfOneFeaVecTot_Glob = %d, nNumOfGener_Glob = %d",
				fFitnOfOneFeaVecf, fWeightedTrainTest_Efficiency_Refinedf, nNumOfFitnessOfOneFeaVecTot_Glob, nNumOfGener_Glob);

			fprintf(fout, "\n\n An unexpected refined fitness in'FitnessOfOneFeaVec'");

			fprintf(fout, "\n fFitnOfOneFeaVecf = %E >= fWeightedTrainTest_Efficiency_Refinedf = %E,  nNumOfFitnessOfOneFeaVecTot_Glob = %d, nNumOfGener_Glob = %d",
				fFitnOfOneFeaVecf, fWeightedTrainTest_Efficiency_Refinedf, nNumOfFitnessOfOneFeaVecTot_Glob, nNumOfGener_Glob);
			printf("\n\nPlease press any key to continue"); getchar();
		} //if (fFitnOfOneFeaVecf >= fWeightedTrainTest_Efficiency_Refinedf)
		else
		{
/*
			printf( "\n\n Adjusting an initial fitness fFitnOfOneFeaVecf = %E to fWeightedTrainTest_Efficiency_Refinedf = %E in'FitnessOfOneFeaVec'",
				fFitnOfOneFeaVecf, fWeightedTrainTest_Efficiency_Refinedf);
			fprintf(fout, "\n\n Adjusting an initial fitness fFitnOfOneFeaVecf = %E to fWeightedTrainTest_Efficiency_Refinedf = %E in'FitnessOfOneFeaVec'",
				fFitnOfOneFeaVecf, fWeightedTrainTest_Efficiency_Refinedf);

			fFitnOfOneFeaVecf = fWeightedTrainTest_Efficiency_Refinedf;
*/
			printf( "\n\n An initial fitness fFitnOfOneFeaVecf = %E is less than fWeightedTrainTest_Efficiency_Refinedf = %E in 'FitnessOfOneFeaVec'",
				fFitnOfOneFeaVecf, fWeightedTrainTest_Efficiency_Refinedf);
			fprintf(fout, "\n\n An initial fitness fFitnOfOneFeaVecf = %E is less than fWeightedTrainTest_Efficiency_Refinedf = %E in 'FitnessOfOneFeaVec'",
				fFitnOfOneFeaVecf, fWeightedTrainTest_Efficiency_Refinedf);

		} //else

	} //if (fFitnOfOneFeaVecf > fPercentageOfCorrect_ToApply_Refinement)

	fflush(fout);
		//printf("\n\n The end of 'FitnessOfOneFeaVec': fFitnOfOneFeaVecf = %E, please press any key to exit", fFitnOfOneFeaVecf);	getchar(); exit(1);
	//printf("\n\n The end of 'FitnessOfOneFeaVec': fFitnOfOneFeaVecf = %E, please press any key to continue", fFitnOfOneFeaVecf);	getchar(); 

	delete[] nPosOfSelec_FeasArrf;
	delete[] fFeasSelec_WithConstTrain_Arrf;
	delete[] fFeasSelec_WithConstTest_Arrf;

	return SUCCESSFUL_RETURN;
} //int FitnessOfOneFeaVec(...
///////////////////////////////////////////////////////////////////////////////

int doPasAggMaxOut_TrainTest_WithSelecFeas(
//train
	const int nNumOfItersOfTrainingTotf,

	//after shuffling
	const float fFeasSelec_WithConstTrain_Arrf[], //[nProdSelec_WithConstTrainTot], already normalized
	const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	const int nNumVecTrainTotf, //== nVecTrainf,
///////////////////////////////////////
//test
	const float fFeasSelec_WithConstTest_Arrf[], //[nProdSelec_WithConstTestTot],
	const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

	const int nNumVecTestTotf, // == nVecTestf,
	
	/////////////////////////////////////
	
	const int nDim_SelecFeasf, //== nNumOfSelecFeasTotCurf

	const int nDim_SelecFeas_WithConstf, // == nDim_SelecFeas_WithConstf <= dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	const int nDim_Uf, //(nDim_SelecFeas_WithConstf*nDim_H*nK)

	///////////////////////
	const float fAlphaf, // < 1.0
	const float fEpsilonf,
	const float fCrf,
	const float fCf,
	///////////////////////////////////////////////////
		//for model writing
	const int nNumOfSelecFeasTotCurf,
	const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]

	float &fWeightedTrainTest_EfficiencyMaxf,
	PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults,
	PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults)
{
	///////////////////////////////////////////////////
	int PasAggMaxOut_Train(
		const int nNumOfItersOfTrainingTotf,

		//after shuffling
		const float fFeasSelec_WithConstTrain_Arrf[], //[nProdSelec_WithConstTrainTot], already normalized to (0,1)
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	//const int nDimf, == nDim_D_SelecFeas_WithConstf

		const int nVecTrainf,
		/////////////////////////

		const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_SelecFeas_WithConstf*nDim_H*nK)

		///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
		///////////////////////////////////////////////////
		float fW_Arrf[],
		float fU_Arrf[], //[nDim_U_Glob],

		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults);
/////////////////

	int PasAggMaxOut_Test(
		const float fFeasSelec_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
		const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		//const int nDimf, == nDim_D_SelecFeas_WithConstf

		const int nVecTestf,

		/////////////////////////
		const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_SelecFeas_WithConstf*nDim_H*nK)

		///////////////////////
		const float fW_Train_Arrf[], //[nDim_Hf]
		const float fU_Train_Arrf[], //[nDim_U_Glob],
		///////////////////////////////////////////////////

		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);
////////////////////////

	void WritingAn_UnRefinedModel_WithSelecFeas(
		const float fPercentageOfCorrectTot_Trainf,
		const float fPercentageOfCorrectTot_Testf,

		const float fConstf,

		/////////////////////////
		const int nNumOfSelecFeasTotCurf,
		const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
	//////////////////////

		const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		const float fW_Train_Arrf[], //[nDim_Hf]
		const float fU_Train_Arrf[]); //[nDim_U_Glob],

	int
		iVecf,
		iFeaf,

		nNumOfActualFeasf = nDim_SelecFeas_WithConstf - 1,

		nIndexf,
		nIndexWithConstf,

		nTempf,
		nTempWithConstf,

		iSrandInitf,
		nResf;

	float
		fWeightedTrainTest_Efficiencyf;
		//fFeaMin_TrainArrf[nDim_D_WithConst],
		//fFeaMax_TrainArrf[nDim_D_WithConst];

///////////////////////////////////////

	nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob = 0; //no printing
///////////////////////////////

/////////////////////////////////////////////
	fWeightedTrainTest_EfficiencyMaxf = -fLarge;

	fU_Init_Min_Glob = fU_Init_Min;
		fU_Init_Max_Glob = fU_Init_Max;

		fW_Init_Min_Glob = fW_Init_Min;
		fW_Init_Max_Glob = fW_Init_Max;
////////////////////////
	float *fW_Train_Arrf = new float[nDim_Hf];
	if (fW_Train_Arrf == NULL)
	{
		printf("\n\n An error in 'doPasAggMaxOut_TrainTest_WithSelecFeas': fW_Train_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'doPasAggMaxOut_TrainTest_WithSelecFeas': fW_Train_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fW_Train_Arrf == NULL)

	float *fU_Train_Arrf = new float[nDim_Uf];
	if (fU_Train_Arrf == NULL)
	{
		printf("\n\n An error in 'doPasAggMaxOut_TrainTest_WithSelecFeas': fFeaVecCur_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'doPasAggMaxOut_TrainTest_WithSelecFeas': fFeaVecCur_Arrf == NULL");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] fW_Train_Arrf;

		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fU_Train_Arrf == NULL)

	////////////////////////////////////////////////
	
//#ifndef COMMENT_OUT_ALL_PRINTS
	//if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66
	{
		fprintf(fout, "\n\n 'doPasAggMaxOut_TrainTest_WithSelecFeas': the train vectors , fFeaConst_Glob = %E", fFeaConst_Glob);

		for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
		{
			//nTempf = iVecf* nDim_D_SelecFeas_WithConstf;
			nTempf = iVecf * nDim_SelecFeasf;

			fprintf(fout, "\n (train) fFeaConst_Glob = %E, iVecf = %d, ", fFeaConst_Glob, iVecf);
			for (iFeaf = 0; iFeaf < nDim_SelecFeasf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;

				fprintf(fout, "%d: %E", iFeaf, fFeasSelec_WithConstTrain_Arrf[nIndexf]);
			} // for (iFeaf = 0; iFeaf < nDim_SelecFeasf; iFeaf++)

			fprintf(fout, ", nY_Train_Actual_Arrf[%d] = %d, ", iVecf, nY_Train_Actual_Arrf[iVecf]);
		}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

		 ////////////////////////////
		fprintf(fout, "\n\n 'doPasAggMaxOut_TrainTest_WithSelecFeas': the test vectors, fFeaConst_Glob = %E", fFeaConst_Glob);
		for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
		{
			//		nTempf = iVecf * nDim_D_SelecFeas_WithConstf;
			nTempf = iVecf * nDim_SelecFeasf;

			fprintf(fout, "\n (test) fFeaConst_Glob = %E, iVecf = %d, ", fFeaConst_Glob, iVecf);
			//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
			for (iFeaf = 0; iFeaf < nDim_SelecFeasf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;

				fprintf(fout, " %E", fFeasSelec_WithConstTest_Arrf[nIndexf]);
			} // for (iFeaf = 0; iFeaf < nDim_SelecFeasf; iFeaf++)

			fprintf(fout, ", nY_Test_Actual_Arrf[%d] = %d", iVecf, nY_Test_Actual_Arrf[iVecf]);
		}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)

	} // if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66

	//printf("\n\n After 'Reading_All_TrainTest_Data': please press any key"); fflush(fout);  getchar();

//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
///////////////////////////////////////////////////////


//#ifndef COMMENT_OUT_ALL_PRINTS
	if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66  // 66
	{
		fprintf(fout, "\n\n 'doPasAggMaxOut_TrainTest_WithSelecFeas': the train vectors with const after shuffling, nDim_SelecFeas_WithConstf = %d", nDim_SelecFeas_WithConstf);

		for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
		{
			nTempf = iVecf * nDim_SelecFeas_WithConstf;
			fprintf(fout, "\n Shuffled train with const iVecf = %d, ", iVecf);
			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;

				fprintf(fout, "%d:%E ", iFeaf,fFeasSelec_WithConstTrain_Arrf[nIndexf]);
			} // for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

			fprintf(fout, ", nY_Train_Actual_Arrf[%d] = %d", iVecf, nY_Train_Actual_Arrf[iVecf]);
		}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	} //if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)  // 66

	//printf("\n\n After shuffling: please press any key"); fflush(fout);  getchar();
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS


	for (iSrandInitf = 0; iSrandInitf < nSrandInit__Unrefin_Max; iSrandInitf++)
	{
		nSrandInit_Glob = nSrandInit_Unrefin_Min + (iSrandInitf*nSrandInit_Step);

		//fprintf(fout, "\n  'doPasAggMaxOut_TrainTest_WithSelecFeas': before 'PasAggMaxOut_Train', nSrandInit_Glob = %d", nSrandInit_Glob);

		nResf = PasAggMaxOut_Train(
			nNumOfItersOfTrainingTotf, //const int nNumOfItersOfTrainingTotf,

			//after shuffling
			fFeasSelec_WithConstTrain_Arrf, //const float fFeasSelec_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], to be normalized
			nY_Train_Actual_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

			//const int nDimf, == nDim_D_SelecFeas_WithConstf

			nNumVecTrainTotf, //const int nVecTrainf,
			/////////////////////////

			nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
			nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

			nKf, //const int nKf, //nNumOfHyperplanes
			nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

			///////////////////////
			fAlphaf, //const float fAlphaf, // < 1.0
			fEpsilonf, //const float fEpsilonf,
			fCrf, //const float fCrf,
			fCf, //const float fCf,
		///////////////////////////////////////////////////
			fW_Train_Arrf, //float fW_Arrf[], //[nDim_Hf]
			fU_Train_Arrf, //float fU_Arrf[], //[nDim_U_Glob],

			sPasAggMaxOut_TrainResults); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'doPasAggMaxOut_TrainTest_WithSelecFeas' by 'PasAggMaxOut_Train'");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest_WithSelecFeas' by 'PasAggMaxOut_Train'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			delete[] fU_Train_Arrf;
			delete[] fW_Train_Arrf;

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
		fprintf(fout, "\n  After 'PasAggMaxOut_Train': sPasAggMaxOut_TrainResults.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf);

		fprintf(fout, "\n ...TrainResults.fPercentageOfCorrect_Positf = %E, ...TrainResults.fPercentageOfCorrect_Negatf = %E",
			sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

		fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
		fprintf(fout, "\n ...TrainResults->nNumOfPosit_Y_Totf = %d, ...TrainResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf,
			sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

		fprintf(fout, "\n ...TrainResults->nNumOfNegat_Y_Totf = %d, ...TrainResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
			sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/*
		printf("\n\n 'doPasAggMaxOut_TrainTest_WithSelecFeas': after 'PasAggMaxOut_Train', sPasAggMaxOut_TrainResults.fPercentageOfCorrectTotf = %E", sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf);

		printf("\n ...TrainResults.fPercentageOfCorrect_Positf = %E, ...TrainResults.fPercentageOfCorrect_Negatf = %E",
			sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

		printf("\n\n ...rainResults->nNumOfPosit_Y_Totf = %d, ...TrainResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf,
			sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

		printf("\n\n ...TrainResults->nNumOfNegat_Y_Totf = %d, ...TrainResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
			sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);
*/

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n///////////////////////////////////////////////////////////");
		printf("\n\n 'doPasAggMaxOut_TrainTest_WithSelecFeas': after 'PasAggMaxOut_Train', sPasAggMaxOut_TrainResults.fPercentageOfCorrectTotf = %E", sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf);

		printf("\n ...TrainResults.fPercentageOfCorrect_Positf = %E, ...TrainResults.fPercentageOfCorrect_Negatf = %E",
			sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

		printf("\n\n ...rainResults->nNumOfPosit_Y_Totf = %d, ...TrainResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf,
			sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

		printf("\n\n ...TrainResults->nNumOfNegat_Y_Totf = %d, ...TrainResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
			sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);

		fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
		fprintf(fout, "\n\n 'doPasAggMaxOut_TrainTest_WithSelecFeas': after 'PasAggMaxOut_Train', sPasAggMaxOut_TrainResults.fPercentageOfCorrectTotf = %E", sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf);

		fprintf(fout, "\n ...TrainResults.fPercentageOfCorrect_Positf = %E, ...TrainResults.fPercentageOfCorrect_Negatf = %E",
			sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

		fprintf(fout, "\n\n ...rainResults->nNumOfPosit_Y_Totf = %d, ...TrainResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf,
			sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

		fprintf(fout, "\n\n ...TrainResults->nNumOfNegat_Y_Totf = %d, ...TrainResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
			sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);
		//printf("\n\n Please press any key to continue:"); fflush(fout); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
/////////////////////////////////////
//testing the train data
		PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TestForTrainingResultsf;

		nResf = PasAggMaxOut_Test(
			fFeasSelec_WithConstTrain_Arrf, //const float fFeasSelec_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], to be normalized
			nY_Train_Actual_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

			nNumVecTrainTotf, //const int nVecTrainf,

			/////////////////////////
			nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
			nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

			nKf, //const int nKf, //nNumOfHyperplanes
			nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

			///////////////////////
			fW_Train_Arrf, //const float fW_Train_Arrf[], //[nDim_Hf]
			fU_Train_Arrf, //const float fU_Train_Arrf[], //[nDim_U_Glob],
			///////////////////////////////////////////////////

			&sPasAggMaxOut_TestForTrainingResultsf); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'doPasAggMaxOut_TrainTest_WithSelecFeas' by 'PasAggMaxOut_Test' for 'sPasAggMaxOut_TestForTrainingResultsf'");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest_WithSelecFeas' by 'PasAggMaxOut_Test' for 'sPasAggMaxOut_TestForTrainingResultsf'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar();	exit(1);

			delete[] fU_Train_Arrf;
			delete[] fW_Train_Arrf;

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

#ifdef EXIT_AFTER_TRAINING
		delete[] fU_Train_Arrf;
		delete[] fW_Train_Arrf;

		return SUCCESSFUL_RETURN;
#endif //#ifdef EXIT_AFTER_TRAINING

		////////////////////////////////////
		nResf = PasAggMaxOut_Test(
			fFeasSelec_WithConstTest_Arrf, // const float fFeasSelec_WithConstTest_Arrf[], //[nProdSelec_WithConstTestTot],
			nY_Test_Actual_Arrf, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

			//const int nDimf, == nDim_D_SelecFeas_WithConstf

			nNumVecTestTotf, //const int nVecTestf,

			/////////////////////////
			nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
			nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

			nKf, //const int nKf, //nNumOfHyperplanes
			nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

			///////////////////////
			fW_Train_Arrf, //const float fW_Train_Arrf[], //[nDim_Hf]
			fU_Train_Arrf, //const float fU_Train_Arrf[], //[nDim_U_Glob],
			///////////////////////////////////////////////////

			sPasAggMaxOut_TestResults); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'doPasAggMaxOut_TrainTest_WithSelecFeas' by 'PasAggMaxOut_Test'");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest_WithSelecFeas' by 'PasAggMaxOut_Test'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar();	exit(1);

			delete[] fU_Train_Arrf;
			delete[] fW_Train_Arrf;

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
		fprintf(fout, "\n  'doPasAggMaxOut_TrainTest_WithSelecFeas': after final 'PasAggMaxOut_Test', sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf = %E, fFeaConst_Glob = %E",
			sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, fFeaConst_Glob);

		fprintf(fout, "\n ...TestResults->fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
			sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

		fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
		fprintf(fout, "\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
			sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

		fprintf(fout, "\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
			sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);

		printf("\n\n///////////////////////////////////////////////////////////");
		printf("\n\n 'doPasAggMaxOut_TrainTest_WithSelecFeas': after final 'PasAggMaxOut_Test', sPasAggMaxOut_TestResults.fPercentageOfCorrectTotf = %E, fFeaConst_Glob = %E", sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, fFeaConst_Glob);
		
		printf("\n ...TestResults.fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
			sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

		printf("\n\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
			sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

		printf("\n\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
			sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);
		//printf("\n\n Please press any key to continue:"); fflush(fout);  getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		fWeightedTrainTest_Efficiencyf = ((sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Positf + sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Negatf)*fWeight_Train) +

			((sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf + sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf)*fWeight_Test);

		//printf("\n\n The end of 'doPasAggMaxOut_TrainTest_WithSelecFeas': current fPercentageOfCorrect_Refined_Max_Glob = %E", fPercentageOfCorrect_Refined_Max_Glob);

		//printf("\n\n sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf = %E, sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf = %E, iSrandInitf = %d",
			//sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf, sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, iSrandInitf);

		if (fWeightedTrainTest_Efficiencyf > fWeightedTrainTest_EfficiencyMaxf)
			fWeightedTrainTest_EfficiencyMaxf = fWeightedTrainTest_Efficiencyf;

#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

		if (fWeightedTrainTest_Efficiencyf > fPercentageOfCorrect_NoRefin_Glob)
#endif //#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

#ifndef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY
			if (sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf + sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf > fPercentageOfCorrect_Refined_Max_Glob)

#endif //#ifndef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

			{
				fPercentageOfCorrect_NoRefin_Glob = fWeightedTrainTest_Efficiencyf;

				printf("\n\n///////////////////////////////////////////////////////////");
				printf("\n 'doPasAggMaxOut_TrainTest_WithSelecFeas': a new fPercentageOfCorrect_NoRefin_Glob = %E, fPercentageOfCorrect_Refined_Max_Glob = %E", 
					fPercentageOfCorrect_NoRefin_Glob, fPercentageOfCorrect_Refined_Max_Glob);
				
				printf("\n\n Testing the train data, sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",

					sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

				printf("\n sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Positf = %E, sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Negatf = %E",
					sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Negatf);

				printf("\n\n sPasAggMaxOut_TestForTrainingResultsf.nNumOfPosit_Y_Totf = %d, sPasAggMaxOut_TestForTrainingResultsf.nNumOfPositCorrect_Y_Totf = %d",
					sPasAggMaxOut_TestForTrainingResultsf.nNumOfPosit_Y_Totf,
					sPasAggMaxOut_TestForTrainingResultsf.nNumOfPositCorrect_Y_Totf);

				printf("\n\n sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegat_Y_Totf = %d, sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegatCorrect_Y_Totf = %d",
					sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegat_Y_Totf,
					sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegatCorrect_Y_Totf);
				/////////////////////////////////////////////
				printf("\n\n sPasAggMaxOut_TestResults.fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E", 
					sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

				printf("\n ...TestResults.fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
					sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

				printf("\n\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
					sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

				printf("\n\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
					sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);
				printf("\n\n iSrandInitf = %d, nNumOfSelecFeasTotCurf = %d", iSrandInitf, nNumOfSelecFeasTotCurf);
		
				fprintf(fout, "\n\n///////////////////////////////////////////////////////////");

				fprintf(fout,"\n 'doPasAggMaxOut_TrainTest_WithSelecFeas': a new fPercentageOfCorrect_NoRefin_Glob = %E, fPercentageOfCorrect_Refined_Max_Glob = %E",
					fPercentageOfCorrect_NoRefin_Glob, fPercentageOfCorrect_Refined_Max_Glob);
				fprintf(fout, "\n\n Testing the train data, sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",

					sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

				fprintf(fout, "\n sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Positf = %E, sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Negatf = %E",
					sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Negatf);

				fprintf(fout, "\n\n sPasAggMaxOut_TestForTrainingResultsf.nNumOfPosit_Y_Totf = %d, sPasAggMaxOut_TestForTrainingResultsf.nNumOfPositCorrect_Y_Totf = %d",
					sPasAggMaxOut_TestForTrainingResultsf.nNumOfPosit_Y_Totf,
					sPasAggMaxOut_TestForTrainingResultsf.nNumOfPositCorrect_Y_Totf);

				fprintf(fout, "\n\n sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegat_Y_Totf = %d, sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegatCorrect_Y_Totf = %d",
					sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegat_Y_Totf,
					sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegatCorrect_Y_Totf);
///////////////////////////
				fprintf(fout, "\n sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",
					sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

				fprintf(fout, "\n ...TestResults->fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
					sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

				fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
				fprintf(fout, "\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
					sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

				fprintf(fout, "\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
					sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);
				fprintf(fout,"\n\n iSrandInitf = %d, nNumOfSelecFeasTotCurf = %d", iSrandInitf, nNumOfSelecFeasTotCurf);

				fprintf(fout, "\n");
				for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)
				{
					printf("\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf, nPosOfSelec_FeasArrf[iFeaf]);
					fprintf(fout,"\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf, nPosOfSelec_FeasArrf[iFeaf]);
				} //for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)

			} //if (fWeightedTrainTest_Efficiencyf > fPercentageOfCorrect_Refined_Max_Glob)
/////////////////////////////////////////

			if (fWeightedTrainTest_Efficiencyf > fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_AnUnrifinedModel)
			{
#ifndef MODEL_WRITING_ONLY_FOR_REFINEMENT
					WritingAn_UnRefinedModel_WithSelecFeas(
						sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrectTotf, //const float fPercentageOfCorrectTot_Trainf,
						sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, //const float fPercentageOfCorrectTot_Testf,

						fFeaConst_UnRefined_Init, //const float fConstf,

			//////////////////
						nNumOfSelecFeasTotCurf, //const int nNumOfSelecFeasTotCurf,
						nPosOfSelec_FeasArrf, //const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
			///////////////////
						nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
						nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

						nKf, //const int nKf, //nNumOfHyperplanes
						nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

						///////////////////////
						fW_Train_Arrf, //const float fW_Train_Arrf[], //[nDim_Hf]
						fU_Train_Arrf); // const float fU_Train_Arrf[]); //[nDim_U_Glob],
#endif //#ifndef MODEL_WRITING_ONLY_FOR_REFINEMENT

					//fPercentageOfCorrect_NoRefin_Glob = fWeightedTrainTest_Efficiencyf;

					fprintf(fout, "\n\n///////////////////////////////////////////////////////////");

					fprintf(fout, "\n 'doPasAggMaxOut_TrainTest_WithSelecFeas': writing the next unrefined model, fWeightedTrainTest_Efficiencyf = %E > fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_AnUnrifinedModel = %E",
						fWeightedTrainTest_Efficiencyf, fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_AnUnrifinedModel);

					fprintf(fout, "\n fPercentageOfCorrect_NoRefin_Glob = %E, fPercentageOfCorrect_Refined_Max_Glob = %E",
						fPercentageOfCorrect_NoRefin_Glob, fPercentageOfCorrect_Refined_Max_Glob);
					fprintf(fout, "\n\n Testing the train data, sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",

						sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

					fprintf(fout, "\n sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Positf = %E, sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Negatf = %E",
						sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TestForTrainingResultsf.fPercentageOfCorrect_Negatf);

					fprintf(fout, "\n\n sPasAggMaxOut_TestForTrainingResultsf.nNumOfPosit_Y_Totf = %d, sPasAggMaxOut_TestForTrainingResultsf.nNumOfPositCorrect_Y_Totf = %d",
						sPasAggMaxOut_TestForTrainingResultsf.nNumOfPosit_Y_Totf,
						sPasAggMaxOut_TestForTrainingResultsf.nNumOfPositCorrect_Y_Totf);

					fprintf(fout, "\n\n sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegat_Y_Totf = %d, sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegatCorrect_Y_Totf = %d",
						sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegat_Y_Totf,
						sPasAggMaxOut_TestForTrainingResultsf.nNumOfNegatCorrect_Y_Totf);
					///////////////////////////
					fprintf(fout, "\n sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",
						sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

					fprintf(fout, "\n ...TestResults->fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
						sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

					fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
					fprintf(fout, "\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
						sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

					fprintf(fout, "\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
						sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);
					fprintf(fout, "\n\n iSrandInitf = %d, nNumOfSelecFeasTotCurf = %d", iSrandInitf, nNumOfSelecFeasTotCurf);

					fprintf(fout, "\n");
					for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)
					{
						//printf("\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf, nPosOfSelec_FeasArrf[iFeaf]);
						fprintf(fout, "\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf, nPosOfSelec_FeasArrf[iFeaf]);
					} //for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)

					fflush(fout);
					fflush(fout_Models_UnRefined);
			} //if (fWeightedTrainTest_Efficiencyf > fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_AnUnrifinedModel)

	} //for (iSrandInitf = 0; iSrandInitf < nSrandInit__Unrefin_Max; iSrandInitf++)
/////////////////////////
	
	delete[] fU_Train_Arrf;
	delete[] fW_Train_Arrf;

	return SUCCESSFUL_RETURN;
}//int doPasAggMaxOut_TrainTest_WithSelecFeas(...
////////////////////////////////////////////////////////////////////

int doPasAggMaxOut_TrainTest_WithSelecFeas_Refined(
	//train
	const int nNumOfItersOfTraining_RefinedTotf,

	//after shuffling
	//const float fFea_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], already normalized

//a const can be changed
		float fFeasSelec_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], already normalized
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		const int nNumVecTrainTotf, //== nVecTrainf,
	///////////////////////////////////////
			//test
	//a const can be changed
			float fFeasSelec_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
			const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		const int nNumVecTestTotf, // == nVecTestf,

		/////////////////////////////////////

		const int nDim_SelecFeasf, //== nNumOfSelecFeasTotCurf

		const int nDim_SelecFeas_WithConstf, // <= dimension of the original space

		//const int nPosit_OneVec_Arrf[], //[nDim_FeasInit]
		/*
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)
		*/
		///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
///////////////////////////////////////////////////
	//for model writing
	const int nNumOfSelecFeasTotCurf,
	const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]

	float &fWeightedTrainTest_Efficiency_Refinedf,
	PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults_Refined,
	PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults_Refined)
{
	///////////////////////////////////////////////////
	int ReplacingAConst_In_2DimFloatArr(
		//const int nDimf,
		const int nDimSelecf, // < nDimf

		const float fFeaConstNewf,

		const int nNumVecf,

		float fVecSelec_WithConstArr[]); //[(nDimSelecf + 1)*nNumVecf]

	int doPasAggMaxOut_SelecFeas_ForRefined(
		const int nNumOfItersOfTrainingTotf,

		//after shuffling
		const float fFeasSelec_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], already normalized
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		const int nNumVecTrainTotf, //== nVecTrainf,
	///////////////////////////////////////
		//test
		const float fFeasSelec_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
		const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		const int nNumVecTestTotf, // == nVecTestf,

		/////////////////////////////////////

		const int nDim_SelecFeasf, //== nNumOfSelecFeasTotCurf

		const int nDim_SelecFeas_WithConstf, // == nDim_SelecFeas_WithConstf <= dimension of the original space
		//const int nPosit_OneVec_Arrf[], //[nDim_FeasInit] -- for writing a model
		/////////////////////////

		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_U_Globf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
		///////////////////////////////////////////////////
		//for writing a model			
		const int nNumOfSelecFeasTotCurf,
		const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]

		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults,
		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

	int
		iVecf,
		iFeaf,

		nNumOfActualFeasf = nDim_SelecFeas_WithConstf - 1,

		nIndexf,
		nIndexWithConstf,

		nTempf,
		nTempWithConstf,

		//nDim_Uf, // = nDim_SelecFeas_WithConstf * nDim_Hf*nKf;

		nResf;
	//////////////////

	//for optimization
	int
		iFeaConstInitf,
		iFeaConstInit_ForMaxf,

		nNumOfFeaConst_UnRefined_Initf = ((fFeaConst_ForRefined_Glob_Max - fFeaConst_ForRefined_Glob_Min) / fFeaConst_ForRefined_Step) + 1,
		/////////////////////////
		iU_Init_HalfRangef,
		nNumOfU_Init_HalfRangesf = ((fU_Init_HalfRange_Max - fU_Init_HalfRange_Min) / fU_Init_HalfRange_Step) + 1,
		///////////////
		iW_Init_HalfRangef,
		nNumOfW_Init_HalfRangesf = ((fW_Init_HalfRange_Max - fW_Init_HalfRange_Min) / fW_Init_HalfRange_Step) + 1,
		//////////////////////////////////
		iKf,
		nK_Glob_ForMaxf,

		iDim_Hf,
		nDim_H_Glob_ForMaxf,

		//	nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)
			/////////////////////////
		iSrandInitf,
		nSrandInit_ForMaxf,

		nNumOfSrandInitf = ((nSrandInit_Refined_Max - nSrandInit_Refined_Min) / nSrand_Refined_Step) + 1;

	float
		fWeightedTrainTest_Efficiencyf;
	//fFeaMin_TrainArrf[nDim_D_WithConst],
	//fFeaMax_TrainArrf[nDim_D_WithConst];

	fWeightedTrainTest_Efficiency_Refinedf = -fLarge;
	////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n The train vectors: fFeaConst_Glob = %E", fFeaConst_Glob);

	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		//nTempf = iVecf* nDim_D_SelecFeas_WithConstf;
		nTempf = iVecf * nDim_D_SelecFeasf;

		fprintf(fout, "\n (train) fFeaConst_Glob = %E, iVecf = %d, ", fFeaConst_Glob, iVecf);
		for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFeasSelec_WithConstTrain_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)

		fprintf(fout, ", nY_Train_Actual_Arrf[%d] = %d, ", iVecf, nY_Train_Actual_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

////////////////////////////
	fprintf(fout, "\n\n The test vectors: fFeaConst_Glob = %E", fFeaConst_Glob);
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		//		nTempf = iVecf * nDim_D_SelecFeas_WithConstf;
		nTempf = iVecf * nDim_D_SelecFeasf;

		fprintf(fout, "\n (test) fFeaConst_Glob = %E, iVecf = %d, ", fFeaConst_Glob, iVecf);
		//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
		for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFeasSelec_WithConstTest_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_D_SelecFeasf; iFeaf++)

		fprintf(fout, ", nY_Test_Actual_Arrf[%d] = %d", iVecf, nY_Test_Actual_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)

	//printf("\n\n After 'Reading_All_TrainTest_Data': please press any key"); fflush(fout);  getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
///////////////////////////////////////////////////////


//#ifndef COMMENT_OUT_ALL_PRINTS
	if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)
	{
		fprintf(fout, "\n\n 'doPasAggMaxOut_TrainTest_WithSelecFeas_Refined': the train vectors after shuffling");

		for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
		{
			nTempf = iVecf * nDim_SelecFeas_WithConstf;
			fprintf(fout, "\n Shuffled train iVecf = %d, ", iVecf);
			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;

				fprintf(fout, "%d:%E ", iFeaf, fFeasSelec_WithConstTrain_Arrf[nIndexf]);
			} // for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

			fprintf(fout, ", nY_Train_Actual_Arrf[%d] = %d", iVecf, nY_Train_Actual_Arrf[iVecf]);
		}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

	} //if (nNumOfFitnessOfOneFeaVecTot_Glob == -1)

	//printf("\n\n After shuffling: please press any key"); fflush(fout);  getchar();
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TestForTrainingResultsf;

/////////////////////////////////////////////////////////////
	nDim_H_Glob = nDim_H_Refined_Min / nDim_H_Refined_Factor;
	for (iDim_Hf = 0; iDim_Hf < nLarge; iDim_Hf++)
	{
		nDim_H_Glob = nDim_H_Glob * nDim_H_Refined_Factor;
		if (nDim_H_Glob > nDim_H_Refined_Max)
		{
			fprintf(fout, "\n\n Exit from iDim_Hf loop at iDim_Hf = %d, nDim_H_Glob = %d", iDim_Hf, nDim_H_Glob);
			break;
		} //if (nDim_H_Glob > nDim_H_Refined_Max)

	////////////////////////////

	//nK_Refined_Min >= nK_Refined_Factor
		nK_Glob = nK_Refined_Min / nK_Refined_Factor;
		for (iKf = 0; iKf < nLarge; iKf++)
		{
			nK_Glob = nK_Glob * nK_Refined_Factor;
			if (nK_Glob > nK_Refined_Max)
			{
				fprintf(fout, "\n\n Exit from iKf loop at iKf = %d, nK_Glob = %d", iKf, nK_Glob);
				//new
				break;
			} //if (nK_Glob > nK_Refined_Max)

			//nDim_U_Glob = nDim_D_WithConst * nDim_H_Glob*nK_Glob;

			nDim_U_Glob = nDim_SelecFeas_WithConstf * nDim_H_Glob*nK_Glob;

			////////////////////////////////////////////////////////////////

			for (iFeaConstInitf = 0; iFeaConstInitf < nNumOfFeaConst_UnRefined_Initf; iFeaConstInitf++)
			{
				fFeaConst_Glob = fFeaConst_ForRefined_Glob_Min + (iFeaConstInitf*fFeaConst_ForRefined_Step);

				nResf = ReplacingAConst_In_2DimFloatArr(
					//const int nDimf,
					nDim_SelecFeasf, //const int nDimSelecf, // < nDimf

					fFeaConst_Glob, //const float fFeaConstNewf,

					nNumVecTrainTotf, //const int nNumVecf,

					fFeasSelec_WithConstTrain_Arrf); // float fVecSelec_WithConstArr[]); //[(nDimSelecf + 1)*nNumVecf]

				nResf = ReplacingAConst_In_2DimFloatArr(
					//const int nDimf,
					nDim_SelecFeasf, //const int nDimSelecf, // < nDimf

					fFeaConst_Glob, //const float fFeaConstNewf,

					nNumVecTestTotf, //const int nNumVecf,

					fFeasSelec_WithConstTest_Arrf); // float fVecSelec_WithConstArr[]); //[(nDimSelecf + 1)*nNumVecf]

				//fU_Init_Min_Glob
				for (iU_Init_HalfRangef = 0; iU_Init_HalfRangef < nNumOfU_Init_HalfRangesf; iU_Init_HalfRangef++)
				{
					fU_Init_Min_Glob = -(fU_Init_HalfRange_Min + (iU_Init_HalfRangef* fU_Init_HalfRange_Step));
					fU_Init_Max_Glob = -fU_Init_Min_Glob;

					for (iW_Init_HalfRangef = 0; iW_Init_HalfRangef < nNumOfW_Init_HalfRangesf; iW_Init_HalfRangef++)
					{
						fW_Init_Min_Glob = -(fW_Init_HalfRange_Min + (iW_Init_HalfRangef* fW_Init_HalfRange_Step));
						fW_Init_Max_Glob = -fW_Init_Min_Glob;

						for (iSrandInitf = 0; iSrandInitf < nNumOfSrandInitf; iSrandInitf++)
						{
							nSrandInit_Glob = nSrandInit_Unrefin_Min + (iSrandInitf*nSrandInit_Step);

							nResf = doPasAggMaxOut_SelecFeas_ForRefined(
								nNumOfItersOfTraining_RefinedTotf, //const int nNumOfItersOfTrainingTotf,

								//after shuffling
								fFeasSelec_WithConstTrain_Arrf, //const float fFeasSelec_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], already normalized
								nY_Train_Actual_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

								nNumVecTrainTotf, //const int nNumVecTrainTotf, //== nVecTrainf,
							///////////////////////////////////////
								//test
								fFeasSelec_WithConstTest_Arrf, //const float fFeasSelec_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
								nY_Test_Actual_Arrf, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

								nNumVecTestTotf, //const int nNumVecTestTotf, // == nVecTestf,

								/////////////////////////////////////

								nDim_SelecFeasf, //const int nDim_SelecFeasf, //== nNumOfSelecFeasTotCurf

								nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, 
								//nPosOfSelec_FeasArrf, //const int nPosit_OneVec_Arrf[], //[nNumOfSelecFeasTotCurf] -- for writing a model
								/////////////////////////

								nDim_H_Glob, //const int nDim_Hf, //dimension of the nonlinear/transformed space

								nK_Glob, //const int nKf, //nNumOfHyperplanes
								nDim_U_Glob, //const int nDim_U_Globf, //(nDim_SelecFeas_WithConstf * nDim_H_Glob*nK_Glob;)

								///////////////////////
								fAlphaf, //const float fAlphaf, // < 1.0
								fEpsilonf, //const float fEpsilonf,
								fCrf, //const float fCrf,
								fCf, //const float fCf,

								///////////////////////////////////////////////////
								//for writing a model			
								nNumOfSelecFeasTotCurf, //const int nNumOfSelecFeasTotCurf,
								nPosOfSelec_FeasArrf, //const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]

								sPasAggMaxOut_TrainResults_Refined, //PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults,
								sPasAggMaxOut_TestResults_Refined); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults)
	//////////////////////////////////////////////////////////////

							fWeightedTrainTest_Efficiencyf = ((sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrect_Positf + sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrect_Negatf)*fWeight_Train) +

								((sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrect_Positf + sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrect_Negatf)*fWeight_Test);

							//printf("\n\n The end of 'doPasAggMaxOut_TrainTest_WithSelecFeas_Refined': current fPercentageOfCorrect_Refined_Max_Glob = %E", fPercentageOfCorrect_Refined_Max_Glob);

							//printf("\n\n sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrectTotf = %E, sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrectTotf = %E, iSrandInitf = %d",
								//sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrectTotf, sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrectTotf, iSrandInitf);

							if (fWeightedTrainTest_Efficiencyf > fWeightedTrainTest_Efficiency_Refinedf)
								fWeightedTrainTest_Efficiency_Refinedf = fWeightedTrainTest_Efficiencyf;
							/////////////////////////////////////////

							if (fWeightedTrainTest_Efficiencyf > fPercentageOfCorrect_Refined_Max_Glob)
							{
								fPercentageOfCorrect_Refined_Max_Glob = fWeightedTrainTest_Efficiencyf;

								printf("\n\n///////////////////////////////////////////////////////////");
								printf("\n 'doPasAggMaxOut_TrainTest_WithSelecFeas_Refined': a new fPercentageOfCorrect_Refined_Max_Glob = %E, nNumOfFitnessOfOneFeaVecTot_Glob = %d",
									fPercentageOfCorrect_Refined_Max_Glob, nNumOfFitnessOfOneFeaVecTot_Glob);

								printf("\n\n Testing the train data, sPasAggMaxOut_TrainResults_Refined.fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",

									sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

								printf("\n sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrect_Positf = %E, sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrect_Negatf = %E",
									sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrect_Negatf);

								printf("\n\n sPasAggMaxOut_TrainResults_Refined->nNumOfPosit_Y_Totf = %d, sPasAggMaxOut_TrainResults_Refined->nNumOfPositCorrect_Y_Totf = %d",
									sPasAggMaxOut_TrainResults_Refined->nNumOfPosit_Y_Totf,
									sPasAggMaxOut_TrainResults_Refined->nNumOfPositCorrect_Y_Totf);

								printf("\n\n sPasAggMaxOut_TrainResults_Refined->nNumOfNegat_Y_Totf = %d, sPasAggMaxOut_TrainResults_Refined->nNumOfNegatCorrect_Y_Totf = %d",
									sPasAggMaxOut_TrainResults_Refined->nNumOfNegat_Y_Totf,
									sPasAggMaxOut_TrainResults_Refined->nNumOfNegatCorrect_Y_Totf);
								/////////////////////////////////////////////
								printf("\n\n sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",
									sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

								printf("\n ...TestResults.fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
									sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrect_Negatf);

								printf("\n\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults_Refined->nNumOfPosit_Y_Totf,
									sPasAggMaxOut_TestResults_Refined->nNumOfPositCorrect_Y_Totf);

								printf("\n\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults_Refined->nNumOfNegat_Y_Totf,
									sPasAggMaxOut_TestResults_Refined->nNumOfNegatCorrect_Y_Totf);

								printf("\n\n iSrandInitf = %d, iW_Init_HalfRangef = %d, nNumOfW_Init_HalfRangesf = %d, iU_Init_HalfRangef = %d, nNumOfU_Init_HalfRangesf = %d",
									iSrandInitf, iW_Init_HalfRangef, nNumOfW_Init_HalfRangesf, iU_Init_HalfRangef, nNumOfU_Init_HalfRangesf);

								fprintf(fout, "\n\n///////////////////////////////////////////////////////////");

								fprintf(fout, "\n 'doPasAggMaxOut_TrainTest_WithSelecFeas_Refined': a new fPercentageOfCorrect_Refined_Max_Glob = %E, nNumOfFitnessOfOneFeaVecTot_Glob = %d",
									fPercentageOfCorrect_Refined_Max_Glob, nNumOfFitnessOfOneFeaVecTot_Glob);
								fprintf(fout, "\n\n Testing the train data, sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",

									sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

								fprintf(fout, "\n sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrect_Positf = %E, sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrect_Negatf = %E",
									sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults_Refined->fPercentageOfCorrect_Negatf);

								fprintf(fout, "\n\n sPasAggMaxOut_TrainResults_Refined->nNumOfPosit_Y_Totf = %d, sPasAggMaxOut_TrainResults_Refined->nNumOfPositCorrect_Y_Totf = %d",
									sPasAggMaxOut_TrainResults_Refined->nNumOfPosit_Y_Totf,
									sPasAggMaxOut_TrainResults_Refined->nNumOfPositCorrect_Y_Totf);

								fprintf(fout, "\n\n sPasAggMaxOut_TrainResults_Refined->nNumOfNegat_Y_Totf = %d, sPasAggMaxOut_TrainResults_Refined->nNumOfNegatCorrect_Y_Totf = %d\n",
									sPasAggMaxOut_TrainResults_Refined->nNumOfNegat_Y_Totf,
									sPasAggMaxOut_TrainResults_Refined->nNumOfNegatCorrect_Y_Totf);
								///////////////////////////
								fprintf(fout, "\n sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",
									sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

								fprintf(fout, "\n ...TestResults->fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
									sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults_Refined->fPercentageOfCorrect_Negatf);

								fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
								fprintf(fout, "\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults_Refined->nNumOfPosit_Y_Totf,
									sPasAggMaxOut_TestResults_Refined->nNumOfPositCorrect_Y_Totf);

								fprintf(fout, "\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults_Refined->nNumOfNegat_Y_Totf,
									sPasAggMaxOut_TestResults_Refined->nNumOfNegatCorrect_Y_Totf);
								fprintf(fout, "\n\n iSrandInitf = %d, iW_Init_HalfRangef = %d, nNumOfW_Init_HalfRangesf = %d, iU_Init_HalfRangef = %d, nNumOfU_Init_HalfRangesf = %d",
									iSrandInitf, iW_Init_HalfRangef, nNumOfW_Init_HalfRangesf, iU_Init_HalfRangef, nNumOfU_Init_HalfRangesf);

								fprintf(fout, "\n");
								for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)
								{
									printf("\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf, nPosOfSelec_FeasArrf[iFeaf]);
									fprintf(fout, "\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf, nPosOfSelec_FeasArrf[iFeaf]);
								} //for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)

							} //if (fWeightedTrainTest_Efficiencyf > fPercentageOfCorrect_Refined_Max_Glob)

						} //for (iSrandInitf = 0; iSrandInitf < nNumOfSrandInitf; iSrandInitf++)

						fflush(fout);
					} //for (iW_Init_HalfRangef = 0; iW_Init_HalfRangef < nNumOfW_Init_HalfRangesf; iW_Init_HalfRangef++)
				} //for (iU_Init_HalfRangef = 0; iU_Init_HalfRangef  < nNumOfU_Init_HalfRangesf; iU_Init_HalfRangef++)

				printf("\n\n Refining: iKf = %d, nDim_H_Glob = %d, nK_Glob = %d, iFeaConstInitf = %d, nNumOfFeaConst_UnRefined_Initf = %d, so far fWeightedTrainTest_Efficiency_Refinedf = %E, fPercentageOfCorrect_Refined_Max_Glob = %E",
					iKf, nDim_H_Glob, nK_Glob, iFeaConstInitf, nNumOfFeaConst_UnRefined_Initf, fWeightedTrainTest_Efficiency_Refinedf, fPercentageOfCorrect_Refined_Max_Glob);
				printf("\n nNumOfFitnessOfOneFeaVecTot_Glob = %d", nNumOfFitnessOfOneFeaVecTot_Glob);

				fprintf(fout, "\n\n Refining: iKf = %d, nDim_H_Glob = %d, nK_Glob = %d, iFeaConstInitf = %d, nNumOfFeaConst_UnRefined_Initf = %d, so far fWeightedTrainTest_Efficiency_Refinedf = %E, fPercentageOfCorrect_Refined_Max_Glob = %E",
					iKf, nDim_H_Glob, nK_Glob, iFeaConstInitf, nNumOfFeaConst_UnRefined_Initf, fWeightedTrainTest_Efficiency_Refinedf, fPercentageOfCorrect_Refined_Max_Glob);

				fprintf(fout, "\n nNumOfFitnessOfOneFeaVecTot_Glob = %d", nNumOfFitnessOfOneFeaVecTot_Glob);
				fflush(fout);
			} // for (iFeaConstInitf = 0; iFeaConstInitf < nNumOfFeaConst_UnRefined_Initf; iFeaConstInitf++)

		} //for (iKf = 0; iKf < nLarge; iKf++)

	} //for (iDim_Hf = 0; iDim_Hf < nLarge; iDim_Hf++)
	  /////////////////////////

	//printf("\n\nPlease press any key:"); fflush(fout); getchar();
//restoring to init const
	nResf = ReplacingAConst_In_2DimFloatArr(
		//const int nDimf,
		nDim_SelecFeasf, //const int nDimSelecf, // < nDimf

		fFeaConst_UnRefined_Init, //const float fFeaConstNewf,

		nNumVecTrainTotf, //const int nNumVecf,

		fFeasSelec_WithConstTrain_Arrf); // float fVecSelec_WithConstArr[]); //[(nDimSelecf + 1)*nNumVecf]

	nResf = ReplacingAConst_In_2DimFloatArr(
		//const int nDimf,
		nDim_SelecFeasf, //const int nDimSelecf, // < nDimf

		fFeaConst_UnRefined_Init, //const float fFeaConstNewf,

		nNumVecTestTotf, //const int nNumVecf,

		fFeasSelec_WithConstTest_Arrf); // float fVecSelec_WithConstArr[]); //[(nDimSelecf + 1)*nNumVecf]

	return SUCCESSFUL_RETURN;
}//int doPasAggMaxOut_TrainTest_WithSelecFeas_Refined(...
/////////////////////////////////////////////////////////////////////

int doPasAggMaxOut_SelecFeas_ForRefined(
	const int nNumOfItersOfTrainingTotf,

	//after shuffling
	const float fFeasSelec_WithConstTrain_Arrf[], //[nProdSelec_WithConstTrainTot], already normalized
	const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	const int nNumVecTrainTotf, //== nVecTrainf,
///////////////////////////////////////
	//test
	const float fFeasSelec_WithConstTest_Arrf[], //[nProdSelec_WithConstTestTot],
	const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

	const int nNumVecTestTotf, // == nVecTestf,

	/////////////////////////////////////

	const int nDim_SelecFeasf, //== nNumOfSelecFeasTotCurf

	const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
	//const int nPosit_OneVec_Arrf[], //[nDim_FeasInit] -- for writing a model
	/////////////////////////

	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	const int nDim_U_Globf, //(nDim_SelecFeas_WithConstf*nDim_Hf*nKf)

	///////////////////////
	const float fAlphaf, // < 1.0
	const float fEpsilonf,
	const float fCrf,
	const float fCf,
	///////////////////////////////////////////////////
//for writing a model			
	const int nNumOfSelecFeasTotCurf,
	const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]

	PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults,
	PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults)
{
	///////////////////////////////////////////////////
	int PasAggMaxOut_Train(
		const int nNumOfItersOfTrainingTotf,

		//after shuffling
		const float fFeasSelec_WithConstTrain_Arrf[], //[nProdSelec_WithConstTrainTot], already normalized to (0,1)
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	//const int nDimf, == nDim_D_SelecFeas_WithConstf

		const int nVecTrainf,
		/////////////////////////

		const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_U_Globf, //(nDim_SelecFeas_WithConstf*nDim_Hf*nKf)

		///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
		///////////////////////////////////////////////////
		float fW_Arrf[],
		float fU_Arrf[], //[nDim_U_Glob],

		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults);

	int PasAggMaxOut_Test(
		const float fFeasSelec_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
		const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		//const int nDimf, == nDim_D_SelecFeas_WithConstf

		const int nVecTestf,

		/////////////////////////
		const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_U_Globf, //(nDim_SelecFeas_WithConstf*nDim_Hf*nKf)

		///////////////////////
		const float fW_Train_Arrf[], //[nDim_Hf]
		const float fU_Train_Arrf[], //[nDim_U_Glob],
		///////////////////////////////////////////////////

		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

#ifdef CALCULATING_Z_SCORES_OF_ALL_FEAS
	void Z_Scores_ForAllFeas(
		const int nDimf,

		const int nNumVecTrainTotf,
		const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		const float fFeaTrain_Arrf[], //[nProdTrainTot]
		const float fFeaTest_Arrf[],

		const float fZ_ScoreLimitf,

		float &fZ_ScoreMaxf,

		int &nNumOfFeasWithZ_ScoreAboveLimitf,

		float fZ_ScoresArrf[]); //[nDimf]
#endif //#ifdef CALCULATING_Z_SCORES_OF_ALL_FEAS

	void WritingARefinedModel_WithSelecFeas(
		const float fPercentageOfCorrectTot_Trainf,
		const float fPercentageOfCorrectTot_Testf,

		const float fConstf,

		/////////////////////////
		const int nNumOfSelecFeasTotCurf,
		const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
	//////////////////////

		const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_SelecFeas_WithConstf*nDim_Hf*nKf)

		///////////////////////
		const float fW_Train_Arrf[], //[nDim_Hf]
		const float fU_Train_Arrf[]); //[nDim_U_Glob],

	int
		iVecf,
		iFeaf,

		nNumOfActualFeasf = nDim_D_WithConst - 1,

		nIndexf,
		nIndexWithConstf,

		nTempf,
		nTempWithConstf,

		nResf;

	float
		fWeightedTrainTest_Efficiencyf;

	///////////////////////////////////////
	nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob = 0; //no printing
///////////////////////////////

	if (nNumOfSelecFeasTotCurf != nDim_SelecFeas_WithConstf - 1)
	{
		printf("\n\n An error in 'doPasAggMaxOut_SelecFeas_ForRefined': nNumOfSelecFeasTotCurf = %d != nDim_SelecFeas_WithConstf - 1 = %d", 
			nNumOfSelecFeasTotCurf, nDim_SelecFeas_WithConstf - 1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'doPasAggMaxOut_SelecFeas_ForRefined': nNumOfSelecFeasTotCurf = %d != nDim_D_SelecFeasf = %d",
			nNumOfSelecFeasTotCurf, nDim_D_SelecFeasf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} // if (nNumOfSelecFeasTotCurf != nDim_D_SelecFeasf)
////////////////////////////////
	float *fW_Train_Arrf = new float[nDim_Hf];
	if (fW_Train_Arrf == NULL)
	{
		printf("\n\n An error in 'doPasAggMaxOut_SelecFeas_ForRefined': fW_Train_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'doPasAggMaxOut_SelecFeas_ForRefined': fW_Train_Arrf == NULL");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fW_Train_Arrf == NULL)

	float *fU_Train_Arrf = new float[nDim_U_Globf];
	if (fU_Train_Arrf == NULL)
	{
		printf("\n\n An error in 'doPasAggMaxOut_SelecFeas_ForRefined': fU_Train_Arrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'doPasAggMaxOut_SelecFeas_ForRefined': fU_Train_Arrf == NULL");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] fW_Train_Arrf;
		return UNSUCCESSFUL_RETURN;
	} //if (fU_Train_Arrf == NULL)

	////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n The train vectors: fFeaConst_Glob = %E", fFeaConst_Glob);

	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		//nTempf = iVecf* nDim_D_SelecFeas_WithConstf;
		nTempf = iVecf * nDim_SelecFeas_WithConstf;

		fprintf(fout, "\n (train) fFeaConst_Glob = %E, iVecf = %d, ", fFeaConst_Glob, iVecf);
		for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFeaTrain_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

		fprintf(fout, ", nY_Train_Arrf[%d] = %d, ", iVecf, nY_Train_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

////////////////////////////
	fprintf(fout, "\n\n The test vectors: fFeaConst_Glob = %E", fFeaConst_Glob);
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		//		nTempf = iVecf * nDim_SelecFeas_WithConstf;
		nTempf = iVecf * nDim_SelecFeas_WithConstf;

		fprintf(fout, "\n (test) fFeaConst_Glob = %E, iVecf = %d, ", fFeaConst_Glob, iVecf);
		//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
		for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFeaTest_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

		fprintf(fout, ", nY_Test_Actual_Arrf[%d] = %d", iVecf, nY_Test_Actual_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)

	//printf("\n\n After 'Reading_All_TrainTest_Data': please press any key"); fflush(fout);  getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
////////////////////////////////////////////////////////

	nResf = PasAggMaxOut_Train(
		nNumOfItersOfTrainingTotf, //const int nNumOfItersOfTrainingTotf,

		//after shuffling
		fFeasSelec_WithConstTrain_Arrf, //const float fFeasSelec_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], to be normalized
		nY_Train_Actual_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		//const int nDimf, == nDim_D_SelecFeas_WithConstf

		nNumVecTrainTotf, //const int nVecTrainf,
		/////////////////////////

		nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_U_Globf, //const int nDim_U_Globf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		fAlphaf, //const float fAlphaf, // < 1.0
		fEpsilonf, //const float fEpsilonf,
		fCrf, //const float fCrf,
		fCf, //const float fCf,
	///////////////////////////////////////////////////
		fW_Train_Arrf, //float fW_Arrf[],
		fU_Train_Arrf, //float fU_Arrf[], //[nDim_U_Glob],

		sPasAggMaxOut_TrainResults); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'doPasAggMaxOut_SelecFeas_ForRefined' by 'PasAggMaxOut_Train'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_SelecFeas_ForRefined' by 'PasAggMaxOut_Train'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] fU_Train_Arrf;
		delete[] fW_Train_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n  After 'PasAggMaxOut_Train': sPasAggMaxOut_TrainResults.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf);

	fprintf(fout, "\n ...TrainResults.fPercentageOfCorrect_Positf = %E, ...TrainResults.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n ...TrainResults->nNumOfPosit_Y_Totf = %d, ...TrainResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n ...TrainResults->nNumOfNegat_Y_Totf = %d, ...TrainResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n///////////////////////////////////////////////////////////");
	printf("\n\n 'doPasAggMaxOut_SelecFeas_ForRefined': after 'PasAggMaxOut_Train', sPasAggMaxOut_TrainResults.fPercentageOfCorrectTotf = %E", sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf);

	printf("\n nNumOfdoPasAggMaxOut_SelecFeas_ForRefined_Tot_Glob = %d, fPercentageOfCorrect_Tot_Max_Glob = %E",
		nNumOfdoPasAggMaxOut_SelecFeas_ForRefined_Tot_Glob, fPercentageOfCorrect_Tot_Max_Glob);

	printf("\n ...TrainResults.fPercentageOfCorrect_Positf = %E, ...TrainResults.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

	printf("\n\n ...rainResults->nNumOfPosit_Y_Totf = %d, ...TrainResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

	printf("\n\n ...TrainResults->nNumOfNegat_Y_Totf = %d, ...TrainResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n\n 'doPasAggMaxOut_SelecFeas_ForRefined': after 'PasAggMaxOut_Train', sPasAggMaxOut_TrainResults.fPercentageOfCorrectTotf = %E", sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf);

	fprintf(fout, "\n nNumOfdoPasAggMaxOut_SelecFeas_ForRefined_Tot_Glob = %d, fPercentageOfCorrect_Tot_Max_Glob = %E",
		nNumOfdoPasAggMaxOut_SelecFeas_ForRefined_Tot_Glob, fPercentageOfCorrect_Tot_Max_Glob);

	fprintf(fout, "\n ...TrainResults.fPercentageOfCorrect_Positf = %E, ...TrainResults.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n ...rainResults->nNumOfPosit_Y_Totf = %d, ...TrainResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n\n ...TrainResults->nNumOfNegat_Y_Totf = %d, ...TrainResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);
	//printf("\n\n Please press any key to continue:"); fflush(fout); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifdef EXIT_AFTER_TRAINING
	delete[] fU_Train_Arrf;
	delete[] fW_Train_Arrf;

	return SUCCESSFUL_RETURN;
#endif //#ifdef EXIT_AFTER_TRAINING

	nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob = 1; // printing for nNumOfFitnessOfOneFeaVecTot_Glob == nNumOfFitnessOfOneFeaVecTot_ForPrinting
	//nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob = 0; // no printing for nNumOfFitnessOfOneFeaVecTot_Glob == nNumOfFitnessOfOneFeaVecTot_ForPrinting

	nResf = PasAggMaxOut_Test(
		fFeasSelec_WithConstTest_Arrf, // const float fFeasSelec_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
		nY_Test_Actual_Arrf, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		//const int nDimf, == nDim_D_SelecFeas_WithConstf

		nNumVecTestTotf, //const int nVecTestf,

		/////////////////////////
		nDim_SelecFeas_WithConstf, //const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_U_Globf, //const int nDim_U_Globf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		fW_Train_Arrf, //const float fW_Train_Arrf[], //[nDim_Hf]
		fU_Train_Arrf, //const float fU_Train_Arrf[], //[nDim_U_Glob],
		///////////////////////////////////////////////////

		sPasAggMaxOut_TestResults); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'doPasAggMaxOut_SelecFeas_ForRefined' by 'PasAggMaxOut_Test'");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_SelecFeas_ForRefined' by 'PasAggMaxOut_Test'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] fU_Train_Arrf;
		delete[] fW_Train_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n  'doPasAggMaxOut_SelecFeas_ForRefined': after final 'PasAggMaxOut_Test', sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf = %E, fFeaConst_Glob = %E",
		sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, fFeaConst_Glob);

	fprintf(fout, "\n nNumOfdoPasAggMaxOut_SelecFeas_ForRefined_Tot_Glob = %d, fPercentageOfCorrect_Tot_Max_Glob = %E",
		nNumOfdoPasAggMaxOut_SelecFeas_ForRefined_Tot_Glob, fPercentageOfCorrect_Tot_Max_Glob);

	fprintf(fout, "\n ...TestResults->fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);

	printf("\n\n///////////////////////////////////////////////////////////");
	printf("\n\n 'doPasAggMaxOut_SelecFeas_ForRefined': after final 'PasAggMaxOut_Test', sPasAggMaxOut_TestResults.fPercentageOfCorrectTotf = %E, fFeaConst_Glob = %E", sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, fFeaConst_Glob);


	printf("\n ...TestResults.fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

	printf("\n\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

	printf("\n\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);
	//printf("\n\n Please press any key to continue:"); fflush(fout);  getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
/////////////////////////////////////////////////////

	//fWeightedTrainTest_Efficiencyf = (fWeight_Train*sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf) + (fWeight_Test*sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf);
	fWeightedTrainTest_Efficiencyf = ((sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf + sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf)*fWeight_Train) +

		((sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf + sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf)*fWeight_Test);

#ifdef USE_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY
	if (sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf + sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf > fPercentageOfCorrect_Tot_Max_Glob)

#endif //#ifdef USE_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

#ifdef USE_JUST_TRAINING_FOR_MODEL_EFFICIENCY
		if (sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf > fPercentageOfCorrect_Tot_Max_Glob)

#endif //#ifdef USE_JUST_TRAINING_FOR_MODEL_EFFICIENCY


#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY
			if (fWeightedTrainTest_Efficiencyf > fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_ARefinedModel)

#endif //#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

			{
/*
				printf("\n\n/////////////////////////////////////////////////////////// nDim_D_SelecFeasf = %d", nDim_D_SelecFeasf);

				printf("\n 'doPasAggMaxOut_SelecFeas_ForRefined': writing the next model at fWeightedTrainTest_Efficiencyf = %E, fPercentageOfCorrect_Refined_Max_Glob = %E",
					fWeightedTrainTest_Efficiencyf, fPercentageOfCorrect_Refined_Max_Glob);

				printf("\n\n Testing the train data, sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",
					sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

				printf("\n sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf = %E, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf = %E",
					sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

				printf("\n\n sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf = %d, sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf = %d",
					sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf,
					sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

				printf("\n\n sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf = %d, sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf = %d",
					sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
					sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);
				///////////////////////////
				printf("\n sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",
					sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

				printf("\n ...TestResults->fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
					sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

				printf("\n\n///////////////////////////////////////////////////////////");
				printf("\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
					sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

				printf("\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
					sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);
*/

				fprintf(fout, "\n\n/////////////////////////////////////////////////////////// nDim_SelecFeas_WithConstf = %d", nDim_SelecFeas_WithConstf);

				fprintf(fout, "\n 'doPasAggMaxOut_SelecFeas_ForRefined': writing the next model at fWeightedTrainTest_Efficiencyf = %E, fPercentageOfCorrect_Refined_Max_Glob = %E",
					fWeightedTrainTest_Efficiencyf, fPercentageOfCorrect_Refined_Max_Glob);
				fprintf(fout, "\n\n Testing the train data, sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",

					sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

				fprintf(fout, "\n sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf = %E, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf = %E",
					sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

				fprintf(fout, "\n\n sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf = %d, sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf = %d",
					sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf,
					sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

				fprintf(fout, "\n\n sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf = %d, sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf = %d",
					sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
					sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);
				///////////////////////////
				fprintf(fout, "\n sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf = %E, fFeaConst_UnRefined_Init = %E",
					sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, fFeaConst_UnRefined_Init);

				fprintf(fout, "\n ...TestResults->fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
					sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

				fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
				fprintf(fout, "\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
					sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

				fprintf(fout, "\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
					sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);

				fprintf(fout, "\n");
				for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)
				{
					//printf("\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf, nPosOfSelec_FeasArrf[iFeaf]);
					fprintf(fout, "\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf, nPosOfSelec_FeasArrf[iFeaf]);
				} //for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)
////////////////////////////////////

				WritingARefinedModel_WithSelecFeas(
					sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf, //const float fPercentageOfCorrectTot_Trainf,
					sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf, //const float fPercentageOfCorrectTot_Testf,

					fFeaConst_Glob, //const float fConstf,
					/////////////////////////
					nDim_SelecFeasf, //const int nNumOfSelecFeasTotCurf,
					nPosOfSelec_FeasArrf, //const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
				//////////////////////

					nDim_SelecFeas_WithConstf, //, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
					nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

					nKf, //const int nKf, //nNumOfHyperplanes
					nDim_U_Globf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

					///////////////////////
					fW_Train_Arrf, //const float fW_Train_Arrf[], //[nDim_Hf]
					fU_Train_Arrf); // const float fU_Train_Arrf[]) //[nDim_U_Glob],

				//printf("\n\n 'doPasAggMaxOut_SelecFeas_ForRefined': please press any key to continue"); fflush(fout); fflush(fout_Models_Refined);  getchar();

			} //if (fWeightedTrainTest_Efficiencyf > fPercentageOfCorrect_Train_And_TestTot_Min_ForWriting_ARefinedModel)

	delete[] fU_Train_Arrf;
	delete[] fW_Train_Arrf;

	return SUCCESSFUL_RETURN;
}//int doPasAggMaxOut_SelecFeas_ForRefined(...
/////////////////////////////////////////////////////////////////////

void Print_A_FloatOneDim_Arr(
	const int nDimf, //

	const float fArrf[]) //nDimf
{
	int
		iFeaf;

	fprintf(fout, "\n\n ///////////////////////////////////////////////////////////////////////////");
	printf("\n 'Print_A_Float_Arr': nDimf = %d\n", nDimf);
	fprintf(fout, "\n 'Print_A_Float_Arr': nDimf = %d\n", nDimf);

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
	printf( "%d:%E, ", iFeaf, fArrf[iFeaf]);
	fprintf(fout, "%d:%E, ", iFeaf, fArrf[iFeaf]);
	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	fprintf(fout, "\n");
}//void Print_A_FloatOneDim_Arr(...

//////////////////////////////////////////////////////////
int Print_fU_Arr(
	const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

	const float fU_Arrf[]) //nDim_Uf
{
	int
		nIndexf,

		nIndexMaxf = nDim_Uf - 1,
		iFea_Hf,
		iHyperplanef,

		nProd_iFea_Hf_nDim_SelecFeas_WithConstf, // = nFea_Hf * nDim_D_SelecFeas_WithConstf,

		nProd_nDim_SelecFeas_WithConstf_nDim_Hf = nDim_SelecFeas_WithConstf * nDim_Hf,

		nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf,

		iFeaf;

	fprintf(fout, "\n\n ///////////////////////////////////////////////////////////////////////////");
	fprintf(fout, "\n 'Print_fU_Arr': nDim_Hf = %d, nKf = %d, nDim_SelecFeas_WithConstf = %d, nDim_Uf = %d", nDim_Hf, nKf, nDim_SelecFeas_WithConstf, nDim_Uf);

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nProd_iFea_Hf_nDim_SelecFeas_WithConstf = iFea_Hf * nDim_SelecFeas_WithConstf;
		fprintf(fout, "\n\n The next nonlinear fea: iFea_Hf = %d", iFea_Hf);

		for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;

			fprintf(fout, "\n\n The next iHyperplanef = %d, iFea_Hf = %d\n", iHyperplanef, iFea_Hf);

			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				//nIndexf = iFeaf + (nDim_D_SelecFeas_WithConstf*iFea_Hf) + (nDim_D_SelecFeas_WithConstf*nDim_Hf*iHyperplanef) ;

				nIndexf = iFeaf + nProd_iFea_Hf_nDim_SelecFeas_WithConstf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf;

				if (nIndexf > nIndexMaxf)
				{
					printf("\n\n An error in 'Print_fU_Arr': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n An error in 'Print_fU_Arr': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					getchar();	exit(1);

					return UNSUCCESSFUL_RETURN;
				}//if (nIndexf > nIndexMaxf)

				fprintf(fout, "%d:%E, ", iFeaf, fU_Arrf[nIndexf]);
			}//for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

		} //for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
	} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
}//int Print_fU_Arr(...
////////////////////////////////////////////////////////////////////////////

int OrthonormalizationOfVectorsByGrammSchimdt(
	const int nDimf, //nDim_WithConst or nDim
	const int nNumOfVecsTotf,

	float fFeaVecsAll_Arrf[])
	//float fFeaVecsAll_Orthonormal_Arrf[])
{
	int Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
		const int nDimf,
		const int nNumOfVecsTotf,
		const int nVecf,

		const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

		float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);

	int Normalizing_FloatVector_ByStDev(
		const int nDimf,
		const float fFeas_InitArrf[],

		float fFeasNormalized_Arrf[]);

	///////////////////////////////////////////
	int
		nResf,
		nIndex_1f,
		nIndex_2f,

		nTemp_1f,
		nTemp_2f,

		iVec_1f, 
		iVec_2f,  
		iFeaf; 

	float
		fFeaVec_1_Arrf[nDim_DifEvo_WithConst], //or [nDim]
		fFeaVec_2_Arrf[nDim_DifEvo_WithConst], //or [nDim]
		fFeaVecNormalized_Arrf[nDim_DifEvo_WithConst], //or [nDim]

		fCorrectingfactorf,
		fScalarProd_1f,
		fScalarProd_2f;

	for (iVec_1f = 1; iVec_1f < nNumOfVecsTotf; ++iVec_1f)
	{

		nResf = Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
			nDimf, //const int nDimf,
			nNumOfVecsTotf, //const int nNumOfVecsTotf,
			iVec_1f, //const int nVecf,

			fFeaVecsAll_Arrf, //const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

			fFeaVec_1_Arrf); //float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt' 1: nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d", iVec_1f);
			fprintf(fout, "\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt' 1: nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d", iVec_1f);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (nResf == UNSUCCESSFUL_RETURN)
/////////////////////////////////////
		nTemp_1f = iVec_1f * nDimf;

		for (iVec_2f = 0; iVec_2f < iVec_1f; ++iVec_2f)
		{
			nResf = Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
				nDimf, //const int nDimf,
				nNumOfVecsTotf, //const int nNumOfVecsTotf,
				iVec_2f, //const int nVecf,

				fFeaVecsAll_Arrf, //const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

				fFeaVec_2_Arrf); //float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt': nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d, iVec_2f = %d",
					iVec_1f, iVec_2f);
				fprintf(fout, "\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt': nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d, iVec_2f = %d",
					iVec_1f, iVec_2f);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nResf == UNSUCCESSFUL_RETURN)
///////////////////////////////
			Scalar_Product(
				nDimf, //const int nDimf,

				fFeaVec_1_Arrf, //const float fFeas_Arr_1f[],

				fFeaVec_2_Arrf, //const float fFeas_Arr_2f[],

				fScalarProd_1f); //float &fScalar_Prodf)

			Scalar_Product(
				nDimf, //const int nDimf,

				fFeaVec_2_Arrf, //const float fFeas_Arr_1f[],

				fFeaVec_2_Arrf, //const float fFeas_Arr_2f[],
				fScalarProd_2f); //float &fScalar_Prodf)

			if (fScalarProd_2f > -eps && fScalarProd_2f < eps)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt': fScalarProd_2f = %E is too small;  iVec_1f = %d, iVec_2f = %d",
					fScalarProd_2f, iVec_1f, iVec_2f);

				fprintf(fout, "\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt': fScalarProd_2f = %E is too small;  iVec_1f = %d, iVec_2f = %d",
					fScalarProd_2f, iVec_1f, iVec_2f);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (fScalarProd_2f > -feps && fScalarProd_2f < feps)

			fCorrectingfactorf = fScalarProd_1f / fScalarProd_2f;
			///////////////////////////////////////////////////
			nTemp_2f = iVec_2f * nDimf;

			for (iFeaf = 0; iFeaf < nDimf; ++iFeaf)
			{
				nIndex_1f = iFeaf + nTemp_1f; //(iVec_1f*nDimf);
				nIndex_2f = iFeaf + nTemp_2f; //iVec_2f*nDimf;

				fFeaVecsAll_Arrf[nIndex_1f] -= fCorrectingfactorf * fFeaVecsAll_Arrf[nIndex_2f];
			} // for (iFeaf = 0; iFeaf < nDimf; ++iFeaf)

		}//for (iVec_2f = 0; iVec_2f < iVec_1f; ++iVec_2f)

	}//for (iVec_1f = 1; iVec_1f < nNumOfVecsTotf; ++iVec_1f)
///////////////////////////////////////////////////////////////////////

//verifying that the vecs are orthogonal

	for (iVec_1f = 1; iVec_1f < nNumOfVecsTotf; ++iVec_1f)
	{

		nResf = Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
			nDimf, //const int nDimf,
			nNumOfVecsTotf, //const int nNumOfVecsTotf,
			iVec_1f, //const int nVecf,

			fFeaVecsAll_Arrf, //const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

			fFeaVec_1_Arrf); //float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt' 1: nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d", iVec_1f);
			fprintf(fout, "\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt' 1: nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d", iVec_1f);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (nResf == UNSUCCESSFUL_RETURN)
/////////////////////////////////////
		nTemp_1f = iVec_1f * nDimf;

		for (iVec_2f = 0; iVec_2f < iVec_1f; ++iVec_2f)
		{
			nResf = Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
				nDimf, //const int nDimf,
				nNumOfVecsTotf, //const int nNumOfVecsTotf,
				iVec_2f, //const int nVecf,

				fFeaVecsAll_Arrf, //const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

				fFeaVec_2_Arrf); //float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt': nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d, iVec_2f = %d",
					iVec_1f, iVec_2f);
				fprintf(fout, "\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt': nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d, iVec_2f = %d",
					iVec_1f, iVec_2f);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nResf == UNSUCCESSFUL_RETURN)
///////////////////////////////
			Scalar_Product(
				nDimf, //const int nDimf,

				fFeaVec_1_Arrf, //const float fFeas_Arr_1f[],

				fFeaVec_2_Arrf, //const float fFeas_Arr_2f[],

				fScalarProd_1f); //float &fScalar_Prodf)

			fprintf(fout, "\n\n  Verification of normality: fScalarProd_1f = %E, iVec_1f = %d, iVec_2f = %d",
				fScalarProd_1f, iVec_1f, iVec_2f);

#ifndef COMMENT_OUT_ALL_PRINTS
			if (fabs(fScalarProd_1f) > fLimitForOrthogonalization)
			{
				printf("\n\n A warning in verification of normality: fScalarProd_1f = %E, iVec_1f = %d, iVec_2f = %d",
					fScalarProd_1f, iVec_1f, iVec_2f);
				fprintf(fout, "\n\n  A warning in verification of normality: fScalarProd_1f = %E, iVec_1f = %d, iVec_2f = %d",
					fScalarProd_1f, iVec_1f, iVec_2f);

				printf("\n\n Please press any key:"); getchar();

			} //if ( fabs(fScalarProd_1f) > fLimitForOrthogonalization)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		}//for (iVec_2f = 0; iVec_2f < iVec_1f; ++iVec_2f)

	}//for (iVec_1f = 1; iVec_1f < nNumOfVecsTotf; ++iVec_1f)

#ifndef COMMENT_OUT_ALL_PRINTS
//	printf("\n\n After Verification of normality: please press any key to exit"); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	///////////////////////////
#ifdef NORMALIOZATION_IN_OrthonormalizationOfVectorsByGrammSchimdt
// normalizing all already orthogonal vectors 
	for (iVec_1f = 0; iVec_1f < nNumOfVecsTotf; ++iVec_1f)
	{
		nResf = Extracting_A_FloatVec_From_2DimArrOf_AllVecs(
			nDimf, //const int nDimf,
			nNumOfVecsTotf, //const int nNumOfVecsTotf,
			iVec_1f, //const int nVecf,

			fFeaVecsAll_Arrf, //const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

			fFeaVec_1_Arrf); //float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt' 2: nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d", iVec_1f);
			fprintf(fout, "\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt' 2: nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d", iVec_1f);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (nResf == UNSUCCESSFUL_RETURN)

		nResf = Normalizing_FloatVector_ByStDev(
			nDimf, //const int nDimf,
			fFeaVec_1_Arrf, //const float fFeas_InitArrf[],

			fFeaVecNormalized_Arrf); //float fFeasNormalized_Arrf[])

		nTemp_1f = iVec_1f * nDimf;
		for (iFeaf = 0; iFeaf < nDimf; ++iFeaf)
		{
			nIndex_1f = iFeaf + nTemp_1f; //(iVec_1f*nDimf);

			fFeaVecsAll_Arrf[nIndex_1f] = fFeaVecNormalized_Arrf[iFeaf];
		} // for (iFeaf = 0; iFeaf < nDimf; ++iFeaf)

	} //for (iVec_1f = 0; iVec_1f < nNumOfVecsTotf; ++iVec_1f)

#endif //#ifdef NORMALIOZATION_IN_OrthonormalizationOfVectorsByGrammSchimdt

	return SUCCESSFUL_RETURN;
} //int OrthonormalizationOfVectorsByGrammSchimdt(...
///////////////////////////////////////////////////

int OrthonormalizationOf_fU_Arrf(
	const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

	 float fU_Arrf[]) //nDim_Uf
{
	int OrthonormalizationOfVectorsByGrammSchimdt(
		const int nDimf, //nDim_WithConst or nDim
		const int nNumOfVecsTotf,

		float fFeaVecsAll_Arrf[]);

	//////////////////////////////////////////////////////////////////////////////////////////////
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);

	int
		nResf,
		nIndex_fU_Arrf,
		nIndex_fFeaVecsOf_OneNonliearFea_Arrf,

		nIndexMaxf = nDim_Uf - 1,
		iFea_Hf,
		iHyperplanef,

		nProd_iFea_Hf_nDim_SelecFeas_WithConstf, // = nFea_Hf * nDim_D_SelecFeas_WithConstf,

		nProd_nDim_SelecFeas_WithConstf_nDim_Hf = nDim_D_SelecFeas_WithConstf * nDim_Hf,

		nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf,
////////////////
		nProd_iHyperplane_nDim_D_SelecFeas_WithConstf,

		nNumOfHyperplanesForOrthonormalizationf,
		iFeaf;

	float
		fScalar_Prodf;

	//	fFeaVecCur_Arrf[nDim_D_WithConst],
		//fFeaVecCur_Arrf[nDim_D_WithConst];

		//fFeaVecsOf_OneNonliearFea_Arrf[nK * nDim_D_WithConst];

	if (nKf <= nDim_D_SelecFeas_WithConstf)
	{
		nNumOfHyperplanesForOrthonormalizationf = nKf;
	} // if (nKf <= nDim_D_SelecFeas_WithConstf)
	else
	{
		nNumOfHyperplanesForOrthonormalizationf = nDim_D_SelecFeas_WithConstf;
	} // 

	float *fFeaVecsOf_OneNonliearFea_Arrf = new float[nNumOfHyperplanesForOrthonormalizationf * nDim_D_SelecFeas_WithConstf];
	if (fFeaVecsOf_OneNonliearFea_Arrf == NULL)
	{
		printf("\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecsOf_OneNonliearFea_Arrf == NULL");
		fprintf(fout,"\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecsOf_OneNonliearFea_Arrf == NULL");
		getchar(); exit(1);
	} //if (fFeaVecsOf_OneNonliearFea_Arrf == NULL)

	float *fFeaVecCur_Arrf = new float[nDim_D_SelecFeas_WithConstf];
	if (fFeaVecCur_Arrf == NULL)
	{
		printf("\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecCur_Arrf == NULL");
		fprintf(fout, "\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecCur_Arrf == NULL");
		delete[] fFeaVecsOf_OneNonliearFea_Arrf;

		getchar(); exit(1);
	} //if (fFeaVecCur_Arrf == NULL)

	float *fFeaVecPrev_Arrf = new float[nDim_D_SelecFeas_WithConstf];
	if (fFeaVecPrev_Arrf == NULL)
	{
		printf("\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecPrev_Arrf == NULL");
		fprintf(fout, "\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecPrev_Arrf == NULL");
		delete[] fFeaVecsOf_OneNonliearFea_Arrf;
		delete[] fFeaVecCur_Arrf;

		getchar(); exit(1);
	} //if (fFeaVecPrev_Arrf == NULL)

///////////////////////////////////////////////////////
//Initialization
	for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
	{
		fFeaVecCur_Arrf[iFeaf] = fLarge;
		fFeaVecPrev_Arrf[iFeaf] = fLarge;
	}//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n ///////////////////////////////////////////////////////////////////////////");
	fprintf(fout, "\n 'OrthonormalizationOf_fU_Arrf': nDim_Hf = %d, nKf = %d, nDim_D_SelecFeas_WithConstf = %d, nDim_Uf = %d, nNumOfHyperplanesForOrthonormalizationf = %d", 
		nDim_Hf, nKf, nDim_D_SelecFeas_WithConstf, nDim_Uf, nNumOfHyperplanesForOrthonormalizationf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/*
	nProd_iFea_Hf_nDim_SelecFeas_WithConstf = iFea_Hf * nDim_D_SelecFeas_WithConstf;
		fprintf(fout, "\n\n The next nonlinear fea: iFea_Hf = %d", iFea_Hf);

		for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;

			fprintf(fout, "\n\n The next iHyperplanef = %d, iFea_Hf = %d\n", iHyperplanef, iFea_Hf);
*/
	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nProd_iFea_Hf_nDim_SelecFeas_WithConstf = iFea_Hf * nDim_D_SelecFeas_WithConstf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n The next nonlinear fea: iFea_Hf = %d", iFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		for (iHyperplanef = 0; iHyperplanef < nNumOfHyperplanesForOrthonormalizationf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_SelecFeas_WithConstf = iHyperplanef* nDim_D_SelecFeas_WithConstf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n 'OrthonormalizationOf_fU_Arrf': the next iHyperplanef = %d, iFea_Hf = %d\n", iHyperplanef, iFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;

			for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
			{
				nIndex_fFeaVecsOf_OneNonliearFea_Arrf = iFeaf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf;

				nIndex_fU_Arrf = iFeaf + nProd_iFea_Hf_nDim_SelecFeas_WithConstf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf;

				if (nIndex_fFeaVecsOf_OneNonliearFea_Arrf > nIndexMaxf)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'OrthonormalizationOf_fU_Arrf': nIndex_fU_Arrf = %d > nIndexMaxf = %d", nIndex_fU_Arrf, nIndexMaxf);
					fprintf(fout, "\n\n An error in 'OrthonormalizationOf_fU_Arrf': nIndex_fU_Arrf = %d > nIndexMaxf = %d", nIndex_fU_Arrf, nIndexMaxf);
					delete[] fFeaVecsOf_OneNonliearFea_Arrf;
					delete[] fFeaVecCur_Arrf;
					delete[] fFeaVecPrev_Arrf;

					getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				}//if (nIndex_fFeaVecsOf_OneNonliearFea_Arrf > nIndexMaxf)

				fFeaVecsOf_OneNonliearFea_Arrf[nIndex_fFeaVecsOf_OneNonliearFea_Arrf] = fU_Arrf[nIndex_fU_Arrf];

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "%d:%E, ", iFeaf, fU_Arrf[nIndex_fU_Arrf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			}//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)

		} //for (iHyperplanef = 0; iHyperplanef < nNumOfHyperplanesForOrthonormalizationf; iHyperplanef++)

		nResf = OrthonormalizationOfVectorsByGrammSchimdt(
			nDim_D_SelecFeas_WithConstf, //const int nDimf, //nDim_WithConst or nDim
			nNumOfHyperplanesForOrthonormalizationf, //const int nNumOfVecsTotf,

			fFeaVecsOf_OneNonliearFea_Arrf); // float fFeaVecsAll_Arrf[]);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'OrthonormalizationOf_fU_Arrf': nResf == UNSUCCESSFUL_RETURN for iFea_Hf = %d", iFea_Hf);
			fprintf(fout, "\n\n An error in 'OrthonormalizationOf_fU_Arrf': nResf == UNSUCCESSFUL_RETURN for iFea_Hf = %d", iFea_Hf);
			delete[] fFeaVecsOf_OneNonliearFea_Arrf;
			delete[] fFeaVecCur_Arrf;
			delete[] fFeaVecPrev_Arrf;
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (nResf == UNSUCCESSFUL_RETURN)

/////////////////////////////////
//copying backwards to 'fU_Arrf[]'
		fScalar_Prodf = -fLarge;
		for (iHyperplanef = 0; iHyperplanef < nNumOfHyperplanesForOrthonormalizationf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_SelecFeas_WithConstf = iHyperplanef * nDim_D_SelecFeas_WithConstf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n After 'Orthonormalization: the next iHyperplanef = %d, iFea_Hf = %d\n", iHyperplanef, iFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;
			
			for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
			{
				nIndex_fFeaVecsOf_OneNonliearFea_Arrf = iFeaf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf;

				nIndex_fU_Arrf = iFeaf + nProd_iFea_Hf_nDim_SelecFeas_WithConstf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf;

				fU_Arrf[nIndex_fU_Arrf] = fFeaVecsOf_OneNonliearFea_Arrf[nIndex_fFeaVecsOf_OneNonliearFea_Arrf];

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "%d:%E, ", iFeaf, fU_Arrf[nIndex_fU_Arrf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				fFeaVecCur_Arrf[iFeaf] = fFeaVecsOf_OneNonliearFea_Arrf[nIndex_fFeaVecsOf_OneNonliearFea_Arrf];

				if (iHyperplanef > 0)
				{
					fFeaVecPrev_Arrf[iFeaf] = fFeaVecCur_Arrf[iFeaf];
				} // if (iHyperplanef > 0)
			}//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)

			if (iHyperplanef > 0)
			{
				Scalar_Product(
					nDim_D_SelecFeas_WithConstf, //const int nDimf,

					fFeaVecCur_Arrf, //const float fFeas_Arr_1f[],

					fFeaVecPrev_Arrf, //const float fFeas_Arr_2f[],
					fScalar_Prodf); // float &fScalar_Prodf)

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n After 'Orthonormalization: iHyperplanef = %d, iFea_Hf = %d, fScalar_Prodf = %E\n", iHyperplanef, iFea_Hf, fScalar_Prodf);
				fprintf(fout, "\n\n After 'Orthonormalization: iHyperplanef = %d, iFea_Hf = %d, fScalar_Prodf = %E\n", iHyperplanef, iFea_Hf, fScalar_Prodf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				if (fScalar_Prodf > fScalarProdOfNormalizedHyperplanesMax)
				{
					//#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n A warning in 'OrthonormalizationOf_fU_Arrf': fScalar_Prodf = %E > fScalarProdOfNormalizedHyperplanesMax = %E\n", fScalar_Prodf, fScalarProdOfNormalizedHyperplanesMax);
					printf("\n iHyperplanef = %d, iFea_Hf = %d, fScalar_Prodf = %E\n", iHyperplanef, iFea_Hf, fScalar_Prodf);

					fprintf(fout, "\n A warning in 'OrthonormalizationOf_fU_Arrf': fScalar_Prodf = %E > fScalarProdOfNormalizedHyperplanesMax = %E\n", fScalar_Prodf, fScalarProdOfNormalizedHyperplanesMax);
					fprintf(fout,"\n iHyperplanef = %d, iFea_Hf = %d, fScalar_Prodf = %E\n", iHyperplanef, iFea_Hf, fScalar_Prodf);
					printf("\n Please press any key to exit:");  getchar(); exit(1);

					//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} // if (fScalar_Prodf > fScalarProdOfNormalizedHyperplanesMax)

			} // if (iHyperplanef > 0)

		} //for (iHyperplanef = 0; iHyperplanef < nNumOfHyperplanesForOrthonormalizationf; iHyperplanef++)

	} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	delete[] fFeaVecsOf_OneNonliearFea_Arrf;
	delete[] fFeaVecCur_Arrf;
	delete[] fFeaVecPrev_Arrf;

	return SUCCESSFUL_RETURN;
}//int OrthonormalizationOf_fU_Arrf(...
///////////////////////////////////////////////////////////////////

//see 'Print_fU_Arr('
void WritingARefinedModel_WithSelecFeas(
	const float fPercentageOfCorrectTot_Trainf,
	const float fPercentageOfCorrectTot_Testf,

	const float fConstf,
	/////////////////////////
	const int nNumOfSelecFeasTotCurf,
	const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
//////////////////////

	const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

	///////////////////////
	const float fW_Train_Arrf[], //[nDim_Hf]
	const float fU_Train_Arrf[]) //[nDim_U_Glob],
{
	int
		nIndexf,

		nIndexMaxf = nDim_Uf - 1,
		iFea_Hf,
		iHyperplanef,

		nProd_iFea_Hf_nDim_SelecFeas_WithConstf, // = nFea_Hf * nDim_D_SelecFeas_WithConstf,

		nProd_nDim_SelecFeas_WithConstf_nDim_Hf = nDim_SelecFeas_WithConstf * nDim_Hf,

		nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf,

		iFeaf;

	fprintf(fout_Models_Refined, "\n\n ///////////////////////////////////////////////////////");
	fprintf(fout_Models_Refined, "\n%E %E", fPercentageOfCorrectTot_Trainf, fPercentageOfCorrectTot_Testf);

	fprintf(fout_Models_Refined, "\n%E", fConstf);
	/////////////////////////
//new the fea positions
	//const int nNumOfSelecFeasTotCurf,
	fprintf(fout_Models_Refined, "\n%d", nNumOfSelecFeasTotCurf);

	//		const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
	for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)
	{
		fprintf(fout_Models_Refined, "\n%d", nPosOfSelec_FeasArrf[iFeaf]);
	}//for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)

	//////////////////////

	fprintf(fout_Models_Refined, "\n%d", nDim_SelecFeas_WithConstf);
	fprintf(fout_Models_Refined, "\n%d", nDim_Hf);
	fprintf(fout_Models_Refined, "\n%d", nKf);

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		fprintf(fout_Models_Refined, "\n%E", fW_Train_Arrf[iFea_Hf]);
	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
//////////////////////////////////////

	fprintf(fout_Models_Refined, "\n%d", nDim_Uf);

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nProd_iFea_Hf_nDim_SelecFeas_WithConstf = iFea_Hf * nDim_SelecFeas_WithConstf;

		for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;

			fprintf(fout_Models_Refined, "\n%d %d\n", iFea_Hf, iHyperplanef);

			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				nIndexf = iFeaf + nProd_iFea_Hf_nDim_SelecFeas_WithConstf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf;

				if (nIndexf > nIndexMaxf)
				{
					printf("\n\n An error in 'WritingARefinedModel_WithSelecFeas': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n An error in 'Print_fU_Arr': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\nPlease press any key to exit");
					getchar();	exit(1);

					//return UNSUCCESSFUL_RETURN;
				}//if (nIndexf > nIndexMaxf)

				fprintf(fout_Models_Refined, "%d:%E ", iFeaf, fU_Train_Arrf[nIndexf]);
			} //for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

		}//for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)

	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	fflush(fout_Models_Refined);
}//void WritingARefinedModel_WithSelecFeas(...
/////////////////////////////////////////////////////////////

//see 'Print_fU_Arr('
void WritingAn_UnRefinedModel_WithSelecFeas(
	const float fPercentageOfCorrectTot_Trainf,
	const float fPercentageOfCorrectTot_Testf,

	const float fConstf,

	/////////////////////////
	const int nNumOfSelecFeasTotCurf,
	const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
//////////////////////

const int nDim_SelecFeas_WithConstf, // <= dimension of the original space
const int nDim_Hf, //dimension of the nonlinear/transformed space

const int nKf, //nNumOfHyperplanes
const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

///////////////////////
const float fW_Train_Arrf[], //[nDim_Hf]
const float fU_Train_Arrf[]) //[nDim_U_Glob],
{
	int
		nIndexf,

		nIndexMaxf = nDim_Uf - 1,
		iFea_Hf,
		iHyperplanef,

		nProd_iFea_Hf_nDim_SelecFeas_WithConstf, // = nFea_Hf * nDim_D_SelecFeas_WithConstf,

		nProd_nDim_SelecFeas_WithConstf_nDim_Hf = nDim_SelecFeas_WithConstf * nDim_Hf,

		nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf,

		iFeaf;

	fprintf(fout_Models_UnRefined, "\n\n ///////////////////////////////////////////////////////");
	fprintf(fout_Models_UnRefined, "\n%E %E", fPercentageOfCorrectTot_Trainf, fPercentageOfCorrectTot_Testf);

	fprintf(fout_Models_Refined, "\n%E", fConstf);

	/////////////////////////
//new the fea positions
	//const int nNumOfSelecFeasTotCurf,
	fprintf(fout_Models_UnRefined, "\n%d", nNumOfSelecFeasTotCurf);

	//		const int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
	for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)
	{
		fprintf(fout_Models_UnRefined, "\n%d", nPosOfSelec_FeasArrf[iFeaf]);
	}//for (iFeaf = 0; iFeaf < nNumOfSelecFeasTotCurf; iFeaf++)

	//////////////////////

	fprintf(fout_Models_UnRefined, "\n%d", nDim_SelecFeas_WithConstf);
	fprintf(fout_Models_UnRefined, "\n%d", nDim_Hf);
	fprintf(fout_Models_UnRefined, "\n%d", nKf);

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		fprintf(fout_Models_UnRefined, "\n%E", fW_Train_Arrf[iFea_Hf]);
	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	fprintf(fout_Models_UnRefined, "\n%d", nDim_Uf);

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nProd_iFea_Hf_nDim_SelecFeas_WithConstf = iFea_Hf * nDim_SelecFeas_WithConstf;

		for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;

			fprintf(fout_Models_UnRefined, "\n%d %d\n", iFea_Hf, iHyperplanef);

			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				nIndexf = iFeaf + nProd_iFea_Hf_nDim_SelecFeas_WithConstf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf;

				if (nIndexf > nIndexMaxf)
				{
					printf("\n\n An error in 'WritingAn_UnRefinedModel_WithSelecFeas': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n An error in 'Print_fU_Arr': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\nPlease press any key to exit");
					getchar();	exit(1);

					//return UNSUCCESSFUL_RETURN;
				}//if (nIndexf > nIndexMaxf)

				fprintf(fout_Models_UnRefined, "%d:%E ", iFeaf, fU_Train_Arrf[nIndexf]);
			} //for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)

		}//for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)

	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	fflush(fout_Models_UnRefined);
}//void WritingAn_UnRefinedModel_WithSelecFeas(...
///////////////////////////////////////////////

void Initializing_All_Feas_Of_Popul(
	const int nDimOfAllFeas_inOnePopulf,

	const float fFeaRangeMinf,
	const float fFeaRangeMaxf,

	float fPopul_Of_FeaVecsArrf[]) //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)
{
	void Initialization_Of_OneRandFloatVec_WithinARange(
		const int nDimForRandf,

		const float fMinf,
		const float fMaxf,

		float fRandArr[]);//[nDimForRandf]

	Initialization_Of_OneRandFloatVec_WithinARange(
		nDimOfAllFeas_inOnePopulf, //const int nDimForRandf,

		fFeaRangeMinf, //const float fMinf,
		fFeaRangeMaxf, //const float fMaxf,

		fPopul_Of_FeaVecsArrf); // float fRandArr[]);//[nDimForRandf]

} //void Initializing_All_Feas_Of_Popul(...
//////////////////////////////////////////////////////////////

void Converting_AFloat_To_PositOfOneFea(

	const float fFloatInputf,

	const float fFeaRangeMinf, //0.0
	const float fFeaRangeMaxf, //1.0

	const float fWidthOfIntervalForOneFeaf,

	int &nPositOfOneFeaf)
{
	if (fFloatInputf < fFeaRangeMinf || fFloatInputf >= fFeaRangeMaxf)
	{

		nPositOfOneFeaf = FEA_DISACTIVATED;
	} //if (fFloatInputf < fFeaRangeMinf || fFloatInputf >= fFeaRangeMaxf)
	else
	{
		nPositOfOneFeaf = floor(fFloatInputf / fWidthOfIntervalForOneFeaf);
	}//else

	//printf("\n fFloatInputf = %E, fFeaRangeMaxf = %E, fFeaRangeMinf = %E, nPositOfOneFeaf = %d", fFloatInputf, fFeaRangeMaxf, fFeaRangeMinf, nPositOfOneFeaf);
} //void Converting_AFloat_To_PositOfOneFea(...

///////////////////////////////////////////////////////////
void Converting_AFloat_To_PositOfOneFea_WithAdjusting(
	const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

		const float fFloatInputf,

	const float fFeaRangeMinf, //0.0
	const float fFeaRangeMaxf, //1.0

	const float fWidthOfIntervalForOneFeaf,

	const int nPosit_OneVec_Arrf[],

	int &nPositOfOneFeaf)
{
	void Selecting_LowestUnfilled_PositionInArr(
		const int nDimf,

		const int nLowestNumberMaxf,
		const int nArrf[], // [nDimf]; // nPosit_OneVec_Arrf[]

		int &nNumLowestf);

	int
		nNumLowestf;

	if (fFloatInputf < fFeaRangeMinf || fFloatInputf >= fFeaRangeMaxf)
	{

		Selecting_LowestUnfilled_PositionInArr(
			nDim_FeasInitf, //const int nDimf,

			nLowestNumberMax, //const int nLowestNumberMaxf,
			nPosit_OneVec_Arrf, //int nArrf[], // [nDimf]; // nPosit_OneVec_Arrf[]

			nNumLowestf); // int &nNumLowestf);

		if (nNumLowestf == -1) //all fea posits from 0 to 'nLowestNumberMax' are present in 'nPosit_OneVec_Arrf[]'
		{
			nPositOfOneFeaf = FEA_DISACTIVATED;
		} // if (nNumLowestf == -1) //all fea posits from 0 to 'nLowestNumberMax' are present in 'nPosit_OneVec_Arrf[]'
		else
		{
			nPositOfOneFeaf = nNumLowestf;
		} //
	} //if (fFloatInputf < fFeaRangeMinf || fFloatInputf >= fFeaRangeMaxf)
	else
	{
		nPositOfOneFeaf = floor(fFloatInputf / fWidthOfIntervalForOneFeaf);
	}//else

	//printf("\n fFloatInputf = %E, fFeaRangeMaxf = %E, fFeaRangeMinf = %E, nPositOfOneFeaf = %d", fFloatInputf, fFeaRangeMaxf, fFeaRangeMinf, nPositOfOneFeaf);
} //void Converting_AFloat_To_PositOfOneFea_WithAdjusting(...
//////////////////////////////////////////////////////////////////////

void Converting_Popul_Of_FeaVecsArrf_To_PositsOfAllFeas(
	const int nDimOfAllFeas_inOnePopulf, //(nDim_FeasInit*nNumOfVecsInOnePopul)

//nDim_FeasInit <= nDim_DifEvo
	const float fPopul_Of_FeaVecsArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

	const float fFeaRangeMinf,
	const float fFeaRangeMaxf,

	const float fWidthOfIntervalForOneFeaf,

	int nPositsOf_Popul_FeasArrf[]) //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)
{
	void Converting_AFloat_To_PositOfOneFea(
		const float fFloatInputf,

		const float fFeaRangeMinf,
		const float fFeaRangeMaxf,

		const float fWidthOfIntervalForOneFeaf,

		int &nPositOfOneFeaf);

	int
		nPositOfOneFeaCurf,
		iFeaf;

	float
		fFloatCurf;

	for (iFeaf = 0; iFeaf < nDimOfAllFeas_inOnePopulf; iFeaf++)
	{
		fFloatCurf = fPopul_Of_FeaVecsArrf[iFeaf];

		Converting_AFloat_To_PositOfOneFea(

			fFloatCurf, //const float fFloatInputf,

			fFeaRangeMinf, //const float fFeaRangeMinf,
			fFeaRangeMaxf, //const float fFeaRangeMaxf,

			fWidthOfIntervalForOneFeaf, //const float fWidthOfIntervalForOneFeaf,

			nPositOfOneFeaCurf); // int &nPositOfOneFeaf);

		nPositsOf_Popul_FeasArrf[iFeaf] = nPositOfOneFeaCurf; // could be 'FEA_DISACTIVATED'
	}//for (iFeaf = 0; iFeaf < nDimOfAllFeas_inOnePopulf; iFeaf++)
	
} //void Converting_Popul_Of_FeaVecsArrf_To_PositsOfAllFeas(...
//////////////////////////////////////////////////

void Selecting_3_Random_Numbers_From_ARange(
	const int nDimf,

	int nArrf[], // [nDimf]; == nSeedArr_Glob[] --always the same, 0,1,2, ...., nDimf - 1

	int &nNumSelec_1f,
	int &nNumSelec_2f,
	int &nNumSelec_3f)
{
	void Shuffling_AnIntArr(
		const int nDimf,
		int nArrf[]); //[nDimf]; initially [0,1,2,...,nDimf - 1]

	srand(nSeedInit_Glob);

	Shuffling_AnIntArr(
		nDimf, //const int nDimf,
		nArrf); // int nArrf[]); //[nDimf]; initially [0,1,2,...,nDimf - 1]

	nNumSelec_1f = nArrf[0];
	nNumSelec_2f = nArrf[1];
	nNumSelec_3f = nArrf[2];

	nSeedInit_Glob += 1;
} //void Selecting_3_Random_Numbers_From_ARange(
/////////////////////////////////////////////////////////////////////////////////////////

void Selecting_LowestUnfilled_PositionInArr(
	const int nDimf,

	const int nLowestNumberMaxf,
	const int nArrf[], // [nDimf]; // nPosit_OneVec_Arrf[]

	int &nNumLowestf)
{
	int
		i1,
		nFeaFoundf,
		iFeaf;

	nNumLowestf = -1;
	for (iFeaf = 0; iFeaf < nLowestNumberMaxf; iFeaf++)
	{
		nFeaFoundf = 0; //not found yet
		for (i1 = 0; i1 < nDimf; i1++)
		{
			if (nArrf[i1] == FEA_DISACTIVATED)
				continue;
			else if (nArrf[i1] == iFeaf)
			{
				nFeaFoundf = 1;
				break;
			} // else if (nArrf[i1] == iFeaf)

		} //for (i1 = 0; i1 < nDimf; i1++)

		if (nFeaFoundf == 0)
		{
			nNumLowestf = iFeaf;
			break;
		} //if (nFeaFoundf == 0)

	} //for (iFeaf = 0; iFeaf < nLowestNumberMaxf; iFeaf++)
	
} //void Selecting_LowestUnfilled_PositionInArr(...
//////////////////////////////////////////////////////////////

int Fitness_Of_InitPopul(

				const int nDim_DifEvof,
				const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

				//const int nNumOfOneVecFromPopulf,
				const int nNumOfVecsInOnePopulTotf,

				//const float fOneFeaVec_DifEvoArrf[], //[nDim_DifEvo]
				const int nPositsOf_Popul_FeasArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

				const float fFeaConst_UnRefined_Initf,

				/////////////////////////////////////
				//train
				const int nNumOfItersOfTrainingTotf,
				const int nNumOfItersOfTraining_RefinedTotf,

				//after shuffling
				const float fFea_Train_DifEvo_Arrf[], //[nProdTrain_DifEvoTot], already normalized
				const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

				const int nNumVecTrainTotf, //== nVecTrainf,
				///////////////////////////////////////
				//test
				const float fFea_Test_DifEvo_Arrf[], //[nProdTest_DifEvoTot],
				const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

				const int nNumVecTestTotf, // == nVecTestf,

				/////////////////////////////////////

				const int nDim_D_SelecFeasf, //for reading

				const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
				const int nDim_Hf, //dimension of the nonlinear/transformed space

				const int nKf, //nNumOfHyperplanes
				//const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

				///////////////////////
				const float fAlphaf, // < 1.0
				const float fEpsilonf,
				const float fCrf,
				const float fCf,
			////////////////////////////
			int &nNumOfBestVecFromPopulf,
			float &fFitnessOfBestVecFromPopulf,

			int nNumOfActiveFeasFor_AllVecFromPopulArrf[], //[nNumOfVecsInOnePopulTotf]
			float fFitnOf_AllVecsFromPopulArrf[]) //[nNumOfVecsInOnePopulTotf]
{
	int FitnessOfOneFeaVec(
		const int nDim_DifEvof,
		const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

		const int nNumOfOneVecFromPopulf,
		const int nNumOfVecsInOnePopulTotf,

		//const float fOneFeaVec_DifEvoArrf[], //[nDim_DifEvo]
		const int nPositsOf_Popul_FeasArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

			const float fFeaConst_UnRefined_Initf,

		/////////////////////////////////////
		//train
		const int nNumOfItersOfTrainingTotf,
		const int nNumOfItersOfTraining_RefinedTotf,

		//after shuffling
		const float fFea_Train_DifEvo_Arrf[], //[nProdTrain_DifEvoTot], already normalized
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		const int nNumVecTrainTotf, //== nVecTrainf,
		///////////////////////////////////////
		//test
		const float fFea_Test_DifEvo_Arrf[], //[nProdTest_DifEvoTot],
		const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		const int nNumVecTestTotf, // == nVecTestf,

		/////////////////////////////////////

		const int nDim_D_SelecFeasf, //for reading

		const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		//const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
		////////////////////////////

		int &nNumOfActiveFeasForOneFeaVecTotf,
		float &fFitnOfOneFeaVecf);
/////////////////////////
	int
		nResf,
		nNumOfActiveFeasForOneFeaVecTotf,

		nProdTempf,
		nIndex_2Dimf,

		iFeaf,
		iVecf;

	float
		fFitnOfOneFeaVecf;
	/////////////////

	fprintf(fout, "\n\n Input training vecs in 'Fitness_Of_InitPopul': fFeaConst_UnRefined_Initf = %E, nDim_DifEvof = %d", fFeaConst_UnRefined_Initf, nDim_DifEvof);
	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		nProdTempf = iVecf * nDim_DifEvof;

		fprintf(fout, "\n Input train: iVecf = %d, ", iVecf);
		for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
		{
			nIndex_2Dimf = iFeaf + nProdTempf;
			fprintf(fout, "%d:%E, ", iFeaf, fFea_Train_DifEvo_Arrf[nIndex_2Dimf]);

		} //for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
	} //for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

	printf("\n\nPlease press any key to continue"); fflush(fout);  getchar();

	fFitnessOfBestVecFromPopulf = -fLarge;
	for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)
	{
		printf("\n 'Fitness_Of_InitPopul': before 'FitnessOfOneFeaVec', iVecf = %d", iVecf);

		//printf("\n\nPlease press any key:"); getchar();

		nResf = FitnessOfOneFeaVec(
			nDim_DifEvof, //const int nDim_DifEvof,
			nDim_FeasInitf, //const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

			iVecf, //const int nNumOfOneVecFromPopulf,
			nNumOfVecsInOnePopulTotf, //const int nNumOfVecsInOnePopulTotf,

			nPositsOf_Popul_FeasArrf, //onst int nPositsOf_Popul_FeasArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

			fFeaConst_UnRefined_Initf, //const float fFeaConst_UnRefined_Initf,

	//////////////////////

			nNumOfItersOfTrainingTotf, //const int nNumOfItersOfTrainingTotf,
			nNumOfItersOfTraining_RefinedTotf, //const int nNumOfItersOfTraining_RefinedTotf,

	//after shuffling
			fFea_Train_DifEvo_Arrf, //const float fFea_Train_DifEvo_Arrf[], //[nProd_WithConstTrainTot], already normalized
			nY_Train_Actual_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

			nNumVecTrainTotf, //const int nNumVecTrainTotf, //== nVecTrainf,
		///////////////////////////////////////
		//test
			fFea_Test_DifEvo_Arrf, //const float fFea_Test_DifEvo_Arrf[], //[nProd_WithConstTestTot],
			nY_Test_Actual_Arrf, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

			nNumVecTestTotf, //const int nNumVecTestTotf, // == nVecTestf,

			/////////////////////////////////////

			nDim_D_SelecFeasf, //const int nDim_D_SelecFeasf, //for reading

			nDim_D_SelecFeas_WithConstf, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
			nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

			nKf, //const int nKf, //nNumOfHyperplanes
			//nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

			///////////////////////
			fAlphaf, //const float fAlphaf, // < 1.0
			fEpsilonf, //const float fEpsilonf,
			fCrf, //const float fCrf,
			fCf, //const float fCf,
			////////////////////////////

			nNumOfActiveFeasForOneFeaVecTotf, //int &nNumOfActiveFeasForOneFeaVecTotf,
			fFitnOfOneFeaVecf); // float &fFitnOfOneFeaVecf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'Fitness_Of_InitPopul' by 'FitnessOfOneFeaVec', iVecf = %d", iVecf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Fitness_Of_InitPopul' by 'FitnessOfOneFeaVec', iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar();  exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

		nNumOfActiveFeasFor_AllVecFromPopulArrf[iVecf] = nNumOfActiveFeasForOneFeaVecTotf;
		fFitnOf_AllVecsFromPopulArrf[iVecf] = fFitnOfOneFeaVecf;

		if (fFitnOfOneFeaVecf > fFitnessOfBestVecFromPopulf)
		{
			fFitnessOfBestVecFromPopulf = fFitnOfOneFeaVecf;
			nNumOfBestVecFromPopulf = iVecf;
		} //if (fFitnOfOneFeaVecf > fFitnessOfBestVecFromPopulf)

		printf("\n 'Fitness_Of_InitPopul': after 'FitnessOfOneFeaVec', iVecf = %d, fFitnOfOneFeaVecf = %E", iVecf, fFitnOfOneFeaVecf);
		printf("\n\n So far: fPercentageOfCorrect_NoRefin_Glob = %E, fPercentageOfCorrect_Refined_Max_Glob = %E", fPercentageOfCorrect_NoRefin_Glob, fPercentageOfCorrect_Refined_Max_Glob);


//	printf("\n\nPlease press any key:"); getchar();
	}//for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)

	return SUCCESSFUL_RETURN;
} //int Fitness_Of_InitPopul(...
/////////////////////////////////////////////////////////

void Generating_nNumRandFeas_ForAllGenersArr(
	const int nDim_FeasInitf,
	const int nDimOfAllVecs_inAllGenersf, // == nNumOfVecsInOnePopulTot*nNumOfGenerTot

	int nNumRandFeas_ForAllGenersArrf[]) // [nDimOfAllVecs_inAllGenersf] // < nDim_FeasInitf
{
	void Initialization_Of_OneRand_IntVec_WithinARange(
		const int nDimForRandf,

		const int nMinf,
		const int nMaxf,

		int nRandArr[]); //[nDimForRandf]
//////////
	Initialization_Of_OneRand_IntVec_WithinARange(
		nDimOfAllVecs_inAllGenersf, //const int nDimForRandf,

		0, //const int nMinf,
		nDim_FeasInitf, //const int nMaxf,

		nNumRandFeas_ForAllGenersArrf); // int nRandArr[]); //[nDimForRandf]

} //void Generating_nNumRandFeas_ForAllGenersArr(...
////////////////////////////////////////////////////////////

void Generating_3_Arrs_OfRandSelecOfVecsForAllGeners(
	const int nNumOfVecsInOnePopulTotf,

	const int nNumOfGenerTotf,
	const int nDimOfAllVecs_inAllGenersf, // == nNumOfVecsInOnePopulTot*nNumOfGenerTot

	int nNumRandSelecOfVecsForAllGeners_1Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
	int nNumRandSelecOfVecsForAllGeners_2Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
	int nNumRandSelecOfVecsForAllGeners_3Arrf[]) //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
{
	void Selecting_3_Random_Numbers_From_ARange(
		const int nDimf,

		int nArrf[], // [nDimf]; == nSeedArr_Glob[] --always the same, 0,1,2, ...., nDimf - 1

		int &nNumSelec_1f,
		int &nNumSelec_2f,
		int &nNumSelec_3f);

	int
		iVecf,
		iGenerf,

		nIndexOfVec_2Dimf,

		nProdCurf,
		nNumSelec_1f,
		nNumSelec_2f,
		nNumSelec_3f,
		nVecsOfPopulf[nNumOfVecsInOnePopulTot];

	for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)
		nVecsOfPopulf[iVecf] = iVecf;

	for (iGenerf = 0; iGenerf < nNumOfGenerTotf; iGenerf++)
	{
		nProdCurf = iGenerf * nNumOfVecsInOnePopulTotf;

		for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)
		{
			Selecting_3_Random_Numbers_From_ARange(
				nNumOfVecsInOnePopulTotf, //const int nDimf,

				nVecsOfPopulf, //int nArrf[], // [nDimf]; == nSeedArr_Glob[] --always the same, 0,1,2, ...., nDimf - 1

				nNumSelec_1f, //int &nNumSelec_1f,
				nNumSelec_2f, //int &nNumSelec_2f,
				nNumSelec_3f); // int &nNumSelec_3f);

			nIndexOfVec_2Dimf = iVecf + nProdCurf;

			nNumRandSelecOfVecsForAllGeners_1Arrf[nIndexOfVec_2Dimf] = nNumSelec_1f;
			nNumRandSelecOfVecsForAllGeners_2Arrf[nIndexOfVec_2Dimf] = nNumSelec_2f;
			nNumRandSelecOfVecsForAllGeners_3Arrf[nIndexOfVec_2Dimf] = nNumSelec_3f;

		} // for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)
	} //for (iGenerf = 0; iGenerf < nNumOfGenerTotf; iGenerf++)

} //void Generating_3_Arrs_OfRandSelecOfVecsForAllGeners(...
/////////////////////////////////////////////////////

void Generating_nFeaNumsForTrialOrNot_AllGenersArrf(

	const float fProbOfFeaForTrialMaxf,

	const int nDimOfAllFeas_inAllGenersf, //(nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)

	const float fFeaRangeMinf,
	const float fFeaRangeMaxf,

	const float fWidthOfIntervalForOneFeaf,

	int nFeaNumsForTrialOrNot_AllGenersArrf[]) //[nDimOfAllFeas_inAllGeners] = (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)
{
	void Initialization_Of_OneRandFloatVec_WithinARange(
		const int nDimForRandf,

		const float fMinf,
		const float fMaxf,

		float fRandArr[]); //[nDimForRandf]

///////////////////////////
	int
		//nIndex_3Dimf,
		iFeaf;

	float
		fAllFeas_InAllGenersArrf[nDimOfAllFeas_inAllGeners];

	Initialization_Of_OneRandFloatVec_WithinARange(
		nDimOfAllFeas_inAllGenersf, //const int nDimForRandf,

		fFeaRangeMinf, //const float fMinf,
		fFeaRangeMaxf, //const float fMaxf,

		fAllFeas_InAllGenersArrf); // float fRandArr[]); //[nDimForRandf]

	for (iFeaf = 0; iFeaf < nDimOfAllFeas_inAllGenersf; iFeaf++)
	{
		if (fAllFeas_InAllGenersArrf[iFeaf] < fProbOfFeaForTrialMaxf)
		{
			nFeaNumsForTrialOrNot_AllGenersArrf[iFeaf] = FEA_FOR_TRIAL; //1

		}//if (fAllFeas_InAllGenersArrf[iFeaf] < fProbOfFeaForTrialMaxf)
		else
		{
			nFeaNumsForTrialOrNot_AllGenersArrf[iFeaf] = FEA_NOT_FOR_TRIAL; //0
		} //else

		fprintf(fout, "\n nFeaNumsForTrialOrNot_AllGenersArrf[%d] = %d", iFeaf, nFeaNumsForTrialOrNot_AllGenersArrf[iFeaf]);

	} //for (iFeaf = 0; iFeaf < nDimOfAllFeas_inAllGenersf; iFeaf++)

	//nIndex_3Dimf = iFeaf + (nNumOfOneVecFromPopulf*nDim_FeasInitf) + (nNumOfGenerCurf*nDim_FeasInitf*nNumOfVecsInOnePopulTotf);

} // void  Generating_nFeaNumsForTrialOrNot_AllGenersArrf(...
////////////////////////////////////////////////

int Updating_And_Testing_OneVecFromPopul(

	const float fCoefForTrialf,

	const int nDim_DifEvof,
	const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

	const int nNumOfGenerTotf,
	const int nNumOfGenerCurf,

	const int nNumOfOneVecFromPopulf,
	const int nNumOfVecsInOnePopulTotf,
	//////////////////////////////////

	const int nNumRandFeas_ForAllGenersArrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot); // < nDim_FeasInitf

//////////////////////////////
	const int nNumRandSelecOfVecsForAllGeners_1Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
	const int nNumRandSelecOfVecsForAllGeners_2Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
	const int nNumRandSelecOfVecsForAllGeners_3Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot

	///////////////////////////////////
	const int nFeaNumsForTrialOrNot_AllGenersArrf[], //[nDimOfAllFeas_inAllGeners] = (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)

///////////////////////////

	const float fFeaRangeMinf,
	const float fFeaRangeMaxf,

	const float fWidthOfIntervalForOneFeaf,
	//////////////

	const float fFeaConst_UnRefined_Initf,
	/////////////////////////////////////
	//train
	const int nNumOfItersOfTrainingTotf,
	const int nNumOfItersOfTraining_RefinedTotf,

	//after shuffling
	const float fFea_Train_DifEvo_Arrf[], //[nProdTrain_DifEvoTot], already normalized
	const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	const int nNumVecTrainTotf, //== nVecTrainf,
	///////////////////////////////////////
	//test
	const float fFea_Test_DifEvo_Arrf[], //[nProdTest_DifEvoTot],
	const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

	const int nNumVecTestTotf, // == nVecTestf,
	/////////////////////////////////////

	const int nDim_D_SelecFeasf, //for reading

	const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	//const int nDim_Uf, //(nDim_D_WithConst*nDim_Hf*nKf)

	///////////////////////
	const float fAlphaf, // < 1.0
	const float fEpsilonf,
	const float fCrf,
	const float fCf,
	////////////////////////////

//could be updated			
	int nNumOfActiveFeasFor_AllVecFromPopulArrf[], //[nNumOfVecsInOnePopulTotf] // == 100
	float fFitnOf_AllVecsFromPopulArrf[], //[nNumOfVecsInOnePopulTotf] //100

	float fPopul_Of_FeaVecsArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

	int nPositsOf_Popul_FeasArrf[]) //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

{
	void Converting_AFloat_To_PositOfOneFea(

		const float fFloatInputf,

		const float fFeaRangeMinf, //0.0
		const float fFeaRangeMaxf, //1.0

		const float fWidthOfIntervalForOneFeaf,

		int &nPositOfOneFeaf);

	void Converting_AFloat_To_PositOfOneFea_WithAdjusting(
		const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

		const float fFloatInputf,

		const float fFeaRangeMinf, //0.0
		const float fFeaRangeMaxf, //1.0

		const float fWidthOfIntervalForOneFeaf,

		const int nPosit_OneVec_Arrf[],

		int &nPositOfOneFeaf);

	void Copying_Int_Arr1_To_Arr2(
		const int nDimf,
		const int nArr1f[], // [nDimf]
		int nArr2f[]); // [nDimf]

	int Extracting_An_IntVec_From_2DimArrOf_AllVecs(
		const int nDimf,
		const int nNumOfVecsTotf,
		const int nNumOfOneVecf,

		const int nPosit_All_Arrf[], //[nDimf*nNumOfVecsTotf]

		int nPosit_OneVec_Arrf[]); //[nDimf]

	int FitnessOfOneFeaVec(
		const int nDim_DifEvof,
		const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

		const int nNumOfOneVecFromPopulf,
		const int nNumOfVecsInOnePopulTotf,

		//const float fOneFeaVec_DifEvoArrf[], //[nDim_DifEvo]
		const int nPositsOf_Popul_FeasArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

		const float fFeaConst_UnRefined_Initf,

		/////////////////////////////////////
		//train
		const int nNumOfItersOfTrainingTotf,
		const int nNumOfItersOfTraining_RefinedTotf,

		//after shuffling
		const float fFea_Train_DifEvo_Arrf[], //[nProdTrain_DifEvoTot], already normalized
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		const int nNumVecTrainTotf, //== nVecTrainf,
		///////////////////////////////////////
		//test
		const float fFea_Test_DifEvo_Arrf[], //[nProdTest_DifEvoTot],
		const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		const int nNumVecTestTotf, // == nVecTestf,

		/////////////////////////////////////

		const int nDim_D_SelecFeasf, //

		const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		//const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
		////////////////////////////

		int &nNumOfActiveFeasForOneFeaVecTotf,
		float &fFitnOfOneFeaVecf);
	
	int
		nResf,
		iFeaf,

		nRandFeaf,
		nNumRandSelecVec_1f,
		nNumRandSelecVec_2f,
		nNumRandSelecVec_3f,

		nIndexOfVec_2Dimf,
		nIndexOfFea_2Dimf,

		nIndexOfFeaNum_2Dim_1f,
		nIndexOfFeaNum_2Dim_2f,
		nIndexOfFeaNum_2Dim_3f,

		nIndex_3Dimf,
		nProdTempf,

		nPositsOfFeaPrevf, 
		nPositOfOneFeaTrial_Initf,

		nPositOfOneFeaTrialf,

		nNumOfTrialFeasTotf = 0,

		nNumOfTrialFeasCurf = 0,

		nIs_nRandFeafBelongsToTrialFeasf = 0, //not initially

			nPosit_OneVec_Arrf[nDim_FeasInit],

		nNumOfActiveFeasForOneFeaVecTot_Trialf,
		nPositsOf_Popul_Feas_TrialArrf[nDimOfAllFeas_inOnePopul], // = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

		nFeaNumsForTrialOrNotOneDimArrf[nDim_FeasInit];
		////////////////////////////
	float
		fTrial_Curf, 

		//fTrialArrf[2],
		fFitnOfOneFeaVec_Trialf,

		fFitnOfOneFeaVec_Curf,
		fFeaFromPopul_1f,
		fFeaFromPopul_2f,
		fFeaFromPopul_3f;
/////////////////////////////////

	nIndexOfVec_2Dimf = nNumOfOneVecFromPopulf + (nNumOfGenerCurf*nNumOfVecsInOnePopulTotf);
	//
	if (nIndexOfVec_2Dimf < 0 || nIndexOfVec_2Dimf >= nDimOfAllVecs_inAllGeners)
	{
		printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nIndexOfVec_2Dimf = %d >= nDimOfAllVecs_inAllGeners = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
			nIndexOfVec_2Dimf, nDimOfAllVecs_inAllGeners, nNumOfOneVecFromPopulf, nNumOfGenerCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'Updating_And_Testing_OneVecFromPopul': nIndexOfVec_2Dimf = %d >= nDimOfAllVecs_inAllGeners = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
			nIndexOfVec_2Dimf, nDimOfAllVecs_inAllGeners, nNumOfOneVecFromPopulf, nNumOfGenerCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nIndexOfVec_2Dimf < 0 || nIndexOfVec_2Dimf >= nDimOfAllVecs_inAllGeners)

	nRandFeaf = nNumRandFeas_ForAllGenersArrf[nIndexOfVec_2Dimf];

	if (nRandFeaf < 0)
	{
		nRandFeaf = 0;
		//printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nRandFeaf = %d >= nDim_FeasInitf = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
			//nRandFeaf, nDim_FeasInitf, nNumOfOneVecFromPopulf, nNumOfGenerCurf);

	} //if (nRandFeaf < 0)

	if (nRandFeaf >= nDim_FeasInitf)
	{
	//	printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nRandFeaf = %d >= nDim_FeasInitf = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
		//	nRandFeaf, nDim_FeasInitf, nNumOfOneVecFromPopulf, nNumOfGenerCurf);

		nRandFeaf = nDim_FeasInitf - 1;
	} //if (nRandFeaf >= nDim_FeasInitf)

		nNumRandSelecVec_1f = nNumRandSelecOfVecsForAllGeners_1Arrf[nIndexOfVec_2Dimf];
		nNumRandSelecVec_2f = nNumRandSelecOfVecsForAllGeners_2Arrf[nIndexOfVec_2Dimf];
		nNumRandSelecVec_3f = nNumRandSelecOfVecsForAllGeners_3Arrf[nIndexOfVec_2Dimf];

		if (nNumRandSelecVec_1f < 0 || nNumRandSelecVec_1f >= nNumOfVecsInOnePopulTotf || nNumRandSelecVec_2f < 0 || nNumRandSelecVec_2f >= nNumOfVecsInOnePopulTotf || 
			nNumRandSelecVec_3f < 0 || nNumRandSelecVec_3f >= nNumOfVecsInOnePopulTotf)
		{
			printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nNumRandSelecVec_1f = %d, nNumRandSelecVec_2f = %d, nNumRandSelecVec_3f = %d >= nNumOfVecsInOnePopulTotf = %d",
				nNumRandSelecVec_1f, nNumRandSelecVec_2f, nNumRandSelecVec_3f, nNumOfVecsInOnePopulTotf);

			printf("\n nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",nNumOfOneVecFromPopulf, nNumOfGenerCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Updating_And_Testing_OneVecFromPopul': nNumRandSelecVec_1f = %d, nNumRandSelecVec_2f = %d, nNumRandSelecVec_3f = %d >= nNumOfVecsInOnePopulTotf = %d",
				nNumRandSelecVec_1f, nNumRandSelecVec_2f, nNumRandSelecVec_3f, nNumOfVecsInOnePopulTotf);

			fprintf(fout,"\n nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d", nNumOfOneVecFromPopulf, nNumOfGenerCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar();  exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (nNumRandSelecVec_1f < 0 || nNumRandSelecVec_1f >= nNumOfVecsInOnePopulTotf || ...
////////////////////////////////////

		Copying_Int_Arr1_To_Arr2(
			nDimOfAllFeas_inOnePopul, //const int nDimf,
			nPositsOf_Popul_FeasArrf, //const int nArr1f[], // [nDimf]
			
			nPositsOf_Popul_Feas_TrialArrf); // int nArr2f[]); // [nDimf]
///////////////////////////

	for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)
	{
	
/*
const int nNumOfGenerTotf,
	const int nNumOfGenerCurf,

	const int nNumOfOneVecFromPopulf,
	const int nNumOfVecsInOnePopulTotf,
*/
		nIndex_3Dimf = iFeaf + (nNumOfOneVecFromPopulf*nDim_FeasInitf) + (nNumOfGenerCurf*nDim_FeasInitf*nNumOfVecsInOnePopulTotf);

		if (nIndex_3Dimf < 0 || nIndex_3Dimf >= nDimOfAllFeas_inAllGeners)
		{
			printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nIndex_3Dimf = %d >= nDimOfAllFeas_inAllGeners = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
				nIndex_3Dimf, nDimOfAllFeas_inAllGeners, nNumOfOneVecFromPopulf, nNumOfGenerCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in ''Updating_And_Testing_OneVecFromPopul': nIndex_3Dimf = %d >= nDimOfAllFeas_inAllGeners = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
				nIndex_3Dimf, nDimOfAllFeas_inAllGeners, nNumOfOneVecFromPopulf, nNumOfGenerCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar();  exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (nIndex_3Dimf < 0 || nIndex_3Dimf >= nDimOfAllFeas_inAllGeners)

		nFeaNumsForTrialOrNotOneDimArrf[iFeaf] = nFeaNumsForTrialOrNot_AllGenersArrf[nIndex_3Dimf]; //either 'FEA_FOR_TRIAL' or 'FEA_NOT_FOR_TRIAL'

		//fprintf(fout, "\n 'Updating_And_Testing_OneVecFromPopul': nIndex_3Dimf = %d, nFeaNumsForTrialOrNotOneDimArrf[%d] = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
			//nIndex_3Dimf, iFeaf, nFeaNumsForTrialOrNotOneDimArrf[iFeaf], nNumOfOneVecFromPopulf, nNumOfGenerCurf);

	} //for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)

//////////////////////////////
	nNumOfTrialFeasTotf = 0;

	for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)
	{
		if (nFeaNumsForTrialOrNotOneDimArrf[iFeaf] == FEA_FOR_TRIAL)
		{
			nNumOfTrialFeasTotf += 1;

			if (nRandFeaf == iFeaf)
				nIs_nRandFeafBelongsToTrialFeasf = 1;

		} //if (nFeaNumsForTrialOrNotOneDimArrf[iFeaf] == FEA_FOR_TRIAL)
	} //for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)

//#ifndef COMMENT_OUT_ALL_PRINTS
	printf( "\n\n 'Updating_And_Testing_OneVecFromPopul': nNumOfTrialFeasTotf = %d, nIs_nRandFeafBelongsToTrialFeasf = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
		nNumOfTrialFeasTotf, nIs_nRandFeafBelongsToTrialFeasf, nNumOfOneVecFromPopulf, nNumOfGenerCurf);

	fprintf(fout, "\n\n 'Updating_And_Testing_OneVecFromPopul': nNumOfTrialFeasTotf = %d, nIs_nRandFeafBelongsToTrialFeasf = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
		nNumOfTrialFeasTotf, nIs_nRandFeafBelongsToTrialFeasf, nNumOfOneVecFromPopulf, nNumOfGenerCurf);
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nIs_nRandFeafBelongsToTrialFeasf == 0)
	{
//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n Adding  nRandFeaf = %d to nNumOfTrialFeasTotf = %d, nIs_nRandFeafBelongsToTrialFeasf = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
			nRandFeaf, nNumOfTrialFeasTotf, nIs_nRandFeafBelongsToTrialFeasf, nNumOfOneVecFromPopulf, nNumOfGenerCurf);

		fprintf(fout, "\n Adding  nRandFeaf = %d to nNumOfTrialFeasTotf = %d, nIs_nRandFeafBelongsToTrialFeasf = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
			nRandFeaf, nNumOfTrialFeasTotf, nIs_nRandFeafBelongsToTrialFeasf, nNumOfOneVecFromPopulf, nNumOfGenerCurf);
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nNumOfTrialFeasTotf += 1;
	}//if (nIs_nRandFeafBelongsToTrialFeasf == 0)

	if (nNumOfTrialFeasTotf <= 0 || nNumOfTrialFeasTotf > nDim_FeasInitf)
	{
		printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nNumOfTrialFeasTotf = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
			nNumOfTrialFeasTotf,nNumOfOneVecFromPopulf, nNumOfGenerCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in ''Updating_And_Testing_OneVecFromPopul': nNumOfTrialFeasTotf = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
			nNumOfTrialFeasTotf, nNumOfOneVecFromPopulf, nNumOfGenerCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfTrialFeasTotf <= 0 || nNumOfTrialFeasTotf > nDim_FeasInitf)

	float *fTrialArrf = new float[nNumOfTrialFeasTotf];
	if (fTrialArrf == NULL)
	{
		printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': fTrialArrf == NULL");
		getchar(); exit(1);
	} //if (fTrialArrf == NULL)

	for (iFeaf = 0; iFeaf < nNumOfTrialFeasTotf; iFeaf++)
		fTrialArrf[iFeaf] = -1.0;
//////////////////////////

	nProdTempf = nNumOfOneVecFromPopulf * nDim_FeasInitf;
	nNumOfTrialFeasCurf = 0;
	for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)
	{
		if (nFeaNumsForTrialOrNotOneDimArrf[iFeaf] == FEA_FOR_TRIAL || iFeaf == nRandFeaf)
		{
			nNumOfTrialFeasCurf += 1;
			
			nIndexOfFeaNum_2Dim_1f = iFeaf + (nNumRandSelecVec_1f*nDim_FeasInitf);

			if (nIndexOfFeaNum_2Dim_1f < 0 || nIndexOfFeaNum_2Dim_1f >= nDimOfAllFeas_inOnePopul)
			{
				printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nIndexOfFeaNum_2Dim_1f = %d >= nDimOfAllFeas_inOnePopul = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
					nIndexOfFeaNum_2Dim_1f, nDimOfAllFeas_inOnePopul, nNumOfOneVecFromPopulf, nNumOfGenerCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in ''Updating_And_Testing_OneVecFromPopul': nIndexOfFeaNum_2Dim_1f = %d >= nDimOfAllFeas_inOnePopul = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
					nIndexOfFeaNum_2Dim_1f, nDimOfAllFeas_inOnePopul, nNumOfOneVecFromPopulf, nNumOfGenerCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] fTrialArrf;
				getchar();  exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfFeaNum_2Dim_1f < 0 || nIndexOfFeaNum_2Dim_1f >= nDimOfAllFeas_inOnePopul)

			fFeaFromPopul_1f = fPopul_Of_FeaVecsArrf[nIndexOfFeaNum_2Dim_1f]; //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

///////////////////
			nIndexOfFeaNum_2Dim_2f = iFeaf + (nNumRandSelecVec_2f*nDim_FeasInitf);

			if (nIndexOfFeaNum_2Dim_2f < 0 || nIndexOfFeaNum_2Dim_2f >= nDimOfAllFeas_inOnePopul)
			{
				printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nIndexOfFeaNum_2Dim_2f = %d >= nDimOfAllFeas_inOnePopul = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
					nIndexOfFeaNum_2Dim_2f, nDimOfAllFeas_inOnePopul, nNumOfOneVecFromPopulf, nNumOfGenerCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in ''Updating_And_Testing_OneVecFromPopul': nIndexOfFeaNum_2Dim_2f = %d >= nDimOfAllFeas_inOnePopul = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
					nIndexOfFeaNum_2Dim_2f, nDimOfAllFeas_inOnePopul, nNumOfOneVecFromPopulf, nNumOfGenerCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] fTrialArrf;
				getchar();  exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfFeaNum_2Dim_2f < 0 || nIndexOfFeaNum_2Dim_2f >= nDimOfAllFeas_inOnePopul)

			fFeaFromPopul_2f = fPopul_Of_FeaVecsArrf[nIndexOfFeaNum_2Dim_2f];
//////////////////////////////

			nIndexOfFeaNum_2Dim_3f = iFeaf + (nNumRandSelecVec_3f*nDim_FeasInitf);

			if (nIndexOfFeaNum_2Dim_3f < 0 || nIndexOfFeaNum_2Dim_3f >= nDimOfAllFeas_inOnePopul)
			{
				printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nIndexOfFeaNum_2Dim_3f = %d >= nDimOfAllFeas_inOnePopul = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
					nIndexOfFeaNum_2Dim_3f, nDimOfAllFeas_inOnePopul, nNumOfOneVecFromPopulf, nNumOfGenerCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in ''Updating_And_Testing_OneVecFromPopul': nIndexOfFeaNum_2Dim_3f = %d >= nDimOfAllFeas_inOnePopul = %d, nNumOfOneVecFromPopulf = %d, nNumOfGenerCurf = %d",
					nIndexOfFeaNum_2Dim_3f, nDimOfAllFeas_inOnePopul, nNumOfOneVecFromPopulf, nNumOfGenerCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] fTrialArrf;
				getchar();  exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfFeaNum_2Dim_3f < 0 || nIndexOfFeaNum_2Dim_3f >= nDimOfAllFeas_inOnePopul)

			fFeaFromPopul_3f = fPopul_Of_FeaVecsArrf[nIndexOfFeaNum_2Dim_3f];

////////////////
			fTrial_Curf = fFeaFromPopul_1f + ( fCoefForTrialf * (fFeaFromPopul_2f - fFeaFromPopul_3f) );

			//fprintf(fout, "\n\n The next fTrial_Curf = %E, iFeaf = %d, iVec_Updating_Glob = %d, nNumOfGener_Glob = %d", fTrial_Curf, iFeaf, iVec_Updating_Glob, nNumOfGener_Glob);
			//fprintf(fout, "\n fFeaFromPopul_1f = %E, fFeaFromPopul_2f = %E, fFeaFromPopul_3f = %E, fCoefForTrialf = %E", 
				//fFeaFromPopul_1f, fFeaFromPopul_2f, fFeaFromPopul_3f, fCoefForTrialf);

			if (fTrial_Curf < fFeaRangeMinf || fTrial_Curf > fFeaRangeMaxf)
			{
				nNumOf_TrialFeas_OffLimits_Glob += 1;
				fprintf(fout, "\n\n A new nNumOf_TrialFeas_OffLimits_Glob = %d, fTrial_Curf = %E, iFeaf = %d", 
					nNumOf_TrialFeas_OffLimits_Glob, fTrial_Curf, iFeaf);

				fprintf(fout, "\n iVec_Updating_Glob = %d, nNumOfGener_Glob = %d", iVec_Updating_Glob, nNumOfGener_Glob);
			}//if (fTrial_Curf < fFeaRangeMinf || fTrial_Curf > fFeaRangeMaxf)

			if (fTrial_Curf < fFeaRangeMin_WithDeviat)
				fTrial_Curf = fFeaRangeMin_WithDeviat;

			if (fTrial_Curf > fFeaRangeMax_WithDeviat)
				fTrial_Curf = fFeaRangeMax_WithDeviat;

			if (fTrial_Curf < fFeaRangeMinf || fTrial_Curf > fFeaRangeMaxf)
			{
				nResf = Extracting_An_IntVec_From_2DimArrOf_AllVecs(
					nDim_FeasInitf, //const int nDimf,

					nNumOfVecsInOnePopulTotf, //const int nNumOfVecsTotf,

					nNumOfOneVecFromPopulf,//const int nNumOfOneVecf,

					nPositsOf_Popul_FeasArrf, //const int nPosit_All_Arrf[], //[nDimf*nNumOfVecsTotf]

					nPosit_OneVec_Arrf); // int nPosit_OneVec_Arrf[]) //[nDimf]

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul' by 'Extracting_An_IntVec_From_2DimArrOf_AllVecs'");

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n  An error in 'Updating_And_Testing_OneVecFromPopul' by 'Extracting_An_IntVec_From_2DimArrOf_AllVecs'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					delete[] fTrialArrf;
					getchar();  exit(1);
					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

				Converting_AFloat_To_PositOfOneFea_WithAdjusting(
					nDim_FeasInitf, //const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof
					fTrial_Curf, //const float fFloatInputf,

					fFeaRangeMinf, //const float fFeaRangeMinf,
					fFeaRangeMaxf, //const float fFeaRangeMaxf,

					fWidthOfIntervalForOneFeaf, //const float fWidthOfIntervalForOneFeaf,

					nPosit_OneVec_Arrf, //const int nPosit_OneVec_Arrf[],

					nPositOfOneFeaTrialf); // int &nPositOfOneFeaf);

				if (nPositOfOneFeaTrialf != FEA_DISACTIVATED) // a fea <= nLowestNumberMax has been added
				{
//adjusting 'fPopul_Of_FeaVecsArrf[]' -- below
					if (nPositOfOneFeaTrialf < 0 || nPositOfOneFeaTrialf > nLowestNumberMax)
					{
						printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nPositOfOneFeaTrialf = %d < 0 || nPositOfOneFeaTrialf > nLowestNumberMax = %d",nPositOfOneFeaTrialf,nLowestNumberMax);

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout, "\n\n  An error in 'Updating_And_Testing_OneVecFromPopul': nPositOfOneFeaTrialf = %d < 0 || nPositOfOneFeaTrialf > nLowestNumberMax = %d", nPositOfOneFeaTrialf, nLowestNumberMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						delete[] fTrialArrf;
						getchar();  exit(1);
						return UNSUCCESSFUL_RETURN;
					}//if (nPositOfOneFeaTrialf < 0 || nPositOfOneFeaTrialf > nLowestNumberMax)

					fTrial_Curf = (float)(nPositOfOneFeaTrialf)*fWidthOfIntervalForOneFeaf + fWidthOfIntervalForOneFeaf / 2.0;
				} //if (nPositOfOneFeaTrialf != FEA_DISACTIVATED)
				else
				{
					//fTrial_Curf remains the same, beyond the range [fFeaRangeMinf,fFeaRangeMaxf)
				} //else

				goto MarkIndex;
			} //if (fTrial_Curf < fFeaRangeMinf || fTrial_Curf > fFeaRangeMaxf)

			Converting_AFloat_To_PositOfOneFea(
				fTrial_Curf, //const float fFloatInputf,

				fFeaRangeMinf, //const float fFeaRangeMinf,
				fFeaRangeMaxf, //const float fFeaRangeMaxf,

				fWidthOfIntervalForOneFeaf, //const float fWidthOfIntervalForOneFeaf,

				nPositOfOneFeaTrialf); // int &nPositOfOneFeaf);

			//(nDim_FeasInit*nNumOfVecsInOnePopulTot)

		MarkIndex: 			nIndexOfFea_2Dimf = iFeaf + nProdTempf;

			if (nIndexOfFea_2Dimf >= nDimOfAllFeas_inOnePopul)
			{
				printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nIndexOfFea_2Dimf = %d >= nDimOfAllFeas_inOnePopul = %d", nIndexOfFea_2Dimf,nDimOfAllFeas_inOnePopul);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n An error in 'Updating_And_Testing_OneVecFromPopul': nIndexOfFea_2Dimf = %d >= nDimOfAllFeas_inOnePopul = %d", nIndexOfFea_2Dimf, nDimOfAllFeas_inOnePopul);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] fTrialArrf;
				getchar();  exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfFea_2Dimf >= nDimOfAllFeas_inOnePopul)

//#ifndef COMMENT_OUT_ALL_PRINTS
			printf( "\n\n Before 'fTrialArrf[]', fTrial_Curf = %E, nNumOfTrialFeasCurf = %d, nIndexOfFea_2Dimf = %d,  iFeaf = %d, nNumOfOneVecFromPopulf = %d",
				fTrial_Curf, nNumOfTrialFeasCurf, nIndexOfFea_2Dimf, iFeaf, nNumOfOneVecFromPopulf);

			fprintf(fout, "\n\n Before 'fTrialArrf[]', fTrial_Curf = %E, nNumOfTrialFeasCurf = %d, nIndexOfFea_2Dimf = %d,  iFeaf = %d, nNumOfOneVecFromPopulf = %d",
				fTrial_Curf, nNumOfTrialFeasCurf, nIndexOfFea_2Dimf, iFeaf, nNumOfOneVecFromPopulf);

			printf("\n\nPlease press any key to continue"); fflush(fout);  getchar();
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (nNumOfTrialFeasCurf - 1 < 0 || nNumOfTrialFeasCurf - 1 >= nNumOfTrialFeasTotf)
			{
				printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul' 1: nNumOfTrialFeasCurf - 1  = %d >= nNumOfTrialFeasTotf = %d", 
					nNumOfTrialFeasCurf - 1, nNumOfTrialFeasTotf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n  An error in 'Updating_And_Testing_OneVecFromPopul' 1: nNumOfTrialFeasCurf - 1  = %d >= nNumOfTrialFeasTotf = %d",
					nNumOfTrialFeasCurf - 1, nNumOfTrialFeasTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] fTrialArrf;
				getchar();  exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nNumOfTrialFeasCurf - 1 < 0 || nNumOfTrialFeasCurf - 1 > 1)

			fTrialArrf[nNumOfTrialFeasCurf - 1] = fTrial_Curf;

			fprintf(fout, "\n fTrialArrf[%d] = %E", nNumOfTrialFeasCurf - 1, fTrialArrf[nNumOfTrialFeasCurf - 1]);
			//nPositsOfFeaPrevf = nPositsOf_Popul_Feas_TrialArrf[nIndexOfFea_2Dimf];

			nPositsOf_Popul_Feas_TrialArrf[nIndexOfFea_2Dimf] = nPositOfOneFeaTrialf;
			
			//fprintf(fout, "\n\n The trial fea: nPositsOfFeaPrevf = %d, nPositOfOneFeaTrialf = %d, nPositsOf_Popul_Feas_TrialArrf[%d] = %d, iFeaf = %d", 
				//nPositsOfFeaPrevf, nPositOfOneFeaTrialf, nIndexOfFea_2Dimf,nPositsOf_Popul_Feas_TrialArrf[nIndexOfFea_2Dimf], iFeaf);

		}//if (nFeaNumsForTrialOrNotOneDimArrf[iFeaf] == FEA_FOR_TRIAL || iFeaf == nRandFeaf)

	} //for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)

	if (nNumOfTrialFeasCurf != nNumOfTrialFeasTotf)
	{
		printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul' 1: nNumOfTrialFeasCurf  = %d != nNumOfTrialFeasTotf = %d",
			nNumOfTrialFeasCurf, nNumOfTrialFeasTotf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'Updating_And_Testing_OneVecFromPopul' 1: nNumOfTrialFeasCurf  = %d != nNumOfTrialFeasTotf = %d",
			nNumOfTrialFeasCurf, nNumOfTrialFeasTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fTrialArrf;
		getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfTrialFeasCurf != nNumOfTrialFeasTotf)

	nResf = FitnessOfOneFeaVec(
		nDim_DifEvof, //const int nDim_DifEvof,
		nDim_FeasInitf, //const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

		nNumOfOneVecFromPopulf, //const int nNumOfOneVecFromPopulf,
		nNumOfVecsInOnePopulTotf, //const int nNumOfVecsInOnePopulTotf,

		//const float fOneFeaVec_DifEvoArrf[], //[nDim_DifEvo]
		nPositsOf_Popul_Feas_TrialArrf, //const int nPositsOf_Popul_FeasArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

		fFeaConst_UnRefined_Initf, //const float fFeaConst_UnRefined_Initf,

		/////////////////////////////////////
		//train
		nNumOfItersOfTrainingTotf, //const int nNumOfItersOfTrainingTotf,
		nNumOfItersOfTraining_RefinedTotf, //const int nNumOfItersOfTraining_RefinedTotf,

		//after shuffling
		fFea_Train_DifEvo_Arrf, //const float fFea_Train_DifEvo_Arrf[], //[nProd_WithConstTrainTot], already normalized
		nY_Train_Actual_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		nNumVecTrainTotf, //const int nNumVecTrainTotf, //== nVecTrainf,
	///////////////////////////////////////
	//test
		fFea_Test_DifEvo_Arrf, //const float fFea_Test_DifEvo_Arrf[], //[nProd_WithConstTestTot],
		nY_Test_Actual_Arrf, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		nNumVecTestTotf, //const int nNumVecTestTotf, // == nVecTestf,

		/////////////////////////////////////

		nDim_D_SelecFeasf, //const int nDim_D_SelecFeasf, //for reading

		nDim_D_SelecFeas_WithConstf, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		//nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		fAlphaf, //const float fAlphaf, // < 1.0
		fEpsilonf, //const float fEpsilonf,
		fCrf, //const float fCrf,
		fCf, //const float fCf,
		////////////////////////////

		nNumOfActiveFeasForOneFeaVecTot_Trialf, //int &nNumOfActiveFeasForOneFeaVecTotf,
		fFitnOfOneFeaVec_Trialf); // float &fFitnOfOneFeaVecf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul' by 'FitnessOfOneFeaVec', nNumOfOneVecFromPopulf = %d", nNumOfOneVecFromPopulf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in 'Updating_And_Testing_OneVecFromPopul' by 'FitnessOfOneFeaVec', nNumOfOneVecFromPopulf = %d", nNumOfOneVecFromPopulf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fTrialArrf;
		getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)
/////////////////
	if (nResf == NUMBER_OF_ACTIVE_FEAS_IS_INSUFFICIENT)
	{
//no changes
		delete[] fTrialArrf;
		return SUCCESSFUL_RETURN;
	} //if (nResf == NUMBER_OF_ACTIVE_FEAS_IS_INSUFFICIENT)

	/////////////////////////////////////////
	fFitnOfOneFeaVec_Curf = fFitnOf_AllVecsFromPopulArrf[nNumOfOneVecFromPopulf];

	if (fFitnOfOneFeaVec_Trialf > fFitnOfOneFeaVec_Curf)
	{
		fprintf(fout, "\n\n Updating: fFitnOfOneFeaVec_Trialf = %E > fFitnOfOneFeaVec_Curf = %E, iVec_Updating_Glob = %d, nNumOfGener_Glob = %d", 
			fFitnOfOneFeaVec_Trialf, fFitnOfOneFeaVec_Curf,iVec_Updating_Glob, nNumOfGener_Glob);

		fFitnOf_AllVecsFromPopulArrf[nNumOfOneVecFromPopulf] = fFitnOfOneFeaVec_Trialf;

		nNumOfActiveFeasFor_AllVecFromPopulArrf[nNumOfOneVecFromPopulf] = nNumOfActiveFeasForOneFeaVecTot_Trialf;

		fprintf(fout, "\n fFitnOf_AllVecsFromPopulArrf[%d] = %E, nNumOfActiveFeasFor_AllVecFromPopulArrf[%d] = %d",
			nNumOfOneVecFromPopulf,fFitnOf_AllVecsFromPopulArrf[nNumOfOneVecFromPopulf], 
			nNumOfOneVecFromPopulf,nNumOfActiveFeasFor_AllVecFromPopulArrf[nNumOfOneVecFromPopulf]);
		
		//nProdTempf = nNumOfOneVecFromPopulf * nDim_FeasInitf;

		nNumOfTrialFeasCurf = 0;
		for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)
		{
			nIndexOfFea_2Dimf = iFeaf + nProdTempf;

		//	nPositsOfFeaPrevf = nPositsOf_Popul_FeasArrf[nIndexOfFea_2Dimf];

			//nPositOfOneFeaTrialf = nPositsOf_Popul_Feas_TrialArrf[nIndexOfFea_2Dimf];

			//fprintf(fout, "\n\n Changing the positions from nPositsOfFeaPrevf = %d to nPositOfOneFeaTrialf = %d, nPositsOf_Popul_Feas_TrialArrf[%d] = %d, iFeaf = %d",
				//nPositsOfFeaPrevf, nPositOfOneFeaTrialf, nIndexOfFea_2Dimf,nPositsOf_Popul_Feas_TrialArrf[nIndexOfFea_2Dimf], iFeaf);
			if (nFeaNumsForTrialOrNotOneDimArrf[iFeaf] == FEA_FOR_TRIAL || iFeaf == nRandFeaf)
			{
				nNumOfTrialFeasCurf += 1;
				
				if (nNumOfTrialFeasCurf - 1 < 0 || nNumOfTrialFeasCurf - 1 >= nNumOfTrialFeasTotf)
				{
					printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul' 2: nNumOfTrialFeasCurf - 1  = %d >= nNumOfTrialFeasTotf = %d",
						nNumOfTrialFeasCurf - 1, nNumOfTrialFeasTotf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n  An error in 'Updating_And_Testing_OneVecFromPopul' 2: nNumOfTrialFeasCurf - 1  = %d >= nNumOfTrialFeasTotf = %d",
						nNumOfTrialFeasCurf - 1, nNumOfTrialFeasTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					delete[] fTrialArrf;
					getchar();  exit(1);
					return UNSUCCESSFUL_RETURN;
				} //if (nNumOfTrialFeasCurf - 1 < 0 || nNumOfTrialFeasCurf - 1 > 1)

				//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n Before using 'fTrialArrf[]', fTrialArrf[%d] = %E, nNumOfTrialFeasCurf = %d, nIndexOfFea_2Dimf = %d,  iFeaf = %d, nNumOfOneVecFromPopulf = %d",
					nNumOfTrialFeasCurf - 1,fTrialArrf[nNumOfTrialFeasCurf - 1], nIndexOfFea_2Dimf, nNumOfTrialFeasCurf, iFeaf, nNumOfOneVecFromPopulf);

				fprintf(fout, "\n\n Before using 'fTrialArrf[]', fTrialArrf[%d] = %E, nNumOfTrialFeasCurf = %d, nIndexOfFea_2Dimf = %d,  iFeaf = %d, nNumOfOneVecFromPopulf = %d",
					nNumOfTrialFeasCurf - 1, fTrialArrf[nNumOfTrialFeasCurf - 1], nIndexOfFea_2Dimf, nNumOfTrialFeasCurf, iFeaf, nNumOfOneVecFromPopulf);

				fflush(fout);
				//printf("\n\nPlease press any key to continue"); fflush(fout);  getchar();
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				fTrial_Curf = fTrialArrf[nNumOfTrialFeasCurf - 1];

				fPopul_Of_FeaVecsArrf[nIndexOfFea_2Dimf] = fTrial_Curf; //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)
			} //if (nFeaNumsForTrialOrNotOneDimArrf[iFeaf] == FEA_FOR_TRIAL || iFeaf == nRandFeaf)

		} //for (iFeaf = 0; iFeaf < nDim_FeasInitf; iFeaf++)

		if (nNumOfTrialFeasCurf != nNumOfTrialFeasTotf)
		{
			printf("\n\n An error in 'Updating_And_Testing_OneVecFromPopul' 2: nNumOfTrialFeasCurf  = %d != nNumOfTrialFeasTotf = %d",
				nNumOfTrialFeasCurf, nNumOfTrialFeasTotf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Updating_And_Testing_OneVecFromPopul' 2: nNumOfTrialFeasCurf  = %d != nNumOfTrialFeasTotf = %d",
				nNumOfTrialFeasCurf, nNumOfTrialFeasTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			delete[] fTrialArrf;
			getchar();  exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (nNumOfTrialFeasCurf != nNumOfTrialFeasTotf)

		Copying_Int_Arr1_To_Arr2(
			nDimOfAllFeas_inOnePopul, //const int nDimf,
			nPositsOf_Popul_Feas_TrialArrf, //const int nArr1f[], // [nDimf]

			nPositsOf_Popul_FeasArrf); // int nArr2f[]); // [nDimf]

	} //if (fFitnOfOneFeaVec_Trialf > fFitnOfOneFeaVec_Curf)

	delete[] fTrialArrf;
	return SUCCESSFUL_RETURN;
} //int Updating_And_Testing_OneVecFromPopul(...
//////////////////////////////////////////////////////////////////////////

int doFeatureSelection_2p0(

	const float fProbOfFeaForTrialMaxf,
	const float fCoefForTrialf,

	const int nDim_DifEvof,
	const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

	const int nNumOfGenerTotf,
	//const int nNumOfGenerCurf,

	//const int nNumOfOneVecFromPopulf,
	const int nNumOfVecsInOnePopulTotf,
	const int nDimOfAllFeas_inOnePopulf,

	const int nDimOfAllVecs_inAllGenersf,
	const int nDimOfAllFeas_inAllGenersf, // == (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)
	
	//////////////////////////////////
	const float fFeaRangeMinf,
	const float fFeaRangeMaxf,

	const float fWidthOfIntervalForOneFeaf,
	//////////////

	const float fFeaConst_UnRefined_Initf,
	/////////////////////////////////////
	//train
	const int nNumOfItersOfTrainingTotf,
	const int nNumOfItersOfTraining_RefinedTotf,

	//after shuffling
	const float fFea_Train_DifEvo_Arrf[], //[nProdTrain_DifEvoTot], already normalized
	const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	const int nNumVecTrainTotf, //== nVecTrainf,
	///////////////////////////////////////
	//test
	const float fFea_Test_DifEvo_Arrf[], //[nProdTest_DifEvoTot],
	const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

	const int nNumVecTestTotf, // == nVecTestf,
	/////////////////////////////////////

	const int nDim_D_SelecFeasf, //== nDim_FeasInit = 15

	const int nDim_D_SelecFeas_WithConstf, // = 16
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	//const int nDim_U_Initf, //(nDim_D_WithConst*nDim_H*nK)

	///////////////////////
	const float fAlphaf, // < 1.0
	const float fEpsilonf,
	const float fCrf,
	const float fCf,
	////////////////////////////
	float fPopul_Of_FeaVecsArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

	float &fFitnessAver_OfLastGenerf,
	float &fBestFitnessf)
{
	void Generating_nFeaNumsForTrialOrNot_AllGenersArrf(

		const float fProbOfFeaForTrialMaxf,

		const int nDimOfAllFeas_inAllGenersf,

		const float fFeaRangeMinf,
		const float fFeaRangeMaxf,

		const float fWidthOfIntervalForOneFeaf,

		int nFeaNumsForTrialOrNot_AllGenersArrf[]); //[nDimOfAllFeas_inAllGeners] = (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)
/////////////////////

	void Generating_3_Arrs_OfRandSelecOfVecsForAllGeners(
		const int nNumOfVecsInOnePopulTotf,

		const int nNumOfGenerTotf,
		const int nDimOfAllVecs_inAllGenersf, // == nNumOfVecsInOnePopulTot*nNumOfGenerTot

		int nNumRandSelecOfVecsForAllGeners_1Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
		int nNumRandSelecOfVecsForAllGeners_2Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
		int nNumRandSelecOfVecsForAllGeners_3Arrf[]); //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
///////////////

	void Generating_nNumRandFeas_ForAllGenersArr(
		const int nDim_FeasInitf,
		const int nDimOfAllVecs_inAllGenersf, // == nNumOfVecsInOnePopulTot*nNumOfGenerTot

		int nNumRandFeas_ForAllGenersArrf[]); // [nDimOfAllVecs_inAllGenersf] // < nDim_FeasInitf
/////////////////

	void Initializing_All_Feas_Of_Popul(
		const int nDimOfAllFeas_inOnePopulf,

		const float fFeaRangeMinf,
		const float fFeaRangeMaxf,

		float fPopul_Of_FeaVecsArrf[]); //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

/*
	void Initialization_Of_OneRand_IntVec_WithinARange(
		const int nDimForRandf,

		const int nMinf,
		const int nMaxf,

		int nRandArr[]); //[nDimForRandf]
*/

	void Converting_Popul_Of_FeaVecsArrf_To_PositsOfAllFeas(
		const int nDimOfAllFeas_inOnePopulf, //(nDim_FeasInit*nNumOfVecsInOnePopul)

	//nDim_FeasInit <= nDim_DifEvo
		const float fPopul_Of_FeaVecsArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

		const float fFeaRangeMinf,
		const float fFeaRangeMaxf,

		const float fWidthOfIntervalForOneFeaf,

		int nPositsOf_Popul_FeasArrf[]); //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

//////////////////////
	int Fitness_Of_InitPopul(

		const int nDim_DifEvof,
		const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

		//const int nNumOfOneVecFromPopulf,
		const int nNumOfVecsInOnePopulTotf,

		//const float fOneFeaVec_DifEvoArrf[], //[nDim_DifEvo]
		const int nPositsOf_Popul_FeasArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

		const float fFeaConst_UnRefined_Initf,

		/////////////////////////////////////
		//train
		const int nNumOfItersOfTrainingTotf,
		const int nNumOfItersOfTraining_RefinedTotf,

		//after shuffling
		const float fFea_Train_DifEvo_Arrf[], //[nProdTrain_DifEvoTot], already normalized
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		const int nNumVecTrainTotf, //== nVecTrainf,
		///////////////////////////////////////
		//test
		const float fFea_Test_DifEvo_Arrf[], //[nProdTest_DifEvoTot],
		const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		const int nNumVecTestTotf, // == nVecTestf,

		/////////////////////////////////////

		const int nDim_D_SelecFeasf, //for reading

		const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		//const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
		////////////////////////////
		int &nNumOfBestVecFromPopulf,
		float &fFitnessOfBestVecFromPopulf,

		int nNumOfActiveFeasFor_AllVecFromPopulArrf[], //[nNumOfVecsInOnePopulTotf]
		float fFitnOf_AllVecsFromPopulArrf[]); //[nNumOfVecsInOnePopulTotf]

////////////
int Updating_And_Testing_OneVecFromPopul(

			const float fCoefForTrialf,

			const int nDim_DifEvof,
			const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

			const int nNumOfGenerTotf,
			const int nNumOfGenerCurf,

			const int nNumOfOneVecFromPopulf,
			const int nNumOfVecsInOnePopulTotf,
			//////////////////////////////////

			const int nNumRandFeas_ForAllGenersArrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot); // < nDim_FeasInitf

//////////////////////////////
			const int nNumRandSelecOfVecsForAllGeners_1Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
			const int nNumRandSelecOfVecsForAllGeners_2Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
			const int nNumRandSelecOfVecsForAllGeners_3Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot

			///////////////////////////////////
			const int nFeaNumsForTrialOrNot_AllGenersArrf[], //[nDimOfAllFeas_inAllGeners] = (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)

			///////////////////////////

			const float fFeaRangeMinf,
			const float fFeaRangeMaxf,
			const float fWidthOfIntervalForOneFeaf,
			//////////////

			const float fFeaConst_UnRefined_Initf,
			/////////////////////////////////////
			//train
			const int nNumOfItersOfTrainingTotf,
			const int nNumOfItersOfTraining_RefinedTotf,

			//after shuffling
			const float fFea_Train_DifEvo_Arrf[], //[nProdTrain_DifEvoTot], already normalized
			const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

			const int nNumVecTrainTotf, //== nVecTrainf,
			///////////////////////////////////////
			//test
			const float fFea_Test_DifEvo_Arrf[], //[nProdTest_DifEvoTot],
			const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

			const int nNumVecTestTotf, // == nVecTestf,
			/////////////////////////////////////

			const int nDim_D_SelecFeasf, //for reading

			const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
			const int nDim_Hf, //dimension of the nonlinear/transformed space

			const int nKf, //nNumOfHyperplanes
			//const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

			///////////////////////
			const float fAlphaf, // < 1.0
			const float fEpsilonf,
			const float fCrf,
			const float fCf,
			////////////////////////////

			//could be updated			
			int nNumOfActiveFeasFor_AllVecFromPopulArrf[], //[nNumOfVecsInOnePopulTotf] // == 100
			float fFitnOf_AllVecsFromPopulArrf[], //[nNumOfVecsInOnePopulTotf] //100

			float fPopul_Of_FeaVecsArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

			int nPositsOf_Popul_FeasArrf[]); //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)
////////////////////

int fMeanOfFloatArr(const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,
	const float fArrf[], //	ObjDataSymbArrAllf.nNumLongFailureAskIntervArrf, //nNumLongEntriesCurf, //const int nCurf,

	float &fMeanf);
///////////////////////////////////

int
	nFeaNumsForTrialOrNot_AllGenersArrf[nDimOfAllFeas_inAllGeners], // == (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)

	nNumRandSelecOfVecsForAllGeners_1Arrf[nDimOfAllVecs_inAllGeners], //  []
	nNumRandSelecOfVecsForAllGeners_2Arrf[nDimOfAllVecs_inAllGeners], //  []
	nNumRandSelecOfVecsForAllGeners_3Arrf[nDimOfAllVecs_inAllGeners], //  []

	nNumRandFeas_ForAllGenersArrf[nDimOfAllVecs_inAllGeners], // [(nNumOfVecsInOnePopulTot*nNumOfGenerTot)]

	nNumOfActiveFeasFor_AllVecFromPopulArrf[nNumOfVecsInOnePopulTot], //[nNumOfVecsInOnePopulTotf]

	nPositsOf_Popul_FeasArrf[nDimOfAllFeas_inOnePopul],

	nNumOfBestVecFromPopulf,

	nProdTempf,
	nIndex_2Dimf,
	nResf,
	iGenerf,
	iFeaf,
	iVecf;

float
	fMeanOfPopulf,
	fFitnessOfBestVecFrom_InitPopulf,

	fFitnessOfBestVecFromPopulForGenersArrf[nNumOfGenerTot],

	fFitnOf_AllVecsFromPopulArrf[nNumOfVecsInOnePopulTot];
	//fPopul_Of_FeaVecsArrf[nDimOfAllFeas_inOnePopul]; //==  (nDim_FeasInit*nNumOfVecsInOnePopulTot)

/////////////////////////////

fBestFitnessf = -fLarge;
///////////////////////////////
	Generating_nFeaNumsForTrialOrNot_AllGenersArrf(

		fProbOfFeaForTrialMaxf, //const float fProbOfFeaForTrialMaxf,

		nDimOfAllFeas_inAllGenersf, //const int nDimOfAllFeas_inAllGenersf, // == (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)

		fFeaRangeMinf, //const float fFeaRangeMinf,
		fFeaRangeMaxf, //const float fFeaRangeMaxf,

		fWidthOfIntervalForOneFeaf, //const float fWidthOfIntervalForOneFeaf,

		nFeaNumsForTrialOrNot_AllGenersArrf); // int nFeaNumsForTrialOrNot_AllGenersArrf[]); //[nDimOfAllFeas_inAllGeners] = (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)
//////////////

	 Generating_3_Arrs_OfRandSelecOfVecsForAllGeners(
		 nNumOfVecsInOnePopulTotf, //const int nNumOfVecsInOnePopulTotf,

		 nNumOfGenerTotf, //const int nNumOfGenerTotf,
		 nDimOfAllVecs_inAllGenersf, //const int nDimOfAllVecs_inAllGenersf, // == nNumOfVecsInOnePopulTot*nNumOfGenerTot

		 nNumRandSelecOfVecsForAllGeners_1Arrf, //int nNumRandSelecOfVecsForAllGeners_1Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
		 nNumRandSelecOfVecsForAllGeners_2Arrf, //int nNumRandSelecOfVecsForAllGeners_2Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
		 nNumRandSelecOfVecsForAllGeners_3Arrf); // int nNumRandSelecOfVecsForAllGeners_3Arrf[]); //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
/////////////////////

	 Generating_nNumRandFeas_ForAllGenersArr(
		 nDim_FeasInitf, //const int nDim_FeasInitf,
		 nDimOfAllVecs_inAllGenersf, // const int nDimOfAllVecs_inAllGenersf, // == nNumOfVecsInOnePopulTot*nNumOfGenerTot

		nNumRandFeas_ForAllGenersArrf); // int nNumRandFeas_ForAllGenersArrf[]); // [(nNumOfVecsInOnePopulTot*nNumOfGenerTot)] // < nDim_FeasInitf
////////////////////
	 /*
	 Initialization_Of_OneRand_IntVec_WithinARange(
		 nDimOfAllFeas_inOnePopul, //const int nDimForRandf,

		 0, //const int nMinf,
		 nDim_DifEvo, //const int nMaxf,

		 nPositsOf_Popul_FeasArrf); // int nRandArr[]); //[nDimForRandf]
		 */

	 Initializing_All_Feas_Of_Popul(
		 nDimOfAllFeas_inOnePopulf, //const int nDimOfAllFeas_inOnePopulf,

		 fFeaRangeMinf, //const float fFeaRangeMinf,
		 fFeaRangeMaxf, //const float fFeaRangeMaxf,

		 fPopul_Of_FeaVecsArrf); // float fPopul_Of_FeaVecsArrf[]); //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)
///////////////////////////////

	 Converting_Popul_Of_FeaVecsArrf_To_PositsOfAllFeas(
		 nDimOfAllFeas_inOnePopulf, //const int nDimOfAllFeas_inOnePopulf, //(nDim_FeasInit*nNumOfVecsInOnePopul)

	 //nDim_FeasInit <= nDim_DifEvo
		 fPopul_Of_FeaVecsArrf, //const float fPopul_Of_FeaVecsArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

		 fFeaRangeMinf, //const float fFeaRangeMinf,
		 fFeaRangeMaxf, //const float fFeaRangeMaxf,

		 fWidthOfIntervalForOneFeaf, //const float fWidthOfIntervalForOneFeaf,

		 nPositsOf_Popul_FeasArrf); // int nPositsOf_Popul_FeasArrf[]); //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

/////////////
	 for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTot; iVecf++)
	 {
		 printf( "\n\n Before 'Fitness_Of_InitPopul': iVecf = %d", iVecf);
		 fprintf(fout, "\n\n Before 'Fitness_Of_InitPopul': iVecf = %d", iVecf);

		 nProdTempf = iVecf * nDim_FeasInit;
		 for (iFeaf = 0; iFeaf < nDim_FeasInit; iFeaf++)
		 {
			 nIndex_2Dimf = iFeaf + nProdTempf;

			 printf("\n iFeaf = %d, iVecf = %d, nPositsOf_Popul_FeasArrf[%d] = %d, fPopul_Of_FeaVecsArrf[%d] = %E",
				 iFeaf, iVecf,nIndex_2Dimf, nPositsOf_Popul_FeasArrf[nIndex_2Dimf], nIndex_2Dimf, fPopul_Of_FeaVecsArrf[nIndex_2Dimf]);


			 fprintf(fout, "\n iFeaf = %d, iVecf = %d, nPositsOf_Popul_FeasArrf[%d] = %d, fPopul_Of_FeaVecsArrf[%d] = %E",
				 iFeaf, iVecf, nIndex_2Dimf, nPositsOf_Popul_FeasArrf[nIndex_2Dimf], nIndex_2Dimf, fPopul_Of_FeaVecsArrf[nIndex_2Dimf]);

		 } //for (iFeaf = 0; iFeaf < nDim_FeasInit; iFeaf++)

	 } // for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTot; iVecf++)

	 //getchar();  //exit(1);

	 /*
	 fprintf(fout, "\n\n Input training vecs in 'doFeatureSelection_2p0': fFeaConst_UnRefined_Initf = %E, nDim_DifEvof = %d", fFeaConst_UnRefined_Initf, nDim_DifEvof);
	 for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	 {
		 nProdTempf = iVecf * nDim_DifEvof;

		 fprintf(fout, "\n\n Input train: iVecf = %d, ", iVecf);
		 for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
		 {
			 nIndex_2Dimf = iFeaf + nProdTempf;
			 fprintf(fout, "%d:%E, ", iFeaf, fFea_Train_DifEvo_Arrf[nIndex_2Dimf]);

		 } //for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
	 } //for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	 */

	 nResf = Fitness_Of_InitPopul(
		 nDim_DifEvof, //const int nDim_DifEvof,
		 nDim_FeasInitf, //const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

		 nNumOfVecsInOnePopulTotf, //const int nNumOfVecsInOnePopulTotf,

		 nPositsOf_Popul_FeasArrf, //const int nPositsOf_Popul_FeasArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

		 fFeaConst_UnRefined_Initf, //const float fFeaConst_UnRefined_Initf,

 //////////////////////

		 nNumOfItersOfTrainingTotf, //const int nNumOfItersOfTrainingTotf,
		 nNumOfItersOfTraining_RefinedTotf, //const int nNumOfItersOfTraining_RefinedTotf,

 //after shuffling
		 fFea_Train_DifEvo_Arrf, //const float fFea_Train_DifEvo_Arrf[], //[nProd_WithConstTrainTot], already normalized
		 nY_Train_Actual_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

		 nNumVecTrainTotf, //const int nNumVecTrainTotf, //== nVecTrainf,
	 ///////////////////////////////////////
	 //test
		 fFea_Test_DifEvo_Arrf, //const float fFea_Test_DifEvo_Arrf[], //[nProd_WithConstTestTot],
		 nY_Test_Actual_Arrf, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		 nNumVecTestTotf, //const int nNumVecTestTotf, // == nVecTestf,

		 /////////////////////////////////////

		 nDim_D_SelecFeasf, //const int nDim_D_SelecFeasf, //for reading

		 nDim_D_SelecFeas_WithConstf, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
		 nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		 nKf, //const int nKf, //nNumOfHyperplanes
		 //nDim_U_Initf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		 ///////////////////////
		 fAlphaf, //const float fAlphaf, // < 1.0
		 fEpsilonf, //const float fEpsilonf,
		 fCrf, //const float fCrf,
		 fCf, //const float fCf,

		 ////////////////////////////
		 nNumOfBestVecFromPopulf, //int &nNumOfBestVecFromPopulf,
		 fFitnessOfBestVecFrom_InitPopulf, // float &fFitnessOfBestVecFrom_InitPopulf,

		 nNumOfActiveFeasFor_AllVecFromPopulArrf, //int nNumOfActiveFeasFor_AllVecFromPopulArrf[], //[nNumOfVecsInOnePopulTotf]
		 fFitnOf_AllVecsFromPopulArrf); //float fFitnOf_AllVecsFromPopulArrf[]); //[nNumOfVecsInOnePopulTotf]

	// printf("\n\n After 'Fitness_Of_InitPopul'");
	// printf("\n\n Please press any key:"); getchar();

	 if (nResf == UNSUCCESSFUL_RETURN)
	 {
		 printf("\n\n An error in 'doFeatureSelection_2p0' by 'Fitness_Of_InitPopul");

#ifndef COMMENT_OUT_ALL_PRINTS
		 fprintf(fout, "\n\n  An error in 'doFeatureSelection_2p0' by 'Fitness_Of_InitPopul");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		 getchar();  exit(1);
		 return UNSUCCESSFUL_RETURN;
	 } //if (nResf == UNSUCCESSFUL_RETURN)
///////////////////////////////////////////////

	 nResf = fMeanOfFloatArr(
		 nNumOfVecsInOnePopulTotf, //const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,
		 fFitnOf_AllVecsFromPopulArrf, //const float fArrf[], //	ObjDataSymbArrAllf.nNumLongFailureAskIntervArrf, //nNumLongEntriesCurf, //const int nCurf,

		 fMeanOfPopulf); // float &fMeanf)

	 printf("\n\n The init popul: fFitnessOfBestVecFrom_InitPopulf = %E, fMeanOfPopulf = %E", fFitnessOfBestVecFrom_InitPopulf, fMeanOfPopulf);
	 printf("\n\n So far: fPercentageOfCorrect_NoRefin_Glob = %E, fPercentageOfCorrect_Refined_Max_Glob = %E", fPercentageOfCorrect_NoRefin_Glob, fPercentageOfCorrect_Refined_Max_Glob);

	 fprintf(fout,"\n\n The init popul: fFitnessOfBestVecFrom_InitPopulf = %E, fMeanOfPopulf = %E", fFitnessOfBestVecFrom_InitPopulf, fMeanOfPopulf);
	 fprintf(fout, "\n\n So far : fPercentageOfCorrect_NoRefin_Glob = %E, fPercentageOfCorrect_Refined_Max_Glob = %E", fPercentageOfCorrect_NoRefin_Glob,fPercentageOfCorrect_Refined_Max_Glob);


	 fprintf(fout, "\n");
	 for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)
	 {
		 printf( "\n The init popul: fFitnOf_AllVecsFromPopulArrf[%d] = %E", iVecf, fFitnOf_AllVecsFromPopulArrf[iVecf]);
		 fprintf(fout, "\n The init popul: fFitnOf_AllVecsFromPopulArrf[%d] = %E", iVecf, fFitnOf_AllVecsFromPopulArrf[iVecf]);
	 } //for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)

	 //printf("\n\nPlease press any key:"); getchar();
///////////////////////////////
	 for (iGenerf = 0; iGenerf < nNumOfGenerTotf; iGenerf++)
	 {
		 nNumOfGener_Glob = iGenerf + 1;

		 for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)
		 {
			 iVec_Updating_Glob = iVecf;
			 nResf = Updating_And_Testing_OneVecFromPopul(

				 fCoefForTrialf, //const float fCoefForTrialf,

				 nDim_DifEvof, //const int nDim_DifEvof,
				 nDim_FeasInitf, //const int nDim_FeasInitf, //the number of selected feas initially <= nDim_DifEvof

				 nNumOfGenerTotf, // const int nNumOfGenerTotf,
				 iGenerf, //const int nNumOfGenerCurf,

				 iVecf, //const int nNumOfOneVecFromPopulf,
				 nNumOfVecsInOnePopulTotf, // const int nNumOfVecsInOnePopulTotf,
				 //////////////////////////////////

				 nNumRandFeas_ForAllGenersArrf, //const int nNumRandFeas_ForAllGenersArrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot); // < nDim_FeasInitf

	 //////////////////////////////
				 nNumRandSelecOfVecsForAllGeners_1Arrf, //const int nNumRandSelecOfVecsForAllGeners_1Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
				 nNumRandSelecOfVecsForAllGeners_2Arrf, //const int nNumRandSelecOfVecsForAllGeners_2Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot
				 nNumRandSelecOfVecsForAllGeners_3Arrf, //const int nNumRandSelecOfVecsForAllGeners_3Arrf[], //  [nDimOfAllVecs_inAllGeners] = (nNumOfVecsInOnePopulTot*nNumOfGenerTot)  // < nNumOfVecsInOnePopulTot

				 ///////////////////////////////////
				 nFeaNumsForTrialOrNot_AllGenersArrf, // const int nFeaNumsForTrialOrNot_AllGenersArrf[], //[nDimOfAllFeas_inAllGeners] = (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)
				 ///////////////////////////

				 fFeaRangeMinf, //const float fFeaRangeMinf,
				 fFeaRangeMaxf, // const float fFeaRangeMaxf,
				 fWidthOfIntervalForOneFeaf, //const float fWidthOfIntervalForOneFeaf,
				 //////////////

				 fFeaConst_UnRefined_Initf, //onst float fFeaConst_UnRefined_Initf,
				 /////////////////////////////////////
				 //train
				 nNumOfItersOfTrainingTotf, //const int nNumOfItersOfTrainingTotf,
				 nNumOfItersOfTraining_RefinedTotf, //const int nNumOfItersOfTraining_RefinedTotf,

	 //after shuffling
				 fFea_Train_DifEvo_Arrf, //const float fFea_Train_DifEvo_Arrf[], //[nProd_WithConstTrainTot], already normalized
				 nY_Train_Actual_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

				 nNumVecTrainTotf, //const int nNumVecTrainTotf, //== nVecTrainf,
			 ///////////////////////////////////////
			 //test
				 fFea_Test_DifEvo_Arrf, //const float fFea_Test_DifEvo_Arrf[], //[nProd_WithConstTestTot],
				 nY_Test_Actual_Arrf, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

				 nNumVecTestTotf, //const int nNumVecTestTotf, // == nVecTestf,

				 /////////////////////////////////////
				 nDim_D_SelecFeasf, //const int nDim_D_SelecFeasf, //== nDim_FeasInit = 15

				 nDim_D_SelecFeas_WithConstf, //const int nDim_D_SelecFeas_WithConstf, // = dimension of the original space
				 nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

				 nKf, //const int nKf, //nNumOfHyperplanes
				// nDim_U_Initf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_Hf*nKf)

				 ///////////////////////
				 fAlphaf, //const float fAlphaf, // < 1.0
				 fEpsilonf, //const float fEpsilonf,
				 fCrf, //const float fCrf,
				 fCf, //const float fCf,
				 ////////////////////////////

				 //could be updated			
				 nNumOfActiveFeasFor_AllVecFromPopulArrf, //int nNumOfActiveFeasFor_AllVecFromPopulArrf[], //[nNumOfVecsInOnePopulTotf] // == 100
				 fFitnOf_AllVecsFromPopulArrf, //float fFitnOf_AllVecsFromPopulArrf[], //[nNumOfVecsInOnePopulTotf] //100

				 fPopul_Of_FeaVecsArrf, //float fPopul_Of_FeaVecsArrf[], //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

				 nPositsOf_Popul_FeasArrf); // int nPositsOf_Popul_FeasArrf[]); //[nDimOfAllFeas_inOnePopulf] = (nDim_FeasInit*nNumOfVecsInOnePopulTot)

			 if (nResf == UNSUCCESSFUL_RETURN)
			 {
				 printf("\n\n An error in 'doFeatureSelection_2p0' by 'Updating_And_Testing_OneVecFromPopul");

#ifndef COMMENT_OUT_ALL_PRINTS
				 fprintf(fout, "\n\n  An error in 'doFeatureSelection_2p0' by 'Updating_And_Testing_OneVecFromPopul");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				 getchar();  exit(1);
				 return UNSUCCESSFUL_RETURN;
			 } //if (nResf == UNSUCCESSFUL_RETURN)

		 } // for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)

//////////////////////////////////////
		 nResf = fMeanOfFloatArr(
				 nNumOfVecsInOnePopulTotf, //const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,
				 fFitnOf_AllVecsFromPopulArrf, //const float fArrf[], //	ObjDataSymbArrAllf.nNumLongFailureAskIntervArrf, //nNumLongEntriesCurf, //const int nCurf,

				 fMeanOfPopulf); // float &fMeanf)

		 if (nResf == UNSUCCESSFUL_RETURN)
		 {
			 printf("\n\n An error in 'doFeatureSelection_2p0' by 'fMeanOfFloatArr");

#ifndef COMMENT_OUT_ALL_PRINTS
			 fprintf(fout, "\n\n  An error in 'doFeatureSelection_2p0' by 'fMeanOfFloatArr");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			 getchar();  exit(1);
			 return UNSUCCESSFUL_RETURN;
		 } //if (nResf == UNSUCCESSFUL_RETURN)

		 fprintf(fout, "\n\n The end of iGenerf = %d, fMeanOfPopulf = %E", iGenerf, fMeanOfPopulf);
		 for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)
		 {
			 printf("\n iGenerf = %d, fFitnOf_AllVecsFromPopulArrf[%d] = %E", iGenerf, iVecf, fFitnOf_AllVecsFromPopulArrf[iVecf]);
			 fprintf(fout, "\n iGenerf = %d, fFitnOf_AllVecsFromPopulArrf[%d] = %E", iGenerf, iVecf, fFitnOf_AllVecsFromPopulArrf[iVecf]);
		 } //for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)

		 fFitnessOfBestVecFromPopulForGenersArrf[iGenerf] = -fLarge;
		 for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)
		 {
			 if (fFitnOf_AllVecsFromPopulArrf[iVecf] > fFitnessOfBestVecFromPopulForGenersArrf[iGenerf])
			 {
				 fFitnessOfBestVecFromPopulForGenersArrf[iGenerf] = fFitnOf_AllVecsFromPopulArrf[iVecf];
			 } // if (fFitnOf_AllVecsFromPopulArrf[iVecf] > fFitnessOfBestVecFromPopulForGenersArrf[iGenerf])

		 } //for (iVecf = 0; iVecf < nNumOfVecsInOnePopulTotf; iVecf++)
		 
		 printf("\n\n The gener popul: fFitnessOfBestVecFromPopulForGenersArrf[%d] = %E, fMeanOfPopulf = %E, fFitnessOfBestVecFrom_InitPopulf = %E", 
			 iGenerf,fFitnessOfBestVecFromPopulForGenersArrf[iGenerf], fMeanOfPopulf, fFitnessOfBestVecFrom_InitPopulf);
		
		 printf("\n\n So far: fPercentageOfCorrect_NoRefin_Glob = %E, fPercentageOfCorrect_Refined_Max_Glob = %E", fPercentageOfCorrect_NoRefin_Glob, fPercentageOfCorrect_Refined_Max_Glob);


		 fprintf(fout, "\n\n The gener popul: fFitnessOfBestVecFromPopulForGenersArrf[%d] = %E, fMeanOfPopulf = %E, fFitnessOfBestVecFrom_InitPopulf = %E",
			 iGenerf, fFitnessOfBestVecFromPopulForGenersArrf[iGenerf], fMeanOfPopulf, fFitnessOfBestVecFrom_InitPopulf);

		 fprintf(fout, "\n\n So far : fPercentageOfCorrect_NoRefin_Glob = %E, fPercentageOfCorrect_Refined_Max_Glob = %E", fPercentageOfCorrect_NoRefin_Glob,fPercentageOfCorrect_Refined_Max_Glob);

		 if (fFitnessOfBestVecFromPopulForGenersArrf[iGenerf] > fBestFitnessf)
		 {
			 fBestFitnessf = fFitnessOfBestVecFromPopulForGenersArrf[iGenerf];
			 
			 printf("\n\n 'doFeatureSelection_2p0': a new fBestFitnessf = %E, iGenerf = %d, fMeanOfPopulf = %E, fFitnessOfBestVecFrom_InitPopulf = %E",
				 fBestFitnessf, iGenerf, fMeanOfPopulf, fFitnessOfBestVecFrom_InitPopulf);

			 fprintf(fout,"\n\n 'doFeatureSelection_2p0': a new fBestFitnessf = %E, iGenerf = %d, fMeanOfPopulf = %E, fFitnessOfBestVecFrom_InitPopulf = %E",
				 fBestFitnessf, iGenerf, fMeanOfPopulf, fFitnessOfBestVecFrom_InitPopulf);


		 } //if (fFitnessOfBestVecFromPopulForGenersArrf[iGenerf] > fBestFitnessf)

	 }//for (iGenerf = 0; iGenerf < nNumOfGenerTotf; iGenerf++)
		 
	 fFitnessAver_OfLastGenerf = fMeanOfPopulf;
return SUCCESSFUL_RETURN;
}//int doFeatureSelection_2p0(...
////////////////////////////////////////////////

int nMa_Wh(
	const int nDim1f,
	const float fArray1f[],
	const int nDim2f,
	const float fArray2f[])
{
	int
		i1,
		i2;

	int
		nReturn = 0;

	for (i1 = 0; i1 < nDim1f; i1++)
	{
		for (i2 = 0; i2 < nDim2f; i2++)
		{
			if (fArray1f[i1] > fArray2f[i2])
				nReturn += 1;
		} // for (i2=0; i2 < nDim2f; i2++)

	} // for (i1=0; i1 < nDim1f; i1++)

	return nReturn;
} // int nMa_Wh(int nDim1f, float fArray1f[],int nDim2f, float fArray2f[])

void Extracting_Train_And_Test_OneDimArrs_For_OneFea_From_2DimArrs(

	const int nPositOfFeaf, // < nDimf
	const int nDimf,

	const int nNumVecTrainTotf,
	const int nNumVecTestTotf,

	///////////////////////////////////////////////////////
	const float fFeaTrain_Arrf[], //[nProdTrainTot]
	const float fFeaTest_Arrf[],

	float fOneDim_TrainArrf[],//[nNumVecTrainTotf]
	float fOneDim_TestArrf[])//[nNumVecTestTotf]
{
	int
		iVecf,
		nTempf,
		nIndexf;
	//iFeaf;

	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		nTempf = iVecf * nDimf;
		nIndexf = nTempf + nPositOfFeaf;

		fOneDim_TrainArrf[iVecf] = fFeaTrain_Arrf[nIndexf];

		if (fOneDim_TrainArrf[iVecf] < -fLarge_ForReading || fOneDim_TrainArrf[iVecf] > fLarge_ForReading)
		{
			printf("\n\n An error in 'Extracting_Train_And_Test_OneDimArrs_For_OneFea_From_2DimArrs', fOneDim_TrainArrf[%d] = %E",
				iVecf, fOneDim_TrainArrf[iVecf]);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Extracting_Train_And_Test_OneDimArrs_For_OneFea_From_2DimArrs', fOneDim_TrainArrf[%d] = %E",
				iVecf, fOneDim_TrainArrf[iVecf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar(); exit(1);
		} //if (fOneDim_TrainArrf[iVecf] < -fLarge_ForReading || fOneDim_TrainArrf[iVecf] > fLarge_ForReading)

	} //for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
////////////////////
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		nTempf = iVecf * nDimf;
		nIndexf = nTempf + nPositOfFeaf;

		fOneDim_TestArrf[iVecf] = fFeaTest_Arrf[nIndexf];

		if (fOneDim_TestArrf[iVecf] < -fLarge_ForReading || fOneDim_TestArrf[iVecf] > fLarge_ForReading)
		{
			printf("\n\n An error in 'Extracting_Train_And_Test_OneDimArrs_For_OneFea_From_2DimArrs', fOneDim_TestArrf[%d] = %E",
				iVecf, fOneDim_TestArrf[iVecf]);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Extracting_Train_And_Test_OneDimArrs_For_OneFea_From_2DimArrs', fOneDim_TestArrf[%d] = %E",
				iVecf, fOneDim_TestArrf[iVecf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar(); exit(1);
		} //if (fOneDim_TestArrf[iVecf] < -fLarge_ForReading || fOneDim_TestArrf[iVecf] > fLarge_ForReading)

	} //for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)

} //void Extracting_Train_And_Test_OneDimArrs_For_OneFea_From_2DimArrs(
///////////////////////////////////////

//based on https://www.sciencedirect.com/topics/biochemistry-genetics-and-molecular-biology/mann-whitney-u-test
void Z_Score_For_2_FloatArrs(
	const int nDim1f,
	const float fArray1f[],
	const int nDim2f,
	const float fArray2f[],

	float &fZ_Scoref)
{
	int nMa_Wh(
		const int nDim1f,
		const float fArray1f[],
		const int nDim2f,
		const float fArray2f[]);
	int
		nScoreMaxf = nDim1f * nDim1f,
		nMa_Wh_Scoref;

	float
		fStDevf = sqrtf((float)(nDim1f)*(float)(nDim2f) * (float)(nDim1f + nDim2f + 1) / 12.0),

		fMeanf = (float)(nDim1f)*(float)(nDim2f) / 2.0;

	if (nDim1f <= 0 || nDim2f <= 0)
	{
		printf("\n\n An error in 'Z_Score_For_2_FloatArrs', nDim1f = %d, nDim2f = %d", nDim1f, nDim2f);
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'Z_Score_For_2_FloatArrs', nDim1f = %d, nDim2f = %d", nDim1f, nDim2f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
	} //if (nDim1f == 0 || nDim2f == 0)

	nMa_Wh_Scoref = nMa_Wh(
		nDim1f, //const int nDim1f,
		fArray1f, //const float fArray1f[],
		nDim2f,//const int nDim2f,
		fArray2f); // const float fArray2f[]);

	fZ_Scoref = ((float)(nMa_Wh_Scoref)-fMeanf) / fStDevf;

	if (fZ_Scoref < 0.0)
		fZ_Scoref = -fZ_Scoref;

}// void Z_Score_For_2_FloatArrs(
/////////////////////////////////////////

void Z_Scores_ForAllFeas(
	const int nDimf,

	const int nNumVecTrainTotf,
	const int nNumVecTestTotf,

	///////////////////////////////////////////////////////
	const float fFeaTrain_Arrf[], //[nProdTrainTot]
	const float fFeaTest_Arrf[],

	const float fZ_ScoreLimitf,

	float &fZ_ScoreMaxf,

	int &nNumOfFeasWithZ_ScoreAboveLimitf,

	float fZ_ScoresArrf[])//[nDimf]
{
	void Extracting_Train_And_Test_OneDimArrs_For_OneFea_From_2DimArrs(

		const int nPositOfFeaf, // < nDimf
		const int nDimf,

		const int nNumVecTrainTotf,
		const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		const float fFeaTrain_Arrf[], //[nProdTrainTot]
		const float fFeaTest_Arrf[],

		float fOneDim_TrainArrf[],//[nNumVecTrainTotf]
		float fOneDim_TestArrf[]); //[nNumVecTestTotf]

	//based on https://www.sciencedirect.com/topics/biochemistry-genetics-and-molecular-biology/mann-whitney-u-test
	void Z_Score_For_2_FloatArrs(
		const int nDim1f,
		const float fArray1f[],
		const int nDim2f,
		const float fArray2f[],

		float &fZ_Scoref);
	////////////////////////
	int
		iVecf, 
		iFeaf;

	float
		fZ_Scoref,
		fOneDim_TrainArrf[nNumVecTrainTot],
		fOneDim_TestArrf[nNumVecTestTot];
	////////////////
	fZ_ScoreMaxf = -fLarge;

	nNumOfFeasWithZ_ScoreAboveLimitf = 0;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		Extracting_Train_And_Test_OneDimArrs_For_OneFea_From_2DimArrs(

			iFeaf, //const int nPositOfFeaf, // < nDimf
			nDimf, //const int nDimf,

			nNumVecTrainTotf, //const int nNumVecTrainTotf,
			nNumVecTestTotf, //const int nNumVecTestTotf,

			///////////////////////////////////////////////////////
			fFeaTrain_Arrf, //const float fFeaTrain_Arrf[], //[nProdTrainTot]
			fFeaTest_Arrf, //const float fFeaTest_Arrf[],

			fOneDim_TrainArrf, //float fOneDim_TrainArrf[],//[nNumVecTrainTotf]
			fOneDim_TestArrf); // float fOneDim_TestArrf[]); //[nNumVecTestTotf]

/*
		fprintf(fout, "\n\n 'Z_Scores_ForAllFeas': feas (train) for iFeaf = %d", iFeaf);
		for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
		{
			fprintf(fout, "\n 'Z_Scores_ForAllFeas': iVecf = %d, fOneDim_TrainArrf[%d] = %E, iFeaf = %d", iVecf, iVecf, fOneDim_TrainArrf[iVecf], iFeaf);
		}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

		fprintf(fout, "\n\n 'Z_Scores_ForAllFeas': feas (test) for iFeaf = %d", iFeaf);
		for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
		{
			fprintf(fout, "\n 'Z_Scores_ForAllFeas': iVecf = %d, fOneDim_TestArrf[%d] = %E, iFeaf = %d", iVecf, iVecf, fOneDim_TestArrf[iVecf], iFeaf);
		}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)

		fflush(fout);
*/
///////////////////
		Z_Score_For_2_FloatArrs(
			nNumVecTrainTotf, //const int nDim1f,
			fOneDim_TrainArrf, //const float fArray1f[],

			nNumVecTestTotf, //const int nDim2f,
			fOneDim_TestArrf, //const float fArray2f[],

			fZ_Scoref); // float &fZ_Scoref);

		if (fZ_Scoref > fZ_ScoreMaxf)
			fZ_ScoreMaxf = fZ_Scoref;

		if (fZ_Scoref > fZ_ScoreLimitf)
		{
			nNumOfFeasWithZ_ScoreAboveLimitf += 1;
		}//if (fZ_Scoref > fZ_ScoreLimitf)

		fZ_ScoresArrf[iFeaf] = fZ_Scoref;

		printf("\n iFeaf = %d, fZ_Scoref = %E, nNumOfFeasWithZ_ScoreAboveLimitf = %d", iFeaf, fZ_Scoref, nNumOfFeasWithZ_ScoreAboveLimitf);
		
		fprintf(fout,"\n\n iFeaf = %d, fZ_Scoref = %E, nNumOfFeasWithZ_ScoreAboveLimitf = %d", iFeaf, fZ_Scoref, nNumOfFeasWithZ_ScoreAboveLimitf);

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	fflush(fout);
	printf("\n\n 'Z_Scores_ForAllFeas': nNumOfFeasWithZ_ScoreAboveLimitf = %d, fZ_ScoreMaxf = %E", nNumOfFeasWithZ_ScoreAboveLimitf, fZ_ScoreMaxf);
	fprintf(fout,"\n\n 'Z_Scores_ForAllFeas': nNumOfFeasWithZ_ScoreAboveLimitf = %d, fZ_ScoreMaxf = %E", nNumOfFeasWithZ_ScoreAboveLimitf, fZ_ScoreMaxf);
}//void Z_Scores_ForAllFeas(
////////////////////////////////////////////

//fin_Min_Max_Mean_StDev
int Reading_Min_Max_Mean_StDev(
	
	int &nDim_AllFeas_Readf, //nNumOfHyperplanes

	float fFea_All_Min_TrainArrf[], //[nDim_AllFeas_Readf]
	float fFea_All_Max_TrainArrf[], //[nDim_AllFeas_Readf]

	float fMean_All_Feas_TrainArrf[], //[nDim_AllFeas_Readf]
	float fStDev_All_Feas_TrainArrf[]) //[nDim_AllFeas_Readf]
{
	int
		iFeaf;

	float
		fFeaMinf,
		fFeaMaxf, 
		fMeanf,
		fStDevf;

	fscanf(fin_Min_Max_Mean_StDev, "%d", &nDim_AllFeas_Readf);
	if (nDim_AllFeas_Readf != nDim_DifEvo)
	{
		printf("\n\nAn error in 'Reading_Min_Max_Mean_StDev': nDim_AllFeas_Readf = %d != nDim_DifEvo = %d", nDim_AllFeas_Readf, nDim_DifEvo);
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nDim_AllFeas_Readf != nDim_DifEvo)

	//printf("\n nDim_D_Readf = %d ", nDim_D_Readf); getchar();
///////////////////////////////////////////////////

	for (iFeaf = 0; iFeaf < nDim_AllFeas_Readf; iFeaf++)
	{
		fscanf(fin_Min_Max_Mean_StDev, "%E %E %E %E", &fFeaMinf, &fFeaMaxf, &fMeanf, &fStDevf);

		fFea_All_Min_TrainArrf[iFeaf] = fFeaMinf;
		fFea_All_Max_TrainArrf[iFeaf] = fFeaMaxf;

		fMean_All_Feas_TrainArrf[iFeaf] = fMeanf;
		fStDev_All_Feas_TrainArrf[iFeaf] = fStDevf;

		if (fFeaMinf > fFeaMaxf || fStDevf < 0.0)
		{
			printf("\n\nAn error in 'Reading_Min_Max_Mean_StDev': fFeaMinf = %E > fFeaMaxf = %E, fStDevf = %E", fFeaMinf, fFeaMaxf, fStDevf);
			getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;

		} //if (fFeaMinf > fFeaMaxf || fStDevf < 0.0)
		//printf("\n %E %E %E %E", fFeaMinf, fFeaMaxf, fMeanf, fStDevf);

	} //for (iFeaf = 0; iFeaf < nDim_AllFeas_Readf; iFeaf++)

	return SUCCESSFUL_RETURN;
} //int Reading_Min_Max_Mean_StDev(...
///////////////////////////////////////

int ReadingAModel_2p0(
	float &fPercentageOfCorrectTot_Train_Readf,
	float &fPercentageOfCorrectTot_Test_Readf,

	float &fFeaConstInitf,

	int &nDim_SelecFeas_Readf,
	int &nDim_SelecFeasWithConst_Readf, // = dimension of the original space

	int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
	int &nDim_H_Readf, //dimension of the nonlinear/transformed space

	int &nK_Readf, //nNumOfHyperplanes
	int &nDim_U_Readf, //(nDim_D_WithConst*nDim_H*nK)

	float fFeaSelecMin_TrainArrf[], //[nDim_SelecFeas_Readf]
	float fFeaSelecMax_TrainArrf[], //[nDim_SelecFeas_Readf]

	float fMean_Selec_Feas_TrainArrf[], //[nDim_SelecFeas_Readf]
	float fStDev_Selec_Feas_TrainArrf[], //[nDim_SelecFeas_Readf]

	///////////////////////
	float fW_Train_Read_Arrf[], //[nDim_H_Read]
	float fU_Train_Read_Arrf[]) //[nDim_U_Read],
{
	int Reading_Min_Max_Mean_StDev(

		int &nDim_SelecFeas_Readf, //nNumOfHyperplanes

		float fFea_All_Min_TrainArrf[], //[nDim_AllFeas_Readf]
		float fFea_All_Max_TrainArrf[], //[nDim_AllFeas_Readf]

		float fMean_All_Feas_TrainArrf[], //[nDim_AllFeas_Readf]
		float fStDev_All_Feas_TrainArrf[]); //[nDim_AllFeas_Readf]

	int Extracting_Data_From_OneLine_ForAModel(
		const char *cInputOneLinef,
		const int nDim_Df,

		//int &nYf,
		float fFeaOneLineArrf[]);  //[nDim_D_WithConst]

	int Converting_Min_Max_Mean_StDev_OfAllFeas_To_SelecFeas(
		const int nDim_AllFeas_Readf, // = dimension of the original space
		const int nDim_SelecFeasf, // = dimension of the original space

		const int nPosOfSelec_FeasArrf[], //[nDim_SelecFeasf]

		const float fFea_All_Min_TrainArrf[], //[nDim_AllFeas_Readf]
		const float fFea_All_Max_TrainArrf[], //[nDim_AllFeas_Readf]

		const float fMean_All_Feas_TrainArrf[], //[nDim_AllFeas_Readf]
		const float fStDev_All_Feas_TrainArrf[], //[nDim_AllFeas_Readf]
		/////////////////
		float fFeaSelecMin_TrainArrf[], //[nDim_SelecFeas_Readf]
		float fFeaSelecMax_TrainArrf[], //[nDim_SelecFeas_Readf]

		float fMean_Selec_Feas_TrainArrf[], //[nDim_SelecFeas_Readf]
		float fStDev_Selec_Feas_TrainArrf[]); //[nDim_SelecFeas_Readf]
////////////////////////////////////////////////
	char cInputLinef[nInputLineLengthMax];

	
	int
		nResf,
		nIndexf,

		nIndexMaxf, // = nDim_U_Readf - 1,
		nPositOfSelecFeaf,

		iFea_Hf,
		iHyperplanef,

		nDim_All_Feas_Readf,
		//nDim_U_Readf,
		nProd_iFea_Hf_nDim_SelecFeas_WithConstf,
		
		nProd_nDim_SelecFeas_WithConstf_nDim_Hf, // = nDim_SelecFeasWithConst_Readf * nDim_H_Readf,

		nProd_iHyperplane_nDim_SelecFeas_WithConstf_nDim_Hf,

		iFeaf;

	float
		fFeaMinf,
		fFeaMaxf,
		
		fFea_All_Min_TrainArrf[nDim_DifEvo], //[nDim_AllFeas_Readf]
		fFea_All_Max_TrainArrf[nDim_DifEvo], //[nDim_AllFeas_Readf]

		fMean_All_Feas_TrainArrf[nDim_DifEvo], //[nDim_AllFeas_Readf]
		fStDev_All_Feas_TrainArrf[nDim_DifEvo], //[nDim_AllFeas_Readf]

		fMeanf,
		fStDevf,
		fReadTempf;
//////////////////////////////////////////////

	rewind(fin_Model);
	rewind(fin_Min_Max_Mean_StDev);
//////////////////////////////////////////
	nResf = Reading_Min_Max_Mean_StDev(

		nDim_All_Feas_Readf, //int &nDim_SelecFeas_Readf, //nNumOfHyperplanes

		fFea_All_Min_TrainArrf, //float fFeaSelecMin_TrainArrf[], //[nDim_SelecFeas_Readf]
		fFea_All_Max_TrainArrf, //float fFeaSelecMax_TrainArrf[], //[nDim_SelecFeas_Readf]

		fMean_All_Feas_TrainArrf, //float fMean_All_Feas_TrainArrf[], //[nDim_SelecFeas_Readf]
		fStDev_All_Feas_TrainArrf); // float fStDev_All_Feas_TrainArrf[]); //[nDim_SelecFeas_Readf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\nAn error in 'ReadingAModel'_2p0' by 'Reading_Min_Max_Mean_StDev'");
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	if (nDim_All_Feas_Readf != nDim_DifEvo)
	{
		printf("\n\nAn error in 'ReadingAModel_2p0': nDim_All_Feas_Readf = %d != nDim_DifEvo = %d", nDim_All_Feas_Readf, nDim_DifEvo);
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nDim_All_Feas_Readf != nDim_DifEvo)

	
//////////////////////
	//fprintf(fout_Models, "\n\n ///////////////////////////////////////////////////////");
	//fprintf(fout_Models, "\n%E %E", fPercentageOfCorrectTot_Trainf, fPercentageOfCorrectTot_Testf);
	//fscanf(fin_Model, "%E ", &fPercentageOfCorrectTot_Train_Readf);
	//fscanf(fin_Model, "%E", &fPercentageOfCorrectTot_Test_Readf);

	fscanf(fin_Model, "%E %E", &fPercentageOfCorrectTot_Train_Readf, &fPercentageOfCorrectTot_Test_Readf);

	fscanf(fin_Model, "%E", &fFeaConstInitf);

	printf("\n\n Efficiencies: %E %E ", fPercentageOfCorrectTot_Train_Readf, fPercentageOfCorrectTot_Test_Readf);
	//printf("\n fFeaConstInitf = %E", fFeaConstInitf);
		//printf("\n Please press any key:"); getchar();	//fin_Model

	//nDim_D = nDim_FeasInit = 15
	fscanf(fin_Model, "%d", &nDim_SelecFeas_Readf);
	if (nDim_SelecFeas_Readf != nDim_Selec_Read)
	{
		printf("\n\nAn error in 'ReadingAModel'_2p0': nDim_SelecFeas_Readf = %d != nDim_Selec_Read = %d", nDim_SelecFeas_Readf, nDim_Selec_Read);
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nDim_SelecFeas_Readf != nDim_Selec_Read)

	for (iFeaf = 0; iFeaf < nDim_SelecFeas_Readf; iFeaf++)
	{
		fprintf(fout, "\nfFea_All_Min_TrainArrf[%d] = %E, fFea_All_Max_TrainArrf[%d] = %E, fMean_All_Feas_TrainArrf[%d] = %E, fStDev_All_Feas_TrainArrf[%d] = %E",
			iFeaf, fFea_All_Min_TrainArrf[iFeaf],
			iFeaf, fFea_All_Max_TrainArrf[iFeaf],
			iFeaf, fMean_All_Feas_TrainArrf[iFeaf],
			iFeaf, fStDev_All_Feas_TrainArrf[iFeaf]);
	}//for (iFeaf = 0; iFeaf < nDim_SelecFeas_Readf; iFeaf++)
	//printf("\n\n 'ReadingAModel_2p0' 1: nDim_All_Feas_Readf = %d, nDim_SelecFeas_Readf = %d", nDim_All_Feas_Readf, nDim_SelecFeas_Readf); getchar();

//		const int nPosOfSelec_FeasArrf[], //[nDim_SelecFeas_Readf]
	for (iFeaf = 0; iFeaf < nDim_SelecFeas_Readf; iFeaf++)
	{
		fscanf(fin_Model, "%d", &nPositOfSelecFeaf); // 

		nPosOfSelec_FeasArrf[iFeaf] = nPositOfSelecFeaf;
	}//for (iFeaf = 0; iFeaf < nDim_SelecFeas_Readf; iFeaf++)

	for (iFeaf = 0; iFeaf < nDim_SelecFeas_Readf; iFeaf++)
	{
		fprintf(fout, "\n nPosOfSelec_FeasArrf[%d] = %d", iFeaf,nPosOfSelec_FeasArrf[iFeaf]);
	}//for (iFeaf = 0; iFeaf < nDim_SelecFeas_Readf; iFeaf++)
	fflush(fout);
	//printf("\n\n 'ReadingAModel_2p0' 1_1: nDim_All_Feas_Readf = %d, nDim_SelecFeas_Readf = %d", nDim_All_Feas_Readf, nDim_SelecFeas_Readf); getchar();

////////////////////////////////////////////////////
//converting

	nResf = Converting_Min_Max_Mean_StDev_OfAllFeas_To_SelecFeas(
		nDim_All_Feas_Readf, //const int nDim_AllFeas_Readf, // = dimension of the original space
		nDim_SelecFeas_Readf, //const int nDim_SelecFeasf, // = dimension of the original space

		nPosOfSelec_FeasArrf, //const int nPosOfSelec_FeasArrf[], //[nDim_SelecFeasf]

		fFea_All_Min_TrainArrf, //const float fFea_All_Min_TrainArrf[], //[nDim_AllFeas_Readf]
		fFea_All_Max_TrainArrf, //const float fFea_All_Max_TrainArrf[], //[nDim_AllFeas_Readf]

		fMean_All_Feas_TrainArrf, //const float fMean_All_Feas_TrainArrf[], //[nDim_AllFeas_Readf]
		fStDev_All_Feas_TrainArrf, //const float fStDev_All_Feas_TrainArrf[], //[nDim_AllFeas_Readf]
		/////////////////
		fFeaSelecMin_TrainArrf, //float fFeaSelecMin_TrainArrf[], //[nDim_SelecFeas_Readf]
		fFeaSelecMax_TrainArrf, //float fFeaSelecMax_TrainArrf[], //[nDim_SelecFeas_Readf]

		fMean_Selec_Feas_TrainArrf, //float fMean_Selec_Feas_TrainArrf[], //[nDim_SelecFeas_Readf]
		fStDev_Selec_Feas_TrainArrf); // float fStDev_Selec_Feas_TrainArrf[]) //[nDim_SelecFeas_Readf]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\nAn error in 'ReadingAModel'_2p0' by 'Converting_Min_Max_Mean_StDev_OfAllFeas_To_SelecFeas'");
			getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

		//printf("\n\n 'ReadingAModel_2p0' 2"); getchar();

///////////////////////////////////////////////////////
/*
	fprintf(fout_Models_Refined, "\n%d", nDim_SelecFeas_WithConstf);

	fprintf(fout_Models_Refined, "\n%d", nDim_Hf);
	fprintf(fout_Models_Refined, "\n%d", nKf);

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		fprintf(fout_Models_Refined, "\n%E", fW_Train_Arrf[iFea_Hf]);
	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
//////////////////////////////////////

	fprintf(fout_Models_Refined, "\n%d", nDim_Uf);

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nProd_iFea_Hf_nDim_SelecFeas_WithConstf = iFea_Hf * nDim_SelecFeas_WithConstf;

		for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;

			fprintf(fout_Models_Refined, "\n%d %d\n", iFea_Hf, iHyperplanef);

			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				nIndexf = iFeaf + nProd_iFea_Hf_nDim_SelecFeas_WithConstf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf;

				if (nIndexf > nIndexMaxf)
				{
					printf("\n\n An error in 'WritingARefinedModel_WithSelecFeas': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

					printf("\n\nPlease press any key to exit");
					getchar();	exit(1);

					//return UNSUCCESSFUL_RETURN;
				}//if (nIndexf > nIndexMaxf)

				fprintf(fout_Models_Refined, "%d:%E ", iFeaf, fU_Train_Arrf[nIndexf]);
			} //for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

		}//for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)

	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

*/

	//fprintf(fout_Models_Refined, "\n%d", nDim_SelecFeas_WithConstf);
	fscanf(fin_Model, "%d", &nDim_SelecFeasWithConst_Readf);

	if (nDim_SelecFeasWithConst_Readf != nDim_SelecFeas_WithConst_Read)
	{
		printf("\n\nAn error in 'ReadingAModel'_2p0': nDim_SelecFeasWithConst_Readf = %d != nDim_SelecFeas_WithConst_Read = %d", nDim_SelecFeasWithConst_Readf, nDim_SelecFeas_WithConst_Read);
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nDim_SelecFeasWithConst_Readf != nDim_SelecFeas_WithConst_Read)

	//printf("\nnDim_SelecFeasWithConst_Readf = %d ", nDim_SelecFeasWithConst_Readf); getchar();
	//fprintf(fout_Models, "\n%d", nDim_Hf);
	fscanf(fin_Model, "%d", &nDim_H_Readf);
	if (nDim_H_Readf != nDim_H_Read)
	{
		printf("\n\nAn error in 'ReadingAModel'_2p0': nDim_H_Readf = %d != nDim_H_Readf = %d", nDim_SelecFeas_Readf, nDim_H_Readf);

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nDim_H_Readf != nDim_H_Readf)
	//printf("\nnDim_H_Readf = %d ", nDim_H_Readf); getchar();

	nProd_nDim_SelecFeas_WithConstf_nDim_Hf = nDim_SelecFeasWithConst_Readf * nDim_H_Readf;

	//check for the 'delete[]' later

			//fprintf(fout_Models, "\n%d", nKf);
	fscanf(fin_Model, "%d", &nK_Readf);
	if (nK_Readf != nK_Read)
	{
		printf("\n\nAn error in 'ReadingAModel'_2p0': nK_Readf = %d != nK_Readf = %d", nDim_SelecFeas_Readf, nK_Readf);

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nK_Readf != nK_Readf)
	//printf("\n nK_Readf = %d ", nK_Readf); getchar();

	for (iFea_Hf = 0; iFea_Hf < nDim_H_Readf; iFea_Hf++)
	{
		//	fprintf(fout_Models_Refined, "\n%E", fW_Train_Arrf[iFea_Hf]);
		fscanf(fin_Model, "%E", &fReadTempf);

		fW_Train_Read_Arrf[iFea_Hf] = fReadTempf;

		//printf("\nfW_Train_Read_Arrf[%d] = %E ", iFea_Hf,fW_Train_Read_Arrf[iFea_Hf]); //getchar();

	}//for (iFea_Hf = 0; iFea_Hf < nDim_H_Readf; iFea_Hf++)
	///////////////////////////////
	//		const int nDim_U_Globf, //(nDim_SelecFeas_WithConst*nDim_H*nK)

			//fprintf(fout_Models_Refined, "\n%d", nDim_Uf);
	fscanf(fin_Model, "%d", &nDim_U_Readf);
	if (nDim_U_Readf != nDim_U_Read)
	{
		printf("\n\nAn error in 'ReadingAModel'_2p0': nDim_U_Readf = %d != nDim_U_Read = %d", nDim_SelecFeas_Readf, nDim_U_Read);

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nK_Readf != nK_Readf)
	//printf("\n nDim_U_Readf = %d ", nDim_U_Readf); getchar();
///////////////////////////////////////////////////
	//printf("\n\n Please press any key "); getchar();

	float *fFeaOneLineArrf = new float[nDim_SelecFeasWithConst_Readf];
	if (fFeaOneLineArrf == NULL)
	{
		printf("\n\nAn error in 'ReadingAModel'_2p0': fFeaOneLineArrf == NULL");

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fFeaOneLineArrf == NULL )

	//printf("\n\n 'ReadingAModel_2p0' 3"); getchar();

///////////////////////////////
/*
	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nProd_iFea_Hf_nDim_SelecFeas_WithConstf = iFea_Hf * nDim_SelecFeas_WithConstf;

		for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;

			fprintf(fout_Models_Refined, "\n%d %d\n", iFea_Hf, iHyperplanef);

			for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)
			{
				nIndexf = iFeaf + nProd_iFea_Hf_nDim_SelecFeas_WithConstf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf;

				if (nIndexf > nIndexMaxf)
				{
					printf("\n\n An error in 'WritingARefinedModel_WithSelecFeas': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

					printf("\n\nPlease press any key to exit");
					getchar();	exit(1);

					//return UNSUCCESSFUL_RETURN;
				}//if (nIndexf > nIndexMaxf)
*/
	nIndexMaxf = nDim_U_Readf - 1;

	for (iFea_Hf = 0; iFea_Hf < nDim_H_Readf; iFea_Hf++)
	{
		nProd_iFea_Hf_nDim_SelecFeas_WithConstf = iFea_Hf * nDim_SelecFeasWithConst_Readf;

		for (iHyperplanef = 0; iHyperplanef < nK_Readf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;

			//fprintf(fout_Models, "\n%d %d\n", iFea_Hf, iHyperplanef);
			fscanf(fin_Model, "%d %d", &iFea_Hf, &iHyperplanef);
			//printf("\n\n iFea_Hf = %d, iHyperplanef = %d\n", iFea_Hf, iHyperplanef);

			memset(cInputLinef, 0, sizeof(cInputLinef));

			if (fgets(cInputLinef, nInputLineLengthMax, fin_Model) == NULL)
			{
				//printf("\n\n No data to read 1: fgets(cInputLinef, nInputLineLengthMaxf, fin_Model) == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n No data to read 1: fgets(cInputLinef, nInputLineLengthMaxf, fin_Train) == NULL, iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				delete[] fFeaOneLineArrf;

				return SUCCESSFUL_RETURN;
			} //if (fgets(cInputLinef, nInputLineLengthMax, fin_Model) == NULL)
////////////////////////////////////
			memset(cInputLinef, 0, sizeof(cInputLinef));

			if (fgets(cInputLinef, nInputLineLengthMax, fin_Model) == NULL)
			{
				//printf("\n\n No data to read 2: fgets(cInputLinef, nInputLineLengthMaxf, fin_Model) == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n No data to read 2: fgets(cInputLinef, nInputLineLengthMaxf, fin_Train) == NULL, iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				delete[] fFeaOneLineArrf;

				return SUCCESSFUL_RETURN;
			} //if (fgets(cInputLinef, nInputLineLengthMax, fin_Model) == NULL)

			//printf("\n\n Before 'Extracting_Data_From_OneLine_ForAModel': %s\n", cInputLinef); getchar();

/////////////////////////
			nResf = Extracting_Data_From_OneLine_ForAModel(
				cInputLinef, //const char *cInputOneLinef,
				nDim_SelecFeasWithConst_Readf, //const int nDim_Df,

				fFeaOneLineArrf); // float fFeaOneLineArrf[]);  //[nDim_SelecFeasWithConst_Readf]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'ReadingAModel'_2p0' by 'Extracting_Data_From_OneLine_ForAModel'");

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n An error in 'ReadingAModel'_2p0' by 'Extracting_Data_From_OneLine_ForAModel'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				printf("\n\nPlease press any key to exit");
				getchar();	exit(1);
				delete[] fFeaOneLineArrf;

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

			for (iFeaf = 0; iFeaf < nDim_SelecFeasWithConst_Readf; iFeaf++)
			{
				nIndexf = iFeaf + nProd_iFea_Hf_nDim_SelecFeas_WithConstf + nProd_iHyperplane_nDim_SelecFeas_WithConstf_nDim_Hf;

				if (nIndexf > nIndexMaxf)
				{
					printf("\n\n An error in 'ReadingAModel'_2p0': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n An error in 'ReadingAModel'_2p0': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\nPlease press any key to exit");
					getchar();	exit(1);
					delete[] fFeaOneLineArrf;

					return UNSUCCESSFUL_RETURN;
				}//if (nIndexf > nIndexMaxf)

				//fprintf(fout_Models, "%d:%E ", iFeaf, fU_Train_Read_Arrf[nIndexf]);
				fU_Train_Read_Arrf[nIndexf] = fFeaOneLineArrf[iFeaf];

				//printf("\n %d:%E ", iFeaf, fU_Train_Read_Arrf[nIndexf]);
				//getchar();
			} //for (iFeaf = 0; iFeaf < nDim_SelecFeas_WithConstf; iFeaf++)

		}//for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)

	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

/*
	for (iFeaf = 0; iFeaf < nDim_U_Readf; iFeaf++)
	{
		//printf("\n 'ReadingAModel'_2p0': fU_Train_Read_Arr[%d] = %E",iFeaf, fU_Train_Read_Arrf[iFeaf]);

		//fprintf(fout, "\n'ReadingAModel'_2p0': fU_Train_Read_Arr[%d] = %E", iFeaf, fU_Train_Read_Arrf[iFeaf]);
		//printf("\n Please press any key"); fflush(fout);  getchar();

	} //for (iFea = 0; iFeaf < nDim_U_Readf;iFeaf++)
*/
//printf("\n\n The end of 'ReadingAModel'_2p0': please press any key"); fflush(fout);  getchar();
	//printf("\n\n 'ReadingAModel_2p0' 4"); getchar();


	delete[] fFeaOneLineArrf;
	return SUCCESSFUL_RETURN;
} //int ReadingAModel_2p0(...
///////////////////////////////////////////////

int Extracting_Data_From_OneLine_ForAModel(
	const char *cInputOneLinef,
	const int nDim_Df,

	//int &nYf,

	float fFeaOneLineArrf[]) //[nDim_D_WithConst]
{
	int
		iFeaf,
		iPositf,
		nPosInTheCurrentSegmentf = 0,
		nSegmentNumberCurf = 0,

		nNumOfFeaCurf = -1, //initially
		nPosInTheLinef = 0,
		nLengthOfInputCharLinef = strlen(cInputOneLinef);

	char
		cCharSubstringf[nSubstringLenMax];

	if (nLengthOfInputCharLinef >= nLengthOneLineMax - 5)
	{
		printf("\n\n An error in 'Extracting_Data_From_OneLine_ForAModel': nLengthOfInputCharLinef >= nLengthOneLineMax - 5");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\nAn error in 'Extracting_Data_From_OneLine_ForAModel': nLengthOfInputCharLinef >= nLengthOneLineMax - 5");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();	exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nLengthOfInputCharLinef >= nLengthOneLineMax - 5)

	memset(cCharSubstringf, 0, sizeof(cCharSubstringf));

	/////////////////////////////////////////////////////////
	//for verification
	for (iFeaf = 0; iFeaf < nDim_Df; iFeaf++)
	{
		fFeaOneLineArrf[iFeaf] = -fLarge;
	} //for (iFeaf = 0; iFeaf < nDim_Df; iFeaf++)

	//printf("\n\n'Extracting_Data_From_OneLine_ForAModel': %s\n", cInputOneLinef);
	//printf("\n nLengthOfInputCharLinef = %d\n", nLengthOfInputCharLinef);

	///////////////////////////////////////////
	nPosInTheCurrentSegmentf = 0;

	//do // while(nPosInTheLinef < nLengthOfInputCharLinef);
	for (nPosInTheLinef = 0; nPosInTheLinef < nLengthOfInputCharLinef; nPosInTheLinef++)
	{
		//printf("\n The next nPosInTheLinef = %d, nPosInTheCurrentSegmentf = %d, nSegmentNumberCurf = %d", nPosInTheLinef, nPosInTheCurrentSegmentf, nSegmentNumberCurf);

		if (nPosInTheCurrentSegmentf == 0)
		{
			if (cInputOneLinef[nPosInTheLinef] == ' ' || cInputOneLinef[nPosInTheLinef] == ':' || cInputOneLinef[nPosInTheLinef] == ',' || cInputOneLinef[nPosInTheLinef] == '\t'
				|| cInputOneLinef[nPosInTheLinef] == '\0' || cInputOneLinef[nPosInTheLinef] == '\r' || cInputOneLinef[nPosInTheLinef] == '\n')
			{
				//printf("\n\n Not a substring symbol at nPosInTheLinef = %d, nPosInTheCurrentSegmentf = %d, nSegmentNumberCurf = %d", nPosInTheLinef, nPosInTheCurrentSegmentf, nSegmentNumberCurf);

				//printf("\n\nPlease press any key to continue:"); getchar();
				continue;
			} //if (cInputOneLinef[nPosInTheLinef] == ' ' || ...
			else
			{
				nPosInTheCurrentSegmentf = 1;
				cCharSubstringf[0] = cInputOneLinef[nPosInTheLinef];

				//printf("\n\n The beginning of next 'cCharSubstringf': %s", cCharSubstringf);

				nSegmentNumberCurf += 1;
				//printf("\n nPosInTheLinef = %d, nPosInTheCurrentSegmentf = %d, the new nSegmentNumberCurf = %d", nPosInTheLinef, nPosInTheCurrentSegmentf, nSegmentNumberCurf);

				//printf("\n\nPlease press any key to continue:"); getchar();
				continue;
			} //

		} //if (nPosInTheCurrentSegmentf == 0)
		else if (nPosInTheCurrentSegmentf != 0)
		{
			if (cInputOneLinef[nPosInTheLinef] == ' ' || cInputOneLinef[nPosInTheLinef] == ':' || cInputOneLinef[nPosInTheLinef] == ',' || cInputOneLinef[nPosInTheLinef] == '\t'
				|| cInputOneLinef[nPosInTheLinef] == '\0' || cInputOneLinef[nPosInTheLinef] == '\r' || cInputOneLinef[nPosInTheLinef] == '\n')
			{
				/////////////////////////////////////////////
				//the end of the substring -- converting the substring to a number
								//printf("\n\n The end of 'cCharSubstringf': %s", cCharSubstringf);
								//printf("\n nPosInTheLinef = %d, nPosInTheCurrentSegmentf = %d, nSegmentNumberCurf = %d", nPosInTheLinef, nPosInTheCurrentSegmentf, nSegmentNumberCurf);

								//if (nSegmentNumberCurf == 0)
				//if (nSegmentNumberCurf > 1 && (nSegmentNumberCurf / 2) * 2 == nSegmentNumberCurf)
				if ((nSegmentNumberCurf / 2) * 2 != nSegmentNumberCurf)
				{
					nNumOfFeaCurf = atoi(cCharSubstringf);

					//printf("\n\n A new nNumOfFeaCurf = %d, nDim_D_WithConstf = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
						//nNumOfFeaCurf, nDim_D_WithConstf, nPosInTheLinef, nSegmentNumberCurf);

					if (nNumOfFeaCurf < 0 || nNumOfFeaCurf > nDim_Df - 1) //for 'Best_Feas_Normal_Malignant_Train.txt'
					{
						printf("\n\n An error in 'Extracting_Data_From_OneLine_ForAModel': nNumOfFeaCurf = %d, nDim_Df = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nNumOfFeaCurf, nDim_Df, nPosInTheLinef, nSegmentNumberCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine_ForAModel': nNumOfFeaCurf = %d, nDim_Df = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nNumOfFeaCurf, nDim_Df, nPosInTheLinef, nSegmentNumberCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						getchar();	exit(1);
						return UNSUCCESSFUL_RETURN;
					}//if (nNumOfFeaCurf != 1)

					memset(cCharSubstringf, 0, sizeof(cCharSubstringf));

					nPosInTheCurrentSegmentf = 0;
					//printf("\n\nPlease press any key to continue:"); getchar();
					continue;
				} //if ((nSegmentNumberCurf / 2) * 2 != nSegmentNumberCurf)
				else if ((nSegmentNumberCurf / 2) * 2 == nSegmentNumberCurf)
				{

					if (nNumOfFeaCurf < 0 || nNumOfFeaCurf > nDim_Df - 1) //for 'Best_Feas_Normal_Malignant_Train.txt'
					{
						printf("\n\n An error in 'Extracting_Data_From_OneLine_ForAModel': nNumOfFeaCurf = %d, nDim_Df = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nNumOfFeaCurf, nDim_Df, nPosInTheLinef, nSegmentNumberCurf);

						getchar();	exit(1);

						return UNSUCCESSFUL_RETURN;
					} //if (nNumOfFeaCurf - 1 < 0 || nNumOfFeaCurf - 1 > nDim_Df - 1)

					fFeaOneLineArrf[nNumOfFeaCurf] = atof(cCharSubstringf); //

					if (fFeaOneLineArrf[nNumOfFeaCurf] < fFeaMin || fFeaOneLineArrf[nNumOfFeaCurf] > fFeaMax) //for 'Best_Feas_Normal_Malignant_Train.txt'
					{
						printf("\n\n An error in 'Extracting_Data_From_OneLine_ForAModel': fFeaOneLineArrf[%d] = %E, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
							nNumOfFeaCurf, fFeaOneLineArrf[nNumOfFeaCurf], nPosInTheLinef, nSegmentNumberCurf);

						printf("\n fFeaMin = %E, fFeaMax = %E", fFeaMin, fFeaMax);
						getchar();	exit(1);

						return UNSUCCESSFUL_RETURN;
					}//if (fFeaOneLineArrf[nNumOfFeaCurf - 1] < fFeaMin || fFeaOneLineArrf[nNumOfFeaCurf - 1] > fFeaMax)

				//	printf("\n\n Fea 'cCharSubstringf': %s", cCharSubstringf);
					//printf("\n A new fFeaOneLineArrf[%d] = %E, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
					//	nNumOfFeaCurf, fFeaOneLineArrf[nNumOfFeaCurf], nPosInTheLinef, nSegmentNumberCurf);

					memset(cCharSubstringf, 0, sizeof(cCharSubstringf));
					nPosInTheCurrentSegmentf = 0;
					//printf("\n\nPlease press any key to continue:"); getchar(); 
					continue;
				} //else if ( (nSegmentNumberCurf / 2) * 2 == nSegmentNumberCurf)

				memset(cCharSubstringf, 0, sizeof(cCharSubstringf));

				//printf("\n\n The end of the substring 'cCharSubstringf' = %s", cCharSubstringf);
				//printf("\n nPosInTheLinef = %d, nSegmentNumberCurf = %d, nNumOfFeaCurf = %d", nPosInTheLinef, nSegmentNumberCurf, nNumOfFeaCurf);

				//printf("\n\n Please press any key to continue:"); getchar();
///////////////////////////////////////////////////
				nPosInTheCurrentSegmentf = 0;
				continue;
			} //if (cInputOneLinef[nPosInTheLinef] == ' ' || ...
			else // no end of the substring
			{
				nPosInTheCurrentSegmentf += 1;
				cCharSubstringf[nPosInTheCurrentSegmentf - 1] = cInputOneLinef[nPosInTheLinef];

				//printf("\n\n Increasing 'cCharSubstringf': %s", cCharSubstringf);
				//printf("\n nPosInTheLinef = %d, a new nPosInTheCurrentSegmentf = %d, the same nSegmentNumberCurf = %d", nPosInTheLinef, nPosInTheCurrentSegmentf, nSegmentNumberCurf);

				//printf("\n\nPlease press any key to continue:"); getchar();
				continue;

			} // else // no end of the substring

		} //else if (nPosInTheCurrentSegmentf != 0)

	//printf("\n\n The end of the loop: nPosInTheLinef = %d, nSegmentNumberCurf = %d, nNumOfFeaCurf = %d", nPosInTheLinef, nSegmentNumberCurf, nNumOfFeaCurf);
	} //for (nPosInTheLinef = 0; nPosInTheLinef < nLengthOfInputCharLinef; nPosInTheLinef++)

	//if (nNumOfFeaCurf != nDim_Df) //for initial testing data of 'svmguide1_train_2000_2178.txt' etc.
	if (nNumOfFeaCurf + 1 != nDim_Df) //for 'Best_Feas_Normal_Malignant_Train.txt'
	{
		printf("\n\n An error in 'Extracting_Data_From_OneLine_ForAModel': nNumOfFeaCurf + 1 = %d != nDim_Df = %d, nPosInTheLinef = %d, nSegmentNumberCurf = %d",
			nNumOfFeaCurf + 1, nDim_Df, nPosInTheLinef, nSegmentNumberCurf);

		getchar();	exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfFeaCurf + 1 != nDim_Df)

////////////////////////////////
/*
	for (iFeaf = 0; iFeaf < nDim_Df; iFeaf++)
	{
		printf("\n fFeaOneLineArrf[%d] = %E", iFeaf, fFeaOneLineArrf[iFeaf]);
	} //for (iFeaf = 0; iFeaf < nDim_Df; iFeaf++)

	//printf("\n\n The end of 'Extracting_Data_From_OneLine_ForAModel': please press any key"); getchar();
*/

	return SUCCESSFUL_RETURN;
}// int Extracting_Data_From_OneLine_ForAModel(...
//////////////////////////////////////////////

void Loss_Estimate_ForOneVec(
	const int nDim_Hf,

	const float fZ_Arrf[], //[nDim_Hf]

	const float fW_Arrf[], //[nDim_Hf]

	int &nY_Estimatedf, //0 or 1 (not -1 or 1)
	float &fLoss_Estimate_ForOneVecf)
{
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);
	////////////////////////////////////////
	int
		nPrintedf = 0,
		iFea_Hf;

	float
		fProdf,
		fScalar_Prodf,

		fSumf = 0.0,
		fScalar_ProdfWithBiasf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Loss_Estimate_ForOneVec': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, nYtf = %d", iVec_Train_Glob, nY_Train_Actual_Glob, nYtf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	Scalar_Product(
		nDim_Hf, //const int nDimf,

		fZ_Arrf, //const float fFeas_Arr_1f[],

		fW_Arrf, //const float fFeas_Arr_2f[],
		fScalar_Prodf); // float &fScalar_Prodf);

	fScalar_ProdfWithBiasf = fScalar_Prodf + fBiasForClassifByLossFunction_Glob;

	//if (fScalar_Prodf < 0.0)
	if (fScalar_ProdfWithBiasf < 0.0)
	{
		nY_Estimatedf = 0; // not -1;
	}// if (fScalar_ProdfWithBiasf < 0.0)
	else
	{
		nY_Estimatedf = 1;
	}//else

	fLoss_Estimate_ForOneVecf = fScalar_ProdfWithBiasf;

//#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Loss_Estimate_ForOneVec': fLoss_Estimate_ForOneVecf = %E, fScalar_Prodf = %E, nY_Estimatedf = %d", fLoss_Estimate_ForOneVecf, fScalar_Prodf, nY_Estimatedf);

	fSumf = 0.0;
	//if (nY_Estimatedf == 0 && fLoss_Estimate_ForOneVecf > 0.0)
	{
		//fprintf(fout, "\n A loss for a neg vector");

		//if (nPrintedf == 0)
		{
			for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
			{
				fSumf += fZ_Arrf[iFea_Hf] * fW_Arrf[iFea_Hf];

				fprintf(fout, "\n fZ_Arrf[%d] = %E, fW_Arrf[%d] = %E, fSumf = %E", iFea_Hf, fZ_Arrf[iFea_Hf], iFea_Hf, fW_Arrf[iFea_Hf], fSumf);

			} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

			nPrintedf = 1;
		} //if (nPrintedf == 0)

	} //if (nYtf == -1 && fLoss_Estimate_ForOneVecf > 0.0)

/*
	if (nY_Train_Actual_Glob != nY_Estimatedf && nY_Estimatedf == 0)
	{
		fprintf(fout, "\n A different classification in 'Loss_Estimate_ForOneVec' for a neg vector");

		//if (nPrintedf == 0)
		{
			for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
			{
				fprintf(fout, "\n fZ_Arrf[%d] = %E, fW_Arrf[%d] = %E", iFea_Hf, fZ_Arrf[iFea_Hf], iFea_Hf, fW_Arrf[iFea_Hf]);

			} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

			//nPrintedf = 1;
		} //if (nPrintedf == 0)

	} //if (nY_Train_Actual_Glob != nY_Estimatedf  && nYtf == -1) 
*/
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

}// void Loss_Estimate_ForOneVec(...
/////////////////////////////////////////////////////////////////

void Converting_OneFeaVec_To_AVecWithConst(
	const int nDim_Df, // = dimension of the original space
	const int nDim_D_WithConstf, // = dimension of the original space

	const float fFeaConstInitf,
	const float fFea_OneVecArrf[], //[nDim_Df]

	float fFeas_OneVecWithConst_Arrf[]) //[nDim_D_WithConst]
{
	int
		iFeaf;

	for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
	{
		if (iFeaf < nDim_Df)
		{
			fFeas_OneVecWithConst_Arrf[iFeaf] = fFea_OneVecArrf[iFeaf];
		} // if (iFeaf < nDim_Df)
		else
		{
			fFeas_OneVecWithConst_Arrf[iFeaf] = fFeaConstInitf;
		} //else

	} // for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
} //void Converting_OneFeaVec_To_AVecWithConst(...
///////////////////////////////////////////////////////////////

void Converting_AllFeaVecs_To_VecsWithConst(
	const int nDim_Df, // = dimension of the original space
	const int nDim_D_WithConstf, // = dimension of the original space

	const int nNumVecsTotf,

	const float fFeaConstInitf,
	const float fFeas_AllVecs_Arrf[], //[nDim_Df*nNumVecsTotf]

	float fFeas_AllVecs_WithConst_Arrf[]) //[nDim_D_WithConst*nNumVecsTotf]
{
	int
		nTempf,
		nTempWithConstf,
		nIndexf,
		nIndexWithConstf,
		iVecf,
		iFeaf;

	//including the const into the fea vector
	for (iVecf = 0; iVecf < nNumVecsTotf; iVecf++)
	{
		nTempf = iVecf * nDim_Df;
		nTempWithConstf = iVecf * nDim_D_WithConstf;

		for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
		{
			nIndexWithConstf = iFeaf + nTempWithConstf;
			if (iFeaf < nDim_Df)
			{
				nIndexf = iFeaf + nTempf;

				fFeas_AllVecs_WithConst_Arrf[nIndexWithConstf] = fFeas_AllVecs_Arrf[nIndexf];
			} // if (iFeaf < nDim_Df)
			else
			{
				fFeas_AllVecs_WithConst_Arrf[nIndexWithConstf] = fFeaConstInitf;
				//fprintf(fout, "\n Init const (train): fFea_WithConstTrain_Arrf[%d] = %E, iFeaf = %d, iVecf = %d", nIndexWithConstf, fFea_WithConstTrain_Arrf[nIndexWithConstf], iFeaf, iVecf);

			} //else

		} // for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

		//fprintf(fout, " nY_Train_Arrf[%d] = %d, ", iVecf, nY_Train_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecsTotf; iVecf++)

} //void Converting_AllFeaVecs_To_VecsWithConst(...
//////////////////////////////////////////////////////

void Normalizing_EveryFea_InOneFeaVec_UsingTrain_Mean_0_And_StDev_1(
	const float fLargef,
	const float fepsf,

	const int nDimf,

	/////////////////////////////////////////////
	const float fFeaMin_TrainArrf[], //[nDimf]
	const float fFeaMax_TrainArrf[], //[nDimf]

	const float fMean_All_Feas_TrainArrf[], //[nDimf]
	const float fStDev_All_Feas_TrainArrf[], //[nDimf]

	float fFea_OneVec_Arrf[]) //[nDimf]//to be normalized using the train Mean and StDev
{

	int
		iFeaf,
		iVecf;

	float
		fMeanFor_OneFeaf,
		fVarianceCurf,
		fStDevf,

		fFeaCurf,
		fDiff_Fea_And_MeanCurf,
		fDiffFeaMaxMinCurf;

	//////////////////////////////////////////////////

		//Normalization for one fea vec
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeaCurf = fFea_OneVec_Arrf[iFeaf];

		if (fStDev_All_Feas_TrainArrf[iFeaf] > eps)
		{
			fFea_OneVec_Arrf[iFeaf] = (fFeaCurf - fMean_All_Feas_TrainArrf[iFeaf]) / fStDev_All_Feas_TrainArrf[iFeaf];
		} //if (fStDev_All_Feas_TrainArrf[iFeaf] > eps)
		else if (fStDev_All_Feas_TrainArrf[iFeaf] <= eps)
		{
			fFea_OneVec_Arrf[iFeaf] = 0.0;
		} //else if (fStDev_All_Feas_TrainArrf[iFeaf] <= eps)

	} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

} //void Normalizing_EveryFea_InOneFeaVec_UsingTrain_Mean_0_And_StDev_1 (...
///////////////////////////////////////////////////////////////////////////

int Normalizing_A_Vector_ByStDev(
	const int nDimf,
	const float fFeas_InitArrf[],

	float fFeasNormalized_Arrf[])
{
	int StDev_Of_A_Vector(
		const int nDimf,
		const float fFeas_InitArrf[],

		float &StDev_Of_A_Vectorf);

	int
		nResf,
		iFeaf;

	float
		StDev_Of_A_Vectorf;

	nResf = StDev_Of_A_Vector(
		nDimf, //const int nDimf,
		fFeas_InitArrf, //const float fFeas_InitArrf[],

		StDev_Of_A_Vectorf); // float &StDev_Of_A_Vectorf);

	//printf( "\n\n 'Normalizing_A_Vector_ByStDev': StDev_Of_A_Vectorf = %E", StDev_Of_A_Vectorf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Normalizing_A_Vector_ByStDev': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, StDev_Of_A_Vectorf = %E", iVec_Train_Glob, nY_Train_Actual_Glob, StDev_Of_A_Vectorf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Normalizing_A_Vector_ByStDev' by 'StDev_Of_A_Vector' ");
		fprintf(fout, "\n\n  An error in 'Normalizing_A_Vector_ByStDev' by 'StDev_Of_A_Vector' ");
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeasNormalized_Arrf[iFeaf] = fFeas_InitArrf[iFeaf] / StDev_Of_A_Vectorf;

		if (fFeasNormalized_Arrf[iFeaf] < -fLarge || fFeasNormalized_Arrf[iFeaf] > fLarge)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Normalizing_A_Vector_ByStDev': fFeasNormalized_Arrf[iFeaf] =  %E < -fLarge || ...m iFeaf = %d", fFeasNormalized_Arrf[iFeaf], iFeaf);

			fprintf(fout, "\n\n  An error in 'Normalizing_A_Vector_ByStDev': fFeasNormalized_Arrf[iFeaf] =  %E < -fLarge || ...m iFeaf = %d", fFeasNormalized_Arrf[iFeaf], iFeaf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (fFeasNormalized_Arrf[iFeaf] < -fLarge || fFeasNormalized_Arrf[iFeaf] > fLarge)

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;
} //int Normalizing_A_Vector_ByStDev (...
////////////////////////////////////////////////////////

int StDev_Of_A_Vector(
	const int nDimf,
	const float fFeas_InitArrf[],

	float &StDev_Of_A_Vectorf)
{
	int
		iFeaf;

	if (nDimf < 2 || nDimf > nLarge)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'StDev_Of_A_Vector': nDimf = %d", nDimf);
		fprintf(fout, "\n\n  An error in 'StDev_Of_A_Vector':  nDimf = %d", nDimf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nDimf < 2 || nDimf > nLarge)

////////////////////////////////////////////
	StDev_Of_A_Vectorf = 0.0;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		StDev_Of_A_Vectorf += fFeas_InitArrf[iFeaf] * fFeas_InitArrf[iFeaf];
	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	StDev_Of_A_Vectorf = sqrt(StDev_Of_A_Vectorf / nDimf);

	if (StDev_Of_A_Vectorf < eps || StDev_Of_A_Vectorf > fLarge)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'StDev_Of_A_Vector': StDev_Of_A_Vectorf = %E", StDev_Of_A_Vectorf);
		fprintf(fout, "\n\n  An error in 'StDev_Of_A_Vector': StDev_Of_A_Vectorf = %E", StDev_Of_A_Vectorf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (StDev_Of_A_Vectorf < eps || StDev_Of_A_Vectorf > fLarge)
	else
		return SUCCESSFUL_RETURN;

} //int StDev_Of_A_Vector (...
///////////////////////////////////////////////////////////

int A_Vec_From_ArrOf_AllVecs(
	const int nDimf,
	const int nNumOfVecsTotf,
	const int nVecf,

	const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

	float fFeas_OneVec_Arrf[]) //[nDim_D_WithConst]
{
	int
		nIndexf,

		nIndexMaxf = (nDimf* nNumOfVecsTotf) - 1,
		nTempf,
		iFeaf;

	nTempf = nVecf * nDimf;

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n 'A_Vec_From_ArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nVecf = %d", nDimf, nNumOfVecsTotf, nVecf);
	fprintf(fout, "\n\n  'A_Vec_From_ArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nVecf = %d, nTempf = %d", nDimf, nNumOfVecsTotf, nVecf, nTempf);
	fprintf(fout, "\n\n iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nIndexf = iFeaf + nTempf;
		if (nIndexf > nIndexMaxf)
		{
			printf("\n\n An error in 'A_Vec_From_ArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'A_Vec_From_ArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

#ifndef COMMENT_OUT_ALL_PRINTS
		if (nVecf < 4)
		{
			fprintf(fout, "\n 'A_Vec_From_ArrOf_AllVecs': nVecf = %d, iFeaf = %d, nIndexf = %d, fFeas_All_Arrf[nIndexf] = %E", nVecf, iFeaf, nIndexf, fFeas_All_Arrf[nIndexf]);
		}//if (nVecf < 4)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		fFeas_OneVec_Arrf[iFeaf] = fFeas_All_Arrf[nIndexf];
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;
}// int A_Vec_From_ArrOf_AllVecs{...
/////////////////////////////////////////////

int Converting_OneVecOfAllFeas_To_OneVecOfSelecFeas(
	const int nDim_AllFeasf, // = dimension of the original space
	const int nDim_SelecFeasf, // = dimension of space of the selec feas

	const int nPosOfSelec_FeasArrf[], //[nDim_SelecFeasf]
	const float fFeasAll_OneVecArrf[], //[nDim_AllFeasf]

	float fFeasSelec_OneVecArrf[]) //[nDim_SelecFeasf]
{
	int
		nFeaSelecf,
		iFeaf;

	for (iFeaf = 0; iFeaf < nDim_SelecFeasf; iFeaf++)
	{
		nFeaSelecf = nPosOfSelec_FeasArrf[iFeaf];

		if (nFeaSelecf < 0 || nFeaSelecf >= nDim_AllFeasf)
		{
			printf("\n\n An error in 'Converting_OneVecOfAllFeas_To_OneVecOfSelecFeas': nFeaSelecf = %d >= nDim_AllFeasf = %d, iFeaf = %d", 
				nFeaSelecf, nDim_AllFeasf, iFeaf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Converting_OneVecOfAllFeas_To_OneVecOfSelecFeas': nFeaSelecf = %d >= nDim_AllFeasf = %d, iFeaf = %d",
				nFeaSelecf, nDim_AllFeasf, iFeaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaSelecf < 0 || nFeaSelecf >= nDim_AllFeasf)

		fFeasSelec_OneVecArrf[iFeaf] = fFeasAll_OneVecArrf[nFeaSelecf];

	} // for (iFeaf = 0; iFeaf < nDim_SelecFeasf; iFeaf++)

	return SUCCESSFUL_RETURN;
} //int Converting_OneVecOfAllFeas_To_OneVecOfSelecFeas(...
///////////////////////////////////////////////////////

int Converting_Min_Max_Mean_StDev_OfAllFeas_To_SelecFeas(
	const int nDim_AllFeas_Readf, // = dimension of the original space
	const int nDim_SelecFeasf, // = dimension of the original space

	const int nPosOfSelec_FeasArrf[], //[nDim_SelecFeasf]

	const float fFea_All_Min_TrainArrf[], //[nDim_AllFeas_Readf]
	const float fFea_All_Max_TrainArrf[], //[nDim_AllFeas_Readf]

	const float fMean_All_Feas_TrainArrf[], //[nDim_AllFeas_Readf]
	const float fStDev_All_Feas_TrainArrf[], //[nDim_AllFeas_Readf]
	/////////////////
	float fFeaSelecMin_TrainArrf[], //[nDim_SelecFeas_Readf]
	float fFeaSelecMax_TrainArrf[], //[nDim_SelecFeas_Readf]

	float fMean_Selec_Feas_TrainArrf[], //[nDim_SelecFeas_Readf]
	float fStDev_Selec_Feas_TrainArrf[]) //[nDim_SelecFeas_Readf]
{
	int
		nFeaSelecf,
		iFeaf;

	for (iFeaf = 0; iFeaf < nDim_SelecFeasf; iFeaf++)
	{
		nFeaSelecf = nPosOfSelec_FeasArrf[iFeaf];

		if (nFeaSelecf < 0 || nFeaSelecf >= nDim_AllFeas_Readf)
		{
			printf("\n\n An error in 'Converting_Min_Max_Mean_StDev_OfAllFeas_To_SelecFeas': nFeaSelecf = %d >= nDim_AllFeas_Readf = %d, iFeaf = %d",
				nFeaSelecf, nDim_AllFeas_Readf, iFeaf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Converting_Min_Max_Mean_StDev_OfAllFeas_To_SelecFeas': nFeaSelecf = %d >= nDim_AllFeas_Readf = %d, iFeaf = %d",
				nFeaSelecf, nDim_AllFeas_Readf, iFeaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			getchar();	exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaSelecf < 0 || nFeaSelecf >= nDim_AllFeasf)

		fFeaSelecMin_TrainArrf[iFeaf] = fFea_All_Min_TrainArrf[nFeaSelecf];
		fFeaSelecMax_TrainArrf[iFeaf] = fFea_All_Max_TrainArrf[nFeaSelecf];
		
		fMean_Selec_Feas_TrainArrf[iFeaf] = fMean_All_Feas_TrainArrf[nFeaSelecf];
		fStDev_Selec_Feas_TrainArrf[iFeaf] = fStDev_All_Feas_TrainArrf[nFeaSelecf];


	} // for (iFeaf = 0; iFeaf < nDim_SelecFeasf; iFeaf++)

	return SUCCESSFUL_RETURN;
} //int Converting_Min_Max_Mean_StDev_OfAllFeas_To_SelecFeas(...
//////////////////////////////////////////////

int Testing_OneFeaVec_byReadingAModel_2p0(
	const int nDim_DifEvof, //
	//const int nDim_Selec_Read_D_WithConstf, //

	float fFeas_OneVec_Arrf[], //[nDim_DifEvo] 

	float &fLossf,
	int &nOutcomef) //1 - positive, 0 - negative
{
	int ReadingAModel_2p0(
		float &fPercentageOfCorrectTot_Train_Readf,
		float &fPercentageOfCorrectTot_Test_Readf,

		float &fFeaConstInitf,

		int &nDim_SelecFeas_Readf,
		int &nDim_SelecFeasWithConst_Readf, // = dimension of the original space

		int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
		int &nDim_H_Readf, //dimension of the nonlinear/transformed space

		int &nK_Readf, //nNumOfHyperplanes
		int &nDim_U_Readf, //(nDim_D_WithConst*nDim_H*nK)

		float fFeaSelecMin_TrainArrf[], //[nDim_SelecFeas_Readf]
		float fFeaSelecMax_TrainArrf[], //[nDim_SelecFeas_Readf]

		float fMean_Selec_Feas_TrainArrf[], //[nDim_SelecFeas_Readf]
		float fStDev_Selec_Feas_TrainArrf[], //[nDim_SelecFeas_Readf]

		///////////////////////
		float fW_Train_Read_Arrf[], //[nDim_H_Read]
		float fU_Train_Read_Arrf[]); //[nDim_U_Read],
/*
	void Normalizing_EveryFea_InOneFeaVec_UsingTrain_Mean_0_And_StDev_1(
		const float fLargef,
		const float fepsf,

		const int nDim_Selec_Readf,

		/////////////////////////////////////////////
		const float fFeaMin_TrainArrf[], //[nDim_Selec_Readf]
		const float fFeaMax_TrainArrf[], //[nDim_Selec_Readf]

		const float fMean_All_Feas_TrainArrf[], //[nDim_Selec_Readf]
		const float fStDev_All_Feas_TrainArrf[], //[nDim_Selec_Readf]

		float fFea_OneVec_Arrf[]); //[nDim_Selec_Readf]//to be normalized using the train Mean and StDev
*/

	int All_Feas_OfNonlinearSpace(
		const int nDim_Selec_Read_D_WithConstf, // = dimension of the original space
		const int nDim_Selec_Read_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes

		const float fX_Arrf[], //[nDim_Selec_Read_D_WithConstf]

		const float fU_Train_Arrf[], // [nDim_Selec_Read_U_Glob] = (nDim_Selec_Read_D_WithConst*nDim_Selec_Read_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

//////////////////////////////////////////
int nHyperplaneWithMaxScaProdArrf[], //[nDim_Selec_Read_Hf]

float fZ_Arrf[]); //[nDim_Selec_Read_H]
/*
	int Normalizing_A_Vector_ByStDev(
		const int nDim_Selec_Readf,
		const float fFeas_InitArrf[],

		float fFeasNormalized_Arrf[]);
*/
	int Normalizing_FloatVector_ByStDev(
		const int nDimf,
		const float fFeas_InitArrf[],

		float fFeasNormalized_Arrf[]);

	void Loss_Estimate_ForOneVec(
		const int nDim_Selec_Read_Hf,

		const float fZ_Arrf[], //[nDim_Selec_Read_Hf]

		const float fW_Arrf[], //[nDim_Selec_Read_Hf]

		int &nY_Estimatedf, //0 or 1 (not -1 or 1)
		float &fLoss_Estimate_ForOneVecf);

	void Converting_OneFeaVec_To_AVecWithConst(
		const int nDim_Selec_Read_Df, // = dimension of the original space
		const int nDim_Selec_Read_D_WithConstf, // = dimension of the original space

		const float fFeaConstInitf,
		const float fFea_OneVecArrf[], //[nDim_Selec_Read_Df]

		float fFeas_OneVecWithConst_Arrf[]); //[nDim_Selec_Read_D_WithConst]
	/////////////////////////////////////////////////
	int
		nResf,
		iFeaf,
		iFea_Nonlinearf,
		nY_Estimatedf;

	float
		fFeaSelecMin_TrainArrf[nDim_Selec_Read], //[nDim_Selec_Readf]
		fFeaSelecMax_TrainArrf[nDim_Selec_Read], //[nDim_Selec_Readf]

		fMean_Selec_Feas_TrainArrf[nDim_Selec_Read], //[nDim_Selec_Readf]
		fStDev_Selec_Feas_TrainArrf[nDim_Selec_Read], //[nDim_Selec_Readf]

		fScalar_Prodf;

	///////////////////////////////
	//for model reading
	int
		nDim_SelecFeas_Readf,
		nDim_SelecFeasWithConst_Readf, // = dimension of the original space

		nDim_H_Readf, //dimension of the nonlinear/transformed space

		nK_Readf, //nNumOfHyperplanes
		nDim_U_Readf,//(nDim_Selec_Read_D_WithConst*nDim_Selec_Read_H*nK)

		nPosOfSelec_FeasArrf[nDim_Selec_Read];

	float
		fPercentageOfCorrectTot_Train_Readf,
		fPercentageOfCorrectTot_Test_Readf,
		fFeaConstInitf,
	
		fFeasSelec_OneVec_Arrf[nDim_Selec_Read],

		fFeasSelecNormalized_OneVecWithConst_Arrf[nDim_SelecFeas_WithConst_Read],

		fW_Train_Read_Arrf[nDim_H_Read],
		fU_Train_Read_Arrf[nDim_U_Read];
	///////////////////////

	nResf = ReadingAModel_2p0(
		fPercentageOfCorrectTot_Train_Readf, //float &fPercentageOfCorrectTot_Train_Readf,
		fPercentageOfCorrectTot_Test_Readf, //float &fPercentageOfCorrectTot_Test_Readf,
		fFeaConstInitf, //float &fFeaConstInitf,

		nDim_SelecFeas_Readf, //int &nDim_Selec_Read_D_Readf,
		nDim_SelecFeasWithConst_Readf, //int &nDim_SelecFeasWithConst_Readf, // 
		
		nPosOfSelec_FeasArrf, // int nPosOfSelec_FeasArrf[], //[nNumOfSelecFeasTotCurf]
		nDim_H_Readf,//int &nDim_H_Readf, //dimension of the nonlinear/transformed space

		nK_Readf, //int &nK_Readf, //nNumOfHyperplanes
		nDim_U_Readf, //int &nDim_U_Readf, //(nDim_Selec_Read_D_WithConst*nDim_Selec_Read_H*nK)

		fFeaSelecMin_TrainArrf, //float fFeaSelecMin_TrainArrf[], //[nDim_Selec_Readf]
		fFeaSelecMax_TrainArrf, //float fFeaSelecMax_TrainArrf[], //[nDim_Selec_Readf]

		fMean_Selec_Feas_TrainArrf, //float fMean_Selec_Feas_TrainArrf[], //[nDim_Selec_Readf]
		fStDev_Selec_Feas_TrainArrf, //float fStDev_Selec_Feas_TrainArrf[], //[nDim_Selec_Readf]

	   ///////////////////////
		fW_Train_Read_Arrf, //float fW_Train_Read_Arrf[], //[nDim_Selec_Read_Hf]
		fU_Train_Read_Arrf); // float fU_Train_Read_Arrf[]); //[nDim_U_Read = nDim_Selec_Read_D_WithConst_Read*nDim_H_Read*nK_Read],

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0' by 'ReadingAModel_2p0'");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0' by 'ReadingAModel_2p0'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}// if (nResf == UNSUCCESSFUL_RETURN)

///////////////////////////
	//printf("\n\n 'Testing_OneFeaVec_byReadingAModel_2p0' (after 'ReadingAModel_2p0')");
	//printf("\n\nPlease press any key:"); getchar();

	//////////////////////////////////////////////////////////

	//printf("\n\n Input vector in 'Testing_OneFeaVec_byReadingAModel_2p0':\n");
	fprintf(fout, "\n\n Initial input vector in 'Testing_OneFeaVec_byReadingAModel_2p0' (no selec feas):\n");
	for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
	{
		//printf("%d:%E ", iFeaf, fFeas_OneVec_Arrf[iFeaf]);
		fprintf(fout, "%d:%E ", iFeaf, fFeas_OneVec_Arrf[iFeaf]);
	} //for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
	fflush(fout);
///////////////////////////////////

	nResf = Converting_OneVecOfAllFeas_To_OneVecOfSelecFeas(
					nDim_DifEvof, //const int nDim_AllFeasf, // = dimension of the original space
					nDim_SelecFeas_Readf, //const int nDim_SelecFeasf, // = dimension of the original space

					nPosOfSelec_FeasArrf, //const int nPosOfSelec_FeasArrf[], //[nDim_SelecFeasf]
					fFeas_OneVec_Arrf, //const float fFeasAll_OneVecArrf[], //[nDim_AllFeasf]

					fFeasSelec_OneVec_Arrf); // float fFeasSelec_OneVecArrf[]) //[nDim_SelecFeasf]

	fprintf(fout, "\n\n Vector of selec feas in 'Testing_OneFeaVec_byReadingAModel_2p0':\n");
	for (iFeaf = 0; iFeaf < nDim_SelecFeas_Readf; iFeaf++)
	{
		//printf("%d:%E ", iFeaf, fFeas_OneVec_Arrf[iFeaf]);
		fprintf(fout, "%d:%E ", iFeaf, fFeasSelec_OneVec_Arrf[iFeaf]);
	} //for (iFeaf = 0; iFeaf < nDim_Selec_Read_Df; iFeaf++)

	fflush(fout);
/*
	Normalizing_EveryFea_InOneFeaVec_UsingTrain_Mean_0_And_StDev_1(
		fLarge, //const float fLargef,
		eps, //const float fepsf,

		nDim_SelecFeas_Readf, //const int nDim_Selec_Readf,

		/////////////////////////////////////////////
		fFeaSelecMin_TrainArrf, //const float fFeaSelecMin_TrainArrf[], //[nDim_Selec_Readf]
		fFeaSelecMax_TrainArrf, //const float fFeaSelecMax_TrainArrf[], //[nDim_Selec_Readf]

		fMean_Selec_Feas_TrainArrf, //const float fMean_Selec_Feas_TrainArrf[], //[nDim_Selec_Readf]
		fStDev_Selec_Feas_TrainArrf, //const float fStDev_Selec_Feas_TrainArrf[], //[nDim_Selec_Readf]

		fFeasSelec_OneVec_Arrf); // float fFea_OneVec_Arrf[]); //[nDim_Selec_Readf]//to be normalized using the train Mean and StDev
*/

	/////////////////
	float* fFeasSelec_OneVec_WithConstArrf = new float[nDim_SelecFeasWithConst_Readf];
	if (fFeasSelec_OneVec_WithConstArrf == NULL)
	{
		printf("\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0': fFeasSelec_OneVec_WithConstArrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0': fFeasSelec_OneVec_WithConstArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fFeasSelec_OneVec_WithConstArrf == NULL)

////////////////////////////////////////////////////////////
	Converting_OneFeaVec_To_AVecWithConst(
		nDim_SelecFeas_Readf, //const int nDim_Selec_Read_Df, // = dimension of the original space
		nDim_SelecFeasWithConst_Readf, //const int nDim_Selec_Read_D_WithConstf, // = dimension of the original space

		fFeaConstInitf, //const float fFeaConstInitf,

		fFeasSelec_OneVec_Arrf, //const float fFea_OneVecArrf[], //[nDim_Selec_Read_Df]
		//fFeasSelecNormalized_OneVecWithConst_Arrf, //c_OneVec_Arrf, //const float fFea_OneVecArrf[], //[nDim_Selec_Read_Df]

		fFeasSelec_OneVec_WithConstArrf); // float fFeasSelec_OneVecWithConst_Arrf[]); //[nDim_Selec_Read_D_WithConst]

	fprintf(fout, "\n\n 'fFeasSelec_OneVec_WithConstArrf' after 'Converting_OneFeaVec_To_AVecWithConst': nDim_SelecFeasWithConst_Readf = %d\n",
		nDim_SelecFeasWithConst_Readf);
	for (iFeaf = 0; iFeaf < nDim_SelecFeasWithConst_Readf; iFeaf++)
	{
		//printf("%d:%E ", iFeaf, fFeas_OneVec_Arrf[iFeaf]);
		fprintf(fout, "%d:%E ", iFeaf, fFeasSelec_OneVec_WithConstArrf[iFeaf]);
	} //for (iFeaf = 0; iFeaf < nDim_SelecFeasWithConst_Readf; iFeaf++)

//////////////	
/*
fFea_OneVecWithConst_Normalized_Arrf == fFeasSelecNormalized_OneVecWithConst_Arrf
	float* fFea_OneVecWithConst_Normalized_Arrf = new float[nDim_SelecFeasWithConst_Readf];
	if (fFea_OneVecWithConst_Normalized_Arrf == NULL)
	{
		printf("\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0': fFea_OneVecWithConst_Normalized_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0': fZ_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		delete[] fFeasSelec_OneVec_WithConstArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fFea_OneVecWithConst_Normalized_Arrf == NULL)
*/
////////////////////////////////////////////////

//new
	nResf = Normalizing_FloatVector_ByStDev(
		//nDim_SelecFeas_Readf, //const int nDimf,
		nDim_SelecFeasWithConst_Readf, //const int nDimf,

		//fFeasSelec_OneVec_Arrf, //const float fFeas_InitArrf[],
		fFeasSelec_OneVec_WithConstArrf, //const float fFeas_InitArrf[],

		fFeasSelecNormalized_OneVecWithConst_Arrf); // float fFeasNormalized_Arrf[]);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0' by 'Normalizing_FloatVector_ByStDev'");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'Testing_OneFeaVec_byReadingAModel_2p0' by 'Normalizing_FloatVector_ByStDev'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	fprintf(fout, "\n\n 'fFeasSelecNormalized_OneVecWithConst_Arrf' after 'Normalizing_FloatVector_ByStDev': nDim_SelecFeasWithConst_Readf = %d\n", 
		nDim_SelecFeasWithConst_Readf);
	for (iFeaf = 0; iFeaf < nDim_SelecFeasWithConst_Readf; iFeaf++)
	{
		//printf("%d:%E ", iFeaf, fFeas_OneVec_Arrf[iFeaf]);
		fprintf(fout, "%d:%E ", iFeaf, fFeasSelecNormalized_OneVecWithConst_Arrf[iFeaf]);
	} //for (iFeaf = 0; iFeaf < nDim_SelecFeasWithConst_Readf; iFeaf++)
///////////////////////////////////

	int* nHyperplaneWithMaxScaProdArrf = new int[nDim_H_Readf];
	if (nHyperplaneWithMaxScaProdArrf == NULL)
	{
		printf("\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0': nHyperplaneWithMaxScaProdArrf == NULL");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0': nHyperplaneWithMaxScaProdArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		delete[] fFeasSelec_OneVec_WithConstArrf;
		//delete[] fFea_OneVecWithConst_Normalized_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nHyperplaneWithMaxScaProdArrf == NULL)

	float* fZ_Arrf = new float[nDim_H_Readf];
	if (fZ_Arrf == NULL)
	{
		printf("\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0': fZ_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0': fZ_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);

		delete[] fFeasSelec_OneVec_WithConstArrf;
	//	delete[] fFea_OneVecWithConst_Normalized_Arrf;

		delete[] nHyperplaneWithMaxScaProdArrf;
		return UNSUCCESSFUL_RETURN;
	} //if (fZ_Arrf == NULL)

	float* fZ_NormalizedArrf = new float[nDim_H_Readf];
	if (fZ_NormalizedArrf == NULL)
	{
		printf("\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0': fZ_NormalizedArrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0': fZ_NormalizedArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		delete[] fFeasSelec_OneVec_WithConstArrf;
		//delete[] fFea_OneVecWithConst_Normalized_Arrf;
		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;
		return UNSUCCESSFUL_RETURN;
	} //if (fZ_NormalizedArrf == NULL)
/////////////////////////////////////////////////

	//printf("\n\n 'Testing_OneFeaVec_byReadingAModel_2p0' 2");
	//printf("\n Please press any key:"); getchar();
/*
//int Normalizing_A_Vector_For_ARange() ?
	nResf = Normalizing_A_Vector_ByStDev(
		nDim_SelecFeasWithConst_Readf, //const int nDim_Selec_Readf,

		fFeasSelec_OneVec_WithConstArrf, //const float fFeas_InitArrf[],

		fFea_OneVecWithConst_Normalized_Arrf); // float fFeasNormalized_Arrf[]);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0' by 'Normalizing_A_Vector_ByStDev' 1 ");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'Testing_OneFeaVec_byReadingAModel_2p0' by 'Normalizing_A_Vector_ByStDev' 1 ");;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);

		delete[] fFeasSelec_OneVec_WithConstArrf;
		delete[] fFea_OneVecWithConst_Normalized_Arrf;
		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;
		delete[] fZ_NormalizedArrf;
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

*/

////////////////////////////////

	nResf = All_Feas_OfNonlinearSpace(
		nDim_SelecFeasWithConst_Readf, //const int nDim_Selec_Read_D_WithConstf, // = dimension of the original space
		nDim_H_Readf, //const int nDim_H_Readf, //dimension of the nonlinear/transformed space

		nK_Readf, //const int nK_Readf, //nNumOfHyperplanes

		//fFea_OneVecWithConst_Normalized_Arrf, //const float fX_Arrf[], //[nDim_Selec_Read_D_WithConstf]
		fFeasSelecNormalized_OneVecWithConst_Arrf, //const float fX_Arrf[], //[nDim_Selec_Read_D_WithConstf]

		fU_Train_Read_Arrf, //const float fU_Train_Read_Arrf[], // [nDim_Selec_Read_U_Glob] = (nDim_Selec_Read_D_WithConst*nDim_Selec_Read_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

		nHyperplaneWithMaxScaProdArrf, //int nHyperplaneWithMaxScaProdArrf[], //[nDim_H_Readf]

		fZ_Arrf); // float fZ_Arrf[]); //[nDim_H_Readf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0' by 'All_Feas_OfNonlinearSpace'");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'Testing_OneFeaVec_byReadingAModel_2p0' by 'All_Feas_OfNonlinearSpace' ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);

		delete[] fFeasSelec_OneVec_WithConstArrf;
		//delete[] fFea_OneVecWithConst_Normalized_Arrf;
		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;
		delete[] fZ_NormalizedArrf;
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	//printf("\n\n 'Testing_OneFeaVec_byReadingAModel_2p0' 2_2");
	//printf("\n Please press any key:"); getchar();

//#ifndef COMMENT_OUT_ALL_PRINTS
	for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_H_Readf; iFea_Nonlinearf++)
	{
		fprintf(fout, "\n 'Testing_OneFeaVec_byReadingAModel_2p0' (after 'All_Feas_OfNonlinearSpace'): nHyperplaneWithMaxScaProdArrf[%d] = %d, fZ_Arrf[%d] = %E",
			 iFea_Nonlinearf, nHyperplaneWithMaxScaProdArrf[iFea_Nonlinearf], iFea_Nonlinearf, fZ_Arrf[iFea_Nonlinearf]);
	} //for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_H_Readf; iFea_Nonlinearf++)

//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//////////////////////////////////////////////////////////////
	nResf = Normalizing_A_Vector_ByStDev(
		nDim_H_Readf, //const int nDim_Selec_Readf,
		fZ_Arrf, //const float fFeas_InitArrf[],

		fZ_NormalizedArrf); // float fFeasNormalized_Arrf[]);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'Testing_OneFeaVec_byReadingAModel_2p0' by 'Normalizing_A_Vector_ByStDev' 2 ");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n  An error in  'Testing_OneFeaVec_byReadingAModel_2p0' by 'Normalizing_A_Vector_ByStDev' 2 ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);

		delete[] fFeasSelec_OneVec_WithConstArrf;
		//delete[] fFea_OneVecWithConst_Normalized_Arrf;
		delete[] nHyperplaneWithMaxScaProdArrf;
		delete[] fZ_Arrf;
		delete[] fZ_NormalizedArrf;
		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	//printf("\n\n 'Testing_OneFeaVec_byReadingAModel_2p0': before 'Loss_Estimate_ForOneVec'");
	//printf("\n Please press any key:"); getchar();

/////////////////////////////
	Loss_Estimate_ForOneVec(
		nDim_H_Readf, //const int nDim_H_Readf,
		//nY_For_LossCurf, //const int nYtf, // 1 or -1 (not 0)

		fZ_NormalizedArrf, //const float fZ_Arrf[], //[nDim_H_Readf]

		fW_Train_Read_Arrf, //const float fW_Train_Read_Arrf[], //[nDim_H_Readf]

		nY_Estimatedf, //int &nY_Estimatedf, //0 or 1
		fLossf); // float &fLoss_Estimate_ForOneVecf);
/////////////////////////////////////////////

	delete[] fFeasSelec_OneVec_WithConstArrf;
	//delete[] fFea_OneVecWithConst_Normalized_Arrf;

	delete[] nHyperplaneWithMaxScaProdArrf;
	delete[] fZ_Arrf;
	delete[] fZ_NormalizedArrf;

	nOutcomef = nY_Estimatedf;

	printf("\n\n The end of 'Testing_OneFeaVec_byReadingAModel_2p0': fLossf = %E, nOutcomef = %d", fLossf, nOutcomef);
	//printf("\n\nPlease press any key:"); getchar();
	//printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);

	return SUCCESSFUL_RETURN;
} // int Testing_OneFeaVec_byReadingAModel_2p0(...
////////////////////////////////////////////////////

//no const
int TestingAllVecs_ByOneVec_WithReadingAModel_2p0(
	const int nDim_DifEvof, // = dimension of the original space

	const int nDim_DifEvo_WithConstf, // 
	const int nVecTestf,

	const float fFeaTest_Arrf[], //[nDim_DifEvof*nVecTestf]

	const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],
///////////////////////////////////////////////////

	PAS_AGG_MAX_OUT_RESUTS *sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results)
{
	int A_Vec_From_ArrOf_AllVecs(
		const int nDimf,
		const int nNumOfVecsTotf,

		const int nVecf,

		const float fFeas_All_Arrf[], //[nProdTestTot]

		float fFeas_OneVec_Arrf[]); //[nDim_DifEvo_WithConst]

	/////////////////////////////////////////
	int Testing_OneFeaVec_byReadingAModel_2p0(
		const int nDim_DifEvof, //
	//const int nDim_Selec_Read_D_WithConstf, //

		float fFeas_OneVec_Arrf[], //[nDim_DifEvo] 

		float &fLossf,
		int &nOutcomef); //1 - positive, 0 - negative

/////////////////////////////////////////////////
	int
		nResf,

		iFea_Nonlinearf,
		iFeaf,
		nY_Estimatedf,

		nProd_WithConstTestTotf = nDim_DifEvo_WithConstf * nVecTestf,

		nNumOfPosit_Y_Totf = 0,
		nNumOfNegat_Y_Totf = 0,

		nNumOfCorrect_Y_Totf = 0,

		nNumOfPositCorrect_Y_Totf = 0,
		nNumOfNegatCorrect_Y_Totf = 0,

		nY_Actualf,
		nY_For_LossCurf,

		iVecf;

	float
		fScalar_Prodf,
		fLossf,

		fLossMinf = fLarge,

		fLossMaxf = -fLarge,
		fPercentageOfCorrectTotf = 0.0,
		fPercentageOfCorrect_Positf = 0.0,
		fPercentageOfCorrect_Negatf = 0.0;
	/////////////////////////////////////////

	float* fFea_OneVec_Arrf = new float[nDim_DifEvof];
	if (fFea_OneVec_Arrf == NULL)
	{
		printf("\n\n An error in 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': fFea_OneVec_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': fFea_OneVec_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fFea_OneVec_Arrf == NULL)
/////////////////////////////////////////////////

	int* nY_Estimated_Arrf = new int[nVecTestf];
	if (nY_Estimated_Arrf == NULL)
	{
		printf("\n\n An error in 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': nY_Estimated_Arrf == NULL");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': nY_Estimated_Arrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar(); exit(1);
		delete[] fFea_OneVec_Arrf;
		return UNSUCCESSFUL_RETURN;
	} //if (nY_Estimated_Arrf == NULL)

///////////////////////
	for (iVecf = 0; iVecf < nVecTestf; iVecf++)
	{
		printf("\n\n 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' 1: iVecf = %d, nVecTestf = %d", iVecf, nVecTestf);
		//printf("\n\nPlease press any key:"); getchar();

		nResf = A_Vec_From_ArrOf_AllVecs(
			nDim_DifEvof, //const int nDimf,

			nVecTestf, //const int nNumOfVecsTotf,

			iVecf, //const int nVecf,

			fFeaTest_Arrf, //const float fFeas_All_Arrf[], //[nProd_WithConstTestTot]

			fFea_OneVec_Arrf); // float fFeas_OneVec_Arrf[]); //[nDim_DifEvof]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' by 'A_Vec_From_ArrOf_AllVecs'at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' by 'A_Vec_From_ArrOf_AllVecs' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar(); exit(1);
			delete[] fFea_OneVec_Arrf;
			delete[] nY_Estimated_Arrf;

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
		for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_Hf; iFea_Nonlinearf++)
		{
			fprintf(fout, "\n 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' (after 'All_Feas_OfNonlinearSpace'): iVecf = %d, nHyperplaneWithMaxScaProdArrf[%d] = %d, fZ_Arrf[%d] = %E",
				iVecf, iFea_Nonlinearf, nHyperplaneWithMaxScaProdArrf[iFea_Nonlinearf], iFea_Nonlinearf, fZ_Arrf[iFea_Nonlinearf]);
		} //for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_Hf; iFea_Nonlinearf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout, "\n\n 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': iVecf = %d, nVecTestf = %d, 'fFea_OneVec_Arrf[]'\n", iVecf, nVecTestf);

		for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)
		{
			fprintf(fout, "%d:%E ", iFeaf,fFea_OneVec_Arrf[iFeaf]);
		} //for (iFeaf = 0; iFeaf < nDim_DifEvof; iFeaf++)

		fflush(fout);

		nY_Actualf = nY_Test_Actual_Arrf[iVecf];

		if (nY_Actualf == 1)
		{
			nNumOfPosit_Y_Totf += 1;
		} // if (nY_Actualf == 1)
		else if (nY_Actualf == 0) //-1)
		{
			nNumOfNegat_Y_Totf += 1;
		} //else if (nY_Actualf == 0) //-1)
		else
		{
			printf("\n\n An error in 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar(); exit(1);
			delete[] fFea_OneVec_Arrf;
			delete[] nY_Estimated_Arrf;

			return UNSUCCESSFUL_RETURN;
		}//else

/////////////////////////////
		printf("\n\n 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' before 'Testing_OneFeaVec_byReadingAModel': iVecf = %d, nVecTestf = %d, nY_Actualf = %d", 
			iVecf, nVecTestf,nY_Actualf);
		//printf("\n\nPlease press any key:"); getchar();

		nResf = Testing_OneFeaVec_byReadingAModel_2p0(
			nDim_DifEvof, //const int nDim_DifEvof, // = dimension of the original space
			//nDim_DifEvo_WithConstf, //const int nDim_DifEvo_WithConstf, // = 

			//to be normalized
			fFea_OneVec_Arrf, //const float fFeas_OneVec_Arrf[], //[nDim_DifEvof]

			fLossf, //float &fLossf,
			nY_Estimatedf); // int &nOutcomef); //1 - positive, 0 - negative

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' by 'Testing_OneFeaVec_byReadingAModel'at iVecf = %d", iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in  'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' by 'Testing_OneFeaVec_byReadingAModel' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar(); exit(1);
			delete[] fFea_OneVec_Arrf;
			delete[] nY_Estimated_Arrf;

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

		//printf("\n\n 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' 3: iVecf = %d, fLossf = %E", iVecf, fLossf);
		//printf("\n\nPlease press any key:"); getchar();

//////////////////////////
		nY_Estimated_Arrf[iVecf] = nY_Estimatedf;

		if (nY_Estimatedf == nY_Actualf)
		{
			nNumOfCorrect_Y_Totf += 1;

			if (nY_Actualf == 1)
			{
				nNumOfPositCorrect_Y_Totf += 1;
			} // if (nY_Actualf == 1)
			else if (nY_Actualf == 0) //-1)
			{
				nNumOfNegatCorrect_Y_Totf += 1;
			} //else if (nY_Actualf == 0) //-1)

		} //if (nY_Estimatedf == nY_Actualf)

		fPercentageOfCorrectTotf = (float)(100.0)*(float)(nNumOfCorrect_Y_Totf) / (float)(iVecf + 1);

		if (nNumOfPosit_Y_Totf > 0)
		{
			fPercentageOfCorrect_Positf = (float)(100.0)*(float)(nNumOfPositCorrect_Y_Totf) / (float)(nNumOfPosit_Y_Totf);
		} //if (nNumOfPosit_Y_Totf > 0)

		if (nNumOfNegat_Y_Totf > 0)
		{
			fPercentageOfCorrect_Negatf = (float)(100.0)*(float)(nNumOfNegatCorrect_Y_Totf) / (float)(nNumOfNegat_Y_Totf);
		} //if (nNumOfNegat_Y_Totf > 0)

		//if (nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob == 1)
		{
			fprintf(fout, "\n  'TestingAllVecs_ByOneVec_WithReadingAModel_2p0' (the test set): iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E",
				iVecf, nY_Estimatedf, nY_Actualf, fLossf);

			fflush(fout);

		} //if (nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob == 1)
#ifndef COMMENT_OUT_ALL_PRINTS

		if ((iVecf / 50) * 50 == iVecf)
		{
			printf("\n\n 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E", iVecf, nY_Estimatedf, nY_Actualf, fLossf);

			printf("\n\n nNumOfCorrect_Y_Totf = %d, iVecf + 1 = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, iVecf + 1, fPercentageOfCorrectTotf);

			printf("\n\n nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
				nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

			printf("\n nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
				nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

		} //f ( (iVecf / 50) * 50 == iVecf)

		fprintf(fout, "\n\n  'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E", iVecf, nY_Estimatedf, nY_Actualf, fLossf);

		fprintf(fout, "\n\n nNumOfCorrect_Y_Totf = %d, iVecf + 1 = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, iVecf + 1, fPercentageOfCorrectTotf);

		fprintf(fout, "\n\n nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
			nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

		fprintf(fout, "\n nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
			nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//for (iVecf = 0; iVecf < nVecTestf; iVecf++)

	//if (nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob == 1)
	{
		printf( "\n\n 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': final nNumOfCorrect_Y_Totf = %d, nVecTestf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTestf, fPercentageOfCorrectTotf);

		printf( "\n\n Test: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
			nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

		printf( "\n Test: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
			nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

		fprintf(fout, "\n\n 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': final nNumOfCorrect_Y_Totf = %d, nVecTestf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTestf, fPercentageOfCorrectTotf);

		fprintf(fout, "\n\n Test: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
			nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

		fprintf(fout, "\n Test: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
			nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

		fflush(fout);
	} //if (nIndicPrintingLossesIn_PasAggMaxOut_Test_Glob == 1)

/////////////////////////////////////////////
	sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results->nNumOfVecs_Totf = nVecTestf;
	sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results->nNumOfCorrect_Y_Totf = nNumOfCorrect_Y_Totf;

	sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results->nNumOfPosit_Y_Totf = nNumOfPosit_Y_Totf;
	sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results->nNumOfPositCorrect_Y_Totf = nNumOfPositCorrect_Y_Totf;

	sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results->nNumOfNegat_Y_Totf = nNumOfNegat_Y_Totf;
	sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results->nNumOfNegatCorrect_Y_Totf = nNumOfNegatCorrect_Y_Totf;

	sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results->fPercentageOfCorrectTotf = fPercentageOfCorrectTotf;
	sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results->fPercentageOfCorrect_Positf = fPercentageOfCorrect_Positf;
	sTestingAllVecs_ByOneVec_WithReadingAModel_2p0Results->fPercentageOfCorrect_Negatf = fPercentageOfCorrect_Negatf;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': final nNumOfCorrect_Y_Totf = %d, nVecTestf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTestf, fPercentageOfCorrectTotf);

	printf("\n\n Test: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
		nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

	printf("\n Test: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
		nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': final nNumOfCorrect_Y_Totf = %d, nVecTestf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTestf, fPercentageOfCorrectTotf);

	fprintf(fout, "\n\n Test: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
		nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

	fprintf(fout, "\n Test: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
		nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

	fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	fflush(fout);
	//printf("\n\n The end of 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': fLossMinf = %E, fLossMaxf = %E", fLossMinf, fLossMaxf);
	delete[] fFea_OneVec_Arrf;
	delete[] nY_Estimated_Arrf;

	//printf("\n\n The end of 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': please press any key to exit"); getchar(); exit(1);
	//printf("\n\n The end of 'TestingAllVecs_ByOneVec_WithReadingAModel_2p0': please press any key to continue"); getchar();

	return SUCCESSFUL_RETURN;
} // int TestingAllVecs_ByOneVec_WithReadingAModel_2p0(...

/*
for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
{
	nProd_iFea_Hf_nDim_SelecFeas_WithConstf = iFea_Hf * nDim_D_SelecFeas_WithConstf;
	fprintf(fout, "\n\n The next nonlinear fea: iFea_Hf = %d", iFea_Hf);

	for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
	{
		nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf = nProd_nDim_SelecFeas_WithConstf_nDim_Hf * iHyperplanef;

		fprintf(fout, "\n\n The next iHyperplanef = %d, iFea_Hf = %d\n", iHyperplanef, iFea_Hf);

		for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)
		{
			//nIndexf = iFeaf + (nDim_D_SelecFeas_WithConstf*iFea_Hf) + (nDim_D_SelecFeas_WithConstf*nDim_Hf*iHyperplanef) ;

			nIndexf = iFeaf + nProd_iFea_Hf_nDim_SelecFeas_WithConstf + nProd_iHyperplane_nDim_D_SelecFeas_WithConstf_nDim_Hf;

			if (nIndexf > nIndexMaxf)
			{
				printf("\n\n An error in 'Print_fU_Arr': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n An error in 'Print_fU_Arr': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				getchar();	exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexf > nIndexMaxf)

			fprintf(fout, "%d:%E, ", iFeaf, fU_Arrf[nIndexf]);
		}//for (iFeaf = 0; iFeaf < nDim_D_SelecFeas_WithConstf; iFeaf++)

	} //for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

*/

//printf("\n\nPlease press any key to continue"); getchar();